{pkgs ? import <nixpkgs> {}}:
pkgs.mkShell {
  buildInputs = with pkgs; [
    #toolchain
    gcc
    cmake
    rustup
    pkg-config
    cargo-flamegraph
    git
    
    #dependencies required by project
    alsa-lib.dev    
    libGL.dev
    libGLU.dev
    xorg.xauth
    xorg.libX11.dev
    xorg.libXft.dev
    xorg.libXext.dev      # X11 extensions library
    xorg.libXinerama.dev  # Xinerama for multi-monitor support
    xorg.libXcursor.dev   # X cursor management library
    xorg.libXfixes.dev    # X Fixes extension
    fontconfig.dev
    pango.dev             # Pango for internationalized text rendering
    cairo.dev             # Cairo graphics library (for 2D vector graphics)
    glib.dev              # GLib (for gobject-2.0)
    SDL2.dev              # SDL2 library for development
  ];


  shellHook = ''
    export RUSTUP_HOME=$HOME/.rustup
    export CARGO_HOME=$HOME/.cargo
    xauth generate $DISPLAY . trusted #needed to be able to open X11 windows
    rustup default stable
  '';
}
