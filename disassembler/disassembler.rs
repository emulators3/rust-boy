use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::fmt;

fn main() -> std::io::Result<()> {
    let mut gb_game = File::open("gioco.gb")?;
    let mut game_binary_vector = Vec::new();
    
    std::io::Read::by_ref(&mut gb_game).read_to_end(&mut game_binary_vector)?;
    
    dump_binary_opcodes_to_file(game_binary_vector);
    
    Ok(())
}

fn dump_binary_opcodes_to_file(binary_vector: Vec<u8>) -> std::io::Result<()> {

    let mut file = File::create("opcodes.txt")?;    
    let mut count = 0;

    for instruction in binary_vector.iter() {
        count = count+1;

        write!(&mut file, "{instruction:02x}", instruction=instruction);
        
        if(count%8 == 0) {
            count = 0;
            file.write(b"\n");                
        } else {
            file.write(b" ");            
        }
    }
    
    Ok(())
}
