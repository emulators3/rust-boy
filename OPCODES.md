# The Inner Structure of the 8-bit Game Boy Opcode

The Game Boy Opcode is an 8-bit datum the CPU reads
in order to determine the operation to execute.

In order to reduce the size of the instruction,
operands, usually registers, are hardcoded in the bits
of the opcode itself, rather than being posited after,
as it usually happens with immediate data.

Note that, for the same reason, that is reducing
opcode size, the set of parameters accepted by each
instruction may not be fully orthogonal.  
In turn, by looking at every such hardcoded
instruction-plus-parameter opcode as its own operation,
the hidden orthogonality of the instruction set
becomes self-evident.

In order to do that, the bits of a Game Boy opcode
can be read, depending on the operation, using
the two possible structures depicted below.

#### The 2-3-3 Structure
    +----+-----+-----+
    | pp | xxx | yyy |
    +----+-----+-----+

#### The 2-2-4 Structure
    +----+----+------+
    | pp | rr | zzzz |
    +----+----+------+

Within these structures, `pp` selects between
the *row* and *column* sections of the opcode table,
while the rest are data encoding an operation and
its operands, interpreted in each case according to
one or more of the following table types.

### The 3-bit "8-bit Register" Operand
    +-----+------+-----+------+-----+------+-----+------+
    | 000 |  B   | 001 |  C   | 010 |  D   | 011 |  E   |
    +-----+------+-----+------+-----+------+-----+------+
    | 100 |  H   | 101 |  L   | 110 | (HL) | 111 |  A   |
    +-----+------+-----+------+-----+------+-----+------+

This operand type contains all the 8-bit registers.

One important exception to note is that `110` does not
represent an 8-bit register, but the *address* stored
in the 16-bit register `HL`.

The set of registers that can appear in such
"indirect address" positions are most inconsistent and
full of exceptions, `HL` being the primary register
for such operations thanks to its appearence in the
above operand table.

For this reason, calling it a "table of registers"
is a slight abuse of language, whose above exception
is assumed to be understood throughout the document,
as it is reflected in the code.

### The 3-bit Arithmetic Operation
    +-----+------+-----+------+-----+------+-----+------+
    | 000 | ADD  | 001 | ADC  | 010 | SUB  | 011 | SBC  |
    +-----+------+-----+------+-----+------+-----+------+
    | 100 | AND  | 101 | XOR  | 110 |  OR  | 111 |  CP  |
    +-----+------+-----+------+-----+------+-----+------+

This table is used to select an arithmetic operation.

These operations are hardcoded to work on `A` as the
first operand, and store their result in it.

It is worth noting that all arithmetic operations, except
`ADD`, allow only `A` as their first operand throughout
the whole set of instructions, and therefore are notated
with their first parameter implicit, as `SUB r8`, etc.  
Most notably, this also holds true for `ADC`, despite
being commonly annotated as `ADC A, r8`, in imitation
of `ADD`, the first operand cannot be anything but `A`.  
For this reason, throughout the document and in the code,
we annotate the other arithmetic operations similarly,
giving `SUB A, r8`, `SBC A, r8`, etc.

#### The 2-bit "16-bit Register" Operand, or "Indirect Address"
    +----+-------+-------+
    | 00 |  BC   | (BC)  |
    +----+-------+-------+
    | 01 |  DE   | (DE)  |
    +----+-------+-------+
    | 10 |  HL   | (HL+) |
    +----+-------+-------+
    | 11 | SP/AF | (HL-) |
    +----+-------+-------+

These operands are used in two modes, specific to the
instruction, which we could call "bare 16-bit register"
and "indirect address" mode.

In "bare register" mode, the operand specified by `11`
is used to access an exceptional register `SP` or `AF`,
depending by the instruction.  
In "indirect address" mode, the operand signifying `HL`
is reinterpreted as an "indirect address plus increment"
operation on `HL`, and the exceptional case analogously
with a decrement of the register.

Note that the `zzzz` portion in the 2-2-4 bit structure
has no structure worth commenting upon, and merely selects
an operation to execute.  
One could argue that the table would be slightly simplified
if the `zzzz` portion preceded the `rr` 2-bit operand,
but noticing this fact will not simplify the actual
table of opcodes.

## The Top Bits `pp` and the Structure of the Opcode Table

### Rows

When `pp` equals `01` or `10`, we speak of the
*row* portion of the instruction table, or we
simply refer them as *rows*.

The reason for this is readily apparent when
we arrange our table rows to contain 8 elements each:  
Opcodes whose upper two bits contain `01` or `10`,
will organize themselves in clear horizontal series.

### Columns

When `pp` equals either `00` or `11`, we speak
of *columns*, or the *column* portion of the
instruction table, which, analogously to rows,
disposes its operations in a columnar fashion.

*Columns* are themselves split in two sets:  
The *upper columns*, meaning those for which
`pp` is set to `00`, located before ("above" in
tabular form) the rows.  
The *lower columns*, which are located after,
and therefore "below" the rows in tabular form,
for which `pp` is set to `11`.

Unlike the rows, which are almost entirely regular
with only a single exception, the columns contain
many instructions that are extraneous to the pattern,
while still being contained in some well-defined
column.

Some of the patterns that appear in the columns
result in too much work when trying to capture them
with code, and therefore are ignored in our model.

## Uses of the 2-3-3 Structure

The *2-3-3 structure* is principally used in the
row section of the instruction table, to describe
two sets of operations:

#### Loads from 8-bit Registers to 8-bit Registers
    +----+-----+-----+-------------+
    | 01 | xxx | yyy | LD xxx, yyy |
    +----+-----+-----+-------------+

These represent the following family of load operations:

    LD r8, r8

Where each register is specified from the
*3-bit operands* `xxx` and `yyy`.

The exceptional cases of `LD (HL), r8` and
`LD r8, (HL)` are handled in the obvious manner,
while the `LD (HL), (HL)` case is excepted
to signify the `HALT` instruction.

#### Arithmetic operations on 8-bit Registers
    +----+-----+-----+-----------+
    | 10 | ooo | rrr | OP A, rrr |
    +----+-----+-----+-----------+

These represent the operation `OP`, specified
as a *3-bit operation*, acting on `A` by the
register specified by the *3-bit operand* `rrr`.

## Exceptional uses of 2-3-3 structure

These two same modes are also used exceptionally
in both column sets, in the following cases:

#### Increment by 1 on 8-bit Registers
    +----+-----+-----+---------+
    | 00 | rrr | 010 | INC rrr |
    +----+-----+-----+---------+
#### Decrement by 1 on 8-bit Registers
    +----+-----+-----+---------+
    | 00 | rrr | 011 | DEC rrr |
    +----+-----+-----+---------+
#### Load Immediate Data to 8-bit Registers
    +----+-----+-----+--------------+
    | 00 | rrr | 100 | LD rrr, imm8 |
    +----+-----+-----+--------------+
#### Arithmetic operations on Immediate Data
    +----+-----+-----+------------+
    | 11 | ooo | 110 | OP A, imm8 |
    +----+-----+-----+------------+

Where both `rrr` and `ooo` are interpreted in the
exact same way as they would be in the row opcodes,
as a *3-bit register operand* and *arithmetic operation*
respectively.

Note that it is a slight miss in the design of the
opcode table itself that these are unevenly distributed
between the two column regions, and that  they cannot be
readily identified by some bit pattern in the opcode.  
For example, arbitrarily fixing `pp` to `00`:

    +----+-----+-----+
    | 00 | 1qq | xxx |
    +----+-----+-----+
Mapping the four possible values of `qq` to the current
values of `010`, `011`, `100`, `110`.

## Uses of the 2-2-4 Structure

The *2-2-4 structure* is used chiefly in the
column portion of the instruction table.

The *operation* is selected by `zzzz`, while `rr`
provides a *2-bit operand* which, depending on the
operation, signifies either a 16-bit register,
or an indirect memory address contained in a
16-bit register, as indicated in the relevant
2-bit operand type definition.

Compared to its width of 4-bits, there are few
operations `zzzz` could represent:
(The exceptional register is specified where applicable)

    +----+----+------+---------------+----+----+----+------+---------------+----+
    | 00 | rr | 0001 | LD r16, imm16 | SP | 00 | rr | 1001 | ADD HL, r16   | SP |
    +----+----+------+---------------+----+----+----+------+---------------+----+
    | 00 | rr | 0010 | LD (r16), A   | -- | 00 | rr | 1010 | LD A, (r16)   | -- |
    +----+----+------+---------------+----+----+----+------+---------------+----+
    | 00 | rr | 0011 | INC r16       | SP | 00 | rr | 1011 | DEC r16       | SP |
    +----+----+------+---------------+----+----+----+------+---------------+----+
    | 11 | rr | 0001 | PUSH r16      | AF | 11 | rr | 0101 | PUSH r16      | AF |
    +----+----+------+---------------+----+----+----+------+---------------+----+

Note that one could lament that `PUSH` and `POP` do not
follow the same pattern as the others, where only the
upper bit differentiates between the two operations.

## Exceptional Opcodes

The remaining combinations of `pp`, `rr` and `zzzz`
are all considered exceptional.

Note that while there exist still opcode patterns,
such as the coordination between `DI` and `EI`,
the `RST`s, or the remaining `LD`s, they are few
and far enough that it is simply easier to handle
them by special case.

## Prefixed Rows of Opcodes

One of the exceptional opcodes is `PREFIX`, which lets
the CPU read the next byte as an opcode in another
table of instructions.

    +----+----+------+--------+
    | 11 | 00 | 1011 | PREFIX |
    +----+----+------+--------+

The prefixed opcode, effectively a *two byte opcode*,
has a simple 2-3-3 structure as follows:

    +----+----+------+ +----+-----+-----+
    | 11 | 00 | 1011 | | qq | xxx | yyy |
    +----+----+------+ +----+-----+-----+
Where `yyy` is always a *3-bit operand*, while `xxx`
could be either a *bit operation* or *immediate data*.

When `qq` is `00`, then `xxx` selects an *operation*
from the following table:

    +----+----+------+ +----+-----+-----+
    | 11 | 00 | 1011 | | 00 | xxx | yyy |
    +----+----+------+ +----+-----+-----+
    +-----+----------+
    | xxx | OP yyy   |
    +-----+----------+-----+---------+
    | 000 | RLC yyy  | 001 | RRC yyy |
    +-----+----------+-----+---------+
    | 010 | RL yyy   | 011 | RR yyy  |
    +-----+----------+-----+---------+
    | 100 | SLA yyy  | 101 | SRA yyy |
    +-----+----------+-----+---------+
    | 110 | SWAP yyy | 111 | SRL yyy |
    +-----+----------+-----+---------+

Otherwise, `qq` itself encodes the *operation*, and `xxx`
represents an *immediate 3-bit operand*, as the following
table illustrates:

    +----+----+------+ +----+-----+-----+
    | 11 | 00 | 1011 | | qq | xxx | yyy |
    +----+----+------+ +----+-----+-----+
    +----+--------------+
    | qq | OP xxx, yyy  |
    +----+--------------+
    | 01 | BIT xxx, yyy |
    +----+--------------+
    | 10 | RES xxx, yyy |
    +----+--------------+
    | 11 | SET xxx, yyy |
    +----+--------------+

#### Garbage

        /* op = pp_xxx_yyy = pp_rr_zzzz
          ** ROWS
          * pp = 01 load rows
          + LD r1, r2
             xxx = r1
             yyy = r2
          * pp = 10 op rows
          + OP A, r
             xxx = OP
             yyy = r
             
          ** UPPER COLUMNS
          * pp = 00: upper columns
          ** yyy = 010 INC r8
           + INC r
              xxx = r
          ** yyy = 011 DEC r8
           + DEC r
              xxx = r
          ** yyy = 100 LD r8, imm8
           + LD r, imm8
              xxx = r
          
          ** zzzz = 0001 LD r16, imm16
           + LD rr, imm16
          ** zzzz = 1001 ADD HL, r16
           + ADD HL, rr
          
          ** zzzz = 0011 INC r16
           + INC rr
          ** zzzz = 1011 DEC 16
           + DEC rr
          
          ** zzzz = 0010 LD (r16), A
           + LD (rr), A
          ** zzzz = 1010 LD A, (r16)
           + LD A, (rr)
          
          ** zzzz = 0000 | 0111 | 1000 | 1111 exceptionals
          
          ** LOWER COLUMNS
          * pp = 11 lower columns
          ** yyy = 110 OP A, imm8
           + OP A, imm8
              xxx = OP
          ** zzzz = 0001 POP r16
           + POP rr
          ** zzzz = 0101 PUSH r16
           + PUSH rr
          ** rest of opcodes all exceptional
         */
