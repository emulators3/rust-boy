
// u16
pub const fn higher_byte(datum: u16) -> u8 {
    (datum >> 8) as u8
}

pub const fn lower_byte(datum: u16) -> u8 {
    (datum & 0x00ff) as u8
}

pub const fn lower_twelve_bits(datum: u16) -> u16 {datum & 0b0000111111111111}

pub const fn test_bit_16(datum: u16, bit_number: u8) -> bool { get_bit_16(datum, bit_number) == 1 }

pub const fn get_bit_16(datum: u16, bit_number: u8) -> u16 { (datum >> bit_number) & 1 }

pub const fn write_bit_16(datum: u16, bit_number: u8, bit_to_write: u8) -> u16 {
    if bit_to_write == 0 {
        reset_bit_16(datum, bit_number)
    } else {
        set_bit_16(datum, bit_number)
    }
}

pub const fn reset_bit_16(datum: u16, bit_to_reset: u8) -> u16 {
    let mask: u16 = 0b1111111111111110;
    datum & (mask.rotate_left(bit_to_reset as u32))
}

pub const fn set_bit_16(datum: u16, bit_to_set: u8) -> u16 {
    let mask: u16 = 1;
    datum | (mask << bit_to_set)
}


pub const fn join_bytes(higher: u8, lower: u8) -> u16 {
    (higher as u16) << 8 | lower as u16
}

// u8
pub const fn join_nibbles(higher_half: u8, lower_half: u8) -> u8 { higher_half << 4 | lower_half }

pub const fn higher_half(datum: u8) -> u8 { datum >> 4 }

pub const fn lower_half(datum: u8) -> u8 { datum & 0x0f }

pub const fn highest_bit(datum: u8) -> u8 { (datum >> 7) & 1 }

pub const fn highest_five_bits(datum: u8) -> u8 { (datum >> 3) & 0b00011111 }
pub const fn highest_two_bits(datum: u8) -> u8 { (datum >> 6) & 0b00000011 }

pub const fn lower_seven_bits(datum: u8) -> u8 {
    datum & 0b01111111
}

pub const fn lower_six_bits(datum: u8) -> u8 {
    datum & 0b00111111
}

pub const fn lower_five_bits(datum: u8) -> u8 {
    datum & 0b00011111
}

pub const fn lower_three_bits(datum: u8) -> u8 {
    datum & 0b00000111
}
pub const fn lower_two_bits(datum: u8) -> u8 {
    datum & 0b00000011
}

pub const fn lowest_bit(datum: u8) -> u8 {
    datum & 0x01
}

//from 0 to 7
pub const fn get_bit(datum: u8, bit_number: u8) -> u8 { (datum >> bit_number) & 1 }

pub const fn write_bit(datum: u8, bit_number: u8, bit_to_write: u8) -> u8 {
    if bit_to_write == 0 {
        reset_bit(datum, bit_number)
    } else {
        set_bit(datum, bit_number)
    }
}

pub const fn reset_bit(datum: u8, bit_to_reset: u8) -> u8 {
    let mask: u8 = 0b11111110;
    datum & (mask.rotate_left(bit_to_reset as u32))
}

pub const fn set_bit(datum: u8, bit_to_set: u8) -> u8 {
    let mask: u8 = 1;
    datum | (mask << bit_to_set)
}

pub const fn test_bit(datum: u8, bit_number: u8) -> bool { get_bit(datum, bit_number) == 1 }