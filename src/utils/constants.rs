use crate::gb::pixel_color::PixelColor;
use crate::gb::types::GbAddr;

pub type WindowLine = [PixelColor; WINDOW_LINE_SIZE];
pub type WindowScreen = [WindowLine; WINDOW_LINES];
pub type Bit = bool;
pub type DigitalAudioOutput = u8;
pub type AnalogAudioOutput = i16;

pub const SCREEN_LINE_SIZE: usize = 256;
pub const WINDOW_LINE_SIZE: usize = 160;
pub const WINDOW_LINES: usize = 144;

pub const RAM_BANK_SIZE: usize = 8 * 1024;

pub const IE_ADDRESS: GbAddr = 0xFFFF;