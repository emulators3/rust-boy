use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::thread;
use std::thread::JoinHandle;
use crossbeam::atomic::AtomicCell;
use kanal::{Receiver, Sender};
use crate::gb::{EmulationState, GameBoy};
use crate::gb::joypad_state::JoypadState;
use crate::gb::stereo_analog_audio_output::StereoAnalogAudioOutput;
use crate::gb::types::RamBank;
use crate::utils::constants::WindowScreen;

pub struct GameboyDaemon {}

impl GameboyDaemon {

    pub fn new() -> Self { Self {} }

    pub fn run(&self,
               rom_selection_rx: Receiver<String>,
               audio_tx: Sender<StereoAnalogAudioOutput>,
               video_tx: Sender<Box<WindowScreen>>,
               joypad_rx: Arc<AtomicCell<JoypadState>>,
               gb_emulation_state: Arc<AtomicCell<EmulationState>>,
               keep_running: Arc<AtomicCell<bool>>) -> JoinHandle<()> {
        let thread = thread::Builder::new().name(String::from("gb_watcher"));

        thread.spawn (move || {
            loop {
                match rom_selection_rx.try_recv() {
                    Ok(Some(rom_path)) => {
                        let rom_path = Path::new(&rom_path);
                        let save_file_path = rom_path.with_extension("sav");

                        let binary_rom = Self::load_game_binary(rom_path);
                        let maybe_save_data = Self::load_optional_save_file(&save_file_path);

                        gb_emulation_state.store(EmulationState::Running);

                        let gameboy_thread = GameBoy::from_binary_rom(binary_rom, maybe_save_data).run(
                            audio_tx.clone(),
                            video_tx.clone(),
                            joypad_rx.clone(),
                            gb_emulation_state.clone());

                        let gb_after_running = gameboy_thread.join().unwrap();

                        let ram = gb_after_running.get_cartridge_ram();
                        Self::dump_ram_into_save_file(ram, save_file_path);

                    },
                    Ok(None) => { },
                    Err(err) => {
                        println!("{:}", err);
                        panic!();
                    }
                }

                if !keep_running.load() {
                    break;
                }
            }
        }).unwrap()
    }


    fn dump_ram_into_save_file(ram: &[RamBank], save_data_path: PathBuf) {
        let mut file = File::create(save_data_path).unwrap();
        for bank in ram {
            file.write_all(bank).unwrap();
        }
    }

    fn load_game_binary(rom_path: &Path) -> Vec<u8> {
        let mut gb_game = File::open(rom_path).unwrap();
        let mut game_binary_vector = Vec::new();

        gb_game.read_to_end(&mut game_binary_vector).unwrap();

        game_binary_vector
    }

    fn load_optional_save_file(save_data_path: &PathBuf) -> Option<Vec<u8>> {
        match File::open(save_data_path) {
            Ok(mut file) => {
                let mut save_data = Vec::new();
                file.read_to_end(&mut save_data).unwrap();
                Some(save_data)
            },
            Err(_) => None
        }
    }
}