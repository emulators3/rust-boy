use std::thread::JoinHandle;
use std::sync::Arc;
use crossbeam::atomic::AtomicCell;
use fltk::{app, dialog, frame::Frame, prelude::*, window::Window};
use fltk::enums::{ColorDepth, FrameType, Key, Shortcut};
use fltk::image::RgbImage;
use fltk::menu::{MenuBar, MenuFlag};
use kanal::unbounded;
use crate::audio_cpal::AudioCpal;
use crate::gameboy_daemon::GameboyDaemon;
use crate::gb::EmulationState;
use crate::gb::joypad_state::{JoypadKey, JoypadState};
use crate::gb::pixel_color::PixelColor;

const KEY_A: Key = Key::from_char('a');
const KEY_S: Key = Key::from_char('s');
const KEY_Z: Key = Key::from_char('z');
const KEY_X: Key = Key::from_char('x');

#[derive(Debug)]
pub struct Gui {}

impl Gui {

    pub fn new() -> Self {
        Self{}
    }

    pub fn run_sync(self) {

        let (video_tx, video_rx) = unbounded();
        let (audio_tx, audio_rx) = unbounded();
        let joypad_tx_rx = Arc::new(AtomicCell::<JoypadState>::new(JoypadState::new()));
        let gameboy_emulation_state = Arc::new(AtomicCell::<EmulationState>::new(EmulationState::Running));
        let app_state = Arc::new(AtomicCell::<bool>::new(true));
        let (rom_selection_tx, rom_selection_rx) = unbounded();

        let gameboy_watcher_thread: JoinHandle<()> = GameboyDaemon::new().run(
            rom_selection_rx.clone(),
            audio_tx.clone(),
            video_tx.clone(),
            joypad_tx_rx.clone(),
            gameboy_emulation_state.clone(),
            app_state.clone()
        );

        let audio_thread: JoinHandle<()> = AudioCpal::new().run(audio_rx.to_owned(),
                                                                app_state.clone());
        let app = app::App::default();

        let mut wind = Window::new(640, 480, 1280, 960, "Rust-boy");
        let mut menu_bar = MenuBar::new(0,0,wind.width(), 25, "");
        let mut chooser = dialog::FileChooser::new(
            ".",                    // directory
            "*.gb\t*.gbc",                    // filter or pattern
            dialog::FileChooserType::Single, // chooser type
            "Choose ROM",     // title
        );

        let joypad_tx_rx2 = joypad_tx_rx.clone();
        let emulation_state2 = gameboy_emulation_state.clone();

        menu_bar.add("&File/&Open", Shortcut::None, MenuFlag::Normal, move |_| {
            emulation_state2.store(EmulationState::Pause);
            chooser.window().center_screen();
            chooser.show();

            while chooser.shown() {
                app::wait();
            }

            if let Some(rom_path) = chooser.value(1) {
                emulation_state2.store(EmulationState::Close);
                rom_selection_tx.send(rom_path).unwrap();
            } else {
                emulation_state2.store(EmulationState::Running);
            }

        });

        let emulation_state3 = gameboy_emulation_state.clone();
        let app_state2 = app_state.clone();
        menu_bar.add("&File/&Exit", Shortcut::None, MenuFlag::Normal, move |_| {
            emulation_state3.store(EmulationState::Close);
            app_state2.store(false);
            app::quit()
        });

        let mut frame = Frame::default().with_size(640, 576).center_of_parent();
        frame.set_color(fltk::enums::Color::White);
        frame.set_frame(FrameType::DownBox);

        wind.make_resizable(true);
        wind.end();
        wind.show();


        let mut joypad_state = JoypadState::new();

        let emulation_state4 = gameboy_emulation_state.clone();
        let app_state3 = app_state.clone();
        frame.handle({
            move | _frame, event| {
                match event {
                    fltk::enums::Event::Focus => true,
                    fltk::enums::Event::KeyDown => {
                        match app::event_key() {
                            Key::Up => joypad_state.key_down(JoypadKey::Up),
                            Key::Down => joypad_state.key_down(JoypadKey::Down),
                            Key::Left => joypad_state.key_down(JoypadKey::Left),
                            Key::Right => joypad_state.key_down(JoypadKey::Right),
                            Key::Escape => {
                                emulation_state4.store(EmulationState::Close);
                                app_state3.store(false);
                                app::quit();
                            },

                            KEY_A => joypad_state.key_down(JoypadKey::A),
                            KEY_S => joypad_state.key_down(JoypadKey::B),
                            KEY_Z => joypad_state.key_down(JoypadKey::Select),
                            KEY_X => joypad_state.key_down(JoypadKey::Start),
                            _ => return false,
                        }
                        joypad_tx_rx2.store(joypad_state);
                        true
                    },
                    fltk::enums::Event::KeyUp => {
                        match app::event_key() {
                            Key::Up => joypad_state.key_up(JoypadKey::Up),
                            Key::Down => joypad_state.key_up(JoypadKey::Down),
                            Key::Left => joypad_state.key_up(JoypadKey::Left),
                            Key::Right => joypad_state.key_up(JoypadKey::Right),


                            KEY_A => joypad_state.key_up(JoypadKey::A),
                            KEY_S => joypad_state.key_up(JoypadKey::B),
                            KEY_Z => joypad_state.key_up(JoypadKey::Select),
                            KEY_X => joypad_state.key_up(JoypadKey::Start),
                            _ => return false,
                        }
                        joypad_tx_rx2.store(joypad_state);
                        true
                    },
                    _ => false,
                }
            }
        });


        app::add_timeout3(0.016, move |handle| {
            match video_rx.try_recv() {
                Ok(Some(screen_data)) => {
                    let data: Vec<u8> = screen_data.iter().flat_map(|window_line|
                        window_line.iter().flat_map(|pix|
                            convert_to_color(pix.to_owned())
                        )
                    ).collect::<Vec<u8>>();

                    let mut image = RgbImage::new(&data, 160, 144, ColorDepth::Rgb8).unwrap();
                    image.scale(frame.width(), frame.height(), false, true);

                    frame.set_image(Some(image));
                    frame.redraw();

                    app::repeat_timeout3(0.016, handle);
                },
                Ok(None) => {
                    app::repeat_timeout3(0.016, handle);
                }
                Err(err) => {
                    println!("{:}", err);
                    panic!();
                },
            };
        });

        app.run().unwrap();

        gameboy_emulation_state.store(EmulationState::Close);
        app_state.store(false);

        gameboy_watcher_thread.join().unwrap();
        audio_thread.join().unwrap();
    }
}

fn convert_to_color(pixel_color: PixelColor) -> [u8;3] {
    match pixel_color {
        PixelColor::Blank => [255,255,255],
        PixelColor::Lightest => [0xFE,0xFE,0xFE],
        PixelColor::Light => [0xAA,0xAA,0xAA],
        PixelColor::Dark => [0x55,0x55,0x55],
        PixelColor::Darkest => [0x00,0x00,0x00],
    }
}