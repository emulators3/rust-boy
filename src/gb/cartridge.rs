mod cartridge_memory;

use crate::gb::types::{GbAddr, GbByte, RamBank};

use cartridge_memory::rom::Rom;
use cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::{Mbc, MemoryBankController, RamSize, RomSize};


#[derive(Debug, PartialEq)]
pub struct Cartridge {
    mbc: MemoryBankController,
    rom: Rom,
    ram: Ram,
}

impl Cartridge {
    pub fn from_binary_rom(binary_rom: Vec<u8>, maybe_save_data: Option<Vec<u8>>) -> Self {
        let rom_in_banks: Vec<Vec<u8>> =
            binary_rom.chunks(Rom::BANK_SIZE)
                .map(|chunk| chunk.to_vec())
                .collect();

        let mbc_type = rom_in_banks[0][0x0147];
        let rom_size = RomSize::from(rom_in_banks[0][0x0148]);
        let ram_size = RamSize::from(rom_in_banks[0][0x0149]);

        Self {
            mbc: MemoryBankController::from(mbc_type, rom_size, ram_size),
            rom: Rom::init(rom_size, &rom_in_banks),
            ram: Ram::init(ram_size, maybe_save_data),
        }
    }
    
    // ROM
    pub fn read_rom_bank0(&self, rom_address: GbAddr) -> GbByte {
        self.mbc.read_rom_bank_0(&self.rom, rom_address)
    }
    
    pub fn read_rom(&self, rom_address: GbAddr) -> GbByte {
        self.mbc.read_rom_bank_n(&self.rom, rom_address)
    }

    // RAM
    pub fn read_ram(&self, ram_address: GbAddr) -> GbByte {
        self.mbc.read_ram(&self.ram, ram_address)
    }
    
    pub fn write_ram(&mut self, ram_address: GbAddr, datum: GbByte) {
        self.mbc.write_ram(&mut self.ram, ram_address, datum)
    }

    pub fn access_mbc(&mut self, command_address: GbAddr, datum: GbByte) {
        self.mbc.execute(datum, command_address)
    }

    pub fn tick(&mut self) {
        self.mbc.tick();
    }

    pub fn get_ram(&self) -> &[RamBank] {
        self.ram.get_all()
    }
}


