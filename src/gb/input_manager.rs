use crate::gb::joypad_state::{JoypadKey, JoypadState};
use crate::gb::memory::p1_register::P1Register;
use crate::gb::types::GbByte;
use crate::utils::bitutils::{higher_half, join_nibbles, lower_half};

enum InputSelected {
    Both, Action, Direction, None
}

impl InputSelected {
    pub fn from(p1: u8) -> Self {
        //we want 4 and 5 bit, so we move right 4 bits and we select only two
        match (p1 >> 4) & 0b11 {
            0b00 => InputSelected::Both,
            0b01 => InputSelected::Action,
            0b10 => InputSelected::Direction,
            0b11 => InputSelected::None,
            _ => panic!("I expect only two bits when understanding which input is enabled")
        }

    }
}

pub fn manage_input(joypad_state: JoypadState, p1_reg: &mut impl P1Register) -> (GbByte, GbByte) {
    let p1 = p1_reg.read_p1();

    let input = match InputSelected::from(p1) {
        InputSelected::Both => calculate_action_input(joypad_state) & calculate_direction_input(joypad_state),
        InputSelected::Action => calculate_action_input(joypad_state),
        InputSelected::Direction => calculate_direction_input(joypad_state),
        InputSelected::None => reset_input(),
    };

    //here we merge the first 4 bits from P1 with the second 4 bits from the calculated input
    let new_p1 = join_nibbles(higher_half(p1), lower_half(input));

    p1_reg.write_detected_input(new_p1);

    (p1, new_p1)
}

fn calculate_action_input(joypad_state: JoypadState) -> u8 {
    (get_bit_from_state(joypad_state.key_state(JoypadKey::Start))  << 3) |
    (get_bit_from_state(joypad_state.key_state(JoypadKey::Select)) << 2) |
    (get_bit_from_state(joypad_state.key_state(JoypadKey::B))      << 1) |
    (get_bit_from_state(joypad_state.key_state(JoypadKey::A)))
}

fn calculate_direction_input(joypad_state: JoypadState) -> u8 {
    (get_bit_from_state(joypad_state.key_state(JoypadKey::Down))  << 3) |
    (get_bit_from_state(joypad_state.key_state(JoypadKey::Up))    << 2) |
    (get_bit_from_state(joypad_state.key_state(JoypadKey::Left))  << 1) |
    (get_bit_from_state(joypad_state.key_state(JoypadKey::Right)))
}

fn reset_input() -> u8 {
    0b11111111
}

fn get_bit_from_state(state: bool) -> u8 {
    u8::from(!state)
}

#[cfg(test)]
mod input_manager_test;