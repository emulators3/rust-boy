#![allow(non_camel_case_types)]

use std::collections::VecDeque;
use crate::gb::cpu::cache::Cache;
use crate::gb::cpu::alu::*;
use crate::gb::cpu::Cpu;
use crate::gb::cpu::bit_operation::{bit_operation, single_bit_operation, specific_bit_operation_for_a};
use crate::gb::cpu::flags::{Flag, Flags};
use crate::gb::cpu::flow_control::*;
use crate::gb::memory::Memory;
use crate::utils::bitutils::*;
use crate::gb::types::{GbByte, GbWord, GbAddr};

use super::registers::{
    Reg8,
    Reg16
};

#[derive(Debug, PartialEq)]
pub enum MicroOpcode {
    /*
        Fetch instruction is shared between all opcodes, as the opcode reading takes the usual 4 cycles.
        There are cases in which after the fetch there is an immediate action, like loading from register to register,
        or some calculation that is done immediately and doesn't require additional cycles. For those
        opcodes, there is not the need to put the Nop instruction and we just model the immediate action.
        Nop is used when the next MicroOpcode is not immediate.
        I could have done also differently, by having MicroOpcode with immediate effect that can be executed
        even after the first MicroOpcode execution.
    */
    Nop,
    Stop,
    Halt,

    DisableInterrupts,
    EnableInterrupts,

    JumpCachedRelativeAddress,
    JumpCachedAbsoluteAddress,
    JumpToHL,

    FlushIfConditionNotMet { condition: Condition },

    CacheImmediate { immediate: u8 },
    CacheImmediateLsb { immediate: u8 },
    CacheImmediateMsb { immediate: u8 },
    CacheImmediateAndFlushIfConditionNotMet { immediate: u8, condition: Condition },
    CacheImmediateMsbAndFlushIfConditionNotMet { immediate: u8, condition: Condition },

    CacheHlMem,

    LoadPCFromCache,
    LoadPCFromCacheAndEnableIME,

    LoadLowerSPFromImmediate { immediate: u8 },
    LoadHigherSPFromImmediate { immediate: u8 },

    LoadLowerImmediateMemFromLowerSP,
    LoadHigherImmediateMemFromHigherSP,

    LoadHlMemFromCachedImmediate,
    LoadReg8FromImmediate { to: Reg8, immediate: u8 },

    LoadReg8 {to: Reg8, from: Reg8},
    LoadReg8FromHlMem {to: Reg8},
    LoadHlMemFromReg8 {from: Reg8},

    LoadBCMemFromA,
    LoadDEMemFromA,
    LoadHlIncMemFromA,
    LoadHlDecMemFromA,

    LoadAFromBCMem,
    LoadAFromDEMem,
    LoadAFromHlIncMem,
    LoadAFromHlDecMem,

    LoadCachedImmediateMemFromA,
    LoadAFromCachedImmediateMem,

    LoadOffsetCachedImmediateMemFromA,
    LoadAFromOffsetCachedImmediateMem,

    LoadCOffsetImmediateMemFromA,
    LoadAFromCOffsetImmediateMem,

    LoadHLFromSPAndCachedSignedImmediate,
    LoadSPFromHl,

    AddHLReg16AndCacheResultAndWriteToL { from: Reg16 },
    WritePreviousResultToH,

    AddHLSPAndCacheResultAndWriteToL,

    AddSPCachedSignedImmediateAndCacheResultAndWriteToLowerSP,
    WritePreviousResultToHigherSP,

    ArithmeticOperationToAFromReg {operation: ArithmeticOperation, from: Reg8},
    ArithmeticOperationToAFromHlMem {operation: ArithmeticOperation},
    ArithmeticOperationToAFromCachedImmediate {operation: ArithmeticOperation},

    IncrementReg8 { reg: Reg8 },
    IncrementCachedHlMem,
    DecrementReg8 { reg: Reg8 },
    DecrementCachedHlMem,

    IncrementReg16 { reg: Reg16 },
    IncrementSP,
    DecrementReg16 { reg: Reg16 },
    DecrementSP,

    DecimalAdjustment,
    SetCarryFlag,
    ComplementA,
    ComplementCarryFlag,

    PushRegisterToStack {reg: Reg8},
    PushFlagsToStack,
    PopRegisterFromStack {reg: Reg8},
    PopFlagsFromStack,

    PopAndCacheLsb,
    PopAndCacheMsb,

    PushUpperPCToStack,
    PushLowerPCToStackAndJumpToCachedImmediate,
    PushLowerPCToStackAndJumpToRst { rst: Rst },

    BitOperationToA { operation: SpecificBitOperationForA },
    BitOperationToReg8 { operation: BitOperation, reg: Reg8 },
    BitOperationToCachedHLMem { operation: BitOperation },

    SingleBitOperationToReg8 { operation: SingleBitOperation, reg: Reg8, bit: u8 },
    SingleBitOperationFromHLMemToFlags { operation: SingleBitOperation, bit: u8 },
    SingleBitOperationFromCachedHLMemToHLMem { operation: SingleBitOperation, bit: u8 },
}

#[derive(Debug, PartialEq)]
pub enum ArithmeticOperation {
    Add, Adc,
    Sub, Sbc,
    And, Xor,
    Or,  Cp,
}

#[derive(Debug, PartialEq)]
pub enum Condition {
    Z, C, NZ, NC
}

#[derive(Debug, PartialEq)]
pub enum Rst {
    h00, h08, h10, h18,
    h20, h28, h30, h38
}

#[derive(Debug, PartialEq)]
pub enum BitOperation {
    Rlc, Rrc,
    Rl, Rr,
    Sla, Sra,
    Swap, Srl,
}

#[derive(Debug, PartialEq)]
pub enum SpecificBitOperationForA {
    Rlca, Rrca,
    Rla, Rra,
}

#[derive(Debug, PartialEq)]
pub enum SingleBitOperation {
    Bit, Res, Set
}

impl Rst {
    pub fn get_address(self) -> GbAddr {
        match self {
            Rst::h00 => 0x0000,
            Rst::h08 => 0x0008,
            Rst::h10 => 0x0010,
            Rst::h18 => 0x0018,
            Rst::h20 => 0x0020,
            Rst::h28 => 0x0028,
            Rst::h30 => 0x0030,
            Rst::h38 => 0x0038,
        }
    }
}

impl MicroOpcode {

    pub fn instruction(self, cpu: &mut Cpu, mmu: &mut impl Memory, cache: &mut Cache) {

        match self {
            MicroOpcode::Nop => (),

            MicroOpcode::Stop => {
                stop(cpu, mmu);
            }

            MicroOpcode::Halt => {
                halt(cpu, mmu);
            },

            MicroOpcode::DisableInterrupts => {
                cpu.ime = false;
            },
            //EI is delayed, while DI not
            MicroOpcode::EnableInterrupts => {
                cpu.next_can_interrupt = Some(true);
            },

            MicroOpcode::JumpCachedRelativeAddress  => {
                cpu.program_counter = add_u16_i8(cpu.program_counter, cache.get_cached_single_byte() as i8);
            }

            MicroOpcode::JumpCachedAbsoluteAddress  => {
                let datum = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
                cpu.program_counter = datum;
            }

            MicroOpcode::JumpToHL  => {
                let datum = cpu.registers.read16(Reg16::HL);
                cpu.program_counter = datum;
            }

            MicroOpcode::FlushIfConditionNotMet { condition } => {
                check_condition_and_flush_if_not_met(condition, cpu);
            },

            MicroOpcode::CacheImmediate { immediate } => {
                cache.cache_single_byte(immediate);
            },

            MicroOpcode::CacheImmediateLsb { immediate } => {
                cache.cache_lsb(immediate);
            },

            MicroOpcode::CacheImmediateMsb { immediate } => {
                cache.cache_msb(immediate);
            },

            MicroOpcode::CacheImmediateAndFlushIfConditionNotMet { immediate, condition } => {
                cache.cache_single_byte(immediate);
                check_condition_and_flush_if_not_met(condition, cpu);
            },

            MicroOpcode::CacheImmediateMsbAndFlushIfConditionNotMet { immediate, condition } => {
                cache.cache_msb(immediate);
                check_condition_and_flush_if_not_met(condition, cpu);
            },

            MicroOpcode::CacheHlMem => {
                let address = mmu.read(cpu.registers.read16(Reg16::HL));
                cache.cache_single_byte(address);
            }

            MicroOpcode::LoadPCFromCache => {
                cpu.program_counter = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
            },

            MicroOpcode::LoadPCFromCacheAndEnableIME => {
                cpu.program_counter = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
                cpu.ime = true;
            },

            MicroOpcode::LoadLowerSPFromImmediate { immediate } => {
                cpu.stack_pointer = join_bytes(higher_byte(cpu.stack_pointer), immediate);
            },

            MicroOpcode::LoadHigherSPFromImmediate { immediate } => {
                cpu.stack_pointer = join_bytes(immediate, lower_byte(cpu.stack_pointer));
            },

            MicroOpcode::LoadLowerImmediateMemFromLowerSP => {
                let addr = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
                mmu.write(addr, lower_byte(cpu.stack_pointer));
            },

            MicroOpcode::LoadHigherImmediateMemFromHigherSP => {
                let addr = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb()) + 1;
                mmu.write(addr, higher_byte(cpu.stack_pointer));
            },

            MicroOpcode::LoadReg8FromImmediate { to, immediate} => {
                cpu.registers.write8(to, immediate);
            },

            MicroOpcode::LoadHlMemFromCachedImmediate => {
                let addr = cpu.registers.read16(Reg16::HL) as GbAddr;
                mmu.write(addr, cache.get_cached_single_byte());
            },

            MicroOpcode::LoadReg8 {to, from} => {
                let datum = cpu.registers.read8(from);
                cpu.registers.write8(to, datum);
            },

            MicroOpcode::LoadReg8FromHlMem {to} => {
                let gb_addr = cpu.registers.read16(Reg16::HL) as GbAddr;
                cpu.registers.write8(to, mmu.read(gb_addr));
            },

            MicroOpcode::LoadHlMemFromReg8 {from} => {
                let datum = cpu.registers.read8(from);
                mmu.write(cpu.registers.read16(Reg16::HL), datum);
            },

            MicroOpcode::LoadBCMemFromA => {
                let datum = cpu.registers.read8(Reg8::A);
                mmu.write(cpu.registers.read16(Reg16::BC), datum);
            },

            MicroOpcode::LoadDEMemFromA => {
                let datum = cpu.registers.read8(Reg8::A);
                mmu.write(cpu.registers.read16(Reg16::DE), datum);
            },

            MicroOpcode::LoadHlIncMemFromA => {
                let address = cpu.registers.read16(Reg16::HL);
                let datum = cpu.registers.read8(Reg8::A);

                mmu.write(address, datum);
                cpu.registers.write16(Reg16::HL, address.overflowing_add(1).0);
            },

            MicroOpcode::LoadHlDecMemFromA => {
                let address = cpu.registers.read16(Reg16::HL);
                let datum = cpu.registers.read8(Reg8::A);

                mmu.write(address, datum);
                cpu.registers.write16(Reg16::HL, address.overflowing_sub(1).0);
            },

            MicroOpcode::LoadAFromBCMem => {
                let datum = mmu.read(cpu.registers.read16(Reg16::BC));
                cpu.registers.write8(Reg8::A, datum);
            },

            MicroOpcode::LoadAFromDEMem => {
                let datum = mmu.read(cpu.registers.read16(Reg16::DE));
                cpu.registers.write8(Reg8::A, datum);
            },

            MicroOpcode::LoadAFromHlIncMem => {
                let address = cpu.registers.read16(Reg16::HL);
                let datum = mmu.read(address);
                cpu.registers.write8(Reg8::A, datum);
                cpu.registers.write16(Reg16::HL, address.overflowing_add(1).0);
            },

            MicroOpcode::LoadAFromHlDecMem => {
                let address = cpu.registers.read16(Reg16::HL);
                let datum = mmu.read(address);
                cpu.registers.write8(Reg8::A, datum);
                cpu.registers.write16(Reg16::HL, address.overflowing_sub(1).0);
            },

            MicroOpcode::LoadCachedImmediateMemFromA => {
                let address = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
                mmu.write(address, cpu.registers.read8(Reg8::A));
            }

            MicroOpcode::LoadAFromCachedImmediateMem => {
                let address = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
                cpu.registers.write8(Reg8::A, mmu.read(address));
            }

            MicroOpcode::LoadOffsetCachedImmediateMemFromA => {
                let address = 0xFF00 + (cache.get_cached_single_byte() as u16);
                mmu.write(address, cpu.registers.read8(Reg8::A));
            }

            MicroOpcode::LoadAFromOffsetCachedImmediateMem => {
                let address = 0xFF00 + (cache.get_cached_single_byte() as u16);
                cpu.registers.write8(Reg8::A, mmu.read(address));
            }

            MicroOpcode::LoadCOffsetImmediateMemFromA => {
                let address = 0xFF00 + (cpu.registers.read8(Reg8::C) as u16);
                mmu.write(address, cpu.registers.read8(Reg8::A));
            }

            MicroOpcode::LoadAFromCOffsetImmediateMem => {
                let address = 0xFF00 + (cpu.registers.read8(Reg8::C) as u16);
                cpu.registers.write8(Reg8::A, mmu.read(address));
            }

            MicroOpcode::LoadHLFromSPAndCachedSignedImmediate => {
                let offset = cache.get_cached_single_byte() as i8;
                let (result, flags) = add_sp_i8(cpu.stack_pointer, offset, cpu.flags);

                cpu.registers.write16(Reg16::HL, result);
                cpu.flags = flags;
            }

            MicroOpcode::LoadSPFromHl => {
                cpu.stack_pointer = cpu.registers.read16(Reg16::HL);
            }

            MicroOpcode::AddHLReg16AndCacheResultAndWriteToL { from } => {
                let (result, flags) = add_u16_u16(
                    cpu.registers.read16(Reg16::HL),
                    cpu.registers.read16(from),
                    cpu.flags,
                );

                cache.cache_msb(higher_byte(result));
                cpu.registers.write8(Reg8::L, lower_byte(result));
                cpu.flags = flags;
            }

            MicroOpcode::WritePreviousResultToH => {
                cpu.registers.write8(Reg8::H, cache.get_cached_msb());
            }

            MicroOpcode::AddHLSPAndCacheResultAndWriteToL => {
                let (result, flags) = add_u16_u16(
                    cpu.registers.read16(Reg16::HL),
                    cpu.stack_pointer,
                    cpu.flags,
                );

                cache.cache_msb(higher_byte(result));
                cpu.registers.write8(Reg8::L, lower_byte(result));
                cpu.flags = flags;
            }

            MicroOpcode::AddSPCachedSignedImmediateAndCacheResultAndWriteToLowerSP => {
                let offset = cache.get_cached_single_byte() as i8;
                let (result, flags) = add_sp_i8(cpu.stack_pointer, offset, cpu.flags);

                cache.cache_msb(higher_byte(result));

                cpu.stack_pointer = join_bytes(higher_byte(cpu.stack_pointer), lower_byte(result));
                cpu.flags = flags;
            },

            MicroOpcode::WritePreviousResultToHigherSP => {
                cpu.stack_pointer = join_bytes(cache.get_cached_msb(), lower_byte(cpu.stack_pointer));
            },

            MicroOpcode::ArithmeticOperationToAFromReg { operation, from } => {
                let (result, flags) = reg8_reg8_arithmetic_operation(
                    operation,
                    cpu.flags,
                    cpu.registers.read8(Reg8::A),
                    cpu.registers.read8(from)
                );

                cpu.flags = flags;
                cpu.registers.write8(Reg8::A, result);
            },

            MicroOpcode::ArithmeticOperationToAFromHlMem { operation } => {
                let gb_addr = cpu.registers.read16(Reg16::HL) as GbAddr;

                let (result, flags) = reg8_reg8_arithmetic_operation(
                    operation,
                    cpu.flags,
                    cpu.registers.read8(Reg8::A),
                    mmu.read(gb_addr)
                );

                cpu.flags = flags;
                cpu.registers.write8(Reg8::A, result);
            },

            MicroOpcode::ArithmeticOperationToAFromCachedImmediate { operation } => {
                let datum = cache.get_cached_single_byte();

                let (result, flags) = reg8_reg8_arithmetic_operation(
                    operation,
                    cpu.flags,
                    cpu.registers.read8(Reg8::A),
                    datum
                );

                cpu.flags = flags;
                cpu.registers.write8(Reg8::A, result);
            },

            MicroOpcode::IncrementReg8 { reg } => {
                let datum = cpu.registers.read8(reg);

                let (result, flags) = increment_byte(datum, cpu.flags);

                cpu.registers.write8(reg, result);
                cpu.flags = flags;
            },

            MicroOpcode::DecrementReg8 { reg } => {
                let datum = cpu.registers.read8(reg);

                let (result, flags) = decrement_byte(datum, cpu.flags);

                cpu.registers.write8(reg, result);
                cpu.flags = flags;
            },

            MicroOpcode::IncrementCachedHlMem => {
                let datum = cache.get_cached_single_byte();
                let (result, flags) = increment_byte(datum, cpu.flags);

                mmu.write(cpu.registers.read16(Reg16::HL), result);
                cpu.flags = flags;
            },

            MicroOpcode::DecrementCachedHlMem => {
                let datum = cache.get_cached_single_byte();
                let (result, flags) = decrement_byte(datum, cpu.flags);

                mmu.write(cpu.registers.read16(Reg16::HL), result);
                cpu.flags = flags;
            },

            MicroOpcode::IncrementReg16 { reg } => {
                let datum = cpu.registers.read16(reg);
                cpu.registers.write16(reg, increment_word(datum));
            },

            MicroOpcode::DecrementReg16 { reg } => {
                let datum = cpu.registers.read16(reg);
                cpu.registers.write16(reg, decrement_word(datum));
            },

            MicroOpcode::IncrementSP => {
                let datum = cpu.stack_pointer;
                cpu.stack_pointer = increment_word(datum);
            },

            MicroOpcode::DecrementSP => {
                let datum = cpu.stack_pointer;
                cpu.stack_pointer = decrement_word(datum);
            },

            MicroOpcode::DecimalAdjustment => {
                let (result, flags) = decimal_adjustment(
                    cpu.registers.read8(Reg8::A),
                    cpu.flags.read(Flag::C),
                    cpu.flags.read(Flag::H),
                    cpu.flags.read(Flag::N),
                );

                cpu.registers.write8(Reg8::A, result);
                cpu.flags = flags;
            },

            MicroOpcode::SetCarryFlag => {
                cpu.flags = set_carry_flag(cpu.flags);
            },

            MicroOpcode::ComplementCarryFlag => {
                cpu.flags = complement_carry_flag(cpu.flags);
            },

            MicroOpcode::ComplementA => {
                let (result, flags) = complement_reg(cpu.registers.read8(Reg8::A), cpu.flags);
                cpu.registers.write8(Reg8::A, result);
                cpu.flags = flags;
            },

            MicroOpcode::PushRegisterToStack { reg } => {
                let datum = cpu.registers.read8(reg);
                cpu.push(mmu, datum);
            },

            MicroOpcode::PushFlagsToStack => {
                let datum = cpu.flags.as_byte();
                cpu.push(mmu, datum);
            },

            MicroOpcode::PopRegisterFromStack { reg } => {
                let datum = cpu.pop(mmu);
                cpu.registers.write8(reg, datum);
            },

            MicroOpcode::PopFlagsFromStack => {
                let datum = cpu.pop(mmu);
                cpu.flags = Flags::from_byte(datum);
            },

            MicroOpcode::PopAndCacheLsb => {
                let datum = cpu.pop(mmu);
                cache.cache_lsb(datum);
            },

            MicroOpcode::PopAndCacheMsb => {
                let datum = cpu.pop(mmu);
                cache.cache_msb(datum);
            },

            MicroOpcode::PushUpperPCToStack => {
                let datum = higher_byte(cpu.program_counter);
                cpu.push(mmu, datum);
            }

            MicroOpcode::PushLowerPCToStackAndJumpToCachedImmediate => {
                let datum = lower_byte(cpu.program_counter);
                cpu.push(mmu, datum);
                cpu.program_counter = join_bytes(cache.get_cached_msb(), cache.get_cached_lsb());
            }

            MicroOpcode::PushLowerPCToStackAndJumpToRst { rst } => {
                let datum = lower_byte(cpu.program_counter);
                cpu.push(mmu, datum);
                cpu.program_counter = rst.get_address();
            }

            MicroOpcode::BitOperationToA { operation} => {
                let datum = cpu.registers.read8(Reg8::A);
                let (result, flags) = specific_bit_operation_for_a(operation, datum, cpu.flags);
                cpu.registers.write8(Reg8::A, result);
                cpu.flags = flags;
            }

            MicroOpcode::BitOperationToReg8 { operation, reg } => {
                let datum = cpu.registers.read8(reg);
                let (result, flags) = bit_operation(operation, datum, cpu.flags);
                cpu.registers.write8(reg, result);
                cpu.flags = flags;
            }

            MicroOpcode::BitOperationToCachedHLMem { operation} => {
                let datum = cache.get_cached_single_byte();
                let (result, flags) = bit_operation(operation, datum, cpu.flags);
                let addr = cpu.registers.read16(Reg16::HL);

                mmu.write(addr, result);
                cpu.flags = flags;
            }

            MicroOpcode::SingleBitOperationToReg8 { operation, reg, bit } => {
                let datum = cpu.registers.read8(reg);
                let (result, flags) = single_bit_operation(operation, datum, bit, cpu.flags);
                cpu.registers.write8(reg, result);
                cpu.flags = flags;
            }

            MicroOpcode::SingleBitOperationFromHLMemToFlags { operation, bit } => {
                let addr = cpu.registers.read16(Reg16::HL);
                let datum = mmu.read(addr);
                let (_, flags) = single_bit_operation(operation, datum, bit, cpu.flags);
                cpu.flags = flags;
            }

            MicroOpcode::SingleBitOperationFromCachedHLMemToHLMem { operation, bit } => {
                let datum = cache.get_cached_single_byte();
                let (result, flags) = single_bit_operation(operation, datum, bit, cpu.flags);
                let addr = cpu.registers.read16(Reg16::HL);

                mmu.write(addr, result);
                cpu.flags = flags;
            }
        }
    }

    pub fn parse<T: Iterator<Item = GbByte>>(iterator: &mut T) -> VecDeque<MicroOpcode> {
        use MicroOpcode::*;
        use ArithmeticOperation::*;
        use SpecificBitOperationForA::*;

        let opcode = iterator.next().unwrap();

        match opcode {
            /* Bit operations */

            //RLCA
            0x07 => vec![
                BitOperationToA { operation: Rlca },
            ],
            //RRCA
            0x0F => vec![
                BitOperationToA { operation: Rrca },
            ],
            //RLA
            0x17 => vec![
                BitOperationToA { operation: Rla },
            ],
            //RRA
            0x1F => vec![
                BitOperationToA { operation: Rra },
            ],

            0xCB => {
                let mut result = vec![Nop];
                result.extend(parse_alternative(iterator));
                result
            },

            /* flow control */

            //NOP
            0x00 => vec![
                Nop,
            ],
            //STOP
            0x10 => vec![
                Stop,
            ],
            //HALT
            0x76 => vec![
                Halt,
            ],
            0xF3 => vec![
                DisableInterrupts,
            ],
            0xFB => vec![
                EnableInterrupts,
            ],
            //JR i8
            0x18 => vec![
                Nop,
                CacheImmediate { immediate: iterator.next().unwrap() },
                JumpCachedRelativeAddress
            ],
            //JR NZ, i8
            0x20 => vec![
                Nop,
                CacheImmediateAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::NZ },
                JumpCachedRelativeAddress
            ],
            //JR NC, i8
            0x30 => vec![
                Nop,
                CacheImmediateAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::NC },
                JumpCachedRelativeAddress
            ],
            //JR Z, i8
            0x28 => vec![
                Nop,
                CacheImmediateAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::Z },
                JumpCachedRelativeAddress
            ],
            //JR C, i8
            0x38 => vec![
                Nop,
                CacheImmediateAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::C },
                JumpCachedRelativeAddress
            ],
            //JP u16
            0xC3 => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsb { immediate: iterator.next().unwrap() },
                JumpCachedAbsoluteAddress
            ],
            //JP NZ, u16
            0xC2 => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::NZ},
                JumpCachedAbsoluteAddress
            ],
            //JP NC, u16
            0xD2 => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::NC},
                JumpCachedAbsoluteAddress
            ],
            //JP Z, u16
            0xCA => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::Z},
                JumpCachedAbsoluteAddress
            ],
            //JP C, u16
            0xDA => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::C},
                JumpCachedAbsoluteAddress
            ],
            //JP HL
            0xE9 => vec![
                JumpToHL
            ],
            //RET
            0xC9 => vec![
                Nop,
                PopAndCacheLsb,
                PopAndCacheMsb,
                LoadPCFromCache
            ],
            //RET NZ
            0xC0 => vec![
                Nop,
                FlushIfConditionNotMet { condition: Condition::NZ },
                PopAndCacheLsb,
                PopAndCacheMsb,
                LoadPCFromCache
            ],
            //RET NC
            0xD0 => vec![
                Nop,
                FlushIfConditionNotMet { condition: Condition::NC },
                PopAndCacheLsb,
                PopAndCacheMsb,
                LoadPCFromCache
            ],
            //RET Z
            0xC8 => vec![
                Nop,
                FlushIfConditionNotMet { condition: Condition::Z },
                PopAndCacheLsb,
                PopAndCacheMsb,
                LoadPCFromCache
            ],
            //RET C
            0xD8 => vec![
                Nop,
                FlushIfConditionNotMet { condition: Condition::C },
                PopAndCacheLsb,
                PopAndCacheMsb,
                LoadPCFromCache
            ],
            //RETI
            0xD9 => vec![
                Nop,
                PopAndCacheLsb,
                PopAndCacheMsb,
                LoadPCFromCacheAndEnableIME
            ],
            //CALL u16
            0xCD => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsb { immediate: iterator.next().unwrap() },
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToCachedImmediate,
            ],
            //CALL NZ, u16
            0xC4 => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::NZ},
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToCachedImmediate,
            ],
            //CALL NC, u16
            0xD4 => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::NC},
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToCachedImmediate,
            ],
            //CALL Z, u16
            0xCC => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::Z},
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToCachedImmediate,
            ],
            //CALL C, u16
            0xDC => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsbAndFlushIfConditionNotMet { immediate: iterator.next().unwrap(), condition: Condition::C},
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToCachedImmediate,
            ],
            //RST 00h
            0xC7 => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h00 },
            ],
            //RST 08h
            0xCF => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h08 },
            ],
            //RST 10h
            0xD7 => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h10 },
            ],
            //RST 18h
            0xDF => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h18 },
            ],
            //RST 20h
            0xE7 => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h20 },
            ],
            //RST 28h
            0xEF => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h28 },
            ],
            //RST 30h
            0xF7 => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h30 },
            ],
            //RST 38h
            0xFF => vec![
                Nop,
                Nop,
                PushUpperPCToStack,
                PushLowerPCToStackAndJumpToRst { rst: Rst::h38 },
            ],

            /*Loads*/

            //LD BC, u16
            0x01 => vec![
                Nop,
                LoadReg8FromImmediate { to: Reg8::C, immediate: iterator.next().unwrap() },
                LoadReg8FromImmediate { to: Reg8::B, immediate: iterator.next().unwrap() },
            ],
            //LD DE, u16
            0x11 => vec![
                Nop,
                LoadReg8FromImmediate { to: Reg8::E, immediate: iterator.next().unwrap() },
                LoadReg8FromImmediate { to: Reg8::D, immediate: iterator.next().unwrap() },
            ],
            //LD HL, u16
            0x21 => vec![
                Nop,
                LoadReg8FromImmediate { to: Reg8::L, immediate: iterator.next().unwrap() },
                LoadReg8FromImmediate { to: Reg8::H, immediate: iterator.next().unwrap() },
            ],
            //LD SP, u16
            0x31 => vec![
                Nop,
                LoadLowerSPFromImmediate { immediate: iterator.next().unwrap() },
                LoadHigherSPFromImmediate { immediate: iterator.next().unwrap() },
            ],
            //LD (u16), SP
            0x08 => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsb { immediate: iterator.next().unwrap() },
                LoadLowerImmediateMemFromLowerSP,
                LoadHigherImmediateMemFromHigherSP,
            ],
            //LD (BC), A
            0x02 => vec![
                Nop,
                LoadBCMemFromA,
            ],
            //LD (DE), A
            0x12 => vec![
                Nop,
                LoadDEMemFromA,
            ],
            //LD (HL+), A
            0x22 => vec![
                Nop,
                LoadHlIncMemFromA,
            ],
            //LD (HL-), A
            0x32 => vec![
                Nop,
                LoadHlDecMemFromA,
            ],
            //LD A, (BC)
            0x0A => vec![
                Nop,
                LoadAFromBCMem,
            ],
            //LD A, (DE)
            0x1A => vec![
                Nop,
                LoadAFromDEMem,
            ],
            //LD A, (HL+)
            0x2A => vec![
                Nop,
                LoadAFromHlIncMem,
            ],
            //LD A, (HL-)
            0x3A => vec![
                Nop,
                LoadAFromHlDecMem,
            ],
            //LD B,u8
            0x06 => vec![
                Nop,
                LoadReg8FromImmediate {to: Reg8::B, immediate: iterator.next().unwrap()}
            ],
            //LD C,u8
            0x0E => vec![
                Nop,
                LoadReg8FromImmediate {to: Reg8::C, immediate: iterator.next().unwrap()}
            ],
            //LD D,u8
            0x16 => vec![
                Nop,
                LoadReg8FromImmediate {to: Reg8::D, immediate: iterator.next().unwrap()}
            ],
            //LD E,u8
            0x1E => vec![
                Nop,
                LoadReg8FromImmediate {to: Reg8::E, immediate: iterator.next().unwrap()}
            ],
            //LD H,u8
            0x26 => vec![
                Nop,
                LoadReg8FromImmediate {to: Reg8::H, immediate: iterator.next().unwrap()}
            ],
            //LD L,u8
            0x2E => vec![
                Nop,
                LoadReg8FromImmediate {to: Reg8::L, immediate: iterator.next().unwrap()}
            ],
            //LD (HL), n
            0x36 => vec![
                Nop,
                CacheImmediate { immediate: iterator.next().unwrap() },
                LoadHlMemFromCachedImmediate
            ],
            //LD A,u8
            0x3E => vec![
                Nop,
                LoadReg8FromImmediate { to: Reg8::A, immediate: iterator.next().unwrap() }
            ],

            //LD B,B
            0x40 => vec![
                LoadReg8 { to: Reg8::B, from: Reg8::B }
            ],
            //LD B,C
            0x41 => vec![
                LoadReg8 { to: Reg8::B, from: Reg8::C }
            ],
            //LD B,D
            0x42 => vec![
                LoadReg8 { to: Reg8::B, from: Reg8::D }
            ],
            //LD B,E
            0x43 => vec![
                LoadReg8{to: Reg8::B, from: Reg8::E}
            ],
            //LD B,H
            0x44 => vec![
                LoadReg8{to: Reg8::B, from: Reg8::H}
            ],
            //LD B,L
            0x45 => vec![
                LoadReg8{to: Reg8::B, from: Reg8::L}
            ],
            //LD B,(HL)
            0x46 => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::B}
            ],
            //LD B,A
            0x47 => vec![
                LoadReg8{to: Reg8::B, from: Reg8::A}
            ],
            //LD C,B
            0x48 => vec![
                LoadReg8{to: Reg8::C, from: Reg8::B}
            ],
            //LD C,C
            0x49 => vec![
                LoadReg8{to: Reg8::C, from: Reg8::C}
            ],
            //LD C,D
            0x4A => vec![
                LoadReg8{to: Reg8::C, from: Reg8::D}
            ],
            //LD C,E
            0x4B => vec![
                LoadReg8{to: Reg8::C, from: Reg8::E}
            ],
            //LD C,H
            0x4C => vec![
                LoadReg8{to: Reg8::C, from: Reg8::H}
            ],
            //LD C,L
            0x4D => vec![
                LoadReg8{to: Reg8::C, from: Reg8::L}
            ],
            //LD C,(HL)
            0x4E => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::C},
            ],
            //LD C,A
            0x4F => vec![
                LoadReg8{ to: Reg8::C, from: Reg8::A}
            ],
            //LD D,B
            0x50 => vec![
                LoadReg8 { to: Reg8::D, from: Reg8::B }
            ],
            //LD D,C
            0x51 => vec![
                LoadReg8 { to: Reg8::D, from: Reg8::C }
            ],
            //LD D,D
            0x52 => vec![
                LoadReg8 { to: Reg8::D, from: Reg8::D }
            ],
            //LD D,E
            0x53 => vec![
                LoadReg8{ to: Reg8::D, from: Reg8::E}
            ],
            //LD D,H
            0x54 => vec![
                LoadReg8{to: Reg8::D, from: Reg8::H}
            ],
            //LD D,L
            0x55 => vec![
                LoadReg8{to: Reg8::D, from: Reg8::L}
            ],
            //LD D,(HL)
            0x56 => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::D}
            ],
            //LD D,A
            0x57 => vec![
                LoadReg8{to: Reg8::D, from: Reg8::A}
            ],
            //LD E,B
            0x58 => vec![
                LoadReg8{to: Reg8::E, from: Reg8::B}
            ],
            //LD E,C
            0x59 => vec![
                LoadReg8{to: Reg8::E, from: Reg8::C}
            ],
            //LD E,D
            0x5A => vec![
                LoadReg8{to: Reg8::E, from: Reg8::D}
            ],
            //LD E,E
            0x5B => vec![
                LoadReg8{to: Reg8::E, from: Reg8::E}
            ],
            //LD E,H
            0x5C => vec![
                LoadReg8{to: Reg8::E, from: Reg8::H}
            ],
            //LD E,L
            0x5D => vec![
                LoadReg8{to: Reg8::E, from: Reg8::L}
            ],
            //LD E,(HL)
            0x5E => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::E},
            ],
            //LD E,A
            0x5F => vec![
                LoadReg8{to: Reg8::E, from: Reg8::A}
            ],
            //LD H,B
            0x60 => vec![
                LoadReg8 { to: Reg8::H, from: Reg8::B }
            ],
            //LD H,C
            0x61 => vec![
                LoadReg8 { to: Reg8::H, from: Reg8::C }
            ],
            //LD H,D
            0x62 => vec![
                LoadReg8 { to: Reg8::H, from: Reg8::D }
            ],
            //LD H,E
            0x63 => vec![
                LoadReg8{ to: Reg8::H, from: Reg8::E}
            ],
            //LD H,H
            0x64 => vec![
                LoadReg8{to: Reg8::H, from: Reg8::H}
            ],
            //LD H,L
            0x65 => vec![
                LoadReg8{to: Reg8::H, from: Reg8::L}
            ],
            //LD H,(HL)
            0x66 => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::H}
            ],
            //LD H,A
            0x67 => vec![
                LoadReg8{to: Reg8::H, from: Reg8::A}
            ],
            //LD L,B
            0x68 => vec![
                LoadReg8{to: Reg8::L, from: Reg8::B}
            ],
            //LD L,C
            0x69 => vec![
                LoadReg8{to: Reg8::L, from: Reg8::C}
            ],
            //LD L,D
            0x6A => vec![
                LoadReg8{to: Reg8::L, from: Reg8::D}
            ],
            //LD L,E
            0x6B => vec![
                LoadReg8{to: Reg8::L, from: Reg8::E}
            ],
            //LD L,H
            0x6C => vec![
                LoadReg8{to: Reg8::L, from: Reg8::H}
            ],
            //LD L,L
            0x6D => vec![
                LoadReg8{to: Reg8::L, from: Reg8::L}
            ],
            //LD L,(HL)
            0x6E => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::L},
            ],
            //LD L,A
            0x6F => vec![
                LoadReg8{to: Reg8::L, from: Reg8::A}
            ],
            //LD (HL),B
            0x70 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::B}
            ],
            //LD (HL),C
            0x71 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::C}
            ],
            //LD (HL),D
            0x72 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::D}
            ],
            //LD (HL),E
            0x73 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::E}
            ],
            //LD (HL),H
            0x74 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::H}
            ],
            //LD (HL),L
            0x75 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::L}
            ],
            //LD (HL),A
            0x77 => vec![
                Nop,
                LoadHlMemFromReg8 {from: Reg8::A}
            ],
            //LD A,B
            0x78 => vec![
                LoadReg8{to: Reg8::A, from: Reg8::B}
            ],
            //LD A,C
            0x79 => vec![
                LoadReg8{to: Reg8::A, from: Reg8::C}
            ],
            //LD A,D
            0x7A => vec![
                LoadReg8{to: Reg8::A, from: Reg8::D}
            ],
            //LD A,E
            0x7B => vec![
                LoadReg8{to: Reg8::A, from: Reg8::E}
            ],
            //LD A,H
            0x7C => vec![
                LoadReg8{to: Reg8::A, from: Reg8::H}
            ],
            //LD A,L
            0x7D => vec![
                LoadReg8{to: Reg8::A, from: Reg8::L}
            ],
            //LD A,(HL)
            0x7E => vec![
                Nop,
                LoadReg8FromHlMem{to: Reg8::A},
            ],
            //LD A,A
            0x7F => vec![
                LoadReg8{to: Reg8::A, from: Reg8::A}
            ],
            //LD (FF00+u8), A
            0xE0 => vec![
                Nop,
                CacheImmediate { immediate: iterator.next().unwrap() },
                LoadOffsetCachedImmediateMemFromA
            ],
            //LD (FF00+C), A
            0xE2 => vec![
                Nop,
                LoadCOffsetImmediateMemFromA
            ],
            //LD A, (FF00+u8)
            0xF0 => vec![
                Nop,
                CacheImmediate { immediate: iterator.next().unwrap() },
                LoadAFromOffsetCachedImmediateMem
            ],
            //LD A, (FF00+C)
            0xF2 => vec![
                Nop,
                LoadAFromCOffsetImmediateMem
            ],
            //LD (u16),A
            0xEA => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsb { immediate: iterator.next().unwrap() },
                LoadCachedImmediateMemFromA,
            ],
            //LD A,(u16)
            0xFA => vec![
                Nop,
                CacheImmediateLsb { immediate: iterator.next().unwrap() },
                CacheImmediateMsb { immediate: iterator.next().unwrap() },
                LoadAFromCachedImmediateMem,
            ],
            //LD HL,SP+i8
            0xF8 => vec![
                Nop,
                CacheImmediate { immediate: iterator.next().unwrap() },
                LoadHLFromSPAndCachedSignedImmediate
            ],
            //LD SP, HL
            0xF9 => vec![
                Nop,
                LoadSPFromHl
            ],

            //Arithmetic operations

            //ADD HL, BC
            0x09 => vec![
                AddHLReg16AndCacheResultAndWriteToL { from: Reg16::BC },
                WritePreviousResultToH
            ],
            //ADD HL, DE
            0x19 => vec![
                AddHLReg16AndCacheResultAndWriteToL { from: Reg16::DE },
                WritePreviousResultToH
            ],
            //ADD HL, HL
            0x29 => vec![
                AddHLReg16AndCacheResultAndWriteToL { from: Reg16::HL },
                WritePreviousResultToH
            ],
            //ADD HL, SP
            0x39 => vec![
                AddHLSPAndCacheResultAndWriteToL,
                WritePreviousResultToH
            ],
            //ADD SP, i8
            0xE8 => vec![
                Nop,
                CacheImmediate { immediate: iterator.next().unwrap() },
                AddSPCachedSignedImmediateAndCacheResultAndWriteToLowerSP,
                WritePreviousResultToHigherSP,
            ],

            //ADD A,B
            0x80 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::B}
            ],
            //ADD A,C
            0x81 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::C}
            ],
            //ADD A,D
            0x82 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::D}
            ],
            //ADD A,E
            0x83 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::E}
            ],
            //ADD A,H
            0x84 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::H}
            ],
            //ADD A,L
            0x85 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::L}
            ],
            //ADD A,(HL)
            0x86 => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Add}

            ],
            //ADD A,A
            0x87 => vec![
                ArithmeticOperationToAFromReg {operation: Add, from: Reg8::A}
            ],
            //ADC A,B
            0x88 => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::B}
            ],
            //ADC A,C
            0x89 => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::C}
            ],
            //ADC A,D
            0x8A => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::D}
            ],
            //ADC A,E
            0x8B => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::E}
            ],
            //ADC A,H
            0x8C => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::H}
            ],
            //ADC A,L
            0x8D => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::L}
            ],
            //ADC A,(HL)
            0x8E => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Adc}

            ],
            //ADC A,A
            0x8F => vec![
                ArithmeticOperationToAFromReg {operation: Adc, from: Reg8::A}
            ],
            //SUB A,B
            0x90 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::B}
            ],
            //SUB A,C
            0x91 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::C}
            ],
            //SUB A,D
            0x92 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::D}
            ],
            //SUB A,E
            0x93 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::E}
            ],
            //SUB A,H
            0x94 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::H}
            ],
            //SUB A,L
            0x95 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::L}
            ],
            //SUB A,(HL)
            0x96 => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Sub}

            ],
            //SUB A,A
            0x97 => vec![
                ArithmeticOperationToAFromReg {operation: Sub, from: Reg8::A}
            ],
            //SBC A,B
            0x98 => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::B}
            ],
            //SBC A,C
            0x99 => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::C}
            ],
            //SBC A,D
            0x9A => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::D}
            ],
            //SBC A,E
            0x9B => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::E}
            ],
            //SBC A,H
            0x9C => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::H}
            ],
            //SBC A,L
            0x9D => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::L}
            ],
            //SBC A,(HL)
            0x9E => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Sbc}

            ],
            //SBC A,A
            0x9F => vec![
                ArithmeticOperationToAFromReg {operation: Sbc, from: Reg8::A}
            ],
            //AND A,B
            0xA0 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::B}
            ],
            //AND A,C
            0xA1 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::C}
            ],
            //AND A,D
            0xA2 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::D}
            ],
            //AND A,E
            0xA3 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::E}
            ],
            //AND A,H
            0xA4 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::H}
            ],
            //AND A,L
            0xA5 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::L}
            ],
            //AND A,(HL)
            0xA6 => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: And}

            ],
            //AND A,A
            0xA7 => vec![
                ArithmeticOperationToAFromReg {operation: And, from: Reg8::A}
            ],
            //XOR A,B
            0xA8 => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::B}
            ],
            //XOR A,C
            0xA9 => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::C}
            ],
            //XOR A,D
            0xAA => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::D}
            ],
            //XOR A,E
            0xAB => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::E}
            ],
            //XOR A,H
            0xAC => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::H}
            ],
            //XOR A,L
            0xAD => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::L}
            ],
            //XOR A,(HL)
            0xAE => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Xor}

            ],
            //XOR A,A
            0xAF => vec![
                ArithmeticOperationToAFromReg {operation: Xor, from: Reg8::A}
            ],

            //OR A,B
            0xB0 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::B}
            ],
            //OR A,C
            0xB1 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::C}
            ],
            //OR A,D
            0xB2 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::D}
            ],
            //OR A,E
            0xB3 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::E}
            ],
            //OR A,H
            0xB4 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::H}
            ],
            //OR A,L
            0xB5 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::L}
            ],
            //OR A,(HL)
            0xB6 => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Or}

            ],
            //OR A,A
            0xB7 => vec![
                ArithmeticOperationToAFromReg {operation: Or, from: Reg8::A}
            ],
            //CP A,B
            0xB8 => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::B}
            ],
            //CP A,C
            0xB9 => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::C}
            ],
            //CP A,D
            0xBA => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::D}
            ],
            //CP A,E
            0xBB => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::E}
            ],
            //CP A,H
            0xBC => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::H}
            ],
            //CP A,L
            0xBD => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::L}
            ],
            //CP A,(HL)
            0xBE => vec![
                Nop,
                ArithmeticOperationToAFromHlMem {operation: Cp}

            ],
            //CP A,A
            0xBF => vec![
                ArithmeticOperationToAFromReg {operation: Cp, from: Reg8::A}
            ],
            //ADD A, u8
            0xC6 => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Add },
            ],
            //ADC A, u8
            0xCE => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Adc },
            ],
            //SUB A, u8
            0xD6 => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Sub },
            ],
            //SBC A, u8
            0xDE => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Sbc },
            ],
            //AND A, u8
            0xE6 => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: And },
            ],
            //XOR A, u8
            0xEE => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Xor },
            ],
            //OR A, u8
            0xF6 => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Or },
            ],
            //CP A, u8
            0xFE => vec![
                CacheImmediate { immediate: iterator.next().unwrap() },
                ArithmeticOperationToAFromCachedImmediate { operation: Cp },
            ],

            //INC B
            0x04 => vec![
                IncrementReg8 { reg: Reg8::B },
            ],
            //INC C
            0x0C => vec![
                IncrementReg8 { reg: Reg8::C },
            ],
            //INC D
            0x14 => vec![
                IncrementReg8 { reg: Reg8::D },
            ],
            //INC E
            0x1C => vec![
                IncrementReg8 { reg: Reg8::E },
            ],
            //INC H
            0x24 => vec![
                IncrementReg8 { reg: Reg8::H },
            ],
            //INC L
            0x2C => vec![
                IncrementReg8 { reg: Reg8::L },
            ],
            //INC (HL)
            0x34 => vec![
                Nop,
                CacheHlMem,
                IncrementCachedHlMem
            ],
            //INC A
            0x3C => vec![
                IncrementReg8 { reg: Reg8::A },
            ],
            //DEC B
            0x05 => vec![
                DecrementReg8 { reg: Reg8::B },
            ],
            //DEC C
            0x0D => vec![
                DecrementReg8 { reg: Reg8::C },
            ],
            //DEC D
            0x15 => vec![
                DecrementReg8 { reg: Reg8::D },
            ],
            //DEC E
            0x1D => vec![
                DecrementReg8 { reg: Reg8::E },
            ],
            //DEC H
            0x25 => vec![
                DecrementReg8 { reg: Reg8::H },
            ],
            //DEC L
            0x2D => vec![
                DecrementReg8 { reg: Reg8::L },
            ],
            //DEC (HL)
            0x35 => vec![
                Nop,
                CacheHlMem,
                DecrementCachedHlMem
            ],
            //DEC A
            0x3D => vec![
                DecrementReg8 { reg: Reg8::A },
            ],
            /*TODO maybe here we want to write to lower then higher instead of nop + write16
              same for decrement
            */
            //INC BC
            0x03 => vec![
                Nop,
                IncrementReg16 { reg: Reg16::BC},
            ],
            //INC DE
            0x13 => vec![
                Nop,
                IncrementReg16 { reg: Reg16::DE},
            ],
            //INC HL
            0x23 => vec![
                Nop,
                IncrementReg16 { reg: Reg16::HL},
            ],
            //INC SP
            0x33 => vec![
                Nop,
                IncrementSP,
            ],
            //DEC BC
            0x0B => vec![
                Nop,
                DecrementReg16 { reg: Reg16::BC},
            ],
            //DEC DE
            0x1B => vec![
                Nop,
                DecrementReg16 { reg: Reg16::DE},
            ],
            //DEC HL
            0x2B => vec![
                Nop,
                DecrementReg16 { reg: Reg16::HL},
            ],
            //DEC SP
            0x3B => vec![
                Nop,
                DecrementSP,
            ],
            //DAA
            0x27 => vec![
                DecimalAdjustment,
            ],
            //SCF
            0x37 => vec![
                SetCarryFlag,
            ],
            //CPL
            0x2F => vec![
                ComplementA
            ],
            //CCF
            0x3F => vec![
                ComplementCarryFlag
            ],

            //PUSH/POP

            //POP BC
            0xC1 => vec![
                Nop,
                PopRegisterFromStack { reg: Reg8::C },
                PopRegisterFromStack { reg: Reg8::B },
            ],
            //POP DE
            0xD1 => vec![
                Nop,
                PopRegisterFromStack { reg: Reg8::E },
                PopRegisterFromStack { reg: Reg8::D },
            ],
            //POP HL
            0xE1 => vec![
                Nop,
                PopRegisterFromStack { reg: Reg8::L },
                PopRegisterFromStack { reg: Reg8::H },
            ],
            //POP AF
            0xF1 => vec![
                Nop,
                PopFlagsFromStack,
                PopRegisterFromStack { reg: Reg8::A },
            ],

            //PUSH BC
            0xC5 => vec![
                Nop,
                Nop,
                PushRegisterToStack { reg: Reg8::B },
                PushRegisterToStack { reg: Reg8::C },
            ],

            //PUSH DE
            0xD5 => vec![
                Nop,
                Nop,
                PushRegisterToStack { reg: Reg8::D },
                PushRegisterToStack { reg: Reg8::E },
            ],

            //PUSH HL
            0xE5 => vec![
                Nop,
                Nop,
                PushRegisterToStack { reg: Reg8::H },
                PushRegisterToStack { reg: Reg8::L },
            ],

            //PUSH AF
            0xF5 => vec![
                Nop,
                Nop,
                PushRegisterToStack { reg: Reg8::A },
                PushFlagsToStack,
            ],

            _ => panic!("Opcode not implemented: {}", opcode),
        }.into()

    }
}

fn parse_alternative<T: Iterator<Item = GbByte>>(iterator: &mut T) -> VecDeque<MicroOpcode> {
    use MicroOpcode::*;
    use BitOperation::*;
    use SingleBitOperation::*;

    let opcode = iterator.next().unwrap();

    match opcode {
        0x00..=0x05 | 0x07 |
        0x08..=0x0D | 0x0F |
        0x10..=0x15 | 0x17 |
        0x18..=0x1D | 0x1F |
        0x20..=0x25 | 0x27 |
        0x28..=0x2D | 0x2F |
        0x30..=0x35 | 0x37 |
        0x38..=0x3D | 0x3F  => vec![
            BitOperationToReg8 { operation: get_bit_operation(opcode), reg: get_reg8(opcode) }
        ],

        0x40..=0x45 | 0x47 |
        0x48..=0x4D | 0x4F |
        0x50..=0x55 | 0x57 |
        0x58..=0x5D | 0x5F |
        0x60..=0x65 | 0x67 |
        0x68..=0x6D | 0x6F |
        0x70..=0x75 | 0x77 |
        0x78..=0x7D | 0x7F |

        0x80..=0x85 | 0x87 |
        0x88..=0x8D | 0x8F |
        0x90..=0x95 | 0x97 |
        0x98..=0x9D | 0x9F |
        0xA0..=0xA5 | 0xA7 |
        0xA8..=0xAD | 0xAF |
        0xB0..=0xB5 | 0xB7 |
        0xB8..=0xBD | 0xBF |

        0xC0..=0xC5 | 0xC7 |
        0xC8..=0xCD | 0xCF |
        0xD0..=0xD5 | 0xD7 |
        0xD8..=0xDD | 0xDF |
        0xE0..=0xE5 | 0xE7 |
        0xE8..=0xED | 0xEF |
        0xF0..=0xF5 | 0xF7 |
        0xF8..=0xFD | 0xFF => vec![
            SingleBitOperationToReg8 { operation: get_single_bit_operation(opcode), reg: get_reg8(opcode), bit: get_bit_for_single_bit_operation(opcode) },
        ],

        0x06 => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Rlc },
        ],
        0x0E => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Rrc },
        ],
        0x16 => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Rl },
        ],
        0x1E => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Rr },
        ],
        0x26 => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Sla },
        ],
        0x2E => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Sra },
        ],
        0x36 => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Swap },
        ],
        0x3E => vec![
            Nop,
            CacheHlMem,
            BitOperationToCachedHLMem { operation: Srl },
        ],
        0x46 => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x4E => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x56 => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x5E => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x66 => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x6E => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x76 => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x7E => vec![
            Nop,
            SingleBitOperationFromHLMemToFlags { operation: Bit, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x86 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x8E => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x96 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0x9E => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xA6 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xAE => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xB6 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xBE => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Res, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xC6 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xCE => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xD6 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xDE => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xE6 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xEE => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xF6 => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
        0xFE => vec![
            Nop,
            CacheHlMem,
            SingleBitOperationFromCachedHLMemToHLMem { operation: Set, bit: get_bit_for_single_bit_operation(opcode) }
        ],
    }.into()
}

fn get_bit_operation(opcode: u8) -> BitOperation {
    use BitOperation::*;
    match opcode {
        0x00..=0x07 => Rlc,
        0x08..=0x0F => Rrc,
        0x10..=0x17 => Rl,
        0x18..=0x1F => Rr,
        0x20..=0x27 => Sla,
        0x28..=0x2F => Sra,
        0x30..=0x37 => Swap,
        0x38..=0x3F => Srl,
        _ => panic!("Opcode {} is not a BitOperation", opcode),
    }
}

fn get_single_bit_operation(opcode: u8) -> SingleBitOperation {
    use SingleBitOperation::*;
    match opcode {
        0x40..=0x7F => Bit,
        0x80..=0xBF => Res,
        0xC0..=0xFF => Set,
        _ => panic!("Opcode {} is not a BitOperation", opcode),
    }
}

fn get_reg8(opcode: u8) -> Reg8 {
    match opcode & 0x0F {
        0x0 | 0x8 => Reg8::B,
        0x1 | 0x9 => Reg8::C,
        0x2 | 0xA => Reg8::D,
        0x3 | 0xB => Reg8::E,
        0x4 | 0xC => Reg8::H,
        0x5 | 0xD => Reg8::L,
        0x7 | 0xF => Reg8::A,
        _ => panic!("Opcode {} is not for a Reg8", opcode),
    }
}

fn get_bit_for_single_bit_operation(opcode: u8) -> u8 {
    match opcode {
        0x40..=0x47 => 0,
        0x48..=0x4F => 1,
        0x50..=0x57 => 2,
        0x58..=0x5F => 3,
        0x60..=0x67 => 4,
        0x68..=0x6F => 5,
        0x70..=0x77 => 6,
        0x78..=0x7F => 7,
        0x80..=0x87 => 0,
        0x88..=0x8F => 1,
        0x90..=0x97 => 2,
        0x98..=0x9F => 3,
        0xA0..=0xA7 => 4,
        0xA8..=0xAF => 5,
        0xB0..=0xB7 => 6,
        0xB8..=0xBF => 7,
        0xC0..=0xC7 => 0,
        0xC8..=0xCF => 1,
        0xD0..=0xD7 => 2,
        0xD8..=0xDF => 3,
        0xE0..=0xE7 => 4,
        0xE8..=0xEF => 5,
        0xF0..=0xF7 => 6,
        0xF8..=0xFF => 7,
        _ => panic!("bit operation not supported for opcode: {}", opcode),
    }
}

fn add_sp_i8(stack_pointer: GbWord, offset: i8, flags: Flags) -> (GbWord, Flags) {
    let (_, unsigned_add_flags) =  reg8_reg8_arithmetic_operation(ArithmeticOperation::Add, flags, lower_byte(stack_pointer), offset as u8);

    let result = add_u16_i8(stack_pointer, offset);

    let flags = Flags::new (
        false,
        false,
        unsigned_add_flags.read(Flag::H),
        unsigned_add_flags.read(Flag::C)
    );

    (result, flags)
}

#[cfg(test)]
mod microopcode_test;
