use crate::utils::bitutils::*;

use crate::gb::types::{
    GbByte, GbWord,
};

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Reg8 {
    A,
    B, C,
    D, E,
    H, L,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Reg16 {
    BC,
    DE,
    HL,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Registers {
    // Registers
    a:  GbByte,
    bc: GbWord,
    de: GbWord,
    hl: GbWord,
}

// XXX: bit math should be correct but it's better to check
impl Registers {
    pub fn new(a: GbByte) -> Self {
        Self {
            a,
            bc: 0x0013,
            de: 0x00D8,
            hl: 0x014D,
        }
    }

    pub fn read8(&self, register: Reg8) -> GbByte {
        use Reg8::*;
        match register {
            A => self.a,

            B  => higher_byte(self.bc),
            C  => lower_byte(self.bc),

            D  => higher_byte(self.de),
            E  => lower_byte(self.de),

            H  => higher_byte(self.hl),
            L  => lower_byte(self.hl),
        }
    }

    pub fn read16(&self, register: Reg16) -> GbWord {
        use Reg16::*;
        match register {
            BC => self.bc,
            DE => self.de,
            HL => self.hl,
        }
    }


    pub fn write8(&mut self, register: Reg8, datum: GbByte) {
        use Reg8::*;
        match register {
            A  => self.a = datum,

            B  => self.bc = join_bytes(datum, lower_byte(self.bc)),
            C  => self.bc = join_bytes(higher_byte(self.bc), datum),

            D  => self.de = join_bytes(datum, lower_byte(self.de)),
            E  => self.de = join_bytes(higher_byte(self.de), datum),

            H  => self.hl = join_bytes(datum, lower_byte(self.hl)),
            L  => self.hl = join_bytes(higher_byte(self.hl), datum),
        }
    }

    pub fn write16(&mut self, register: Reg16, datum: GbWord) {
        use Reg16::*;
        match register {
            BC => self.bc = datum,
            DE => self.de = datum,
            HL => self.hl = datum,
        }
    }
}
