use crate::gb::types::GbByte;

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Cache {
    single_byte: GbByte,
    lsb: GbByte,
    msb: GbByte
}

impl Cache {
    pub fn new() -> Self {
        Self {
            single_byte: 0,
            lsb: 0,
            msb: 0
        }
    }

    pub fn cache_single_byte(&mut self, datum: GbByte) {
        self.single_byte = datum;
    }

    pub fn cache_lsb(&mut self, datum: GbByte) {
        self.lsb = datum;
    }

    pub fn cache_msb(&mut self, datum: GbByte) {
        self.msb = datum;
    }

    pub fn get_cached_single_byte(&self) -> GbByte {
        self.single_byte
    }

    pub fn get_cached_lsb(&self) -> GbByte {
        self.lsb
    }

    pub fn get_cached_msb(&self) -> GbByte {
        self.msb
    }
}