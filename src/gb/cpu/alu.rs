use crate::gb::cpu::flags::{Flag, Flags};
use crate::gb::cpu::microopcode::ArithmeticOperation;
use crate::gb::types::{GbByte, GbFlag, GbWord};
use crate::utils::bitutils::{lower_half, lower_twelve_bits};

pub fn add_u16_u16(x: u16, y:u16, flags: Flags) -> (GbWord, Flags) {
    let (result, carry_flag) = x.overflowing_add(y);
    let flags = Flags::new (
        flags.read(Flag::Z),
        false,
        detect_half_carry_u16(x, y),
        carry_flag
    );
    (result, flags)
}

pub fn add_u16_i8(x: u16, y: i8) -> u16 {
    if y < 0 {
        x.wrapping_sub(-y as u8 as u16)
    } else {
        x.wrapping_add(y as u8 as u16)
    }
}

pub fn increment_word(datum: GbWord) -> GbWord {
    datum.overflowing_add(1).0
}

pub fn decrement_word(datum: GbWord) -> GbWord {
    datum.overflowing_sub(1).0
}

pub fn increment_byte(datum: GbByte, old_flags: Flags) -> (GbByte, Flags) {
    let (result, partial_flags) = add_reg8(datum, 1);

    let flags = Flags::new (
        partial_flags.read(Flag::Z),
        partial_flags.read(Flag::N),
        partial_flags.read(Flag::H),
        old_flags.read(Flag::C)
    );

    (result, flags)
}

pub fn decrement_byte(datum: GbByte, old_flags: Flags) -> (GbByte, Flags) {
    let (result, partial_flags) = sub_reg8(datum, 1);

    let flags = Flags::new (
        partial_flags.read(Flag::Z),
        partial_flags.read(Flag::N),
        partial_flags.read(Flag::H),
        old_flags.read(Flag::C)
    );

    (result, flags)
}

pub fn set_carry_flag(flags: Flags) -> Flags {
    Flags::new(
        flags.read(Flag::Z),
        false,
        false,
        true
    )
}

pub fn complement_reg(x: u8, flags: Flags) -> (GbByte, Flags) {
    let new_flags = Flags::new(
        flags.read(Flag::Z),
        true,
        true,
        flags.read(Flag::C)
    );

    (!x, new_flags)
}

pub fn complement_carry_flag(flags: Flags) -> Flags {
    Flags::new(
        flags.read(Flag::Z),
        false,
        false,
        !flags.read(Flag::C)
    )
}

pub fn decimal_adjustment(x: u8, carry: GbFlag, half_carry: GbFlag, subtract: GbFlag) -> (GbByte, Flags) {
    let mut datum = x;
    let mut new_carry = carry;

    if subtract {
        if carry {
            datum = datum.overflowing_sub(0x60).0;
        }
        if half_carry {
            datum = datum.overflowing_sub(0x6).0;
        }
    }
    else{
        if carry || datum > 0x99 {
            datum = datum.overflowing_add(0x60).0;
            new_carry = true;
        }
        if half_carry || ((datum & 0xF) > 0x9) {
            datum = datum.overflowing_add(0x6).0;
        }
    }

    let flags = Flags::new(
        datum == 0,
        subtract,
        false,
        new_carry
    );

    (datum, flags)
}

pub fn reg8_reg8_arithmetic_operation(operation: ArithmeticOperation, flags: Flags, x: u8, y: u8) -> (GbByte, Flags) {
    use ArithmeticOperation::*;
    match operation {
        Add => add_reg8(x,y),
        Adc => adc_reg8(x,y, flags),
        Sub => sub_reg8(x,y),
        Sbc => sbc_reg8(x,y, flags),
        And => and_reg8(x,y),
        Xor => xor_reg8(x,y),
        Or =>  or_reg8(x,y),
        Cp =>  cp_reg8(x,y),
    }
}

fn add_reg8(x: u8, y: u8) -> (GbByte, Flags) {
    let (result, is_overflow) = x.overflowing_add(y);
    let flags = Flags::new (
        result == 0,
        false,
        detect_half_carry(x, y),
        is_overflow,
    );

    (result, flags)
}

fn adc_reg8(x: u8, y: u8, flags: Flags) -> (GbByte, Flags) {
    let old_carry = flags.read(Flag::C) as u8;
    let (partial_result, partial_overflow) = x.overflowing_add(y);
    let (result, overflow) = partial_result.overflowing_add(old_carry);
    let flags = Flags::new (
        result == 0,
        false,
        detect_half_carry_with_carry(x, y, old_carry),
        partial_overflow || overflow,
    );
    (result, flags)
}

fn sub_reg8(x: u8, y: u8) -> (GbByte, Flags) {
    let (result, carry_flag) = x.overflowing_sub(y);
    let flags = Flags::new (
        result == 0,
        true,
        detect_half_borrow(x, y),
        carry_flag
    );
    (result, flags)
}

fn sbc_reg8(x: u8, y: u8, flags: Flags) -> (GbByte, Flags) {
    let old_carry = flags.read(Flag::C) as u8;
    let(partial_result, partial_carry_flag) = x.overflowing_sub(y);
    let (result, carry_flag) = partial_result.overflowing_sub(old_carry);
    let flags = Flags::new (
        result == 0,
        true,
        detect_half_borrow_with_carry(x, y, old_carry),
        partial_carry_flag || carry_flag,
    );
    (result, flags)
}

fn and_reg8(x: u8, y: u8) -> (GbByte, Flags) {
    let result = x & y;
    let flags = Flags::new (
        result == 0,
        false,
        true,
        false,
    );
    (result, flags)
}

fn xor_reg8(x: u8, y: u8) -> (GbByte, Flags) {
    let result = x ^ y;
    let flags = Flags::new (
        result == 0,
        false,
        false,
        false,
    );
    (result, flags)
}

fn or_reg8(x: u8, y: u8) -> (GbByte, Flags) {
    let result = x | y;
    let flags = Flags::new (
        result == 0,
        false,
        false,
        false,
    );
    (result, flags)
}

fn cp_reg8(x: u8, y: u8) -> (GbByte, Flags) {
    let (result, carry_flag) = x.overflowing_sub(y);
    let flags = Flags::new (
        result == 0,
        true,
        detect_half_borrow(x, y),
        carry_flag,
    );
    (x, flags)
}

fn detect_half_carry(x: u8, y: u8) -> bool {
    lower_half(x) + lower_half(y) > 0b1111
}

fn detect_half_carry_with_carry(x: u8, y: u8, c: u8) -> bool {
    lower_half(x) + lower_half(y) + c > 0b1111
}

fn detect_half_borrow(x: u8, y: u8) -> bool {
    lower_half(y) > lower_half(x)
}

fn detect_half_borrow_with_carry(x: u8, y: u8, carry: u8) -> bool {
    (lower_half(y)+carry) > lower_half(x)
}

fn detect_half_carry_u16(x: u16, y: u16) -> bool {
    lower_twelve_bits(x) + lower_twelve_bits(y) > 0b0000111111111111
}
