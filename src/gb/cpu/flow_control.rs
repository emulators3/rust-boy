use crate::gb::cpu::{Cpu, CpuStatus};
use crate::gb::cpu::flags::Flag;
use crate::gb::cpu::microopcode::Condition;
use crate::gb::memory::io_registers::IoReg;
use crate::gb::memory::Memory;
use crate::utils::bitutils::test_bit;
use crate::utils::constants::IE_ADDRESS;

pub fn check_condition_and_flush_if_not_met(condition: Condition, cpu: &mut Cpu) {
    let condition_met = match condition {
        Condition::Z => cpu.flags.read(Flag::Z),
        Condition::C => cpu.flags.read(Flag::C),
        Condition::NZ => !cpu.flags.read(Flag::Z),
        Condition::NC => !cpu.flags.read(Flag::C),
    };

    if !condition_met {
        cpu.cached_micro_opcodes.clear();
    }
}

pub fn stop(cpu: &mut Cpu, mmu: &mut impl Memory) {
    let p1 = mmu.read_io_internal(IoReg::P1);
    let interrupt_requests = mmu.read_io_internal(IoReg::IF);
    let enabled_interrupts = mmu.read(IE_ADDRESS);

    let button_selected = !test_bit(p1, 0) ||
        !test_bit(p1, 1) ||
        !test_bit(p1, 2) ||
        !test_bit(p1, 3);

    let is_interrupt_pending = (interrupt_requests & enabled_interrupts) > 0;

    match (button_selected, is_interrupt_pending) {

        //in this case, it's like a nop
        (true, true) => (),

        //stop is 2 bytes, halt mode entered, div not reset
        (true, false) => {
            halt(cpu, mmu);
            cpu.stop_skip_byte = true;
        },

        //stop is 1 byte, stop mode entered, div reset
        (false, true) => {
            cpu.status = CpuStatus::Stopped;
            mmu.set_div_reset_requested(true);
        },

        //stop is 2 bytes, stop mode entered, div reset
        (false, false) => {
            cpu.status = CpuStatus::Stopped;
            mmu.set_div_reset_requested(true);
            cpu.stop_skip_byte = true;
        },
    }
}

pub fn halt(cpu: &mut Cpu, mmu: &mut impl Memory) {
    let interrupt_requests = mmu.read_io_internal(IoReg::IF);
    let enabled_interrupts = mmu.read(IE_ADDRESS);

    if !cpu.ime && (interrupt_requests & enabled_interrupts > 0) && cpu.next_can_interrupt.is_none() {
        //IME=0 and [IE] & [IF] != 0, opcode after halt will be read twice
        cpu.halt_bug = true
    }

    cpu.status = CpuStatus::Halted;
}