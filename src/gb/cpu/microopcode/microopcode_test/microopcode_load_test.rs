use crate::gb::cpu::flags::Flags;
use crate::gb::cpu::microopcode::MicroOpcode;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::cpu::microopcode::microopcode_test::microopcode_testing_helpers::*;
use crate::gb::memory::Memory;

#[test]
fn exec_LD_REG16_IMMEDIATE() {
    for test_data in [
        Reg16ImmediateTestData { binary_instructions: [0x01, 0x10, 0xE5], reg16: Reg16::BC },
        Reg16ImmediateTestData { binary_instructions: [0x11, 0x10, 0xE5], reg16: Reg16::DE },
        Reg16ImmediateTestData { binary_instructions: [0x21, 0x10, 0xE5], reg16: Reg16::HL },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(test_data.reg16, 0xFFFF);

        let (mut expected_cpu,expected_mmu, _) = setup();
        expected_cpu.registers.write16(test_data.reg16, 0xE510);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_IMMEDIATE_MEM_FROM_SP() {
    let binary_instructions = [0x08, 0x50, 0xCD];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0xABCD;

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_mmu.write(0xCD50, 0xCD);
    expected_mmu.write(0xCD51, 0xAB);
    expected_cpu.stack_pointer = 0xABCD;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_SP_IMMEDIATE() {
    let binary_instructions = [0x31, 0x10, 0xE5];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0xFFFF;

    let (mut expected_cpu,expected_mmu, _) = setup();
    expected_cpu.stack_pointer = 0xE510;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_REG8_IMMEDIATE() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0x06, 0xF1], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0x0E, 0xF1], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0x16, 0xF1], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0x1E, 0xF1], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0x26, 0xF1], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0x2E, 0xF1], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0x3E, 0xF1], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 10);

        let (mut expected_cpu,expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0xF1);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_HLMEM_IMMEDIATE() {
    let binary_instructions =  [0x36, 0xF3];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0xFF81);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xFF81);
    expected_mmu.write(0xFF81, 0xF3);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_REG16_MEM_A() {
    for test_data in [
        Reg16Reg8TestData { binary_instructions: [0x02], reg16: Reg16::BC, reg8: Reg8::A},
        Reg16Reg8TestData { binary_instructions: [0x12], reg16: Reg16::DE, reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 10);
        cpu.registers.write16(test_data.reg16, 0xFF81);
        mmu.write(0xFF81, 15);

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 10);
        expected_cpu.registers.write16(test_data.reg16, 0xFF81);
        expected_mmu.write(0xFF81, 10);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_HL_MEM_INC_A() {
    let binary_instructions = [0x22];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 10);
    cpu.registers.write16(Reg16::HL, 0xFF81);
    mmu.write(0xFF81, 15);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 10);
    expected_cpu.registers.write16(Reg16::HL, 0xFF82);
    expected_mmu.write(0xFF81, 10);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_HL_MEM_DEC_A() {
    let binary_instructions = [0x32];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 10);
    cpu.registers.write16(Reg16::HL, 0xFF81);
    mmu.write(0xFF81, 15);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 10);
    expected_cpu.registers.write16(Reg16::HL, 0xFF80);
    expected_mmu.write(0xFF81, 10);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_A_REG16_MEM() {
    for test_data in [
        Reg16Reg8TestData { binary_instructions: [0x0A], reg16: Reg16::BC, reg8: Reg8::A},
        Reg16Reg8TestData { binary_instructions: [0x1A], reg16: Reg16::DE, reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 10);
        cpu.registers.write16(test_data.reg16, 0xFF81);
        mmu.write(0xFF81, 15);

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 15);
        expected_cpu.registers.write16(test_data.reg16, 0xFF81);
        expected_mmu.write(0xFF81, 15);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_A_HL_MEM_INC() {
    let binary_instructions = [0x2A];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 10);
    cpu.registers.write16(Reg16::HL, 0xFF81);
    mmu.write(0xFF81, 15);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 15);
    expected_cpu.registers.write16(Reg16::HL, 0xFF82);
    expected_mmu.write(0xFF81, 15);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_A_HL_MEM_DEC() {
    let binary_instructions = [0x3A];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 10);
    cpu.registers.write16(Reg16::HL, 0xFF81);
    mmu.write(0xFF81, 15);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 15);
    expected_cpu.registers.write16(Reg16::HL, 0xFF80);
    expected_mmu.write(0xFF81, 15);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_REG8_REG8() {
    for test_data in [
        Reg8Reg8TestData { binary_instructions: [0x40], to: Reg8::B, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x41], to: Reg8::B, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x42], to: Reg8::B, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x43], to: Reg8::B, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x44], to: Reg8::B, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x45], to: Reg8::B, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x47], to: Reg8::B, from: Reg8::A },
        Reg8Reg8TestData { binary_instructions: [0x48], to: Reg8::C, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x49], to: Reg8::C, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x4A], to: Reg8::C, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x4B], to: Reg8::C, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x4C], to: Reg8::C, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x4D], to: Reg8::C, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x4F], to: Reg8::C, from: Reg8::A },

        Reg8Reg8TestData { binary_instructions: [0x50], to: Reg8::D, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x51], to: Reg8::D, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x52], to: Reg8::D, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x53], to: Reg8::D, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x54], to: Reg8::D, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x55], to: Reg8::D, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x57], to: Reg8::D, from: Reg8::A },
        Reg8Reg8TestData { binary_instructions: [0x58], to: Reg8::E, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x59], to: Reg8::E, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x5A], to: Reg8::E, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x5B], to: Reg8::E, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x5C], to: Reg8::E, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x5D], to: Reg8::E, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x5F], to: Reg8::E, from: Reg8::A },

        Reg8Reg8TestData { binary_instructions: [0x60], to: Reg8::H, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x61], to: Reg8::H, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x62], to: Reg8::H, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x63], to: Reg8::H, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x64], to: Reg8::H, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x65], to: Reg8::H, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x67], to: Reg8::H, from: Reg8::A },
        Reg8Reg8TestData { binary_instructions: [0x68], to: Reg8::L, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x69], to: Reg8::L, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x6A], to: Reg8::L, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x6B], to: Reg8::L, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x6C], to: Reg8::L, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x6D], to: Reg8::L, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x6F], to: Reg8::L, from: Reg8::A },

        Reg8Reg8TestData { binary_instructions: [0x78], to: Reg8::A, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x79], to: Reg8::A, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x7A], to: Reg8::A, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x7B], to: Reg8::A, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x7C], to: Reg8::A, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x7D], to: Reg8::A, from: Reg8::L },
        Reg8Reg8TestData { binary_instructions: [0x7F], to: Reg8::A, from: Reg8::A },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.to, 10);
        cpu.registers.write8(test_data.from, 15);

        let(mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.to, 15);
        expected_cpu.registers.write8(test_data.from, 15);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_REG8_HL_MEM() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x46], reg8: Reg8::B},
        Reg8TestData { binary_instructions: [0x4E], reg8: Reg8::C},
        Reg8TestData { binary_instructions: [0x56], reg8: Reg8::D},
        Reg8TestData { binary_instructions: [0x5E], reg8: Reg8::E},
        Reg8TestData { binary_instructions: [0x7E], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 10);
        cpu.registers.write16(Reg16::HL, 0xFF81);
        mmu.write(0xFF81, 15);

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 15);
        expected_cpu.registers.write16(Reg16::HL, 0xFF81);
        expected_mmu.write(0xFF81, 15);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_H_HL_MEM() {
    let binary_instructions = [0x66];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0xFF81);
    mmu.write(0xFF81, 15);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x0F81);
    expected_mmu.write(0xFF81, 15);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_L_HL_MEM() {
    let binary_instructions = [0x6E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0xFF81);
    mmu.write(0xFF81, 15);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xFF0F);
    expected_mmu.write(0xFF81, 15);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

fn exec_LD_HL_MEM_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x70], reg8: Reg8::B},
        Reg8TestData { binary_instructions: [0x71], reg8: Reg8::C},
        Reg8TestData { binary_instructions: [0x72], reg8: Reg8::D},
        Reg8TestData { binary_instructions: [0x73], reg8: Reg8::E},
        Reg8TestData { binary_instructions: [0x74], reg8: Reg8::H},
        Reg8TestData { binary_instructions: [0x75], reg8: Reg8::L},
        Reg8TestData { binary_instructions: [0x77], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 10);
        cpu.registers.write16(Reg16::HL, 0xFF81);
        mmu.write(0xFF81, 15);

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 10);
        expected_cpu.registers.write16(Reg16::HL, 0xFF81);
        expected_mmu.write(0xFF81, 10);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_LD_IMMEDIATE_MEM_FROM_A() {
    let binary_instructions = [0xEA, 0x55, 0xCD];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x12);
    mmu.write(0xCD55, 0x20);

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x12);
    expected_mmu.write(0xCD55, 0x12);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_A_FROM_IMMEDIATE_MEM() {
    let binary_instructions = [0xFA, 0x55, 0xCD];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x12);
    mmu.write(0xCD55, 0x20);

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x20);
    expected_mmu.write(0xCD55, 0x20);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_OFFSET_IMMEDIATE_MEM_FROM_A() {
    let binary_instructions = [0xE0, 0x18];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x12);
    mmu.write(0xFF18, 0x20);

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x12);
    expected_mmu.write(0xFF18, 0x12);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_A_FROM_OFFSET_IMMEDIATE_MEM() {
    let binary_instructions = [0xF0, 0x90];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x12);
    mmu.write(0xFF90, 0x20);

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x20);
    expected_mmu.write(0xFF90, 0x20);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_C_OFFSET_IMMEDIATE_MEM_FROM_A() {
    let binary_instructions = [0xE2];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x12);
    cpu.registers.write8(Reg8::C, 0x18);
    mmu.write(0xFF18, 0x20);

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x12);
    expected_cpu.registers.write8(Reg8::C, 0x18);
    expected_mmu.write(0xFF18, 0x12);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_A_FROM_C_OFFSET_IMMEDIATE_MEM() {
    let binary_instructions = [0xF2];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x12);
    cpu.registers.write8(Reg8::C, 0x90);
    mmu.write(0xFF90, 0x20);

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x20);
    expected_cpu.registers.write8(Reg8::C, 0x90);
    expected_mmu.write(0xFF90, 0x20);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_HL_FROM_SP_AND_IMMEDIATE_SIGNED_POSITIVE() {
    let binary_instructions = [0xF8, 0x7D];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x0090);
    cpu.stack_pointer = 0xF109;
    cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xF186);
    expected_cpu.stack_pointer = 0xF109;
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_HL_FROM_SP_AND_IMMEDIATE_SIGNED_NEGATIVE() {
    let binary_instructions = [0xF8, 0xFD];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x0090);
    cpu.stack_pointer = 0xF109;
    cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xF106);
    expected_cpu.stack_pointer = 0xF109;
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        true,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_LD_SP_FROM_HL() {
    let binary_instructions = [0xF9];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0xF112);
    cpu.stack_pointer = 0xF109;

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xF112);
    expected_cpu.stack_pointer = 0xF112;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}