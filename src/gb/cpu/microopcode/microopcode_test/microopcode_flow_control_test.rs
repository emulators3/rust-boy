use crate::gb::cpu::CpuStatus;
use crate::gb::cpu::flags::Flags;
use crate::gb::cpu::microopcode::MicroOpcode;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::cpu::microopcode::microopcode_test::microopcode_testing_helpers::*;
use crate::gb::memory::div_register::DivRegister;
use crate::gb::memory::Memory;
use crate::gb::memory::p1_register::P1Register;

#[test]
fn exec_NOP() {
    let binary_instructions =  [0x00];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();

    let(mut expected_cpu, expected_mmu, _) = setup();

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_STOP_JOYPAD_PRESSED_INTERRUPT_PENDING_RESULTS_IN_NOP() {
    let binary_instructions =  [0x10];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    mmu.write_detected_input(0b00001110);
    mmu.write(0xFF0F, 0b00001111);
    mmu.write(0xFFFF, 0b00001111);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.status=CpuStatus::Running;
    expected_mmu.write_detected_input(0b00001110);
    expected_mmu.write(0xFF0F, 0b00001111);
    expected_mmu.write(0xFFFF, 0b00001111);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_STOP_JOYPAD_PRESSED_INTERRUPT_NOT_PENDING_RESULTS_IN_HALT_AND_SKIP_NEXT_OPCODE() {
    let binary_instructions =  [0x10];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    mmu.write_detected_input(0b00001110);
    mmu.write(0xFF0F, 0b00001100);
    mmu.write(0xFFFF, 0b00000011);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.status=CpuStatus::Halted;
    expected_cpu.stop_skip_byte=true;
    expected_mmu.write_detected_input(0b00001110);
    expected_mmu.write(0xFF0F, 0b00001100);
    expected_mmu.write(0xFFFF, 0b00000011);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_STOP_JOYPAD_NOT_PRESSED_INTERRUPT_PENDING_RESULTS_IN_STOP_AND_DIV_RESET() {
    let binary_instructions =  [0x10];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    mmu.write_detected_input(0b00001111);
    mmu.write(0xFF0F, 0b00000011);
    mmu.write(0xFFFF, 0b00001110);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.status=CpuStatus::Stopped;
    expected_mmu.set_div_reset_requested(true);
    expected_mmu.write_detected_input(0b00001111);
    expected_mmu.write(0xFF0F, 0b00000011);
    expected_mmu.write(0xFFFF, 0b00001110);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_STOP_JOYPAD_NOT_PRESSED_INTERRUPT_NOT_PENDING_RESULTS_IN_STOP_DIV_RESET_AND_SKIP_OPCODE() {
    let binary_instructions =  [0x10];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    mmu.write_detected_input(0b00001111);
    mmu.write(0xFF0F, 0b00000110);
    mmu.write(0xFFFF, 0b00001001);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.status=CpuStatus::Stopped;
    expected_cpu.stop_skip_byte=true;
    expected_mmu.set_div_reset_requested(true);
    expected_mmu.write_detected_input(0b00001111);
    expected_mmu.write(0xFF0F, 0b00000110);
    expected_mmu.write(0xFFFF, 0b00001001);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}


#[test]
fn exec_HALT_NORMAL() {
    let binary_instructions =  [0x76];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.status=CpuStatus::Halted;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_HALT_IE_IF_BUG() {
    let binary_instructions =  [0x76];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.ime = false;
    mmu.write(0xFF0F, 0b00000110);
    mmu.write(0xFFFF, 0b00001011);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.status=CpuStatus::Halted;
    expected_cpu.ime = false;
    expected_cpu.halt_bug=true;
    expected_mmu.write(0xFF0F, 0b00000110);
    expected_mmu.write(0xFFFF, 0b00001011);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_DI() {
    let binary_instructions =  [0xF3];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.ime = true;

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.ime = false;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_EI() {
    let binary_instructions =  [0xFB];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.ime = false;

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.ime = false;
    expected_cpu.next_can_interrupt = Some(true);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_IMMEDIATE_SIGNED() {
    let binary_instructions =  [0x18, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0109;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_IMMEDIATE_SIGNED_NEGATIVE() {
    let binary_instructions =  [0x18, 0xFF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x00FF;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_NZ_IMMEDIATE_SIGNED_CONDITION_NOT_MET() {
    let binary_instructions =  [0x20, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_NZ_IMMEDIATE_SIGNED_CONDITION_MET() {
    let binary_instructions =  [0x20, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0109;
    expected_cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_NC_IMMEDIATE_SIGNED_CONDITION_NOT_MET() {
    let binary_instructions =  [0x30, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_NC_IMMEDIATE_SIGNED_CONDITION_MET() {
    let binary_instructions =  [0x30, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0109;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_Z_IMMEDIATE_SIGNED_CONDITION_NOT_MET() {
    let binary_instructions =  [0x28, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_Z_IMMEDIATE_SIGNED_CONDITION_MET() {
    let binary_instructions =  [0x28, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        false,
        false,
        false
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0109;
    expected_cpu.flags = Flags::new (
        true,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_C_IMMEDIATE_SIGNED_CONDITION_NOT_MET() {
    let binary_instructions =  [0x38, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JR_C_IMMEDIATE_SIGNED_CONDITION_MET() {
    let binary_instructions =  [0x38, 0x09];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0109;
    expected_cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}


#[test]
fn exec_JP_IMMEDIATE() {
    let binary_instructions =  [0xC3, 0x09, 0x02];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0209;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_NZ_IMMEDIATE_CONDITION_NOT_MET() {
    let binary_instructions =  [0xC2, 0x09, 0x02];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_NZ_IMMEDIATE_CONDITION_MET() {
    let binary_instructions =  [0xC2, 0x09, 0x02];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0209;
    expected_cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_NC_IMMEDIATE_CONDITION_NOT_MET() {
    let binary_instructions =  [0xD2, 0x09, 0x03];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_NC_IMMEDIATE_CONDITION_MET() {
    let binary_instructions =  [0xD2, 0x09, 0x13];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x1309;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_Z_IMMEDIATE_CONDITION_NOT_MET() {
    let binary_instructions =  [0xCA, 0x09, 0x10];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_Z_IMMEDIATE_CONDITION_MET() {
    let binary_instructions =  [0xCA, 0x09, 0x10];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        false,
        false,
        false
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x1009;
    expected_cpu.flags = Flags::new (
        true,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_C_IMMEDIATE_CONDITION_NOT_MET() {
    let binary_instructions =  [0xDA, 0x09, 0x41];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_C_IMMEDIATE_CONDITION_MET() {
    let binary_instructions =  [0xDA, 0xAA, 0x41];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x41AA;
    expected_cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_JP_HL() {
    let binary_instructions =  [0xE9];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.registers.write16(Reg16::HL, 0x1234);

    let(mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x1234;
    expected_cpu.registers.write16(Reg16::HL, 0x1234);


    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET() {
    let binary_instructions =  [0xC9];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0F01;
    expected_cpu.stack_pointer = 0xFFFD;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_NZ_CONDITION_NOT_MET() {
    let binary_instructions =  [0xC0];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFB;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_NZ_CONDITION_MET() {
    let binary_instructions =  [0xC0];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0F01;
    expected_cpu.stack_pointer = 0xFFFD;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_NC_CONDITION_NOT_MET() {
    let binary_instructions =  [0xD0];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFB;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_NC_CONDITION_MET() {
    let binary_instructions =  [0xD0];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0F01;
    expected_cpu.stack_pointer = 0xFFFD;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_Z_CONDITION_NOT_MET() {
    let binary_instructions =  [0xC8];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFB;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_Z_CONDITION_MET() {
    let binary_instructions =  [0xC8];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0F01;
    expected_cpu.stack_pointer = 0xFFFD;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_C_CONDITION_NOT_MET() {
    let binary_instructions =  [0xD8];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFB;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RET_C_CONDITION_MET() {
    let binary_instructions =  [0xD8];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0F01;
    expected_cpu.stack_pointer = 0xFFFD;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RETI() {
    let binary_instructions =  [0xD9];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.ime = false;
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFB;
    mmu.write(0xFFFB, 0x01);
    mmu.write(0xFFFC, 0x0F);

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.ime = true;
    expected_cpu.program_counter = 0x0F01;
    expected_cpu.stack_pointer = 0xFFFD;
    expected_mmu.write(0xFFFB, 0x01);
    expected_mmu.write(0xFFFC, 0x0F);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL() {
    let binary_instructions =  [0xCD, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x5F2D;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_NZ_CONDITION_NOT_MET() {
    let binary_instructions =  [0xC4, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFE;
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_NZ_CONDITION_MET() {
    let binary_instructions =  [0xC4, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x5F2D;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);
    expected_cpu.flags = Flags::new(
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_NC_CONDITION_NOT_MET() {
    let binary_instructions =  [0xD4, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFE;
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_NC_CONDITION_MET() {
    let binary_instructions =  [0xD4, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x5F2D;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);
    expected_cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_Z_CONDITION_NOT_MET() {
    let binary_instructions =  [0xCC, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        false,
        true,
        true,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFE;
    expected_cpu.flags = Flags::new(
        false,
        true,
        true,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_Z_CONDITION_MET() {
    let binary_instructions =  [0xCC, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x5F2D;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_C_CONDITION_NOT_MET() {
    let binary_instructions =  [0xDC, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0100;
    expected_cpu.stack_pointer = 0xFFFE;
    expected_cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CALL_C_CONDITION_MET() {
    let binary_instructions =  [0xDC, 0x2D, 0x5F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x5F2D;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_00H() {
    let binary_instructions =  [0xC7];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0000;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_08H() {
    let binary_instructions =  [0xCF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0008;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_10H() {
    let binary_instructions =  [0xD7];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0010;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_18H() {
    let binary_instructions =  [0xDF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0018;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_20H() {
    let binary_instructions =  [0xE7];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0020;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_28H() {
    let binary_instructions =  [0xEF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0028;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_30H() {
    let binary_instructions =  [0xF7];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0030;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RST_38H() {
    let binary_instructions =  [0xFF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.program_counter = 0x0100;
    cpu.stack_pointer = 0xFFFE;

    let(mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.program_counter = 0x0038;
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0x01);
    expected_mmu.write(0xFFFC, 0x00);

    let micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    cpu.cached_micro_opcodes = micro_opcodes;

    while !cpu.cached_micro_opcodes.is_empty() {
        cpu.cached_micro_opcodes.pop_front().unwrap().instruction(&mut cpu, &mut mmu, &mut cache);
    }

    //has read until the end
    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}