use crate::gb::cpu::flags::Flags;
use crate::gb::cpu::microopcode::MicroOpcode;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::cpu::microopcode::microopcode_test::microopcode_testing_helpers::*;
use crate::gb::memory::Memory;

#[test]
fn exec_POP_REG16() {
    for test_data in [
        Reg16TestData { binary_instructions: [0xC1], reg16: Reg16::BC, msb: Reg8::B, lsb: Reg8::C},
        Reg16TestData { binary_instructions: [0xD1], reg16: Reg16::DE, msb: Reg8::D, lsb: Reg8::E},
        Reg16TestData { binary_instructions: [0xE1], reg16: Reg16::HL, msb: Reg8::H, lsb: Reg8::L},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.stack_pointer = 0xFFFC;
        mmu.write(0xFFFD, 0b00000010);
        mmu.write(0xFFFC, 0b11110000);
        cpu.flags = Flags::new(
            true,
            false,
            false,
            true,
        );


        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.stack_pointer = 0xFFFE;
        expected_cpu.registers.write8(test_data.msb, 0b00000010);
        expected_cpu.registers.write8(test_data.lsb, 0b11110000);
        expected_cpu.flags = Flags::new(
            true,
            false,
            false,
            true,
        );
        expected_mmu.write(0xFFFD, 0b00000010);
        expected_mmu.write(0xFFFC, 0b11110000);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_POP_AF() {
    let binary_instructions = [0xF1];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0xFFFC;
    mmu.write(0xFFFD, 0b00000010);
    mmu.write(0xFFFC, 0b01100000);
    cpu.flags = Flags::new(
        true,
        false,
        false,
        true,
    );


    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.stack_pointer = 0xFFFE;
    expected_cpu.registers.write8(Reg8::A, 0b00000010);
    expected_cpu.flags = Flags::new (
        false,
        true,
        true,
        false,
    );

    expected_mmu.write(0xFFFD, 0b00000010);
    expected_mmu.write(0xFFFC, 0b01100000);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}


#[test]
fn exec_PUSH_REG16() {
    for test_data in [
        Reg16TestData { binary_instructions: [0xC5], reg16: Reg16::BC, msb: Reg8::B, lsb: Reg8::C},
        Reg16TestData { binary_instructions: [0xD5], reg16: Reg16::DE, msb: Reg8::D, lsb: Reg8::E},
        Reg16TestData { binary_instructions: [0xE5], reg16: Reg16::HL, msb: Reg8::H, lsb: Reg8::L},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.msb, 0b00000010);
        cpu.registers.write8(test_data.lsb, 0b11110000);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            true,
        );


        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.msb, 0b00000010);
        expected_cpu.registers.write8(test_data.lsb, 0b11110000);
        expected_cpu.flags = Flags::new(
            true,
            true,
            true,
            true,
        );
        expected_cpu.stack_pointer = 0xFFFC;
        expected_mmu.write(0xFFFD, 0b00000010);
        expected_mmu.write(0xFFFC, 0b11110000);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_PUSH_AF() {
    let binary_instructions = [0xF5];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000010);
    cpu.flags = Flags::new(
        false,
        true,
        false,
        true,
    );


    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000010);
    expected_cpu.flags = Flags::new(
        false,
        true,
        false,
        true,
    );
    expected_cpu.stack_pointer = 0xFFFC;
    expected_mmu.write(0xFFFD, 0b00000010);
    expected_mmu.write(0xFFFC, 0b01010000);

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}