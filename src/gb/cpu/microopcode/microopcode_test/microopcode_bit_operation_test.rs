use crate::gb::cpu::flags::Flags;
use crate::gb::cpu::microopcode::MicroOpcode;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::cpu::microopcode::microopcode_test::microopcode_testing_helpers::*;
use crate::gb::memory::Memory;

#[test]
fn exec_RLC_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x00], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x01], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x02], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x03], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x04], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x05], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x07], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10100001);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b01000011);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_RLC_HL_MEM() {
    let binary_instructions = [0xCB, 0x06];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b01000011);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RRC_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x08], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x09], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x0A], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x0B], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x0C], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x0D], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x0F], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10100001);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b11010000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_RRC_HL_MEM() {
    let binary_instructions = [0xCB, 0x0E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b11010000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RL_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x10], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x11], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x12], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x13], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x14], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x15], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x17], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10100001);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b01000010);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}


#[test]
fn exec_RL_HL_MEM() {
    let binary_instructions = [0xCB, 0x16];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b01000010);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}


#[test]
fn exec_RR_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x18], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x19], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x1A], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x1B], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x1C], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x1D], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x1F], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10100001);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b01010000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_RR_HL_MEM() {
    let binary_instructions = [0xCB, 0x1E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b01010000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SLA_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x20], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x21], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x22], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x23], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x24], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x25], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x27], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10000000);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b00000000);
        expected_cpu.flags = Flags::new(
            true,
            false,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}


#[test]
fn exec_SLA_HL_MEM() {
    let binary_instructions = [0xCB, 0x16];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10000000);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b00000000);
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SRA_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x28], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x29], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x2A], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x2B], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x2C], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x2D], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x2F], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10100000);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            true
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b11010000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SRA_HL_MEM() {
    let binary_instructions = [0xCB, 0x2E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b11010000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SWAP_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x30], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x31], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x32], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x33], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x34], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x35], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x37], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b11110000);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            true
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b00001111);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}


#[test]
fn exec_SWAP_HL_MEM() {
    let binary_instructions = [0xCB, 0x36];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b00000000);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        true
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b00000000);
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        false
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SRL_REG8() {
    for test_data in [
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x38], reg8: Reg8::B},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x39], reg8: Reg8::C},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x3A], reg8: Reg8::D},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x3B], reg8: Reg8::E},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x3C], reg8: Reg8::H},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x3D], reg8: Reg8::L},
        TwoBytesReg8TestData { binary_instructions: [0xCB, 0x3F], reg8: Reg8::A},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b10100000);
        cpu.flags = Flags::new(
            true,
            true,
            true,
            true
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b01010000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SRL_HL_MEM() {
    let binary_instructions = [0xCB, 0x3E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0x9F9F);
    mmu.write(0x9F9F, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
    expected_mmu.write(0x9F9F, 0b01010000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_BIT_REG8() {
    for test_data in [
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x40], reg8: Reg8::B, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x41], reg8: Reg8::C, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x42], reg8: Reg8::D, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x43], reg8: Reg8::E, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x44], reg8: Reg8::H, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x45], reg8: Reg8::L, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x47], reg8: Reg8::A, bit: 0},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x48], reg8: Reg8::B, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x49], reg8: Reg8::C, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x4A], reg8: Reg8::D, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x4B], reg8: Reg8::E, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x4C], reg8: Reg8::H, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x4D], reg8: Reg8::L, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x4F], reg8: Reg8::A, bit: 1},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x50], reg8: Reg8::B, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x51], reg8: Reg8::C, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x52], reg8: Reg8::D, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x53], reg8: Reg8::E, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x54], reg8: Reg8::H, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x55], reg8: Reg8::L, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x57], reg8: Reg8::A, bit: 2},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x58], reg8: Reg8::B, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x59], reg8: Reg8::C, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x5A], reg8: Reg8::D, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x5B], reg8: Reg8::E, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x5C], reg8: Reg8::H, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x5D], reg8: Reg8::L, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x5F], reg8: Reg8::A, bit: 3},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x60], reg8: Reg8::B, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x61], reg8: Reg8::C, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x62], reg8: Reg8::D, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x63], reg8: Reg8::E, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x64], reg8: Reg8::H, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x65], reg8: Reg8::L, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x67], reg8: Reg8::A, bit: 4},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x68], reg8: Reg8::B, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x69], reg8: Reg8::C, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x6A], reg8: Reg8::D, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x6B], reg8: Reg8::E, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x6C], reg8: Reg8::H, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x6D], reg8: Reg8::L, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x6F], reg8: Reg8::A, bit: 5},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x70], reg8: Reg8::B, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x71], reg8: Reg8::C, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x72], reg8: Reg8::D, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x73], reg8: Reg8::E, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x74], reg8: Reg8::H, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x75], reg8: Reg8::L, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x77], reg8: Reg8::A, bit: 6},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x78], reg8: Reg8::B, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x79], reg8: Reg8::C, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x7A], reg8: Reg8::D, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x7B], reg8: Reg8::E, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x7C], reg8: Reg8::H, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x7D], reg8: Reg8::L, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x7F], reg8: Reg8::A, bit: 7},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();
        let datum = 0b11111111 ^ (1 << test_data.bit);

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, datum);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, datum);
        expected_cpu.flags = Flags::new(
            true,
            false,
            true,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_BIT_HL_MEM() {
    for test_data in [
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x46], reg8: Reg8::B, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x4E], reg8: Reg8::B, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x56], reg8: Reg8::B, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x5E], reg8: Reg8::B, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x66], reg8: Reg8::B, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x6E], reg8: Reg8::B, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x76], reg8: Reg8::B, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x7E], reg8: Reg8::B, bit: 7},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();
        let datum = 0b00000000 | (1 << test_data.bit);

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(Reg16::HL, 0x9F9F);
        mmu.write(0x9F9F, datum);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
        expected_mmu.write(0x9F9F, datum);
        expected_cpu.flags = Flags::new(
            false,
            false,
            true,
            false
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_RES_REG8() {
    for test_data in [
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x80], reg8: Reg8::B, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x81], reg8: Reg8::C, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x82], reg8: Reg8::D, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x83], reg8: Reg8::E, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x84], reg8: Reg8::H, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x85], reg8: Reg8::L, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x87], reg8: Reg8::A, bit: 0},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x88], reg8: Reg8::B, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x89], reg8: Reg8::C, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x8A], reg8: Reg8::D, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x8B], reg8: Reg8::E, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x8C], reg8: Reg8::H, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x8D], reg8: Reg8::L, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x8F], reg8: Reg8::A, bit: 1},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x90], reg8: Reg8::B, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x91], reg8: Reg8::C, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x92], reg8: Reg8::D, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x93], reg8: Reg8::E, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x94], reg8: Reg8::H, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x95], reg8: Reg8::L, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x97], reg8: Reg8::A, bit: 2},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0x98], reg8: Reg8::B, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x99], reg8: Reg8::C, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x9A], reg8: Reg8::D, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x9B], reg8: Reg8::E, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x9C], reg8: Reg8::H, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x9D], reg8: Reg8::L, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x9F], reg8: Reg8::A, bit: 3},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA0], reg8: Reg8::B, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA1], reg8: Reg8::C, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA2], reg8: Reg8::D, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA3], reg8: Reg8::E, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA4], reg8: Reg8::H, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA5], reg8: Reg8::L, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA7], reg8: Reg8::A, bit: 4},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA8], reg8: Reg8::B, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA9], reg8: Reg8::C, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xAA], reg8: Reg8::D, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xAB], reg8: Reg8::E, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xAC], reg8: Reg8::H, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xAD], reg8: Reg8::L, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xAF], reg8: Reg8::A, bit: 5},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB0], reg8: Reg8::B, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB1], reg8: Reg8::C, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB2], reg8: Reg8::D, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB3], reg8: Reg8::E, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB4], reg8: Reg8::H, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB5], reg8: Reg8::L, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB7], reg8: Reg8::A, bit: 6},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB8], reg8: Reg8::B, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB9], reg8: Reg8::C, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xBA], reg8: Reg8::D, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xBB], reg8: Reg8::E, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xBC], reg8: Reg8::H, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xBD], reg8: Reg8::L, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xBF], reg8: Reg8::A, bit: 7},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();
        let datum = 0b11111111;
        let expected_result = 0b11111111 ^ (1 << test_data.bit);


        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, datum);
        cpu.flags = Flags::new(
            false,
            true,
            false,
            true
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, expected_result);
        expected_cpu.flags = Flags::new(
            false,
            true,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_RES_HL_MEM() {
    for test_data in [
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x86], reg8: Reg8::B, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x8E], reg8: Reg8::B, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x96], reg8: Reg8::B, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0x9E], reg8: Reg8::B, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xA6], reg8: Reg8::B, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xAE], reg8: Reg8::B, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xB6], reg8: Reg8::B, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xBE], reg8: Reg8::B, bit: 7},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();
        let datum = 0b11111111;
        let expected_result = 0b11111111 ^ (1 << test_data.bit);

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(Reg16::HL, 0x9F9F);
        mmu.write(0x9F9F, datum);
        cpu.flags = Flags::new(
            true,
            false,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
        expected_mmu.write(0x9F9F, expected_result);
        expected_cpu.flags = Flags::new(
            true,
            false,
            true,
            false
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SET_REG8() {
    for test_data in [
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC0], reg8: Reg8::B, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC1], reg8: Reg8::C, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC2], reg8: Reg8::D, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC3], reg8: Reg8::E, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC4], reg8: Reg8::H, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC5], reg8: Reg8::L, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC7], reg8: Reg8::A, bit: 0},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC8], reg8: Reg8::B, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC9], reg8: Reg8::C, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xCA], reg8: Reg8::D, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xCB], reg8: Reg8::E, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xCC], reg8: Reg8::H, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xCD], reg8: Reg8::L, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xCF], reg8: Reg8::A, bit: 1},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD0], reg8: Reg8::B, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD1], reg8: Reg8::C, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD2], reg8: Reg8::D, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD3], reg8: Reg8::E, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD4], reg8: Reg8::H, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD5], reg8: Reg8::L, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD7], reg8: Reg8::A, bit: 2},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD8], reg8: Reg8::B, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD9], reg8: Reg8::C, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xDA], reg8: Reg8::D, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xDB], reg8: Reg8::E, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xDC], reg8: Reg8::H, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xDD], reg8: Reg8::L, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xDF], reg8: Reg8::A, bit: 3},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE0], reg8: Reg8::B, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE1], reg8: Reg8::C, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE2], reg8: Reg8::D, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE3], reg8: Reg8::E, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE4], reg8: Reg8::H, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE5], reg8: Reg8::L, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE7], reg8: Reg8::A, bit: 4},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE8], reg8: Reg8::B, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE9], reg8: Reg8::C, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xEA], reg8: Reg8::D, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xEB], reg8: Reg8::E, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xEC], reg8: Reg8::H, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xED], reg8: Reg8::L, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xEF], reg8: Reg8::A, bit: 5},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF0], reg8: Reg8::B, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF1], reg8: Reg8::C, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF2], reg8: Reg8::D, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF3], reg8: Reg8::E, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF4], reg8: Reg8::H, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF5], reg8: Reg8::L, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF7], reg8: Reg8::A, bit: 6},

        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF8], reg8: Reg8::B, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF9], reg8: Reg8::C, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xFA], reg8: Reg8::D, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xFB], reg8: Reg8::E, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xFC], reg8: Reg8::H, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xFD], reg8: Reg8::L, bit: 7},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xFF], reg8: Reg8::A, bit: 7},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();
        let datum = 0b00000000;
        let expected_result = 0b00000000 | (1 << test_data.bit);


        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, datum);
        cpu.flags = Flags::new(
            false,
            true,
            false,
            true
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, expected_result);
        expected_cpu.flags = Flags::new(
            false,
            true,
            false,
            true
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SET_HL_MEM() {
    for test_data in [
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xC6], reg8: Reg8::B, bit: 0},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xCE], reg8: Reg8::B, bit: 1},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xD6], reg8: Reg8::B, bit: 2},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xDE], reg8: Reg8::B, bit: 3},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xE6], reg8: Reg8::B, bit: 4},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xEE], reg8: Reg8::B, bit: 5},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xF6], reg8: Reg8::B, bit: 6},
        SingleBitReg8TestData { binary_instructions: [0xCB, 0xFE], reg8: Reg8::B, bit: 7},
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();
        let datum = 0b00000000;
        let expected_result = 0b00000000 | (1 << test_data.bit);

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(Reg16::HL, 0x9F9F);
        mmu.write(0x9F9F, datum);
        cpu.flags = Flags::new(
            true,
            false,
            true,
            false
        );

        let (mut expected_cpu, mut expected_mmu, _) = setup();
        expected_cpu.registers.write16(Reg16::HL, 0x9F9F);
        expected_mmu.write(0x9F9F, expected_result);
        expected_cpu.flags = Flags::new(
            true,
            false,
            true,
            false
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_RLCA() {
    let binary_instructions = [0x07];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b10000000);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000001);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RLCA_ZERO() {
    let binary_instructions = [0x07];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000000);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RRCA() {
    let binary_instructions = [0x0F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b10000000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RRCA_ZERO() {
    let binary_instructions = [0x0F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000000);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RLA() {

    let binary_instructions = [0x17];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01000010);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RLA_ZERO() {

    let binary_instructions = [0x17];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b10000000);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RRA() {
    let binary_instructions = [0x1F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b10100001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01010000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_RRA_ZERO() {
    let binary_instructions = [0x1F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000001);
    cpu.flags = Flags::new(
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}