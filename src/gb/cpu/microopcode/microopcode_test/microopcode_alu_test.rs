use crate::gb::cpu::flags::Flags;
use crate::gb::cpu::microopcode::MicroOpcode;
use crate::gb::cpu::microopcode::microopcode_test::microopcode_testing_helpers::*;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::memory::Memory;

#[test]
fn exec_ADD_REG16_REG16_HALF_CARRY() {
    for test_data in [
        Reg16Reg16TestData { binary_instructions: [0x09], to: Reg16::HL, from: Reg16::BC },
        Reg16Reg16TestData { binary_instructions: [0x19], to: Reg16::HL, from: Reg16::DE },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(test_data.to, 0b0000111111111101);
        cpu.registers.write16(test_data.from, 0b0000000000000011);
        cpu.flags = Flags::new(
            true,
            true,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write16(test_data.to, 0b0001000000000000);
        expected_cpu.registers.write16(test_data.from, 0b0000000000000011);
        expected_cpu.flags = Flags::new(
            true,
            false,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}


#[test]
fn exec_ADD_REG16_REG16_OVERFLOW() {
    for test_data in [
        Reg16Reg16TestData { binary_instructions: [0x09], to: Reg16::HL, from: Reg16::BC },
        Reg16Reg16TestData { binary_instructions: [0x19], to: Reg16::HL, from: Reg16::DE },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(test_data.to, 0b1111000000000000);
        cpu.registers.write16(test_data.from, 0b0001000000000000);
        cpu.flags = Flags::new(
            false,
            true,
            true,
            false,
        );


        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write16(test_data.to, 0b0000000000000000);
        expected_cpu.registers.write16(test_data.from, 0b0001000000000000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADD_REG16_REG16() {
    for test_data in [
        Reg16Reg16TestData { binary_instructions: [0x09], to: Reg16::HL, from: Reg16::BC },
        Reg16Reg16TestData { binary_instructions: [0x19], to: Reg16::HL, from: Reg16::DE },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(test_data.to, 0b0111000000000000);
        cpu.registers.write16(test_data.from, 0b0000000000000100);
        cpu.flags = Flags::new(
            false,
            true,
            true,
            false,
        );


        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write16(test_data.to, 0b0111000000000100);
        expected_cpu.registers.write16(test_data.from, 0b0000000000000100);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADD_SAME_REG16() {
    let binary_instructions = [0x29];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0b1111000000001111);
    cpu.flags = Flags::new(
        false,
        true,
        true,
        false,
    );


    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0b1110000000011110);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADD_HL_SP() {
    let binary_instructions = [0x39];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0b1111000000000000);
    cpu.stack_pointer = 0b1000000000000100;
    cpu.flags = Flags::new(
        false,
        true,
        true,
        false,
    );


    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0b0111000000000100);
    expected_cpu.stack_pointer = 0b1000000000000100;
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADD_SP_IMMEDIATE_SIGNED_POSITIVE() {
    let binary_instructions = [0xE8, 0x7D];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0xF109;
    cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.stack_pointer = 0xF186;
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADD_SP_IMMEDIATE_SIGNED_NEGATIVE() {
    let binary_instructions = [0xE8, 0xFD];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0xF109;
    cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let (mut expected_cpu,mut expected_mmu, _) = setup();
    expected_cpu.stack_pointer = 0xF106;
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        true,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADD_REG8_HALF_CARRY() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x80], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x81], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x82], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x83], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x84], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x85], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00001111);
        cpu.registers.write8(test_data.reg8, 0b00000001);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00010000);
        expected_cpu.registers.write8(test_data.reg8, 0b00000001);
        expected_cpu.flags = Flags::new(
            false,
            false,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADD_REG8_OVERFLOW() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x80], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x81], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x82], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x83], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x84], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x85], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b11111111);
        cpu.registers.write8(test_data.reg8, 0b00000001);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00000000);
        expected_cpu.registers.write8(test_data.reg8, 0b00000001);
        expected_cpu.flags = Flags::new(
            true,
            false,
            true,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADD_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x80], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x81], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x82], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x83], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x84], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x85], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00000001);
        cpu.registers.write8(test_data.reg8, 0b00000001);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00000010);
        expected_cpu.registers.write8(test_data.reg8, 0b00000001);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADD_REG8_HL_MEM() {
    let binary_instructions = [0x86];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00010001);
    cpu.registers.write16(Reg16::HL, 0xC100);
    mmu.write(0xC100, 0b00000110);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00010111);
    expected_cpu.registers.write16(Reg16::HL, 0xC100);
    expected_mmu.write(0xC100, 0b00000110);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADD_SAME_REG8() {
    let binary_instructions = [0x87];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000001);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000010);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADD_A_IMMEDIATE() {
    let binary_instructions = [0xC6, 0b00000110];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000001);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000111);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADC_REG8_HALF_CARRY() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x88], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x89], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x8A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x8B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x8C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x8D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00001111);
        cpu.registers.write8(test_data.reg8, 0b00000000);
        cpu.flags = Flags::new(
            true,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00010000);
        expected_cpu.registers.write8(test_data.reg8, 0b00000000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADC_REG8_OVERFLOW_NO_HALF_CARRY() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x88], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x89], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x8A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x8B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x8C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x8D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b11111110);
        cpu.registers.write8(test_data.reg8, 0b10000000);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b01111111);
        expected_cpu.registers.write8(test_data.reg8, 0b10000000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADC_REG8_OVERFLOW_WITH_HALF_CARRY() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x88], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x89], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x8A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x8B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x8C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x8D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b11111111);
        cpu.registers.write8(test_data.reg8, 0b10000000);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b10000000);
        expected_cpu.registers.write8(test_data.reg8, 0b10000000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            true,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADC_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x88], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x89], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x8A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x8B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x8C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x8D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00000001);
        cpu.registers.write8(test_data.reg8, 0b00000001);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00000011);
        expected_cpu.registers.write8(test_data.reg8, 0b00000001);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_ADC_A_HL_MEM() {
    let binary_instructions = [0x8E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00010001);
    cpu.registers.write16(Reg16::HL, 0xC100);
    mmu.write(0xC100, 0b00000110);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00011000);
    expected_cpu.registers.write16(Reg16::HL, 0xC100);
    expected_mmu.write(0xC100, 0b00000110);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADC_SAME_REG8() {
    let binary_instructions = [0x8F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000001);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000011);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_ADC_A_IMMEDIATE() {
    let binary_instructions = [0xCE, 0b00000010];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000001);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000100);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SUB_REG8_HALF_CARRY() {
    for test_data in [
        Reg8Reg8TestData { binary_instructions: [0x90], to: Reg8::A, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x91], to: Reg8::A, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x92], to: Reg8::A, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x93], to: Reg8::A, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x94], to: Reg8::A, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x95], to: Reg8::A, from: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.to, 0b10000001);
        cpu.registers.write8(test_data.from, 0b00001000);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.to, 0b01111001);
        expected_cpu.registers.write8(test_data.from, 0b00001000);
        expected_cpu.flags = Flags::new(
            false,
            true,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SUB_REG8_OVERFLOW() {
    for test_data in [
        Reg8Reg8TestData { binary_instructions: [0x90], to: Reg8::A, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x91], to: Reg8::A, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x92], to: Reg8::A, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x93], to: Reg8::A, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x94], to: Reg8::A, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x95], to: Reg8::A, from: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.to, 0b00011111);
        cpu.registers.write8(test_data.from, 0b00100000);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.to, 0b11111111);
        expected_cpu.registers.write8(test_data.from, 0b00100000);
        expected_cpu.flags = Flags::new(
            false,
            true,
            false,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SUB_REG8() {
    for test_data in [
        Reg8Reg8TestData { binary_instructions: [0x90], to: Reg8::A, from: Reg8::B },
        Reg8Reg8TestData { binary_instructions: [0x91], to: Reg8::A, from: Reg8::C },
        Reg8Reg8TestData { binary_instructions: [0x92], to: Reg8::A, from: Reg8::D },
        Reg8Reg8TestData { binary_instructions: [0x93], to: Reg8::A, from: Reg8::E },
        Reg8Reg8TestData { binary_instructions: [0x94], to: Reg8::A, from: Reg8::H },
        Reg8Reg8TestData { binary_instructions: [0x95], to: Reg8::A, from: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.to, 0b00000101);
        cpu.registers.write8(test_data.from, 0b00000010);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.to, 0b00000011);
        expected_cpu.registers.write8(test_data.from, 0b00000010);
        expected_cpu.flags = Flags::new(
            false,
            true,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SUB_A_HL_MEM() {
    let binary_instructions = [0x96];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b01001001);
    cpu.registers.write16(Reg16::HL, 0xC110);
    mmu.write(0xC110, 0b00000110);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01000011);
    expected_cpu.registers.write16(Reg16::HL, 0xC110);
    expected_mmu.write(0xC110, 0b00000110);
    expected_cpu.flags = Flags::new(
        false,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SUB_A_A() {
    let binary_instructions = [0x97];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SUB_A_IMMEDIATE() {
    let binary_instructions = [0xD6, 0b00011111];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SBC_REG8_HALF_CARRY() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x98], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x99], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x9A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x9B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x9C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x9D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b10000111);
        cpu.registers.write8(test_data.reg8, 0b00000111);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b01111111);
        expected_cpu.registers.write8(test_data.reg8, 0b00000111);
        expected_cpu.flags = Flags::new(
            false,
            true,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SBC_REG8_OVERFLOW() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x98], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x99], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x9A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x9B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x9C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x9D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b01111111);
        cpu.registers.write8(test_data.reg8, 0b01111111);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b11111111);
        expected_cpu.registers.write8(test_data.reg8, 0b01111111);
        expected_cpu.flags = Flags::new(
            false,
            true,
            true,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SBC_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x98], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x99], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x9A], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x9B], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x9C], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x9D], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00000011);
        cpu.registers.write8(test_data.reg8, 0b00000010);
        cpu.flags = Flags::new(
            false,
            false,
            false,
            true,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00000000);
        expected_cpu.registers.write8(test_data.reg8, 0b00000010);
        expected_cpu.flags = Flags::new(
            true,
            true,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_SBC_A_HL_MEM() {
    let binary_instructions = [0x9E];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000011);
    cpu.registers.write16(Reg16::HL, 0xC110);
    mmu.write(0xC110, 0b00000010);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000001);
    expected_cpu.registers.write16(Reg16::HL, 0xC110);
    expected_mmu.write(0xC110, 0b00000010);
    expected_cpu.flags = Flags::new(
        false,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SBC_A_A() {
    let binary_instructions = [0x9F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000111);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b11111111);
    expected_cpu.flags = Flags::new(
        false,
        true,
        true,
        true,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SBC_A_IMMEDIATE() {
    let binary_instructions = [0xDE, 0b00000110];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00000111);
    cpu.flags = Flags::new(
        false,
        false,
        false,
        true,
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);

}

#[test]
fn exec_AND_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0xA0], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0xA1], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0xA2], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0xA3], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0xA4], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0xA5], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00110101);
        cpu.registers.write8(test_data.reg8, 0b00100011);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00100001);
        expected_cpu.registers.write8(test_data.reg8, 0b00100011);
        expected_cpu.flags = Flags::new(
            false,
            false,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_AND_A_HL_MEM() {
    let binary_instructions = [0xA6];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b01001001);
    cpu.registers.write16(Reg16::HL, 0xC110);
    mmu.write(0xC110, 0b00000110);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.registers.write16(Reg16::HL, 0xC110);
    expected_mmu.write(0xC110, 0b00000110);
    expected_cpu.flags = Flags::new(
        true,
        false,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_AND_A_A() {
    let binary_instructions = [0xA7];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00011111);
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_AND_A_IMMEDIATE() {
    let binary_instructions = [0xE6, 0b11110101];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00010101);
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_XOR_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0xA8], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0xA9], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0xAA], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0xAB], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0xAC], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0xAD], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00110101);
        cpu.registers.write8(test_data.reg8, 0b00100011);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00010110);
        expected_cpu.registers.write8(test_data.reg8, 0b00100011);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_XOR_A_HL_MEM() {
    let binary_instructions = [0xAE];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b01001001);
    cpu.registers.write16(Reg16::HL, 0xC110);
    mmu.write(0xC110, 0b00000110);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01001111);
    expected_cpu.registers.write16(Reg16::HL, 0xC110);
    expected_mmu.write(0xC110, 0b00000110);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_XOR_A_A() {
    let binary_instructions = [0xAF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00000000);
    expected_cpu.flags = Flags::new(
        true,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_XOR_A_IMMEDIATE() {
    let binary_instructions = [0xEE, 0b10110101];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b10101010);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_OR_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0xB0], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0xB1], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0xB2], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0xB3], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0xB4], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0xB5], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00110101);
        cpu.registers.write8(test_data.reg8, 0b00100011);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00110111);
        expected_cpu.registers.write8(test_data.reg8, 0b00100011);
        expected_cpu.flags = Flags::new(
            false,
            false,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_OR_A_HL_MEM() {
    let binary_instructions = [0xB6];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b01001001);
    cpu.registers.write16(Reg16::HL, 0xC110);
    mmu.write(0xC110, 0b00000111);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01001111);
    expected_cpu.registers.write16(Reg16::HL, 0xC110);
    expected_mmu.write(0xC110, 0b00000111);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_OR_A_A() {
    let binary_instructions = [0xB7];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00011111);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_OR_A_IMMEDIATE() {
    let binary_instructions = [0xF6, 0b11110000];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b11111111);
    expected_cpu.flags = Flags::new(
        false,
        false,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CP_REG8_HALF_CARRY() {
    for test_data in [
        Reg8TestData { binary_instructions: [0xB8], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0xB9], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0xBA], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0xBB], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0xBC], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0xBD], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b10000001);
        cpu.registers.write8(test_data.reg8, 0b00001000);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b10000001);
        expected_cpu.registers.write8(test_data.reg8, 0b00001000);
        expected_cpu.flags = Flags::new(
            false,
            true,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_CP_REG8_OVERFLOW() {
    for test_data in [
        Reg8TestData { binary_instructions: [0xB8], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0xB9], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0xBA], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0xBB], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0xBC], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0xBD], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00011111);
        cpu.registers.write8(test_data.reg8, 0b00100000);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00011111);
        expected_cpu.registers.write8(test_data.reg8, 0b00100000);
        expected_cpu.flags = Flags::new(
            false,
            true,
            false,
            true,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes { micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache) };

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_CP_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0xB8], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0xB9], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0xBA], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0xBB], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0xBC], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0xBD], reg8: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(Reg8::A, 0b00000101);
        cpu.registers.write8(test_data.reg8, 0b00000010);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(Reg8::A, 0b00000101);
        expected_cpu.registers.write8(test_data.reg8, 0b00000010);
        expected_cpu.flags = Flags::new(
            false,
            true,
            false,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_CP_A_HL_MEM() {
    let binary_instructions = [0xBE];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b01001001);
    cpu.registers.write16(Reg16::HL, 0xC110);
    mmu.write(0xC110, 0b00000110);

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01001001);
    expected_cpu.registers.write16(Reg16::HL, 0xC110);
    expected_mmu.write(0xC110, 0b00000110);
    expected_cpu.flags = Flags::new(
        false,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CP_A_A() {
    let binary_instructions = [0xBF];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011111);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00011111);
    expected_cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CP_A_IMMEDIATE() {
    let binary_instructions = [0xFE, 0b00001111];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b00011110);

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b00011110);
    expected_cpu.flags = Flags::new(
        false,
        true,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_INC_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x04], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x0C], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x14], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x1C], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x24], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x2C], reg8: Reg8::L },
        Reg8TestData { binary_instructions: [0x3C], reg8: Reg8::A },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b00001111);
        cpu.flags = Flags::new(
            true,
            true,
            false,
            false,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b00010000);
        expected_cpu.flags = Flags::new(
            false,
            false,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_INC_HL_MEM() {
    let binary_instructions = [0x34];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0xC115);
    mmu.write(0xC115, 0b00001111);
    cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xC115);
    expected_mmu.write(0xC115, 0b00010000);
    expected_cpu.flags = Flags::new(
        false,
        false,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_DEC_REG8() {
    for test_data in [
        Reg8TestData { binary_instructions: [0x05], reg8: Reg8::B },
        Reg8TestData { binary_instructions: [0x0D], reg8: Reg8::C },
        Reg8TestData { binary_instructions: [0x15], reg8: Reg8::D },
        Reg8TestData { binary_instructions: [0x1D], reg8: Reg8::E },
        Reg8TestData { binary_instructions: [0x25], reg8: Reg8::H },
        Reg8TestData { binary_instructions: [0x2D], reg8: Reg8::L },
        Reg8TestData { binary_instructions: [0x3D], reg8: Reg8::A },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write8(test_data.reg8, 0b00010000);
        cpu.flags = Flags::new(
            true,
            false,
            false,
            false,
        );

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write8(test_data.reg8, 0b00001111);
        expected_cpu.flags = Flags::new(
            false,
            true,
            true,
            false,
        );

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_DEC_HL_MEM() {
    let binary_instructions = [0x35];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write16(Reg16::HL, 0xC115);
    mmu.write(0xC115, 0b00010000);
    cpu.flags = Flags::new(
        true,
        true,
        false,
        false,
    );

    let (mut expected_cpu, mut expected_mmu, _) = setup();
    expected_cpu.registers.write16(Reg16::HL, 0xC115);
    expected_mmu.write(0xC115, 0b00001111);
    expected_cpu.flags = Flags::new(
        false,
        true,
        true,
        false,
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_INC_REG16() {
    for test_data in [
        Reg16TestData { binary_instructions: [0x03], reg16: Reg16::BC, msb: Reg8::B, lsb: Reg8::C },
        Reg16TestData { binary_instructions: [0x13], reg16: Reg16::DE, msb: Reg8::D, lsb: Reg8::E },
        Reg16TestData { binary_instructions: [0x23], reg16: Reg16::HL, msb: Reg8::H, lsb: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(test_data.reg16, 0b0000111100001101);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write16(test_data.reg16, 0b0000111100001110);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_INC_SP() {
    let binary_instructions = vec![0x33];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0b0000111100001101;

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.stack_pointer = 0b0000111100001110;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_DEC_REG16() {
    for test_data in [
        Reg16TestData { binary_instructions: [0x0B], reg16: Reg16::BC, msb: Reg8::B, lsb: Reg8::C },
        Reg16TestData { binary_instructions: [0x1B], reg16: Reg16::DE, msb: Reg8::D, lsb: Reg8::E },
        Reg16TestData { binary_instructions: [0x2B], reg16: Reg16::HL, msb: Reg8::H, lsb: Reg8::L },
    ] {
        let mut memory_iterator = test_data.binary_instructions.into_iter();

        let (mut cpu, mut mmu, mut cache) = setup();
        cpu.registers.write16(test_data.reg16, 0b0000111100001101);

        let (mut expected_cpu, expected_mmu, _) = setup();
        expected_cpu.registers.write16(test_data.reg16, 0b0000111100001100);

        let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
        for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

        assert_eq!(memory_iterator.next(), None);
        assert_eq!(cpu, expected_cpu);
        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn exec_DEC_SP() {
    let binary_instructions = vec![0x3B];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.stack_pointer = 0b0000111100001101;

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.stack_pointer = 0b0000111100001100;

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_DAA_no_carry() {
    let binary_instructions = vec![0x27];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0xC0);
    cpu.flags = Flags::new (
        false,
        false,
        false,
        false
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x20);
    expected_cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_DAA_with_carry() {
    let binary_instructions = vec![0x27];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x20);
    cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x80);
    expected_cpu.flags = Flags::new (
        false,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_DAA_subtraction() {
    let binary_instructions = vec![0x27];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0x2F);
    cpu.flags = Flags::new (
        false,
        true,
        true,
        false
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0x29);
    expected_cpu.flags = Flags::new (
        false,
        true,
        false,
        false
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_SCF() {
    let binary_instructions = vec![0x37];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.flags = Flags::new (
        true,
        false,
        false,
        true
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}


#[test]
fn exec_CPL() {
    let binary_instructions = vec![0x2F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.registers.write8(Reg8::A, 0b10101100);
    cpu.flags = Flags::new (
        true,
        false,
        false,
        false
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.registers.write8(Reg8::A, 0b01010011);
    expected_cpu.flags = Flags::new (
        true,
        true,
        true,
        false
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn exec_CCF() {
    let binary_instructions = vec![0x3F];
    let mut memory_iterator = binary_instructions.into_iter();

    let (mut cpu, mut mmu, mut cache) = setup();
    cpu.flags = Flags::new (
        false,
        true,
        true,
        true
    );

    let (mut expected_cpu, expected_mmu, _) = setup();
    expected_cpu.flags = Flags::new (
        false,
        false,
        false,
        false
    );

    let mut micro_opcodes = MicroOpcode::parse(&mut memory_iterator);
    for micro_opcode in micro_opcodes {micro_opcode.instruction(&mut cpu, &mut mmu, &mut cache)};

    assert_eq!(memory_iterator.next(), None);
    assert_eq!(cpu, expected_cpu);
    assert_eq!(mmu, expected_mmu);
}