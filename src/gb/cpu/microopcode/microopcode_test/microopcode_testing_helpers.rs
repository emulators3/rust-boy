use crate::gb::cartridge::Cartridge;
use crate::gb::cpu::cache::Cache;
use crate::gb::cpu::Cpu;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::mmu::Mmu;


#[derive(Debug)]
pub struct Reg8TestData {
    pub binary_instructions: [u8;1],
    pub reg8: Reg8,
}

#[derive(Debug)]
pub struct TwoBytesReg8TestData {
    pub binary_instructions: [u8;2],
    pub reg8: Reg8,
}

#[derive(Debug)]
pub struct SingleBitReg8TestData {
    pub binary_instructions: [u8;2],
    pub reg8: Reg8,
    pub bit: u8,
}

#[derive(Debug)]
pub struct SingleBitHlMemTestData {
    pub binary_instructions: [u8;2],
    pub bit: u8,
}

#[derive(Debug)]
pub struct Reg8Reg8TestData {
    pub binary_instructions: [u8;1],
    pub to: Reg8,
    pub from: Reg8,
}

#[derive(Debug)]
pub struct Reg16TestData {
    pub binary_instructions: [u8;1],
    pub reg16: Reg16,
    pub msb: Reg8,
    pub lsb: Reg8,
}

#[derive(Debug)]
pub struct Reg16ImmediateTestData {
    pub binary_instructions: [u8;3],
    pub reg16: Reg16,
}

#[derive(Debug)]
pub struct Reg16Reg8TestData {
    pub binary_instructions: [u8;1],
    pub reg16: Reg16,
    pub reg8: Reg8
}

#[derive(Debug)]
pub struct Reg16Reg16TestData {
    pub binary_instructions: [u8;1],
    pub to: Reg16,
    pub from: Reg16
}


pub fn setup() -> (Cpu, Mmu, Cache) {
    let cartridge = Cartridge::from_binary_rom([0u8;0x8000].to_vec(), None);
    let cpu = Cpu::new(&cartridge);
    let mmu = Mmu::new(cartridge);
    let cache = Cache::new();
    (cpu, mmu, cache)
}