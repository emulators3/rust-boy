use crate::gb::cpu::flags::{Flag, Flags};
use crate::gb::cpu::microopcode::{BitOperation, SingleBitOperation, SpecificBitOperationForA};
use crate::gb::types::GbByte;
use crate::utils::bitutils::{get_bit, higher_half, highest_bit, join_nibbles, lower_half, lowest_bit, reset_bit, set_bit};

pub fn specific_bit_operation_for_a(operation: SpecificBitOperationForA, datum: GbByte, flags: Flags) -> (GbByte, Flags) {
    match operation {
        SpecificBitOperationForA::Rlca => rlca(datum),
        SpecificBitOperationForA::Rrca => rrca(datum),
        SpecificBitOperationForA::Rla => rla(datum, flags),
        SpecificBitOperationForA::Rra => rra(datum, flags),
    }
}

pub fn bit_operation(operation: BitOperation, datum: GbByte, flags: Flags) -> (GbByte, Flags) {
    match operation {
        BitOperation::Rlc => rlc(datum),
        BitOperation::Rrc => rrc(datum),
        BitOperation::Rl => rl(datum, flags),
        BitOperation::Rr => rr(datum, flags),
        BitOperation::Sla => sla(datum),
        BitOperation::Sra => sra(datum),
        BitOperation::Swap => swap(datum),
        BitOperation::Srl => srl(datum)
    }
}

pub fn single_bit_operation(operation: SingleBitOperation, datum: GbByte, bit_to_operate: u8, flags: Flags) -> (GbByte, Flags) {
    match operation {
        SingleBitOperation::Bit => bit(datum, bit_to_operate, flags),
        SingleBitOperation::Res => res(datum, bit_to_operate, flags),
        SingleBitOperation::Set => set(datum, bit_to_operate, flags),
    }
}

fn rlca(datum: GbByte) -> (GbByte, Flags) {
    let(result, flags) = rlc(datum);

    let rlca_flags = Flags::new(
        false,
        false,
        false,
        flags.read(Flag::C),
    );
    (result, rlca_flags)
}

fn rrca(datum: GbByte) -> (GbByte, Flags) {
    let(result, flags) = rrc(datum);

    let rrca_flags = Flags::new(
        false,
        false,
        false,
        flags.read(Flag::C),
    );
    (result, rrca_flags)
}

fn rla(datum: GbByte, old_flags: Flags) -> (GbByte, Flags) {
    let(result, partial_flags) = rl(datum, old_flags);

    let rla_flags = Flags::new(
        false,
        false,
        false,
        partial_flags.read(Flag::C),
    );
    (result, rla_flags)
}

fn rra(datum: GbByte, old_flags: Flags) -> (GbByte, Flags) {
    let(result, partial_flags) = rr(datum, old_flags);

    let rra_flags = Flags::new(
        false,
        false,
        false,
        partial_flags.read(Flag::C),
    );
    (result, rra_flags)
}

fn rlc(datum: GbByte) -> (GbByte, Flags) {
    let carry = highest_bit(datum);
    let result = datum.rotate_left(1);

    let flags = Flags::new (
        result == 0,
        false,
        false,
        carry == 1,
    );
    (result, flags)
}

fn rrc(datum: GbByte) -> (GbByte, Flags) {
    let carry = lowest_bit(datum);
    let result = datum.rotate_right(1);

    let flags = Flags::new (
        result == 0,
        false,
        false,
        carry == 1,
    );
    (result, flags)
}

fn rl(datum: GbByte, flags: Flags) -> (GbByte, Flags) {
    let old_carry = flags.read(Flag::C) as u8;
    let new_carry = highest_bit(datum);
    let result = (datum << 1) | old_carry;

    let flags = Flags::new (
        result == 0,
        false,
        false,
        new_carry == 1,
    );
    (result, flags)
}

fn rr(datum: GbByte, flags: Flags) -> (GbByte, Flags) {
    let old_carry = flags.read(Flag::C) as u8;
    let new_carry = lowest_bit(datum);
    let result = (datum >> 1) | ( old_carry << 7);

    let flags = Flags::new (
        result == 0,
        false,
        false,
        new_carry == 1,
    );
    (result, flags)
}

fn sla(datum: GbByte) -> (GbByte, Flags) {
    let carry = highest_bit(datum);
    let result = datum << 1;

    let flags = Flags::new (
        result == 0,
        false,
        false,
        carry == 1,
    );
    (result, flags)
}

fn sra(datum: GbByte) -> (GbByte, Flags) {
    let old_msb = highest_bit(datum);
    let carry = lowest_bit(datum);
    let result = (datum >> 1) | (old_msb << 7);

    let flags = Flags::new (
        result == 0,
        false,
        false,
        carry == 1,
    );
    (result, flags)
}

fn swap(datum: GbByte) -> (GbByte, Flags) {
    let lower_half = lower_half(datum);
    let higher_half = higher_half(datum);
    let result = join_nibbles(lower_half, higher_half);

    let flags = Flags::new (
        result == 0,
        false,
        false,
        false,
    );
    (result, flags)
}

fn srl(datum: GbByte) -> (GbByte, Flags) {
    let carry = lowest_bit(datum);
    let result = datum >> 1;

    let flags = Flags::new (
        result == 0,
        false,
        false,
        carry == 1,
    );
    (result, flags)
}

fn bit(datum: GbByte, bit: u8, flags: Flags) -> (GbByte, Flags) {
    let bit = get_bit(datum, bit);
    let flags = Flags::new (
        bit == 0,
        false,
        true,
        flags.read(Flag::C),
    );
    (datum, flags)
}

fn res(datum: GbByte, bit: u8, flags: Flags) -> (GbByte, Flags) {
    let result = reset_bit(datum, bit);
    (result, flags)
}

fn set(datum: GbByte, bit: u8, flags: Flags) -> (GbByte, Flags) {
    let result = set_bit(datum, bit);
    (result, flags)
}