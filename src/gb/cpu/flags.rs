use crate::gb::types::{
    GbFlag,
};

#[derive(Debug, Copy, Clone)]
pub enum Flag {
    Z, N,
    H, C,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Flags {
    zero:       GbFlag,
    subtract:   GbFlag,
    half_carry: GbFlag,
    carry:      GbFlag,
}

// XXX: bit math should be correct but it's better to check
impl Flags {
    pub fn init() -> Self {
        Self {
            zero:       true,
            subtract:   false,
            half_carry: true,
            carry:      true,
        }
    }

    pub fn new(zero: GbFlag, subtract: GbFlag, half_carry: GbFlag, carry: GbFlag) -> Self {
        Self { zero, subtract, half_carry, carry }
    }

    pub fn read(&self, flag: Flag) -> GbFlag {
        use Flag::*;
        match flag {
            Z => self.zero,
            N => self.subtract,
            H => self.half_carry,
            C => self.carry,
        }
    }
    
    pub fn as_byte(&self) -> u8 {
          (bit(self.zero)       << 7)
        | (bit(self.subtract)   << 6)
        | (bit(self.half_carry) << 5)
        | (bit(self.carry)      << 4)
    }
    
    pub fn from_byte(b: u8) -> Self {
        Self {
            zero:       (b & 0b1000_0000) >> 7 == 1,
            subtract:   (b & 0b0100_0000) >> 6 == 1,
            half_carry: (b & 0b0010_0000) >> 5 == 1,
            carry:      (b & 0b0001_0000) >> 4 == 1,
        }
    }
}

fn bit(b: bool) -> u8 {
    u8::from(b)
}
