pub mod io_registers;
pub mod video_memory;
pub mod div_register;
pub mod p1_register;
pub mod audio_registers;

use crate::gb::memory::div_register::DivRegister;
use crate::gb::memory::p1_register::P1Register;
use crate::gb::memory::video_memory::VideoMemory;
use crate::gb::types::{GbAddr, GbByte, GbFlag};

pub trait Memory: VideoMemory + DivRegister + P1Register {
    fn read(&self, address: GbAddr) -> GbByte;
    fn write(&mut self, address: GbAddr, datum: GbByte);
    fn iter(&self, address: GbAddr, halt_bug: bool) -> MemoryIterator<Self>;
}

// Iter
pub struct MemoryIterator<'a, M: ?Sized> {
    pub memory: &'a M,
    pub address: GbAddr,
    pub halt_bug: GbFlag
}

impl<M: Memory> Iterator for MemoryIterator<'_, M> {
    type Item = GbByte;
    
    fn next(&mut self) -> Option<Self::Item> {
        let item = self.memory.read(self.address);

        //this happens because of the halt bug, and I need to read the same byte two times
        if self.halt_bug {
            self.halt_bug = false
        } else {
            self.address += 1;
        }

        Some(item)
    }
}
