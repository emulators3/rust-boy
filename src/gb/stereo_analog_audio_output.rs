use crate::utils::constants::AnalogAudioOutput;

#[derive(Debug, Copy, Clone)]
pub struct StereoAnalogAudioOutput {
    pub left_output: AnalogAudioOutput,
    pub right_output: AnalogAudioOutput
}

impl StereoAnalogAudioOutput {
    pub fn mute() -> Self {
        Self {
            left_output: 0,
            right_output: 0
        }
    }
}
