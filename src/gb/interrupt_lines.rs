use crate::gb::interrupt::Interrupt;
use crate::gb::memory::io_registers::{IoReg, IoRegistersInternal};
use crate::utils::bitutils::set_bit;

#[derive(Debug, Copy, Clone)]
pub struct InterruptLines {
    vblank:  LineState,
    lcdstat: LineState,
    timer:   LineState,
    serial:  LineState,
    joypad:  LineState,
}

#[derive(Debug, Copy, Clone)]
enum LineState {
    Low, High
}

impl InterruptLines {

    pub fn dump(&mut self) {
        println!("\tSTAT_IRQ: {:?}", self.lcdstat);
    }

    pub fn new() -> Self {
        Self {
            vblank:  LineState::Low,
            lcdstat: LineState::Low,
            timer:   LineState::Low,
            serial:  LineState::Low,
            joypad:  LineState::Low,
        }
    }

    fn get(&self, interrupt: Interrupt) -> LineState {
        match interrupt {
            Interrupt::VBlank  => self.vblank,
            Interrupt::LcdStat => self.lcdstat,
            Interrupt::Timer   => self.timer,
            Interrupt::Serial  => self.serial,
            Interrupt::Joypad  => self.joypad
        }
    }

    pub fn set_low(&mut self, interrupt: Interrupt) {
        use Interrupt::*;

        match interrupt {
            VBlank  => self.vblank  = LineState::Low,
            LcdStat => self.lcdstat = LineState::Low,
            Timer   => self.timer   = LineState::Low,
            Serial  => self.serial  = LineState::Low,
            Joypad  => self.joypad  = LineState::Low,
        };
    }

    pub fn set_high(&mut self, interrupt: Interrupt, io_regs: &mut impl IoRegistersInternal) {
        use Interrupt::*;
        let old_state = self.get(interrupt);

        match interrupt {
            VBlank  => self.vblank  = LineState::High,
            LcdStat => self.lcdstat = LineState::High,
            Timer   => self.timer   = LineState::High,
            Serial  => self.serial  = LineState::High,
            Joypad  => self.joypad  = LineState::High,
        };

        if let LineState::Low = old_state {
            Self::request_interrupt(io_regs, interrupt)
        }
    }

    fn request_interrupt(io_regs: &mut impl IoRegistersInternal, interrupt: Interrupt) {
        let interrupt_requests = io_regs.read_io_internal(IoReg::IF);

        // set the corresponding bit of the interrupt requested to be handled
        io_regs.write_io_internal(IoReg::IF, set_bit(interrupt_requests, interrupt.index()));
    }

}
