use crate::gb::cartridge::Cartridge;
use crate::gb::memory::div_register::DivRegister;
use crate::gb::memory::io_registers::{IoReg, IoRegistersInternal};
use crate::gb::mmu::Mmu;
use crate::gb::timer::Timer;

/*
    Timer itself generally changes just the IO registers. The only time when we need to check
    the timer itself, is when interrupt is requested

    //TODO re-do completely - commented out as timer has gone a huge refactoring
*/

#[test]
fn tick_should_change_nothing_when_starting_from_0() {
    let (mut timer, mut mmu) = setup();

    timer.tick(&mut mmu);

    let (mut expected_timer, mut expected_mmu) = setup();
    expected_timer.counter = 1;

    assert_eq!(timer, expected_timer);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn tick_should_increase_div_when_counter_reaches_256() {
    let (mut timer, mut mmu) = setup();
    timer.counter=255;
    timer.tick(&mut mmu);

    let (mut expected_timer, mut expected_mmu) = setup();
    expected_timer.counter = 256;
    expected_mmu.increment_div();

    assert_eq!(timer, expected_timer);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn tick_should_not_increase_div_when_counter_does_not_reach_256() {
    let (mut timer, mut mmu) = setup();
    timer.counter=254;
    timer.tick(&mut mmu);

    let (mut expected_timer, mut expected_mmu) = setup();
    expected_timer.counter = 255;

    assert_eq!(timer, expected_timer);
    assert_eq!(mmu, expected_mmu);
}

#[test]
fn tick_should_increase_tima_when_counter_reaches_selected_tac_frequency() {
    for test_data in [
        TacTestData { counter: 4095, tac: 0b00000100 },
        TacTestData { counter: 63, tac: 0b00000101 },
        TacTestData { counter: 255, tac: 0b00000110 },
        TacTestData { counter: 1023, tac: 0b00000111 },
    ] {

        let (mut timer, mut mmu) = setup();
        timer.counter = test_data.counter;
        timer.tac = test_data.tac;

        mmu.write_io_internal(IoReg::TAC, test_data.tac);
        mmu.write_io_internal(IoReg::TIMA, 5);

        timer.tick(&mut mmu);

        assert_eq!(mmu.read_io_internal(IoReg::TIMA), 6);
    }
}

// #[test]
// fn tick_should_increase_tima_when_tac_changes_and_edge_detected() {
//     for test_data in [
//         TacChangeTestData { counter: 0b0000000000001000, old_tac: 0b00000101, new_tac: 0b00000100 },
//         TacChangeTestData { counter: 0b0000000000100000, old_tac: 0b00000110, new_tac: 0b00000111 },
//         TacChangeTestData { counter: 0b0000000010000000, old_tac: 0b00000111, new_tac: 0b00000110 },
//         TacChangeTestData { counter: 0b0000001010000000, old_tac: 0b00000100, new_tac: 0b00000110 },
//     ] {
//
//         let (mut timer, mut mmu) = setup();
//         timer.counter = test_data.counter;
//         timer.tac = test_data.old_tac;
//
//         mmu.write_io(IoReg::TAC, test_data.new_tac);
//         mmu.write_io(IoReg::TIMA, 5);
//
//         timer.tick(&mut mmu);
//
//         assert_eq!(mmu.read_io(IoReg::TIMA), 6);
//     }
// }

// #[test]
// fn tick_should_increase_tima_when_output_is_1_and_timer_is_disabled() {
//     for test_data in [
//         TacChangeTestData { counter: 0b0000000000001000, old_tac: 0b00000101, new_tac: 0b00000001 },
//         TacChangeTestData { counter: 0b0000000000100000, old_tac: 0b00000110, new_tac: 0b00000010 },
//         TacChangeTestData { counter: 0b0000000010000000, old_tac: 0b00000111, new_tac: 0b00000011 },
//         TacChangeTestData { counter: 0b0000001010000000, old_tac: 0b00000100, new_tac: 0b00000000 },
//     ] {
//
//         let (mut timer, mut mmu) = setup();
//         timer.counter = test_data.counter;
//         timer.tac = test_data.old_tac;
//
//         mmu.write_io(IoReg::TAC, test_data.new_tac);
//         mmu.write_io(IoReg::TIMA, 5);
//
//         timer.tick(&mut mmu);
//
//         assert_eq!(mmu.read_io(IoReg::TIMA), 6);
//     }
// }

#[test]
fn tick_should_not_increase_tima_when_tac_changes_but_timer_was_disabled() {
    for test_data in [
        TacChangeTestData { counter: 0b0000000000001000, old_tac: 0b00000001, new_tac: 0b00000100 },
        TacChangeTestData { counter: 0b0000000000100000, old_tac: 0b00000010, new_tac: 0b00000111 },
        TacChangeTestData { counter: 0b0000000010000000, old_tac: 0b00000011, new_tac: 0b00000110 },
        TacChangeTestData { counter: 0b0000001010000000, old_tac: 0b00000000, new_tac: 0b00000110 },
    ] {

        let (mut timer, mut mmu) = setup();
        timer.counter = test_data.counter;
        timer.tac = test_data.old_tac;

        mmu.write_io_internal(IoReg::TAC, test_data.new_tac);
        mmu.write_io_internal(IoReg::TIMA, 5);

        timer.tick(&mut mmu);

        assert_eq!(mmu.read_io_internal(IoReg::TIMA), 5);
    }
}

#[test]
fn tick_should_not_increase_tima_when_tac_changes_but_output_stays_1() {
    for test_data in [
        TacChangeTestData { counter: 0b0000001000001000, old_tac: 0b00000101, new_tac: 0b00000100 },
        TacChangeTestData { counter: 0b0000000000101000, old_tac: 0b00000110, new_tac: 0b00000101 },
        TacChangeTestData { counter: 0b0000000010100000, old_tac: 0b00000111, new_tac: 0b00000110 },
        TacChangeTestData { counter: 0b0000001000100000, old_tac: 0b00000100, new_tac: 0b00000110 },
    ] {

        let (mut timer, mut mmu) = setup();
        timer.counter = test_data.counter;
        timer.tac = test_data.old_tac;

        mmu.write_io_internal(IoReg::TAC, test_data.new_tac);
        mmu.write_io_internal(IoReg::TIMA, 5);

        timer.tick(&mut mmu);

        assert_eq!(mmu.read_io_internal(IoReg::TIMA), 5);
    }
}

// #[test]
// fn tick_should_request_interrupt_and_reset_tima_if_enough_cycles_passed_from_tima_overflow() {
//     let counter = 4095;
//     let tac = 0b11111100;
//     let (mut timer, mut mmu) = setup();
//     timer.counter = counter;
//     timer.tac = tac;
//
//     mmu.write_io(IoReg::TAC, tac);
//     mmu.write_io(IoReg::TIMA, 0xFF);
//     mmu.write_io(IoReg::TMA, 0xFE);
//
//     timer.tick(&mut mmu); //overflow, TIMA is 00
//
//     timer.tick(&mut mmu); //-1
//     timer.tick(&mut mmu); //-2
//     timer.tick(&mut mmu); //-3
//     timer.tick(&mut mmu); //-4
//
//     let expected_timer = Timer {
//         counter: 4100,
//         tac: tac,
//         timer_interrupt_cycles_wait_time: 0,
//         timer_overflow_happened: false,
//         timer_interrupt_request: true,
//         timer_overflow_last_tima_value: 0,
//     };
//
//     assert_eq!(mmu.read_io(IoReg::TIMA), 0xFE);
//     assert_eq!(timer, expected_timer);
// }

// #[test]
// fn tick_should_reset_interrupt_request_after_1_cycle() {
//     let counter = 4095;
//     let tac = 0b11111100;
//     let (mut timer, mut mmu) = setup();
//     timer.counter = counter;
//     timer.tac = tac;
//
//     mmu.write_io(IoReg::TAC, tac);
//     mmu.write_io(IoReg::TIMA, 0xFF);
//     mmu.write_io(IoReg::TMA, 0xFE);
//
//     timer.tick(&mut mmu); //overflow, TIMA is 00
//
//     timer.tick(&mut mmu); //-1
//     timer.tick(&mut mmu); //-2
//     timer.tick(&mut mmu); //-3
//     timer.tick(&mut mmu); //-4
//     timer.tick(&mut mmu); //-5
//
//     let expected_timer = Timer {
//         counter: 4101,
//         tac: tac,
//         timer_interrupt_cycles_wait_time: 0,
//         timer_overflow_happened: false,
//         timer_overflow_last_tima_value: 0,
//         timer_interrupt_request: false,
//     };
//
//     assert_eq!(mmu.read_io(IoReg::TIMA), 0xFE);
//     assert_eq!(timer, expected_timer);
// }

// #[test]
// fn tick_should_not_request_interrupt_and_reset_tima_if_not_enough_cycles_passed_from_tima_overflow() {
//     let counter = 4095;
//     let tac = 0b11111100;
//     let (mut timer, mut mmu) = setup();
//     timer.counter = counter;
//     timer.tac = tac;
//
//     mmu.write_io(IoReg::TAC, tac);
//     mmu.write_io(IoReg::TIMA, 0xFF);
//     mmu.write_io(IoReg::TMA, 0xFE);
//
//     timer.tick(&mut mmu); //overflow, TIMA is 00
//
//     timer.tick(&mut mmu); //-1
//     timer.tick(&mut mmu); //-2
//
//     let expected_timer = Timer {
//         counter: 4098,
//         tac: tac,
//         timer_interrupt_cycles_wait_time: 1,
//         timer_overflow_happened: true,
//         timer_interrupt_request: false,
//         timer_overflow_last_tima_value: 0,
//     };
//
//     assert_eq!(mmu.read_io(IoReg::TIMA), 00);
//     assert_eq!(timer, expected_timer);
// }

// #[test]
// fn tick_should_manage_div_resetting_and_tima_increase_for_edge_check() {
//     for test_data in [
//         TacTestData { counter: 4095, tac: 0b00000100 },
//         TacTestData { counter: 63, tac: 0b00000101 },
//         TacTestData { counter: 255, tac: 0b00000110 },
//         TacTestData { counter: 1023, tac: 0b00000111 },
//     ] {
//
//         let (mut timer, mut mmu) = setup();
//         timer.counter = test_data.counter;
//         timer.tac = test_data.tac;
//
//         mmu.write_io(IoReg::TAC, test_data.tac);
//         mmu.write_io(IoReg::TIMA, 5);
//         mmu.set_div_reset_requested(true);
//
//         timer.tick(&mut mmu);
//
//         let expected_timer = Timer {
//             counter: 0,
//             tac: test_data.tac,
//             timer_interrupt_cycles_wait_time: 0,
//             timer_overflow_happened: false,
//             timer_interrupt_request: false,
//             timer_overflow_last_tima_value: 0,
//         };
//
//         assert_eq!(mmu.read_io(IoReg::TIMA), 6);
//         assert_eq!(mmu.is_div_reset_requested(), false);
//         assert_eq!(timer, expected_timer);
//     }
// }


#[derive(Debug)]
pub struct TacTestData {
    pub counter: u16,
    pub tac: u8
}

#[derive(Debug)]
pub struct TacChangeTestData {
    pub counter: u16,
    pub old_tac: u8,
    pub new_tac: u8
}

pub fn setup() -> (Timer, Mmu) {
    let cartridge = Cartridge::from_binary_rom([0u8;0x8000].to_vec(), None);

    (Timer::new(), Mmu::new(cartridge))
}