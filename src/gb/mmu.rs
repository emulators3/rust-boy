mod signals;

use crate::gb::cartridge::Cartridge;
use crate::gb::memory::{Memory, MemoryIterator};
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, Envelope, MasterVolume, NoiseChannelRandomness, SoundPanning, SquareChannelNumber, Sweep, WaveChannelVolume, WaveForm};
use crate::gb::memory::div_register::DivRegister;
use crate::gb::memory::io_registers::{IoReg, IoRegistersInternal};
use crate::gb::memory::p1_register::P1Register;
use crate::gb::memory::video_memory::lcd_control_flags::{LcdControlFlags, SpriteSize, TileSelected};
use crate::gb::memory::video_memory::lcd_status_flags::LcdStatusFlags;
use crate::gb::memory::video_memory::pixel_color_palette::PixelColorPalette;
use crate::gb::memory::video_memory::sprite_attributes::SpriteAttributes;
use crate::gb::memory::video_memory::VideoMemory;
use crate::gb::mmu::signals::{Signals, WaveSampleSignal};
use crate::gb::types::{GbByte, GbAddr, RamBank};
use crate::utils::bitutils::{higher_byte, higher_half, highest_bit, highest_five_bits, highest_two_bits, join_bytes, join_nibbles, lower_byte, lower_half, lower_six_bits, lower_three_bits, lower_two_bits, test_bit, write_bit};
use crate::utils::constants::{Bit, DigitalAudioOutput};

const DMA_ADDRESS_IN_IO_REGISTERS: u16 = 0x0046;
const SC_ADDRESS_IN_IO_REGISTERS: u16 = 0x0002;
const TAC_ADDRESS_IN_IO_REGISTERS: u16 = 0x0007;
const IF_ADDRESS_IN_IO_REGISTERS: u16 = 0x000F;

const SPRITE_RAM_BEGINNING_ADDRESS: usize = 0xFE00;

const INTERNAL_RAM_SIZE: usize = 0xE000 - 0xC000;
const SPRITE_RAM_SIZE:   usize = 0xFEA0 - SPRITE_RAM_BEGINNING_ADDRESS;
const TOP_RAM_SIZE:      usize = 0xFFFF - 0xFF80;
const VIDEO_RAM_SIZE:    usize = 0xA000 - 0x8000;
const IO_REGISTERS_SIZE: usize = 0xFF80 - 0xFF00;

#[derive(Debug, PartialEq)]
pub struct Mmu {
    video_ram:[GbByte; VIDEO_RAM_SIZE],    // [0x8000, 0xA000)
    internal_ram: [GbByte; INTERNAL_RAM_SIZE], // [0xC000, 0xE000)
    sprite_ram:   [GbByte; SPRITE_RAM_SIZE],   // [0xFE00, 0xFEA0)
    io_registers: [GbByte; IO_REGISTERS_SIZE], // [0xFF00, 0xFF80)
    top_ram: [GbByte; TOP_RAM_SIZE],           // [0xFF80, 0xFFFF)
    interrupt_enable_register: GbByte,         //  0xFFFF

    cartridge: Cartridge,

    signals: Signals,
}

impl Mmu {
    pub fn new(cartridge: Cartridge) -> Self {
        Self {
            video_ram:    [0; VIDEO_RAM_SIZE],
            internal_ram: [0; INTERNAL_RAM_SIZE],
            sprite_ram:   [0; SPRITE_RAM_SIZE],
            top_ram:      [0; TOP_RAM_SIZE],

            io_registers: initialize_io_registers(),
            interrupt_enable_register: 0x00,
            cartridge,

            signals: Signals::new()
        }
    }

    pub fn get_cartridge_ram(&self) -> &[RamBank] {
        self.cartridge.get_ram()
    }

    pub fn tick(&mut self) {
        self.cartridge.tick();
    }
}

impl Memory for Mmu {
    fn read(&self, memory_address: GbAddr) -> GbByte {
        match memory_address {
            // Cartridge ROM
            0x0000..=0x3FFF => self.cartridge.read_rom_bank0(memory_address),
            0x4000..=0x7FFF => self.cartridge.read_rom(memory_address - 0x4000),
            // VRAM
            0x8000..=0x9FFF => self.read_video_ram(memory_address - 0x8000),
            // Cartridge RAM
            0xA000..=0xBFFF => self.cartridge.read_ram(memory_address - 0xA000),
            // GB Internal RAM
            0xC000..=0xDFFF => self.read_internal_ram(memory_address - 0xC000),
            0xE000..=0xFDFF => self.read_internal_ram(memory_address - 0xE000),
            // Sprite Attribute Memory
            0xFE00..=0xFE9F => self.read_sprite_ram(memory_address - 0xFE00),
            // Unusable memory
            0xFEA0..=0xFEFF => self.read_unusable(),
            // IO Ports
            0xFF00..=0xFF7F => self.read_io_registers(memory_address - 0xFF00),
            // TOP ram
            0xFF80..=0xFFFE => self.read_top_ram(memory_address - 0xFF80),
            // IE
            0xFFFF => self.interrupt_enable_register,
        }
    }

    fn write(&mut self, memory_address: GbAddr, datum: GbByte) {
        match memory_address {
            // Cartridge ROM
            0x0000..=0x7FFF => self.cartridge.access_mbc(memory_address, datum),
            // VRAM
            0x8000..=0x9FFF => self.write_video_ram(memory_address - 0x8000, datum),
            // Cartridge RAM
            0xA000..=0xBFFF => self.cartridge.write_ram(memory_address - 0xA000, datum),
            // GB Internal RAM
            0xC000..=0xDFFF => self.write_internal_ram(memory_address - 0xC000, datum),
            0xE000..=0xFDFF => self.write_internal_ram(memory_address - 0xE000, datum),
            // Sprite Attribute Memory
            0xFE00..=0xFE9F => self.write_sprite_ram(memory_address - 0xFE00, datum),
            // Unusable memory
            0xFEA0..=0xFEFF => self.write_unusable(),
            // IO Ports
            0xFF00..=0xFF7F => self.write_io_registers(memory_address - 0xFF00, datum),
            // TOP ram
            0xFF80..=0xFFFE => self.write_top_ram(memory_address - 0xFF80, datum),
            // IE
            0xFFFF => self.interrupt_enable_register = datum,
        }
    }
    
    fn iter(&self, address: GbAddr, halt_bug: bool) -> MemoryIterator<Self> {
        MemoryIterator { memory: self, address, halt_bug }
    }
}

impl VideoMemory for Mmu {
    fn read_lcdc(&self) -> LcdControlFlags {
        let lcdc_register = self.read_io_internal(IoReg::LCDC);
        LcdControlFlags::from(lcdc_register)
    }

    fn read_stat(&self) -> LcdStatusFlags {
        let stat_register = self.read_io_internal(IoReg::STAT);
        LcdStatusFlags::from(stat_register)
    }

    fn write_stat(&mut self, datum: u8) {
        let writable_stat = datum & 0b11111100;
        self.io_registers[Self::STAT_ADDRESS_IN_IO_REGISTERS as usize] = writable_stat | lower_two_bits(self.io_registers[Self::STAT_ADDRESS_IN_IO_REGISTERS as usize]);
    }

    fn write_stat_internal(&mut self, stat_register: LcdStatusFlags) {
        let datum = stat_register.to_byte();
        self.io_registers[Self::STAT_ADDRESS_IN_IO_REGISTERS as usize] = datum;
    }

    fn write_ly_internal(&mut self, datum: GbByte) {
        self.io_registers[Self::LY_ADDRESS_IN_IO_REGISTERS as usize] = datum;
    }

    fn read_sprite_attributes(&self, sprite_index: GbAddr) -> SpriteAttributes {
        let sprite_address: GbAddr = Self::OAM_ADDRESS + (0x0004 * sprite_index);

        SpriteAttributes::from([
                                   self.read(sprite_address),
                                   self.read(sprite_address + 1),
                                   self.read(sprite_address + 2),
                                   self.read(sprite_address + 3),
                               ], sprite_address)

    }

    fn read_background_window_palette(&self) -> PixelColorPalette {
        let bgp_register = self.read_io_internal(IoReg::BGP);
        PixelColorPalette::from(bgp_register)
    }

    fn read_sprite_palette_0(&self) -> PixelColorPalette {
        let obp0_register = self.read_io_internal(IoReg::OBP0);
        PixelColorPalette::from(obp0_register)
    }

    fn read_sprite_palette_1(&self) -> PixelColorPalette {
        let obp1_register = self.read_io_internal(IoReg::OBP1);
        PixelColorPalette::from(obp1_register)
    }

    fn read_line_pixels_from_background_window_tile_map_address(&self, tile_address_in_tile_map: GbAddr, line_in_tile: GbAddr, tile_data_selected: TileSelected) -> [(Bit, Bit); 8] {
        let tile_pattern_index = self.read(tile_address_in_tile_map);
        let tile_pattern_starting_address = Self::calculate_background_window_tile_data_starting_address(tile_data_selected, tile_pattern_index);

        self.read_tile_bytes_for_line(tile_pattern_starting_address, line_in_tile)
    }

    fn read_line_pixels_from_sprite_tile_index(&self, sprite_tile_index: GbAddr, line_in_sprite: GbAddr, sprite_size: SpriteSize) -> [(Bit, Bit); 8] {
        let sprite_pattern_starting_address = match sprite_size {
            SpriteSize::Size8x8 => {
                Self::SPRITE_PATTERN_ADDRESS + (sprite_tile_index * 16)
            },
            SpriteSize::Size8x16 => {
                let sprite_index = sprite_tile_index & 0b11111110 as GbAddr;
                Self::SPRITE_PATTERN_ADDRESS + (sprite_index * 16)
            }
        };

        self.read_tile_bytes_for_line(sprite_pattern_starting_address, line_in_sprite)
    }

}

impl AudioRegisters for Mmu {
    fn read_sound_panning(&self) -> SoundPanning {
        let datum = self.read_io_internal(IoReg::NR51);
        SoundPanning::from(datum)
    }

    fn read_master_volume(&self) -> MasterVolume {
        let datum = self.read_io_internal(IoReg::NR50);
        MasterVolume::from(datum)
    }

    fn read_frequency_timer(&self, channel: Channel) -> u16 {

        let(lower_byte, higher_byte) = match channel {
            Channel::Square1 => (self.read_io_internal(IoReg::NR13), self.read_io_internal(IoReg::NR14)),
            Channel::Square2 => (self.read_io_internal(IoReg::NR23), self.read_io_internal(IoReg::NR24)),
            Channel::Wave => (self.read_io_internal(IoReg::NR33), self.read_io_internal(IoReg::NR34)),
            Channel::Noise => panic!("Noise channel doesn't have any frequency timer!"),
        };

        join_bytes(lower_three_bits(higher_byte), lower_byte)
    }

    fn read_channel_envelope(&self, channel: Channel) -> Envelope {

        let datum = match channel {
            Channel::Square1 => self.read_io_internal(IoReg::NR12),
            Channel::Square2 => self.read_io_internal(IoReg::NR22),
            Channel::Wave => panic!("Wave channel doesn't have any envelope!"),
            Channel::Noise => self.read_io_internal(IoReg::NR42),
        };

        Envelope::from(datum)
    }

    fn read_wave_channel_volume(&self) -> WaveChannelVolume {
        let datum = self.read_io_internal(IoReg::NR32);
        WaveChannelVolume::from(datum)
    }

    fn read_wave_pattern_sample(&self, index: u8) -> u8 {
        //from 0xFF30 to 0xFF3F - each register holds 2 samples
        //that's why we get offset dividing by 2

        let offset = index / 2;
        self.io_registers[((IoReg::WAVE_RAM as u16 - 0xFF00) + offset as GbAddr) as usize]
    }

    fn read_length_timer(&self, channel: Channel) -> u8 {
        match channel {
            Channel::Square1 => lower_six_bits(self.read_io_internal(IoReg::NR11)),
            Channel::Square2 => lower_six_bits(self.read_io_internal(IoReg::NR21)),
            Channel::Wave => self.read_io_internal(IoReg::NR31),
            Channel::Noise => lower_six_bits(self.read_io_internal(IoReg::NR41)),
        }
    }

    fn read_square_channel_waveform(&self, channel: SquareChannelNumber) -> WaveForm {
        let datum = match channel {
            SquareChannelNumber::One => self.read_io_internal(IoReg::NR11),
            SquareChannelNumber::Two => self.read_io_internal(IoReg::NR21),
        };

        WaveForm::from(highest_two_bits(datum))
    }

    fn read_square_channel_sweep(&self) -> Sweep {
        let datum = self.read_io_internal(IoReg::NR10);
        Sweep::from(datum)
    }

    fn is_dac_enabled(&self, channel: Channel) -> bool {
        match channel {
            Channel::Square1 => highest_five_bits(self.read_io_internal(IoReg::NR12)) > 0,
            Channel::Square2 => highest_five_bits(self.read_io_internal(IoReg::NR22)) > 0,
            Channel::Wave => highest_bit(self.read_io_internal(IoReg::NR30)) > 0,
            Channel::Noise => highest_five_bits(self.read_io_internal(IoReg::NR42)) > 0,
        }
    }

    fn is_apu_enabled(&self) -> bool {
        highest_bit(self.read_io_internal(IoReg::NR52)) > 0
    }

    fn read_noise_channel_randomness(&self) -> NoiseChannelRandomness {
        let datum = self.read_io_internal(IoReg::NR43);
        NoiseChannelRandomness::from(datum)
    }

    fn get_signal_nr_10(&self) -> Option<u8> {
        self.signals.nr_10_written
    }

    fn get_signal_nr_x1(&self, channel: Channel) -> bool {
        match channel {
            Channel::Square1 => self.signals.nr_11_written,
            Channel::Square2 => self.signals.nr_21_written,
            Channel::Wave => self.signals.nr_31_written,
            Channel::Noise => self.signals.nr_41_written,
        }
    }

    fn get_signal_nr_x4(&self, channel: Channel) -> Option<u8> {
        match channel {
            Channel::Square1 => self.signals.nr_14_written,
            Channel::Square2 => self.signals.nr_24_written,
            Channel::Wave => self.signals.nr_34_written,
            Channel::Noise => self.signals.nr_44_written,
        }
    }

    //we will rewrite only the first 6 bits, the other ones are untouched
    fn write_length_timer(&mut self, datum: u8, channel: Channel) {
        match channel {
            Channel::Square1 => self.write_io_internal(IoReg::NR11, (self.read_io_internal(IoReg::NR11) & 0b11000000) | lower_six_bits(datum) ),
            Channel::Square2 => self.write_io_internal(IoReg::NR21, (self.read_io_internal(IoReg::NR21) & 0b11000000) | lower_six_bits(datum) ),
            Channel::Wave => self.write_io_internal(IoReg::NR31, datum), //except this timer: it contains full 8 bits
            Channel::Noise => self.write_io_internal(IoReg::NR41, (self.read_io_internal(IoReg::NR41) & 0b11000000) | lower_six_bits(datum) ),
        }
    }

    fn write_frequency_timer(&mut self, channel: Channel, wavelength: u16) -> u16 {
        let lower_byte = lower_byte(wavelength);
        let higher_byte = lower_three_bits(higher_byte(wavelength));

        match channel {
            Channel::Square1 => {
                self.write_io_internal(IoReg::NR13, lower_byte);

                let old_higher_byte = self.read_io_internal(IoReg::NR14);
                self.write_io_internal(IoReg::NR14, (old_higher_byte & 0b11111000) | higher_byte);
            },

            Channel::Square2 => {
                self.write_io_internal(IoReg::NR23, lower_byte);

                let old_higher_byte = self.read_io_internal(IoReg::NR24);
                self.write_io_internal(IoReg::NR24, (old_higher_byte & 0b11111000) | higher_byte);
            },

            Channel::Wave => {
                self.write_io_internal(IoReg::NR33, lower_byte);

                let old_higher_byte = self.read_io_internal(IoReg::NR34);
                self.write_io_internal(IoReg::NR34, (old_higher_byte & 0b11111000) | higher_byte);
            },

            Channel::Noise => panic!("Noise channel doesn't have any wavelength!"),
        };

        wavelength
    }



    fn write_channel_status_in_master_register_internal(&mut self, channel: Channel, enabled: bool) {
        let enabled_as_bit = u8::from(enabled);
        let datum = self.read_io_internal(IoReg::NR52);

        let updated_datum = match channel {
            Channel::Square1 => write_bit(datum, 0, enabled_as_bit),
            Channel::Square2 => write_bit(datum, 1, enabled_as_bit),
            Channel::Wave => write_bit(datum, 2, enabled_as_bit),
            Channel::Noise => write_bit(datum, 3, enabled_as_bit),
        };

        self.write_io_internal(IoReg::NR52, updated_datum);
    }

    fn write_pcm12_internal(&mut self, channel1_output: DigitalAudioOutput, channel2_output: DigitalAudioOutput) {
        let datum = join_nibbles(lower_half(channel2_output), lower_half(channel1_output));
        self.io_registers[Self::PCM_12_ADDRESS_IN_IO_REGISTERS as usize] = datum;
    }

    fn write_pcm34_internal(&mut self, channel3_output: DigitalAudioOutput, channel4_output: DigitalAudioOutput) {
        let datum = join_nibbles(lower_half(channel4_output), lower_half(channel3_output));
        self.io_registers[Self::PCM_34_ADDRESS_IN_IO_REGISTERS as usize] = datum;
    }

    fn write_wave_pattern_sample(&mut self, offset: u8, datum: u8) {
        self.io_registers[(Self::BASE_WAVE_RAM_ADDRESS_IN_IO_REGISTERS + (offset as u16)) as usize] = datum;
    }

    fn write_signal_wave_sample_currently_read(&mut self, index: u8, datum: u8) {
        self.signals.wave_sample = Some(WaveSampleSignal {
            index_currently_read: index,
            sample_currently_read: datum,
        });
    }

    fn reset_signal_wave_sample_currently_read(&mut self) {
        self.signals.wave_sample = None;
    }

    fn reset_signal_nr_10(&mut self) {
        self.signals.nr_10_written = None;
    }

    fn reset_signal_nr_x1(&mut self, channel: Channel) {
        match channel {
            Channel::Square1 => self.signals.nr_11_written = false,
            Channel::Square2 => self.signals.nr_21_written = false,
            Channel::Wave => self.signals.nr_31_written = false,
            Channel::Noise => self.signals.nr_41_written = false,
        };
    }

    fn reset_signal_nr_x4(&mut self, channel: Channel) {
        match channel {
            Channel::Square1 => self.signals.nr_14_written = None,
            Channel::Square2 => self.signals.nr_24_written = None,
            Channel::Wave => self.signals.nr_34_written = None,
            Channel::Noise => self.signals.nr_44_written = None,
        };
    }

    fn reset_all_audio_registers(&mut self) {
        let datum = 0b00000000u8;
        let datum_for_length_timers = 0b00111111u8;

        self.write_io_internal(IoReg::NR51, datum);
        self.write_io_internal(IoReg::NR50, datum);

        self.write_io_internal(IoReg::NR10, datum);
        self.write_io_internal(IoReg::NR11, self.read_io_internal(IoReg::NR11) & datum_for_length_timers);
        self.write_io_internal(IoReg::NR12, datum);
        self.write_io_internal(IoReg::NR13, datum);
        self.write_io_internal(IoReg::NR14, datum);

        self.write_io_internal(IoReg::NR21, self.read_io_internal(IoReg::NR21) & datum_for_length_timers);
        self.write_io_internal(IoReg::NR22, datum);
        self.write_io_internal(IoReg::NR23, datum);
        self.write_io_internal(IoReg::NR24, datum);

        self.write_io_internal(IoReg::NR30, datum);
        self.write_io_internal(IoReg::NR32, datum);
        self.write_io_internal(IoReg::NR33, datum);
        self.write_io_internal(IoReg::NR34, datum);

        self.write_io_internal(IoReg::NR41, self.read_io_internal(IoReg::NR41) & datum_for_length_timers);
        self.write_io_internal(IoReg::NR42, datum);
        self.write_io_internal(IoReg::NR43, datum);
        self.write_io_internal(IoReg::NR44, datum);

        self.signals.nr_10_written = Some(datum);

        self.signals.nr_14_written = Some(datum);
        self.signals.nr_24_written = Some(datum);
        self.signals.nr_34_written = Some(datum);
        self.signals.nr_44_written = Some(datum);
    }

}

impl IoRegistersInternal for Mmu {

    fn read_io_internal(&self, register: IoReg) -> GbByte {
        self.io_registers[(register as u16 - 0xFF00) as usize]
    }

    fn write_io_internal(&mut self, register: IoReg, datum: GbByte) {
        self.io_registers[(register as u16 - 0xFF00) as usize] = datum;
    }
}

impl DivRegister for Mmu {
    fn set_div_reset_requested(&mut self, value: bool) {
        self.signals.div_reset_requested = value;
    }

    fn is_div_reset_requested(&self) -> bool {
        self.signals.div_reset_requested
    }

    fn increment_div(&mut self) {
        self.write_io_internal(IoReg::DIV, self.read_io_internal(IoReg::DIV).overflowing_add(1).0);
    }

    fn reset_div(&mut self) {
        self.write_io_internal(IoReg::DIV, 0);
    }
}

impl P1Register for Mmu {
    fn read_p1(&self) -> GbByte {
        self.io_registers[Self::P1_ADDRESS_IN_IO_REGISTERS as usize]
    }

    fn write_detected_input(&mut self, datum: GbByte) {
        self.io_registers[Self::P1_ADDRESS_IN_IO_REGISTERS as usize] = join_nibbles(higher_half(self.io_registers[Self::P1_ADDRESS_IN_IO_REGISTERS as usize]),lower_half(datum));
    }
}


impl Mmu {
    // VRAM
    fn read_video_ram(&self, vram_address: GbAddr) -> GbByte {
        self.video_ram[vram_address as usize]
    }
    fn write_video_ram(&mut self, vram_address: GbAddr, datum: GbByte) {
        self.video_ram[vram_address as usize] = datum;
    }
    
    // GB Internal RAM
    fn read_internal_ram(&self, internal_ram_address: GbAddr) -> GbByte {
        self.internal_ram[internal_ram_address as usize]
    }
    fn write_internal_ram(&mut self, internal_ram_address: GbAddr, datum: GbByte) {
        self.internal_ram[internal_ram_address as usize] = datum;
    }

    // Sprite Attribute Memory
    fn read_sprite_ram(&self, sprite_ram_address: GbAddr) -> GbByte {
        self.sprite_ram[sprite_ram_address as usize]
    }
    fn write_sprite_ram(&mut self, sprite_ram_address: GbAddr, datum: GbByte) {
        self.sprite_ram[sprite_ram_address as usize] = datum;
    }

    // GB Internal TOP RAM
    fn read_top_ram(&self, top_ram_address: GbAddr) -> GbByte {
        self.top_ram[top_ram_address as usize]
    }
    fn write_top_ram(&mut self, top_ram_address: GbAddr, datum: GbByte) {
        self.top_ram[top_ram_address as usize] = datum;
    }

    // IO Ports
    fn read_io_registers(&self, io_address: GbAddr) -> GbByte {
        match io_address {
            //special OR masks for unused bits in io registers
            Self::P1_ADDRESS_IN_IO_REGISTERS => 0b11000000 | self.io_registers[io_address as usize],
            SC_ADDRESS_IN_IO_REGISTERS => 0b01111110 | self.io_registers[io_address as usize],
            TAC_ADDRESS_IN_IO_REGISTERS => 0b11111000 | self.io_registers[io_address as usize],
            IF_ADDRESS_IN_IO_REGISTERS => 0b11100000 | self.io_registers[io_address as usize],
            Self::STAT_ADDRESS_IN_IO_REGISTERS => 0b10000000 | self.io_registers[io_address as usize],

            Self::AUDIO_MASTER_CONTROL_ADDRESS_IN_IO_REGISTERS => 0b01110000 | self.io_registers[io_address as usize],

            Self::NR10_ADDRESS_IN_IO_REGISTERS => 0b10000000 | self.io_registers[io_address as usize],
            Self::NR11_ADDRESS_IN_IO_REGISTERS => 0b00111111 | self.io_registers[io_address as usize],
            Self::NR13_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR14_ADDRESS_IN_IO_REGISTERS => 0b10111111 | self.io_registers[io_address as usize],

            Self::NR20_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR21_ADDRESS_IN_IO_REGISTERS => 0b00111111 | self.io_registers[io_address as usize],
            Self::NR23_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR24_ADDRESS_IN_IO_REGISTERS => 0b10111111 | self.io_registers[io_address as usize],

            Self::NR30_ADDRESS_IN_IO_REGISTERS => 0b01111111 | self.io_registers[io_address as usize],
            Self::NR31_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR32_ADDRESS_IN_IO_REGISTERS => 0b10011111 | self.io_registers[io_address as usize],
            Self::NR33_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR34_ADDRESS_IN_IO_REGISTERS => 0b10111111 | self.io_registers[io_address as usize],

            Self::NR40_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR41_ADDRESS_IN_IO_REGISTERS => 0b11111111,
            Self::NR44_ADDRESS_IN_IO_REGISTERS => 0b10111111 | self.io_registers[io_address as usize],

            //cartridge can read from WAVE RAM only when channel is not active. When it's active, then
            //it can read only the sample byte that is currently being read by wave channel, and only
            //in the very same t-cycle in which it's read
            Self::BASE_WAVE_RAM_ADDRESS_IN_IO_REGISTERS..= Self::MAX_WAVE_RAM_ADDRESS_IN_IO_REGISTERS =>  {
                if self.is_wave_channel_enabled() {
                    if let Some(wave_sample_signal) = self.signals.wave_sample {
                        wave_sample_signal.sample_currently_read
                    } else {
                        0xFF
                    }
                } else {
                    self.io_registers[io_address as usize]
                }
            }

            //unused regs always return 0xFF
            0x0003 | 0x0008..=0x000E | 0x0027..=0x002F | 0x004C |
            0x004E | 0x0057..=0x0067 | 0x006D..=0x006F |
            0x0078..=0x007F => 0xFF,

            //for now these are GBC only registers that return always 0xFF on DMG
            0x0070 | 0x004F | 0x004D | 0x0068 | 0x006A | 0x0056 | 0x0051..=0x0054 => 0xFF,

            0x0076 => {
                self.io_registers[io_address as usize]
            }
            0x0077 => {
                self.io_registers[io_address as usize]
            }

            _ => self.io_registers[io_address as usize],
        }
    }

    fn write_io_registers(&mut self, io_address: GbAddr, datum: GbByte) {
        match io_address {

            //first 4 bits of P1 are protected
            Self::P1_ADDRESS_IN_IO_REGISTERS => self.io_registers[io_address as usize] = join_nibbles(higher_half(datum), lower_half(self.io_registers[io_address as usize])),

            //Writes to DIV only resets it
            Self::DIV_ADDRESS_IN_IO_REGISTERS => self.set_div_reset_requested(true),

            Self::STAT_ADDRESS_IN_IO_REGISTERS => self.write_stat(datum),

            Self::LY_ADDRESS_IN_IO_REGISTERS | Self::PCM_12_ADDRESS_IN_IO_REGISTERS | Self::PCM_34_ADDRESS_IN_IO_REGISTERS => (),

            //only highest bit gets written, the others are read-only
            Self::AUDIO_MASTER_CONTROL_ADDRESS_IN_IO_REGISTERS => self.io_registers[io_address as usize] = write_bit(self.io_registers[io_address as usize], 7, highest_bit(datum)),

            //if it's DMA, execute it
            DMA_ADDRESS_IN_IO_REGISTERS => self.dma(datum),

            //when APU is off, then writes to the APU registers are not honored, apart from length timers writes (supported only on DMG - NR31 is a full 8bit timer register so there is no need to match it here)
            Self::NR10_ADDRESS_IN_IO_REGISTERS | Self::NR11_ADDRESS_IN_IO_REGISTERS | Self::NR12_ADDRESS_IN_IO_REGISTERS | Self::NR13_ADDRESS_IN_IO_REGISTERS | Self::NR14_ADDRESS_IN_IO_REGISTERS |
            Self::NR20_ADDRESS_IN_IO_REGISTERS | Self::NR21_ADDRESS_IN_IO_REGISTERS | Self::NR22_ADDRESS_IN_IO_REGISTERS | Self::NR23_ADDRESS_IN_IO_REGISTERS | Self::NR24_ADDRESS_IN_IO_REGISTERS |
            Self::NR30_ADDRESS_IN_IO_REGISTERS | Self::NR32_ADDRESS_IN_IO_REGISTERS | Self::NR33_ADDRESS_IN_IO_REGISTERS | Self::NR34_ADDRESS_IN_IO_REGISTERS |
            Self::NR40_ADDRESS_IN_IO_REGISTERS | Self::NR41_ADDRESS_IN_IO_REGISTERS | Self::NR42_ADDRESS_IN_IO_REGISTERS | Self::NR43_ADDRESS_IN_IO_REGISTERS | Self::NR44_ADDRESS_IN_IO_REGISTERS |
            Self::NR50_ADDRESS_IN_IO_REGISTERS | Self::NR51_ADDRESS_IN_IO_REGISTERS => {
                if self.is_apu_enabled() {
                    if io_address == Self::NR10_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_10_written = Some(datum);
                    }
                    if io_address == Self::NR11_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_11_written = true;
                    }
                    if io_address == Self::NR14_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_14_written = Some(datum);
                    }
                    if io_address == Self::NR21_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_21_written = true;
                    }
                    if io_address == Self::NR24_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_24_written = Some(datum);
                    }
                    if io_address == Self::NR34_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_34_written = Some(datum);
                    }
                    if io_address == Self::NR41_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_41_written = true;
                    }
                    if io_address == Self::NR44_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_44_written = Some(datum);
                    }
                    self.io_registers[io_address as usize] = datum;
                } else if io_address == Self::NR11_ADDRESS_IN_IO_REGISTERS || io_address == Self::NR21_ADDRESS_IN_IO_REGISTERS || io_address == Self::NR41_ADDRESS_IN_IO_REGISTERS {
                    if io_address == Self::NR11_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_11_written = true;
                    }
                    if io_address == Self::NR21_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_21_written = true;
                    }
                    if io_address == Self::NR41_ADDRESS_IN_IO_REGISTERS {
                        self.signals.nr_41_written = true;
                    }
                    self.io_registers[io_address as usize] = datum & 0b00111111u8;
                }
            }

            Self::NR31_ADDRESS_IN_IO_REGISTERS  => {
                self.signals.nr_31_written = true;
                self.io_registers[io_address as usize] = datum;
            },

            //cartridge can write to WAVE RAM only when channel is not active. When it's active, then
            //it can write only in the same byte that is currently being read by wave channel, and only
            //in the very same t-cycle in which it's read
            Self::BASE_WAVE_RAM_ADDRESS_IN_IO_REGISTERS..= Self::MAX_WAVE_RAM_ADDRESS_IN_IO_REGISTERS =>  {
                if self.is_wave_channel_enabled() {
                    if let Some(wave_sample_signal) = self.signals.wave_sample {
                        self.io_registers[(Self::BASE_WAVE_RAM_ADDRESS_IN_IO_REGISTERS + (wave_sample_signal.index_currently_read/2) as u16) as usize] = datum;
                    }
                } else {
                    self.io_registers[io_address as usize] = datum;
                }
            }

            TAC_ADDRESS_IN_IO_REGISTERS => {
                let mask = 0b11111000;
                self.io_registers[io_address as usize] = datum | mask;
            },

            _ =>   self.io_registers[io_address as usize] = datum,
        }
    }

    fn dma(&mut self, datum: GbByte) {
        let memory_address_to_load = (datum as GbAddr) << 8;
        for memory_address_offset in 0..0x00A0 as GbAddr {
            let oam_datum = self.read(memory_address_to_load + memory_address_offset);
            self.write((SPRITE_RAM_BEGINNING_ADDRESS as GbAddr) + memory_address_offset, oam_datum);
        }
    }

    // Unusable memory portion
    fn read_unusable(&self) -> GbByte {
        0x00
    }
    fn write_unusable(&self) {}

    /*
        Here we are calculating what is the tile_data starting address, given it's index.
        Index may be a number between 0 and 255 or -128 and 127 (depending on the tile selected).
        Every tile is an 8x8 pixel square. Every pixel is comprised of 2 bits, because color is 0-3 so
        two bits are needed. Given the tile is 8x8 => 64 pixels, then each tile takes 128 bits, that are
        16 bytes. That's why we do tile_pattern_index * 16, because that is the offset from the start of
        the tile_data address (16 bytes for each tile)
    */
    fn calculate_background_window_tile_data_starting_address(tile_selected: TileSelected, tile_pattern_index: GbByte) -> GbAddr {
        match tile_selected {
            TileSelected::Lower => {
                0x8000 + (tile_pattern_index as u16 * 16) as GbAddr
            },
            TileSelected::Upper => {
                let signed_tile_pattern_index = tile_pattern_index as i8 as i16;
                add_u16_i16(0x9000, signed_tile_pattern_index * 16)
            }
        }
    }

    //they're inverted because when reading, the first bit is the least significant bit
    fn read_tile_bytes_for_line(&self, tile_address: GbAddr, line_in_tile: GbAddr) ->  [(Bit, Bit); 8] {
        let tile_bytes = (self.read(tile_address + (line_in_tile * 2)), self.read(tile_address + (line_in_tile * 2) + 1));
        [
            (test_bit(tile_bytes.1, 7), test_bit(tile_bytes.0, 7)),
            (test_bit(tile_bytes.1, 6), test_bit(tile_bytes.0, 6)),
            (test_bit(tile_bytes.1, 5), test_bit(tile_bytes.0, 5)),
            (test_bit(tile_bytes.1, 4), test_bit(tile_bytes.0, 4)),
            (test_bit(tile_bytes.1, 3), test_bit(tile_bytes.0, 3)),
            (test_bit(tile_bytes.1, 2), test_bit(tile_bytes.0, 2)),
            (test_bit(tile_bytes.1, 1), test_bit(tile_bytes.0, 1)),
            (test_bit(tile_bytes.1, 0), test_bit(tile_bytes.0, 0)),
        ]
    }

    fn is_wave_channel_enabled(&self) -> bool {
        test_bit(self.io_registers[Self::AUDIO_MASTER_CONTROL_ADDRESS_IN_IO_REGISTERS as usize], 2)
    }
}

fn add_u16_i16(x: u16, y: i16) -> u16 {
    if y < 0 {
        x.wrapping_sub(-y as u16)
    } else {
        x.wrapping_add(y as u16)
    }
}


fn initialize_io_registers() -> [GbByte; IO_REGISTERS_SIZE] {
    let mut io_registers = [0u8; IO_REGISTERS_SIZE];
    io_registers[0x10] = 0x80;
    io_registers[0x11] = 0xBF;
    io_registers[0x12] = 0xF3;
    io_registers[0x14] = 0xBF;
    io_registers[0x16] = 0x3F;
    io_registers[0x19] = 0xBF;
    io_registers[0x1A] = 0x7F;
    io_registers[0x1B] = 0xFF;
    io_registers[0x1C] = 0x9F;
    io_registers[0x1E] = 0xBF;
    io_registers[0x20] = 0xFF;
    io_registers[0x23] = 0xBF;
    io_registers[0x24] = 0x77;
    io_registers[0x25] = 0xF3;
    io_registers[0x26] = 0xF1; 
    io_registers[0x40] = 0x91;
    io_registers[0x47] = 0xFC;
    io_registers[0x48] = 0xFF;
    io_registers[0x49] = 0xFF;

    io_registers
}
