
#[derive(Debug)]
pub enum JoypadKey {
    A, B, Up, Down, Left, Right, Start, Select
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct JoypadState {
    a_pressed: bool,
    b_pressed: bool,
    up_pressed: bool,
    down_pressed: bool,
    left_pressed: bool,
    right_pressed: bool,
    start_pressed: bool,
    select_pressed: bool,
}

impl JoypadState {
    pub fn new() -> Self {
        Self {
            a_pressed: false,
            b_pressed: false,
            up_pressed: false,
            down_pressed: false,
            left_pressed: false,
            right_pressed: false,
            start_pressed: false,
            select_pressed: false,
        }
    }

    pub fn key_down(&mut self, key: JoypadKey) {
        match key {
            JoypadKey::A => self.a_pressed = true,
            JoypadKey::B => self.b_pressed = true,
            JoypadKey::Select => self.select_pressed = true,
            JoypadKey::Start => self.start_pressed = true,
            JoypadKey::Up => self.up_pressed = true,
            JoypadKey::Down => self.down_pressed = true,
            JoypadKey::Left => self.left_pressed = true,
            JoypadKey::Right => self.right_pressed = true,
        }
    }

    pub fn key_up(&mut self, key: JoypadKey) {
        match key {
            JoypadKey::A => self.a_pressed = false,
            JoypadKey::B => self.b_pressed = false,
            JoypadKey::Select => self.select_pressed = false,
            JoypadKey::Start => self.start_pressed = false,
            JoypadKey::Up => self.up_pressed = false,
            JoypadKey::Down => self.down_pressed = false,
            JoypadKey::Left => self.left_pressed = false,
            JoypadKey::Right => self.right_pressed = false,
        }
    }

    pub fn key_state(&self, key: JoypadKey) -> bool {
        match key {
            JoypadKey::A => self.a_pressed,
            JoypadKey::B => self.b_pressed,
            JoypadKey::Select => self.select_pressed,
            JoypadKey::Start => self.start_pressed,
            JoypadKey::Up => self.up_pressed,
            JoypadKey::Down => self.down_pressed,
            JoypadKey::Left => self.left_pressed,
            JoypadKey::Right => self.right_pressed,
        }
    }

}