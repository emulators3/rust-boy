use std::cmp::Ordering;
use crate::gb::memory::io_registers::IoReg;
use crate::gb::memory::video_memory::lcd_control_flags::LcdControlFlags;
use crate::gb::memory::video_memory::lcd_status_flags::StatMode;
use crate::gb::memory::video_memory::pixel_color_palette::PixelColorPalette;
use crate::gb::memory::video_memory::sprite_attributes::{SpriteAttributes, SpritePalette};
use crate::gb::memory::video_memory::VideoMemory;
use crate::gb::pixel_color::PixelColor;
use crate::gb::types::GbAddr;
use crate::utils::constants::{Bit, SCREEN_LINE_SIZE, WINDOW_LINE_SIZE, WINDOW_LINES, WindowScreen};

const SPRITES_NUMBER: u16 = 40;
const TILE_HEIGHT: u8 = 8;
const SCREEN_TILES_PER_LINE: u8 = 32;
const MAX_SPRITES_PER_LINE: usize = 10;
const MIN_SCREEN_X_FOR_WRAP: usize = 96;
const MAX_SPRITE_X: u8 = (WINDOW_LINE_SIZE as u8) + 8;
const MAX_WINDOW_X: u8 = (WINDOW_LINE_SIZE as u8) + 7;

#[derive(Debug)]
pub struct Ppu {
    // the screen buffer 
    screen: WindowScreen,
    next_status: StatMode,
    current_ly: u8,
    sprites_loaded: Vec<SpriteAttributes>,

    window_y: u8,
    window_ly: u8,


    initial_dot: i32,
}

impl Ppu {
    pub fn new() -> Self {
        Self {
            screen: [[PixelColor::Darkest; WINDOW_LINE_SIZE]; WINDOW_LINES],
            next_status: StatMode::OamSearch,
            current_ly: 0,
            sprites_loaded: Vec::new(),
            window_y: 0,
            window_ly: 0,
            initial_dot: 0,
        }
    }

    pub fn dump(&mut self, video_mem: &mut impl VideoMemory, n: i32) {
        let current_ly = video_mem.read_io_internal(IoReg::LY);

        print!("\t PPU: {},{}", n - self.initial_dot, current_ly);
    }

    pub fn check_ly_lyc(&self, video_mem: &mut impl VideoMemory) {
        let ly = video_mem.read_io_internal(IoReg::LY);
        let lyc = video_mem.read_io_internal(IoReg::LYC);
        let mut stat_register = video_mem.read_stat();

        stat_register.coincidence_flag = ly==lyc;
        video_mem.write_stat_internal(stat_register);
    }

    /*
        TODO for the future pixel FIFO penalties to be calculated
        - SCX & 7 (or mod 8) is the penalty for the scroll (at the very beginning of mode3 [drawing pixels for the line])
        - if in a line there is BG and window, other 6 dots penalty
        - (OAM[i].xpos + SCX) % 8 dot penalties for first object over a given tile, and 6 more dot periods for each additional object over the same tile
          The idea is that we should count all the pixels of the tile that are on right from the OAM pixel hitting the tile. It can be maximum 7. This number
          needs to be subtracted by 2 and then add 6 flat dot time. This means 11 max for each sprite.
          172 is the correct initial mode 3 timing, and it can grow probably until 295 (110sprites +7scroll + 6windowjump)
        - properly timed SCX writes can let mode3 run endlessly (until mode2 of new line) -> this can be done only in pixel FIFO

           * note that sprites that are far enough to the right to overlap the window (if enabled) would require a formula based on WX rather than SCX
            and only the leftmost pixel of the object matters, overlapping the window with other pixels isn't relevant
    */

    pub fn run(&mut self, video_mem: &mut impl VideoMemory, dot: i32) -> u32 {
        let mut stat_register = video_mem.read_stat();

        let cycles_to_wait = match self.next_status {

            StatMode::OamSearch => {
                stat_register.mode_flag = StatMode::OamSearch;

                video_mem.write_ly_internal(self.current_ly);
                self.initial_dot = dot;

                self.search_oam(self.current_ly, video_mem);

                self.next_status = StatMode::DataTransferToLcdDriver;
                79
            },
            StatMode::DataTransferToLcdDriver => {
                stat_register.mode_flag = StatMode::DataTransferToLcdDriver;

                self.render_line(self.current_ly, video_mem);

                self.next_status = StatMode::HBlank;
                171
            },
            StatMode::HBlank => {
                stat_register.mode_flag = StatMode::HBlank;
                self.sprites_loaded.clear();

                self.next_status = if self.current_ly == 143 {
                    StatMode::VBlank
                } else {
                    StatMode::OamSearch
                };
                self.increment_current_ly();
                203
            },
            StatMode::VBlank => {
                stat_register.mode_flag = StatMode::VBlank;

                video_mem.write_ly_internal(self.current_ly);
                self.initial_dot = dot;

                self.next_status = if self.current_ly == 153 {
                    self.window_y = video_mem.read_io_internal(IoReg::WY);
                    self.window_ly = 0;
                    StatMode::OamSearch
                } else {
                    StatMode::VBlank
                };
                self.increment_current_ly();
                455
            },
        };

        video_mem.write_stat_internal(stat_register);
        cycles_to_wait
    }


    fn search_oam(&mut self, current_ly: u8, video_mem: &mut impl VideoMemory) {
        let lcdc_register = video_mem.read_lcdc();

        if lcdc_register.sprites_displayed {

            let sprite_height = lcdc_register.get_sprite_height();
            let minimum_y_position = current_ly + 16 - (sprite_height - 1);
            let maximum_y_position = current_ly + 16;
            
            for sprite_index in 0..SPRITES_NUMBER {
                let sprite_attributes = video_mem.read_sprite_attributes(sprite_index);
                if ly_hits_sprite(sprite_attributes.y_position, minimum_y_position, maximum_y_position) {
                    self.sprites_loaded.push(sprite_attributes);
                }

                //only up to 10 sprites can be loaded, then immediately stops
                if self.sprites_loaded.len()==MAX_SPRITES_PER_LINE {
                    break;
                }
            }

            //Let's reorder the vector by the leftmost sprites, and
            //for sprites with same x, the ones with the lower address in OAM
            //then we can truncate the vector to 10. This will allow drawing to be easier
            self.sprites_loaded.sort_by(compare_sprite_position);
        }
    }

    fn increment_current_ly(&mut self) {
        self.current_ly = (self.current_ly + 1) % 154;
    }

    pub fn get_window(&self) -> [[PixelColor; WINDOW_LINE_SIZE]; WINDOW_LINES] {
        self.screen
    }

    fn render_line(&mut self, current_ly: u8, video_mem: &impl VideoMemory) {
        let lcdc_register = video_mem.read_lcdc();
        let line_background_pixels = retrieve_background_pixels_for_line(current_ly, lcdc_register, video_mem);
        let line_window_pixels = self.retrieve_window_pixels_for_current_line(current_ly, lcdc_register, video_mem);
        let line_sprite_pixels = retrieve_sprite_pixels_for_line(&mut self.sprites_loaded, current_ly, lcdc_register, video_mem);

        let composed_line = compose_line(
            line_background_pixels,
            line_window_pixels,
            line_sprite_pixels,
            video_mem.read_background_window_palette(),
            video_mem.read_sprite_palette_0(),
            video_mem.read_sprite_palette_1()
        );

        self.screen[current_ly as usize][..WINDOW_LINE_SIZE].copy_from_slice(&composed_line[..WINDOW_LINE_SIZE]);
    }

    //TODO we really need to extract the window entity from here
    fn retrieve_window_pixels_for_current_line(&mut self, ly_to_render: u8, lcdc_register: LcdControlFlags, video_mem: &impl VideoMemory) -> [PixelColorIndex; WINDOW_LINE_SIZE] {
        if lcdc_register.all_displayed && lcdc_register.bg_window_displayed && lcdc_register.window_displayed {
            let window_x = video_mem.read_io_internal(IoReg::WX);

            if self.window_y <= ly_to_render && self.window_y <= 143 && window_x <= 166 {
                let response = retrieve_window_pixels_for_line(self.window_ly, window_x, lcdc_register, video_mem);
                self.window_ly += 1;
                response
            } else {
                [PixelColorIndex::Blank; WINDOW_LINE_SIZE]
            }
        } else {
            [PixelColorIndex::Blank; WINDOW_LINE_SIZE]
        }
    }
}

fn compare_sprite_position(sprite_1: &SpriteAttributes, sprite_2: &SpriteAttributes) -> Ordering {
    match sprite_1.x_position.cmp(&sprite_2.x_position) {
        Ordering::Equal => sprite_1.oam_address.cmp(&sprite_2.oam_address),
        not_equal       => not_equal,
    }
}

fn retrieve_background_pixels_for_line(ly_to_render: u8, lcdc_register: LcdControlFlags, video_mem: &impl VideoMemory) -> [PixelColorIndex;WINDOW_LINE_SIZE] {
    if lcdc_register.all_displayed && lcdc_register.bg_window_displayed {
        let screen_x = video_mem.read_io_internal(IoReg::SCX);
        let screen_y = video_mem.read_io_internal(IoReg::SCY);

        //it seems that for every line, we need to take the tile offsetted from the scroll_y
        let line_to_render = (ly_to_render as GbAddr + screen_y as GbAddr) % 256;

        let tile_map_starting_address = lcdc_register.get_background_tile_map_starting_address();

        let tile_selected = lcdc_register.bg_window_tile_data_selected;
        let tile_map_line_offset = (line_to_render / TILE_HEIGHT as GbAddr) * SCREEN_TILES_PER_LINE as GbAddr;
        let tile_map_address = tile_map_starting_address + tile_map_line_offset;
        let line_in_tile = line_to_render % TILE_HEIGHT as GbAddr;

        let line_background_pixels_to_wrap = (0..SCREEN_TILES_PER_LINE).flat_map(|tile| {
            produce_colored_pixel_array_from(video_mem.read_line_pixels_from_background_window_tile_map_address(tile_map_address + tile as GbAddr, line_in_tile, tile_selected))
        }).collect::<Vec<PixelColorIndex>>().try_into().unwrap();

        wrap_background_pixel_line(screen_x as usize, line_background_pixels_to_wrap)
    } else {
        [PixelColorIndex::Blank; WINDOW_LINE_SIZE]
    }
}

fn retrieve_window_pixels_for_line(window_ly: u8, window_x: u8, lcdc_register: LcdControlFlags, video_mem: &impl VideoMemory) -> [PixelColorIndex; WINDOW_LINE_SIZE] {
    let tile_map_starting_address = lcdc_register.get_window_tile_map_starting_address();

    let tile_selected = lcdc_register.bg_window_tile_data_selected;
    let tile_map_line_offset = (window_ly / TILE_HEIGHT) as GbAddr * SCREEN_TILES_PER_LINE as GbAddr;
    let tile_map_address = tile_map_starting_address + tile_map_line_offset;
    let line_in_tile = (window_ly % TILE_HEIGHT) as GbAddr;

    let mut response = [PixelColorIndex::Blank; WINDOW_LINE_SIZE];

    let mut buff = [PixelColorIndex::Blank; 8];
    let mut tile = 0;

    //remember that window_x is X position plus 7, so we need to remove 7 later
    for i in window_x..MAX_WINDOW_X {
        let loop_cycle_number_in_8_cycles = ((i - window_x) % 8) as usize;

        //every initial loop cycle on a group of 8 loop cycles
        if loop_cycle_number_in_8_cycles == 0 {
            buff = produce_colored_pixel_array_from(video_mem.read_line_pixels_from_background_window_tile_map_address(tile_map_address + tile, line_in_tile, tile_selected));
            tile += 1;
        }

        //if i<7, then the pixel is on the left of the screen, so we skip
        if i>=7 {
            response[(i-7) as usize] = buff[loop_cycle_number_in_8_cycles];
        }

    }
    response
}

fn ly_hits_sprite(sprite_y: u8, min_y: u8, max_y: u8) -> bool {
    sprite_y >= min_y && sprite_y <= max_y
}

fn retrieve_sprite_pixels_for_line(sprites_loaded: &mut Vec<SpriteAttributes>, current_ly: u8, lcdc_register: LcdControlFlags, video_mem: &impl VideoMemory) -> [SpriteColoredPixel; WINDOW_LINE_SIZE] {
    let mut line_sprite_bytes: [SpriteColoredPixel;WINDOW_LINE_SIZE] = [SpriteColoredPixel::blank();WINDOW_LINE_SIZE];

    //TODO check if lcdc_register.sprite_displayed is checked when rendering the line, or not
    if lcdc_register.all_displayed && lcdc_register.sprites_displayed {
        sprites_loaded.reverse();

        for sprite_attributes in sprites_loaded {

            let sprite_height = lcdc_register.get_sprite_height();

            //ideally it should be  current_ly - (sprite_attributes.y_position - 16); but in this
            //way I'm also solving underflow problems for when current_ly < 16
            let line_in_sprite_before_flip = current_ly + 16 - sprite_attributes.y_position;

            let line_in_sprite = (if sprite_attributes.flags.y_flip {
                (sprite_height - 1) - line_in_sprite_before_flip
            } else {
                line_in_sprite_before_flip
            }) as GbAddr;


            let mut tile_bytes = video_mem.read_line_pixels_from_sprite_tile_index(sprite_attributes.tile_index as GbAddr, line_in_sprite, lcdc_register.sprite_size);

            let tile_pixels = if sprite_attributes.flags.x_flip {
                tile_bytes.reverse();
                produce_colored_pixel_array_from(tile_bytes)
            } else {
                produce_colored_pixel_array_from(tile_bytes)
            };


            /*Here we're using 8 as an offset because otherwise we can incur in an underflow error
              Example: if x_position is 0, then retrieving the initial x position of the sprite would be -8.
              By checking if tile_pixel_position_in_line >= 8 and only after removing 8, we're sure that
              the underflow never occurs.

              The first IF is used to avoid executing 8 cycles for a sprite that will surely be off the screen
             */

            if sprite_attributes.x_position < MAX_SPRITE_X {
                for tile_pixel_index in 0..8 {
                    let tile_pixel_position_in_line = sprite_attributes.x_position + tile_pixel_index;
                    //if it can be seen
                    if (8..MAX_SPRITE_X).contains(&tile_pixel_position_in_line) {

                        //pixel color 0 is transparent in sprites, so I'm not going to write anything if it's ZERO
                        if tile_pixels[tile_pixel_index as usize] != PixelColorIndex::Zero {
                            line_sprite_bytes[(tile_pixel_position_in_line - 8) as usize] = SpriteColoredPixel {
                                pixel_color_index: tile_pixels[tile_pixel_index as usize],
                                hidden_by_bg_and_window: sprite_attributes.flags.hidden_by_bg_and_window,
                                palette: sprite_attributes.flags.palette,
                            };
                        }
                    }
                }
            }
        }
    }
    line_sprite_bytes
}

fn produce_colored_pixel_array_from(pixels: [(Bit, Bit); 8]) -> [PixelColorIndex; 8] {
    [
        PixelColorIndex::from(pixels[0].0, pixels[0].1),
        PixelColorIndex::from(pixels[1].0, pixels[1].1),
        PixelColorIndex::from(pixels[2].0, pixels[2].1),
        PixelColorIndex::from(pixels[3].0, pixels[3].1),
        PixelColorIndex::from(pixels[4].0, pixels[4].1),
        PixelColorIndex::from(pixels[5].0, pixels[5].1),
        PixelColorIndex::from(pixels[6].0, pixels[6].1),
        PixelColorIndex::from(pixels[7].0, pixels[7].1),
    ]
}

fn wrap_background_pixel_line(screen_x: usize, background_pixel_screen_line: [PixelColorIndex;SCREEN_LINE_SIZE]) -> [PixelColorIndex;WINDOW_LINE_SIZE] {
    if screen_x > MIN_SCREEN_X_FOR_WRAP {
        //Here we're retrieving the index for which we need to wrap
        //example: screen_x is 243 and 243+159 is 402
        //159 - (255 - 243) = 147 that is the index that we need
        let wrapping_background_index = 159 - (255 - screen_x);
        [&background_pixel_screen_line[screen_x..256], &background_pixel_screen_line[0..wrapping_background_index]].concat().try_into().unwrap()
    } else {
        background_pixel_screen_line[screen_x..screen_x+160].try_into().unwrap()
    }
}

fn compose_line(line_background_pixels: [PixelColorIndex;WINDOW_LINE_SIZE],
                line_window_pixels: [PixelColorIndex;WINDOW_LINE_SIZE],
                line_sprites_pixels: [SpriteColoredPixel;WINDOW_LINE_SIZE],
                background_window_palette: PixelColorPalette,
                sprite_palette_0: PixelColorPalette,
                sprite_palette_1: PixelColorPalette) -> [PixelColor;WINDOW_LINE_SIZE] {
    (0..WINDOW_LINE_SIZE).map(|i| {
        //first we aggregate background and window
        let bgw_pixel = match(line_background_pixels[i], line_window_pixels[i]) {
            (background_pixel, PixelColorIndex::Blank) => background_pixel,
            (_,window_pixel) => window_pixel,
        };

        let sprite_palette = match line_sprites_pixels[i].palette {
            SpritePalette::Palette0 => sprite_palette_0,
            SpritePalette::Palette1 => sprite_palette_1,
        };

        //and then we match the result with the sprite
        match (bgw_pixel, line_sprites_pixels[i]) {
            (_, sprite_pixel) if sprite_pixel.is_blank() => background_window_palette.get_pixel_color_from_index(bgw_pixel),

            //for sprites, zero means transparent so bgw wins
            (_, sprite_pixel) if sprite_pixel.pixel_color_index == PixelColorIndex::Zero => background_window_palette.get_pixel_color_from_index(bgw_pixel),

            (PixelColorIndex::Blank, sprite_pixel) => sprite_palette.get_pixel_color_from_index(sprite_pixel.pixel_color_index),

            (_, sprite_pixel) if !sprite_pixel.hidden_by_bg_and_window => sprite_palette.get_pixel_color_from_index(sprite_pixel.pixel_color_index),

            (_, sprite_pixel) => if bgw_pixel == PixelColorIndex::Zero {
                sprite_palette.get_pixel_color_from_index(sprite_pixel.pixel_color_index)
            } else {
                background_window_palette.get_pixel_color_from_index(bgw_pixel)
            },
        }
    }).collect::<Vec<PixelColor>>().try_into().unwrap()
}


#[derive(Debug, Copy, Clone)]
struct SpriteColoredPixel {
    pixel_color_index: PixelColorIndex,
    hidden_by_bg_and_window: bool,
    palette: SpritePalette
}

impl SpriteColoredPixel {
    pub fn blank() -> Self {
        Self {
            pixel_color_index: PixelColorIndex::Blank,
            hidden_by_bg_and_window: true,
            palette: SpritePalette::Palette0
        }
    }

    pub fn is_blank(self) -> bool {
        self.pixel_color_index == PixelColorIndex::Blank
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PixelColorIndex {
    Blank,
    Zero,
    One,
    Two,
    Three,
}

impl PixelColorIndex {
    pub fn from(bit1: Bit, bit2: Bit) -> Self {
        match (bit1,bit2) {
            (false, false)  => PixelColorIndex::Zero,
            (false, true)   => PixelColorIndex::One,
            (true, false)   => PixelColorIndex::Two,
            (true, true)    => PixelColorIndex::Three,
        }
    }
}
