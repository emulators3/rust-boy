pub mod lcd_control_flags;
pub mod lcd_status_flags;
pub mod pixel_color_palette;
pub mod sprite_attributes;

use crate::gb::memory::io_registers::IoRegistersInternal;
use crate::gb::memory::video_memory::lcd_control_flags::{LcdControlFlags, SpriteSize, TileSelected};
use crate::gb::memory::video_memory::lcd_status_flags::LcdStatusFlags;
use crate::gb::memory::video_memory::pixel_color_palette::PixelColorPalette;
use crate::gb::memory::video_memory::sprite_attributes::SpriteAttributes;
use crate::gb::types::{GbAddr, GbByte};
use crate::utils::constants::Bit;


pub trait VideoMemory: IoRegistersInternal {
    const SPRITE_PATTERN_ADDRESS: GbAddr = 0x8000;
    const OAM_ADDRESS: GbAddr = 0xFE00;
    const STAT_ADDRESS_IN_IO_REGISTERS: u16 = 0x0041;
    const LY_ADDRESS_IN_IO_REGISTERS: u16 = 0x0044;

    fn read_lcdc(&self) -> LcdControlFlags;
    fn read_stat(&self) -> LcdStatusFlags;
    fn write_stat(&mut self, datum: u8);
    fn write_stat_internal(&mut self, stat_register: LcdStatusFlags);
    fn write_ly_internal(&mut self, datum: GbByte);
    fn read_sprite_attributes(&self, sprite_index: GbAddr) -> SpriteAttributes;
    fn read_background_window_palette(&self) -> PixelColorPalette;
    fn read_sprite_palette_0(&self) -> PixelColorPalette;
    fn read_sprite_palette_1(&self) -> PixelColorPalette;
    fn read_line_pixels_from_background_window_tile_map_address(&self, tile_address_in_tile_map: GbAddr, line_in_tile: GbAddr, tile_data_selected: TileSelected) -> [(Bit, Bit); 8];
    fn read_line_pixels_from_sprite_tile_index(&self, sprite_tile_index: GbAddr, line_in_sprite: GbAddr, sprite_size: SpriteSize) -> [(Bit, Bit); 8];
}

