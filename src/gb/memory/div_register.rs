use crate::gb::memory::io_registers::IoRegistersInternal;

pub trait DivRegister: IoRegistersInternal {
    const DIV_ADDRESS_IN_IO_REGISTERS: u16 = 0x0004;

    fn set_div_reset_requested(&mut self, value: bool);
    fn is_div_reset_requested(&self) -> bool;
    fn increment_div(&mut self);
    fn reset_div(&mut self);
}