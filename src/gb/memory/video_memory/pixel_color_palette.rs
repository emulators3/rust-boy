use crate::gb::pixel_color::PixelColor;
use crate::gb::ppu::PixelColorIndex;

#[derive(Debug, Copy, Clone)]
pub struct PixelColorPalette {
    color_for_index_0: PixelColor,
    color_for_index_1: PixelColor,
    color_for_index_2: PixelColor,
    color_for_index_3: PixelColor,
}

impl PixelColorPalette {
    pub fn from(datum: u8) -> Self {
        Self {
            color_for_index_0: Self::get_color(datum & 0b11),
            color_for_index_1: Self::get_color((datum >> 2) & 0b11),
            color_for_index_2: Self::get_color((datum >> 4) & 0b11),
            color_for_index_3: Self::get_color((datum >> 6) & 0b11),
        }
    }

    pub fn get_pixel_color_from_index(&self, pixel_color_index: PixelColorIndex) -> PixelColor {
        match pixel_color_index  {
            PixelColorIndex::Blank => PixelColor::Blank,
            PixelColorIndex::Zero  => self.color_for_index_0,
            PixelColorIndex::One   => self.color_for_index_1,
            PixelColorIndex::Two   => self.color_for_index_2,
            PixelColorIndex::Three => self.color_for_index_3,
        }
    }

    fn get_color(two_bits: u8) -> PixelColor {
        match two_bits {
            0b00 => PixelColor::Lightest,
            0b01 => PixelColor::Light,
            0b10 => PixelColor::Dark,
            0b11 => PixelColor::Darkest,
            _ => panic!("expected two bits, got {two_bits:8b}."),
        }
    }
}
