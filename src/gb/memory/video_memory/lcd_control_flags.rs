use crate::gb::types::GbAddr;
use crate::utils::bitutils::test_bit;

#[derive(Debug, Copy, Clone)]
pub struct LcdControlFlags {
    pub all_displayed: bool,
    window_tile_map_display_selected: TileSelected,
    pub window_displayed: bool,
    pub bg_window_tile_data_selected: TileSelected,
    bg_tile_map_display_selected: TileSelected,
    pub sprite_size: SpriteSize,
    pub sprites_displayed: bool,
    pub bg_window_displayed: bool,
}

impl LcdControlFlags {
    pub fn from(datum: u8) -> Self {
        Self {
            all_displayed: test_bit(datum, 7),
            window_tile_map_display_selected: if test_bit(datum, 6) { TileSelected::Upper } else { TileSelected::Lower },
            window_displayed: test_bit(datum, 5),
            bg_window_tile_data_selected: if test_bit(datum, 4) { TileSelected::Lower } else { TileSelected::Upper },
            bg_tile_map_display_selected: if test_bit(datum, 3) { TileSelected::Upper } else { TileSelected::Lower },
            sprite_size: if test_bit(datum, 2) { SpriteSize::Size8x16 } else { SpriteSize::Size8x8 },
            sprites_displayed: test_bit(datum, 1),
            bg_window_displayed: test_bit(datum, 0),
        }
    }

    pub fn get_sprite_height(&self) -> u8 {
        match self.sprite_size {
            SpriteSize::Size8x8  => 8,
            SpriteSize::Size8x16 => 16,
        }
    }

    pub fn get_window_tile_map_starting_address(&self) -> GbAddr {
        match self.window_tile_map_display_selected {
            TileSelected::Lower => 0x9800,
            TileSelected::Upper => 0x9C00,
        }
    }

    pub fn get_background_tile_map_starting_address(&self) -> GbAddr {
        match self.bg_tile_map_display_selected {
            TileSelected::Lower => 0x9800,
            TileSelected::Upper => 0x9C00,
        }
    }

}

#[derive(Debug, Copy, Clone)]
pub enum TileSelected {
    Upper, Lower,
}

#[derive(Debug, Copy, Clone)]
pub enum SpriteSize {
    Size8x8, Size8x16
}
