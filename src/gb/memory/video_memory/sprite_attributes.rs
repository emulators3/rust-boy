use crate::gb::memory::video_memory::sprite_attributes::SpritePalette::{Palette0, Palette1};
use crate::gb::types::GbAddr;
use crate::utils::bitutils::{get_bit, test_bit};

#[derive(Debug)]
pub struct SpriteAttributes {
    pub y_position: u8,
    pub x_position: u8,
    pub tile_index: u8,
    pub flags: SpriteFlags,
    //useful data, not there in original oam data
    pub oam_address: GbAddr,
}

impl SpriteAttributes {
    pub fn from(data: [u8;4], oam_address: GbAddr) -> Self {
        Self {
            y_position: data[0],
            x_position: data[1],
            tile_index: data[2],
            flags: SpriteFlags::new(data[3]),
            oam_address,
        }
    }
}

#[derive(Debug)]
pub struct SpriteFlags {
    pub hidden_by_bg_and_window: bool,
    pub y_flip: bool,
    pub x_flip: bool,
    pub palette: SpritePalette,
    /*
    next 4 bits are ignored in GB, they're used only in GBC
    tile_vram_bank bit 3
    palette_number bit 2-0
    */
}

impl SpriteFlags {
    fn new(datum: u8) -> Self {
        Self {
            hidden_by_bg_and_window: test_bit(datum, 7),
            y_flip: test_bit(datum, 6),
            x_flip: test_bit(datum, 5),
            palette: SpritePalette::from(get_bit(datum, 4)),
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum SpritePalette {
    Palette0, Palette1
}

impl SpritePalette {
    fn from(bit: u8) -> Self {
        match bit {
            0b0 => Palette0,
            0b1 => Palette1,
            _ => panic!("When reading SpritePalette, I expect only 0 or 1 as values"),
        }
    }
}