use crate::utils::bitutils::{lower_two_bits, test_bit};

#[derive(Debug, Copy, Clone)]
pub struct LcdStatusFlags {
    pub ly_coincidence_interrupt: bool,
    pub mode2_oam_interrupt:      bool,
    pub mode1_vblank_interrupt:   bool,
    pub mode0_hblank_interrupt:   bool,
    pub coincidence_flag:         bool,
    pub mode_flag:                StatMode,
}

impl LcdStatusFlags {

    pub fn from(datum: u8) -> Self {
        LcdStatusFlags {
            ly_coincidence_interrupt: test_bit(datum, 6),
            mode2_oam_interrupt:      test_bit(datum, 5),
            mode1_vblank_interrupt:   test_bit(datum, 4),
            mode0_hblank_interrupt:   test_bit(datum, 3),
            coincidence_flag:         test_bit(datum, 2),
            mode_flag:                StatMode::from(lower_two_bits(datum)),
        }
    }

    pub fn to_byte(self) -> u8 {
        ((self.ly_coincidence_interrupt as u8) << 6) |
            ((self.mode2_oam_interrupt as u8) << 5)      |
            ((self.mode1_vblank_interrupt as u8) << 4)   |
            ((self.mode0_hblank_interrupt as u8) << 3)   |
            ((self.coincidence_flag as u8) << 2)         |
            (self.mode_flag.to_byte())
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum StatMode {
    HBlank,
    VBlank,
    OamSearch,
    DataTransferToLcdDriver,
}

impl StatMode {
    pub fn from(two_bits: u8) -> Self {
        match two_bits {
            0b00 => StatMode::HBlank,
            0b01 => StatMode::VBlank,
            0b10 => StatMode::OamSearch,
            0b11 => StatMode::DataTransferToLcdDriver,
            _ => panic!("expected two bits, got {two_bits:8b}."),
        }
    }

    pub fn to_byte(self) -> u8 {
        match self {
            StatMode::HBlank  => 0b00,
            StatMode::VBlank  => 0b01,
            StatMode::OamSearch  => 0b10,
            StatMode::DataTransferToLcdDriver  => 0b11,
        }
    }
}