use crate::gb::types::GbByte;

pub trait P1Register {
    const P1_ADDRESS_IN_IO_REGISTERS: u16 = 0x0000;

    fn read_p1(&self) -> GbByte;
    fn write_detected_input(&mut self, datum: GbByte);
}