#![allow(non_camel_case_types)]

use crate::gb::memory::io_registers::IoRegistersInternal;
use crate::utils::bitutils::test_bit;
use crate::utils::constants::DigitalAudioOutput;

pub trait AudioRegisters: IoRegistersInternal {
    const AUDIO_MASTER_CONTROL_ADDRESS_IN_IO_REGISTERS: u16 = 0x0026;

    const NR10_ADDRESS_IN_IO_REGISTERS: u16 = 0x0010;
    const NR11_ADDRESS_IN_IO_REGISTERS: u16 = 0x0011;
    const NR12_ADDRESS_IN_IO_REGISTERS: u16 = 0x0012;
    const NR13_ADDRESS_IN_IO_REGISTERS: u16 = 0x0013;
    const NR14_ADDRESS_IN_IO_REGISTERS: u16 = 0x0014;

    const NR20_ADDRESS_IN_IO_REGISTERS: u16 = 0x0015;
    const NR21_ADDRESS_IN_IO_REGISTERS: u16 = 0x0016;
    const NR22_ADDRESS_IN_IO_REGISTERS: u16 = 0x0017;
    const NR23_ADDRESS_IN_IO_REGISTERS: u16 = 0x0018;
    const NR24_ADDRESS_IN_IO_REGISTERS: u16 = 0x0019;

    const NR30_ADDRESS_IN_IO_REGISTERS: u16 = 0x001A;
    const NR31_ADDRESS_IN_IO_REGISTERS: u16 = 0x001B;
    const NR32_ADDRESS_IN_IO_REGISTERS: u16 = 0x001C;
    const NR33_ADDRESS_IN_IO_REGISTERS: u16 = 0x001D;
    const NR34_ADDRESS_IN_IO_REGISTERS: u16 = 0x001E;

    const NR40_ADDRESS_IN_IO_REGISTERS: u16 = 0x001F;
    const NR41_ADDRESS_IN_IO_REGISTERS: u16 = 0x0020;
    const NR42_ADDRESS_IN_IO_REGISTERS: u16 = 0x0021;
    const NR43_ADDRESS_IN_IO_REGISTERS: u16 = 0x0022;
    const NR44_ADDRESS_IN_IO_REGISTERS: u16 = 0x0023;

    const NR50_ADDRESS_IN_IO_REGISTERS: u16 = 0x0024;
    const NR51_ADDRESS_IN_IO_REGISTERS: u16 = 0x0025;

    const BASE_WAVE_RAM_ADDRESS_IN_IO_REGISTERS: u16 = 0x0030;
    const MAX_WAVE_RAM_ADDRESS_IN_IO_REGISTERS: u16 = 0x003F;

    const PCM_12_ADDRESS_IN_IO_REGISTERS: u16 = 0x0076;
    const PCM_34_ADDRESS_IN_IO_REGISTERS: u16 = 0x0077;

    fn read_sound_panning(&self) -> SoundPanning;
    fn read_master_volume(&self) -> MasterVolume;

    fn read_frequency_timer(&self, audio_channel: Channel) -> u16;

    fn read_channel_envelope(&self, channel: Channel) -> Envelope;

    fn read_wave_channel_volume(&self) -> WaveChannelVolume;
    fn read_wave_pattern_sample(&self, index: u8) -> u8;

    fn read_length_timer(&self, channel: Channel) -> u8;
    fn read_square_channel_waveform(&self, channel: SquareChannelNumber) -> WaveForm;
    fn read_square_channel_sweep(&self) -> Sweep;

    fn is_dac_enabled(&self, channel: Channel) -> bool;
    fn is_apu_enabled(&self) -> bool;
    fn read_noise_channel_randomness(&self) -> NoiseChannelRandomness;
    fn get_signal_nr_10(&self) -> Option<u8>;
    fn get_signal_nr_x1(&self, channel: Channel) -> bool;
    fn get_signal_nr_x4(&self, channel: Channel) -> Option<u8>;

    fn write_length_timer(&mut self, datum: u8, channel: Channel);
    fn write_frequency_timer(&mut self, audio_channel: Channel, wavelength: u16) -> u16;
    fn write_channel_status_in_master_register_internal(&mut self, channel: Channel, enabled: bool);

    fn write_pcm12_internal(&mut self, channel1_output: DigitalAudioOutput, channel2_output: DigitalAudioOutput);
    fn write_pcm34_internal(&mut self, channel3_output: DigitalAudioOutput, channel4_output: DigitalAudioOutput);

    fn write_wave_pattern_sample(&mut self, offset:u8, datum: u8);
    fn write_signal_wave_sample_currently_read(&mut self, index:u8, datum: u8);

    fn reset_signal_wave_sample_currently_read(&mut self);

    fn reset_signal_nr_10(&mut self);
    fn reset_signal_nr_x1(&mut self, channel: Channel);
    fn reset_signal_nr_x4(&mut self, channel: Channel);

    fn reset_all_audio_registers(&mut self);
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SquareChannelNumber {
    One, Two
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Channel {
    Square1,
    Square2,
    Wave,
    Noise
}

impl Channel {
    pub fn from(square_channel_number: SquareChannelNumber) -> Self {
        match square_channel_number {
            SquareChannelNumber::One => Channel::Square1,
            SquareChannelNumber::Two => Channel::Square2,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct SoundPanning {
    pub channel_4_in_left_output: bool,
    pub channel_3_in_left_output: bool,
    pub channel_2_in_left_output: bool,
    pub channel_1_in_left_output: bool,
    pub channel_4_in_right_output: bool,
    pub channel_3_in_right_output: bool,
    pub channel_2_in_right_output: bool,
    pub channel_1_in_right_output: bool,
}

impl SoundPanning {
    pub fn from(datum: u8) -> Self {
        Self {
            channel_4_in_left_output: test_bit(datum, 7),
            channel_3_in_left_output: test_bit(datum, 6),
            channel_2_in_left_output: test_bit(datum, 5),
            channel_1_in_left_output: test_bit(datum, 4),
            channel_4_in_right_output: test_bit(datum, 3),
            channel_3_in_right_output: test_bit(datum, 2),
            channel_2_in_right_output: test_bit(datum, 1),
            channel_1_in_right_output: test_bit(datum, 0),
        }
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(non_snake_case)]
pub struct MasterVolume {
    //VIN_in_left_output: bool,
    pub left_output_volume: u8,
    //VIN_in_right_output: bool,
    pub right_output_volume: u8,
}

impl MasterVolume {
    pub fn from(datum: u8) -> Self {
        Self {
            //VIN_in_left_output: test_bit(datum, 7),
            left_output_volume: (datum >> 4) & 0b111,
            //VIN_in_right_output: test_bit(datum, 3),
            right_output_volume: datum & 0b111,
        }
    }
}

pub struct Sweep {
    pub pace: u8,
    pub direction: Direction,
    pub slope: u8
}

impl Sweep {
    pub fn from(datum: u8) -> Self {
        Self {
            pace: (datum >> 4) & 0b111,
            direction: if test_bit(datum, 3) {Direction::Decrease} else {Direction::Increase},
            slope: datum & 0b111,
        }
    }
}

#[derive(Debug)]
pub struct Envelope {
    pub volume: u8,
    pub direction: Direction,
    pub pace: u8
}

impl Envelope {
    pub fn from(datum: u8) -> Self {
        Self {
            volume: (datum >> 4) & 0b1111,
            direction: if test_bit(datum, 3) {Direction::Increase} else {Direction::Decrease},
            pace: datum & 0b111,
        }
    }
}

#[derive(Debug)]
pub struct NoiseChannelRandomness {
    pub clock_shift: u8,
    pub width: LFSRWidth,
    pub clock_divider: u8,
}

impl NoiseChannelRandomness {
    pub fn from(datum: u8) -> Self {
        Self {
            clock_shift: (datum >> 4) & 0b111,
            width: if test_bit(datum, 3) {LFSRWidth::bits_7} else {LFSRWidth::bits_15},
            clock_divider: datum & 0b111,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LFSRWidth {
    bits_15, bits_7
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Direction {
    Increase, Decrease
}

#[derive(Debug, Copy, Clone)]
pub enum WaveChannelVolume {
    Mute, Max, Half, Quarter
}

impl WaveChannelVolume {
    pub fn from(datum: u8) -> Self {
        let bits = (datum >> 5) & 0b11;
        match bits {
            0b00 => WaveChannelVolume::Mute,
            0b01 => WaveChannelVolume::Max,
            0b10 => WaveChannelVolume::Half,
            0b11 => WaveChannelVolume::Quarter,
            _ => panic!("Trying to build a WaveChannelVolume from more than two bits!")
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum WaveForm {
    Zero, One, Two, Three
}

impl WaveForm {
    pub fn from(datum: u8) -> Self {
        match datum {
            0b00 => WaveForm::Zero,
            0b01 => WaveForm::One,
            0b10 => WaveForm::Two,
            0b11 => WaveForm::Three,
            _ => panic!("Trying to build a Waveform from more than two bits!"),
        }
    }
}

impl WaveForm {
    pub fn wave_duty(&self) -> [u8;8] {
        match self {
            WaveForm::Zero =>  [1,1,1,1,1,1,1,0],
            WaveForm::One =>   [0,1,1,1,1,1,1,0],
            WaveForm::Two =>   [0,1,1,1,1,0,0,0],
            WaveForm::Three => [1,0,0,0,0,0,0,1]
        }
    }
}