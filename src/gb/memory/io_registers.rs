#![allow(non_camel_case_types)]
#![allow(clippy::upper_case_acronyms)]

use crate::gb::types::GbByte;

pub trait IoRegistersInternal {
    fn read_io_internal(&self, register: IoReg) -> GbByte;
    fn write_io_internal(&mut self, register: IoReg, datum: GbByte);
}

#[repr(u16)]
#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub enum IoReg {
    P1   = 0xFF00, // (RW) Joypad info
    SB   = 0xFF01, // (RW) Serial transfer data //TODO
    SC   = 0xFF02, // (RW) SIO control //TODO
    //     0xFF03, // (--) (unused)
    DIV  = 0xFF04, // (RW) Divider register (incremented 16384 times a second (16779 ca. on SGB))
    TIMA = 0xFF05, // (RW) Timer counter
    TMA  = 0xFF06, // (RW) Timer modulo (set on timer overflow)
    TAC  = 0xFF07, // (RW) Timer control
    //     0xFF08, // (--) ...
    //   ..0xFF0E, // (--) (unused)
    IF   = 0xFF0F, // (RW) Interrupt flag

    NR10 = 0xFF10, // (RW) Sound mode 1 -
    NR11 = 0xFF11, // (RW) Sound mode 1
    NR12 = 0xFF12, // (RW) Sound mode 1
    NR13 = 0xFF13, // (RW) Sound mode 1
    NR14 = 0xFF14, // (RW) Sound mode 1
    //     0xFF15, // (--) (unused)
    NR21 = 0xFF16, // (RW) Sound mode 2
    NR22 = 0xFF17, // (RW) Sound mode 2
    NR23 = 0xFF18, // (RW) Sound mode 2
    NR24 = 0xFF19, // (RW) Sound mode 2
    NR30 = 0xFF1A, // (RW) Sound mode 3
    NR31 = 0xFF1B, // (RW) Sound mode 3
    NR32 = 0xFF1C, // (RW) Sound mode 3
    NR33 = 0xFF1D, // (RW) Sound mode 3
    NR34 = 0xFF1E, // (RW) Sound mode 3
    //     0xFF1F, // (--) (unused)

    NR41 = 0xFF20, // (RW) Sound mode 4
    NR42 = 0xFF21, // (RW) Sound mode 4
    NR43 = 0xFF22, // (RW) Sound mode 4
    NR44 = 0xFF23, // (RW) Sound mode 4
    NR50 = 0xFF24, // (RW) Channel control
    NR51 = 0xFF25, // (RW) Sound output terminal
    NR52 = 0xFF26, // (RW) Sound on/of
    //     0xFF27, // (--) ...
    //   ..0xFF2F, // (--) (unused)
    WAVE_RAM = 0xFF30,
    // 0xFF30-0xFF3F wave pattern ram

    LCDC = 0xFF40, // (RW) LCD control
    STAT = 0xFF41, // (RW) LCD status
    SCY  = 0xFF42, // (RW) Background scroll Y
    SCX  = 0xFF43, // (RW) Background scroll X
    LY   = 0xFF44, // (RW) LCD Y coordinate
    LYC  = 0xFF45, // (RW) LCD Y compare coordinate
    DMA  = 0xFF46, // (RW) DMA transfer and start address
    BGP  = 0xFF47, // (RW) BG & Window palette data
    OBP0 = 0xFF48, // (RW) Object Palette 0 data
    OBP1 = 0xFF49, // (RW) Object Palette 1 data
    WY   = 0xFF4A, // (RW) Window Y position
    WX   = 0xFF4B  // (RW) Window X position
}