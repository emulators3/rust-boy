use crate::gb::memory::div_register::DivRegister;
use crate::gb::memory::io_registers::{IoReg, IoRegistersInternal};
use crate::gb::types::{GbByte, GbWord};
use crate::utils::bitutils::{test_bit, test_bit_16};

/*
This timer is mimicking really how the timer works in the GB: two bytes for both DIV and timer counter
When resetting DIV, the whole counter is reset, and that may generate TIMA increases
*/
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Timer {
    counter: GbWord,
    tac: GbByte,
    timer_overflow_last_tima_value: GbByte,
    timer_interrupt_cycles_wait_time: GbByte,
    timer_overflow_happened: bool,
    timer_interrupt_request: bool,
}

#[repr(u8)]
#[derive(Debug, Copy, Clone)]
enum TimerUpdateFrequency {
    Zero = 9,
    One = 3,
    Two = 5,
    Three = 7
}

impl TimerUpdateFrequency {
    pub fn from(tac: u8) -> Self {
        match tac & 0b11 {
            0b00 => Self::Zero,
            0b01 => Self::One,
            0b10 => Self::Two,
            0b11 => Self::Three,
            _ => panic!("When mapping to TimerUpdateFrequency we expect only two bits")
        }
    }
}

impl Timer {
    pub fn new() -> Self {
        Self {
            counter: 0,
            tac: 0b11111000,
            timer_overflow_last_tima_value: 0,
            timer_interrupt_cycles_wait_time: 0,
            timer_overflow_happened: false,
            timer_interrupt_request: false,
        }
    }

    pub fn dump(&self, io_regs: & impl IoRegistersInternal) {
        print!("\tCOUNTER:{} TAC:{:08b} TIMA:{:08b} TMA:{:08b} DIV:{:08b}", self.counter, self.tac, io_regs.read_io_internal(IoReg::TIMA), io_regs.read_io_internal(IoReg::TMA), io_regs.read_io_internal(IoReg::DIV));
    }

    pub fn tick(&mut self, io_regs: &mut impl DivRegister) {
        let old_counter = self.counter;
        self.counter = self.counter.overflowing_add(1).0;

        self.manage_counter_change(old_counter, io_regs);
    }

    pub fn manage_div_reset(&mut self, io_regs: &mut impl DivRegister) {
        if io_regs.is_div_reset_requested() {
            io_regs.set_div_reset_requested(false);
            io_regs.reset_div();
            self.reset_counter_and_manage_change(io_regs);
        }
    }

    pub fn manage_timer_overflow(&mut self, io_regs: &mut impl IoRegistersInternal) {
        //if overflow happened, we have to wait 4 cycles before requesting the interrupt
        if self.timer_overflow_happened {
            //this is the last cycle before the cycle where CPU would execute/TIMA would be overridden
            //and we want interrupt to be serviced at this M-cycle, so we need to write IF before actually running cpu
            if self.timer_interrupt_cycles_wait_time == 1 {
                self.timer_interrupt_request = true;
            }
            if self.timer_interrupt_cycles_wait_time > 0 {
                self.timer_overflow_last_tima_value = io_regs.read_io_internal(IoReg::TIMA);
                self.timer_interrupt_cycles_wait_time -= 1;
            } else {
                //now we can request the interrupt and reload TMA into TIMA, but if and only if TIMA value was still 00 until T-1
                //otherwise interrupt will not happen. Why T-1? Because if it has been written now, at the TMA reload cycle, then that write
                //should be ignored.
                self.timer_overflow_happened = false;
                if self.timer_overflow_last_tima_value == 0 {
                    let tma = io_regs.read_io_internal(IoReg::TMA);
                    io_regs.write_io_internal(IoReg::TIMA, tma);
                }
            }
        } else {
            //if overflow was resetted on previous cycle, let's put the timer_interrupt_request to false
            self.timer_interrupt_request = false;
        }
    }

    pub fn is_timer_interrupt_requested(&self) -> bool {
        self.timer_interrupt_request
    }

    fn reset_counter_and_manage_change(&mut self, io_regs: &mut impl DivRegister) {
        let old_counter = self.counter;
        self.counter = 0;

        self.manage_counter_change_for_reset(old_counter, io_regs);
    }

    pub fn manage_counter_change(&mut self, old_counter: u16, io_regs: &mut impl DivRegister) {
        let current_counter = self.counter;
        let timer_enabled = test_bit(self.tac, 2);
        let timer_update_frequency = TimerUpdateFrequency::from(self.tac);

        //div is always checked/increased even if timer disabled
        //this is just a way to not overwrite every time DIV but just when it increases...
        if check_div_increase(old_counter, current_counter) {
            io_regs.increment_div();
        }

        //now let's check for TIMA increase given the TAC frequency
        if timer_enabled && check_increase_for_frequency(old_counter, current_counter, timer_update_frequency) {
            self.increase_tima(io_regs);
        }
    }

    fn manage_counter_change_for_reset(&mut self, old_counter: u16, io_regs: &mut impl DivRegister) {
        let current_counter = self.counter;
        let timer_enabled = test_bit(self.tac, 2);
        let timer_update_frequency = TimerUpdateFrequency::from(self.tac);

        //now let's check for TIMA increase given the TAC frequency
        if timer_enabled && check_increase_for_frequency(old_counter, current_counter, timer_update_frequency) {
            self.increase_tima(io_regs);
        }
    }


    /*
       Here we manage the tac change glitch: if the previous output of the counter multiplexer was 1,
       then if the timer was enabled and now it's disabled, it's like an edge fall, so TIMA increases

       TIMA increases also if timer was and still is enabled, and the multiplexer change would result in
       the counter output change from 1 to 0
     */
    pub fn manage_tac_change(&mut self, io_regs: &mut impl IoRegistersInternal) {
        let new_tac = io_regs.read_io_internal(IoReg::TAC);

        if self.tac != new_tac {
            let old_tac = self.tac;
            let old_timer_enabled = test_bit(old_tac, 2);
            let new_timer_enabled = test_bit(new_tac, 2);

            let old_timer_update_frequency = TimerUpdateFrequency::from(old_tac);
            let new_timer_update_frequency = TimerUpdateFrequency::from(new_tac);

            //TAC glitch
            #[allow(clippy::collapsible_if)]
            if old_timer_enabled && test_bit_16(self.counter, old_timer_update_frequency as u8) {
                if !new_timer_enabled || test_bit_16(self.counter, new_timer_update_frequency as u8) {
                    self.increase_tima(io_regs);
                }
            }
        }

        self.tac = new_tac;
    }


    fn increase_tima(&mut self, io_regs: &mut impl IoRegistersInternal) {
        let old_tima = io_regs.read_io_internal(IoReg::TIMA);
        let (result, overflow) = old_tima.overflowing_add(1);

        io_regs.write_io_internal(IoReg::TIMA, result);

        //for one cycle, TIMA will be 0x00 - only next cycle (4 t-cycles) we will reload from TMA and request interrupt
        if overflow {
            self.tima_overflow_happened();
        }
    }

    fn tima_overflow_happened(&mut self) {
        self.timer_overflow_happened = true;
        self.timer_interrupt_cycles_wait_time = 4;
    }

}

//DIV will increase if the bit 7 was 1 and now is 0 - it means 256 clocks happened
fn check_div_increase(old_counter: u16, current_counter: u16) -> bool {
    check_increase_for_frequency(old_counter, current_counter, TimerUpdateFrequency::Three)
}

fn check_increase_for_frequency(old_counter: u16, current_counter: u16, timer_update_frequency: TimerUpdateFrequency) -> bool {
    test_bit_16(old_counter, timer_update_frequency as u8) && !test_bit_16(current_counter, timer_update_frequency as u8)
}

#[cfg(test)]
mod timer_test;
