use crate::gb::cartridge::Cartridge;
use crate::gb::input_manager::manage_input;
use crate::gb::joypad_state::{JoypadKey, JoypadState};
use crate::gb::memory::io_registers::{IoReg, IoRegistersInternal};
use crate::gb::memory::p1_register::P1Register;
use crate::gb::mmu::Mmu;

#[test]
fn manage_input_should_write_0_to_bit_if_corresponding_action_is_enabled() {
    for test_data in [
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::A, expected_p1:0b00011110 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::B, expected_p1:0b00011101 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::Select, expected_p1:0b00011011 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::Start, expected_p1:0b00010111 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::Right, expected_p1:0b00101110 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::Left, expected_p1:0b00101101 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::Up, expected_p1:0b00101011 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::Down, expected_p1:0b00100111 },
    ] {
        let mut mmu = setup();
        let mut state = JoypadState::new();

        write_full_p1_register(&mut mmu, test_data.initial_p1);

        state.key_down(test_data.key_pressed);

        manage_input(state, &mut mmu);

        let mut expected_mmu = setup();
        write_full_p1_register(&mut expected_mmu, test_data.expected_p1);

        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn manage_input_should_write_0_to_bit_if_both_bit_5_6_are_enabled() {
    for test_data in [
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::A, expected_p1:0b00001110 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::B, expected_p1:0b00001101 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::Select, expected_p1:0b00001011 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::Start, expected_p1:0b00000111 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::Right, expected_p1:0b00001110 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::Left, expected_p1:0b00001101 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::Up, expected_p1:0b00001011 },
        InputTestData { initial_p1: 0b00001111, key_pressed: JoypadKey::Down, expected_p1:0b00000111 },
    ] {
        let mut mmu = setup();
        let mut state = JoypadState::new();

        write_full_p1_register(&mut mmu, test_data.initial_p1);
        state.key_down(test_data.key_pressed);

        manage_input(state, &mut mmu);

        let mut expected_mmu = setup();
        write_full_p1_register(&mut expected_mmu, test_data.expected_p1);

        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn manage_input_should_not_write_0_to_bit_if_corresponding_action_is_not_enabled() {
    for test_data in [
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::A, expected_p1:0b00101111 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::B, expected_p1:0b00101111 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::Select, expected_p1:0b00101111 },
        InputTestData { initial_p1: 0b00101111, key_pressed: JoypadKey::Start, expected_p1:0b00101111 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::Right, expected_p1:0b00011111 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::Left, expected_p1:0b00011111 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::Up, expected_p1:0b00011111 },
        InputTestData { initial_p1: 0b00011111, key_pressed: JoypadKey::Down, expected_p1:0b00011111 },
    ] {
        let mut mmu = setup();
        let mut state = JoypadState::new();

        write_full_p1_register(&mut mmu, test_data.initial_p1);
        state.key_down(test_data.key_pressed);

        manage_input(state, &mut mmu);

        let mut expected_mmu = setup();
        write_full_p1_register(&mut expected_mmu, test_data.expected_p1);

        assert_eq!(mmu, expected_mmu);
    }
}

#[test]
fn manage_input_should_not_write_0_to_bit_if_nothing_is_enabled() {
    for test_data in [
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::A, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::B, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::Select, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::Start, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::Right, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::Left, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::Up, expected_p1:0b00111111 },
        InputTestData { initial_p1: 0b00111111, key_pressed: JoypadKey::Down, expected_p1:0b00111111 },
    ] {
        let mut mmu = setup();
        let mut state = JoypadState::new();

        write_full_p1_register(&mut mmu, test_data.initial_p1);
        state.key_down(test_data.key_pressed);

        manage_input(state, &mut mmu);

        let mut expected_mmu = setup();
        write_full_p1_register(&mut expected_mmu, test_data.expected_p1);

        assert_eq!(mmu, expected_mmu);
    }
}

fn write_full_p1_register(mmu: &mut Mmu, datum: u8) {
    mmu.write_io_internal(IoReg::P1, datum);
    mmu.write_detected_input(datum);
}

#[derive(Debug)]
pub struct InputTestData {
    pub initial_p1: u8,
    pub key_pressed: JoypadKey,
    pub expected_p1: u8
}

pub fn setup() -> Mmu {
    let cartridge = Cartridge::from_binary_rom([0u8;0x8000].to_vec(), None);
    Mmu::new(cartridge)
}