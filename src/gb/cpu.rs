mod registers;
mod flags;
mod microopcode;
mod cache;
mod alu;
mod flow_control;
mod bit_operation;

use std::collections::VecDeque;
use crate::utils::bitutils::*;

use crate::gb::types::{
    GbByte, GbWord,
    GbAddr,
    GbFlag,
};

use flags::Flags;
use registers::Registers
;
use crate::gb::cartridge::Cartridge;
use crate::gb::cpu::cache::Cache;
use crate::gb::cpu::CpuStatus::*;
use crate::gb::cpu::microopcode::MicroOpcode;
use crate::gb::cpu::registers::{Reg16, Reg8};
use crate::gb::interrupt::Interrupt;
use crate::gb::memory::io_registers::IoReg;
use crate::gb::memory::Memory;
use crate::utils::constants::IE_ADDRESS;

#[derive(Debug, PartialEq)]
enum CpuStatus {
    Stopped,
    Halted,
    Running
}

#[derive(Debug, PartialEq)]
pub struct Cpu {
    registers: Registers, // general registers

    pub stack_pointer:   GbWord, // SP
    pub program_counter: GbAddr, // PC

    pub previous_program_counter: GbAddr,
    
    flags: Flags, // general flags
    
    next_can_interrupt: Option<GbFlag>, // set by EI/DI to update `can_interrupt` after a step

    ime: GbFlag,

    status: CpuStatus,
    halt_bug: bool,
    stop_skip_byte: bool,

    cache: Cache,
    cached_micro_opcodes: VecDeque<MicroOpcode>,
}

impl Cpu {
    pub fn new(_cartridge: &Cartridge) -> Self {
/*        let gb_model = cartridge.read_rom_bank0(0x0143 as GbAddr);
        
        let a_register = match gb_model {
            // XXX: eventually add SGB?
            0x80 | 0xC0 => 0x11, // GBC or GBC compatible
            _ => 0x01,           // default to GB
        };*/

        //XXX for now it's always GB
        let a_register =  0x01;


        Self {
            registers: Registers::new(a_register),
            flags: Flags::init(),

            stack_pointer:   0xFFFE,
            program_counter: 0x0100,
            previous_program_counter: 0,
            
            // TODO: check
            next_can_interrupt: None,
            ime: true,

            status: Running,

            halt_bug: false,
            stop_skip_byte: false,

            cache: Cache::new(),
            cached_micro_opcodes: VecDeque::new()
        }
    }
}

//stack helpers
impl Cpu {

    pub fn push(&mut self, mem: &mut impl Memory, datum: GbByte) {
        mem.write(self.decrement_stack_pointer(), datum);
    }

    pub fn pop(&mut self, mem: &mut impl Memory) -> GbByte {
        let datum = mem.read(self.stack_pointer);
        self.increment_stack_pointer();
        datum
    }

    fn push_word(&mut self, mem: &mut impl Memory, datum: GbWord) {
        self.push(mem, higher_byte(datum));
        self.push(mem, lower_byte(datum));
    }

    fn decrement_stack_pointer(&mut self) -> GbWord {
        self.stack_pointer = self.stack_pointer.wrapping_sub(1);
        self.stack_pointer
    }
    fn increment_stack_pointer(&mut self) -> GbWord {
        self.stack_pointer = self.stack_pointer.wrapping_add(1);
        self.stack_pointer
    }
}

// Step
impl Cpu {

    pub fn dump(&mut self) {
        print!("\t{:4x}\tA:{:2x} F:{:2x} BC:{:4x} DE:{:4x} HL:{:4x} SP:{:4x}",
               self.previous_program_counter,
               self.registers.read8(Reg8::A),
               self.flags.as_byte(),
               self.registers.read16(Reg16::BC),
               self.registers.read16(Reg16::DE),
               self.registers.read16(Reg16::HL),
               self.stack_pointer
        );
    }

    pub fn step(&mut self, mem: &mut impl Memory) -> u32 {

        // this is to enable the IE delayed behaviour. if this value was enabled on previous cycle,
        //it will be assigned now to can_interrupt (that is IME)
        if let Some(val) = self.next_can_interrupt {
            self.ime = val;
            self.next_can_interrupt = None;
        }

        let interrupt_requests = mem.read_io_internal(IoReg::IF);
        //TODO these is not really IO and should be changed
        let enabled_interrupts = mem.read(IE_ADDRESS);
        
        let maybe_interrupt_requested = highest_priority_enabled_interrupt(interrupt_requests, enabled_interrupts);

        match self.status {
            Stopped => {
                if self.stop_skip_byte {
                    self.program_counter +=1;
                    self.stop_skip_byte = false;
                }

                let p1 = mem.read_io_internal(IoReg::P1);

                let button_pressed = !test_bit(p1, 0) ||
                    !test_bit(p1, 1) ||
                    !test_bit(p1, 2) ||
                    !test_bit(p1, 3);

                if button_pressed {
                    self.status = Running;
                }
            },
            Halted => {
                //skips until an interrupt is requested
                if maybe_interrupt_requested.is_some() {
                    self.status = Running;
                }
            },
            Running => {
                if can_service_interrupt(maybe_interrupt_requested.is_some(), self.ime, self.cached_micro_opcodes.is_empty()) {
                    let interrupt = maybe_interrupt_requested.unwrap();
                    // reset the corresponding bit of the interrupt being handled
                    mem.write_io_internal(IoReg::IF, reset_bit(interrupt_requests, interrupt.index()));
                    // call the interrupt
                    self.ime = false;
                    self.push_word(mem, self.program_counter);
                    self.program_counter = interrupt.address();
                } else {
                    self.previous_program_counter = self.program_counter;
                    if self.cached_micro_opcodes.is_empty() {
                        let mut mem_iter = mem.iter(self.program_counter, self.halt_bug);

                        if self.halt_bug {
                            self.halt_bug = false;
                        }

                        self.cached_micro_opcodes = MicroOpcode::parse(&mut mem_iter);
                        self.program_counter = mem_iter.address;
                    }

                    let mut cache = self.cache;
                    self.cached_micro_opcodes.pop_front().unwrap().instruction(self, mem, &mut cache);
                    self.cache = cache;
                }
            },
        }
        3
    }
}

fn can_service_interrupt(is_interrupt_requested: bool, ime: GbFlag, is_cache_microopcode_empty: bool) -> bool {
    is_interrupt_requested && ime && is_cache_microopcode_empty
}

fn highest_priority_enabled_interrupt(interrupt_requests: u8, enabled_interrupts: u8) -> Option<Interrupt> {
    use Interrupt::*;

    let vblank   = check_enabled(VBlank,  interrupt_requests, enabled_interrupts);
    let lcd_stat = check_enabled(LcdStat, interrupt_requests, enabled_interrupts);
    let timer    = check_enabled(Timer,   interrupt_requests, enabled_interrupts);
    let serial   = check_enabled(Serial,  interrupt_requests, enabled_interrupts);
    let joypad   = check_enabled(Joypad,  interrupt_requests, enabled_interrupts);

    /**/ if vblank   { Some(VBlank)  }
    else if lcd_stat { Some(LcdStat) }
    else if timer    { Some(Timer)   }
    else if serial   { Some(Serial)  }
    else if joypad   { Some(Joypad)  }
    else /*********/ { None /******/ }
}

const fn check_enabled(interrupt: Interrupt, interrupt_requests: u8, enabled_interrupts: u8) -> bool {
    let mask = interrupt.mask();
    (interrupt_requests & mask == mask) && (enabled_interrupts & mask == mask)
}
