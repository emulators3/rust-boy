use channels::noise_channel::NoiseChannel;
use channels::square_channel::SquareChannel;
use channels::wave_channel::WaveChannel;
use crate::gb::apu::div_apu_step::DivApuStep;
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, Direction, SoundPanning, SquareChannelNumber};
use crate::gb::stereo_analog_audio_output::StereoAnalogAudioOutput;
use crate::utils::constants::{AnalogAudioOutput, DigitalAudioOutput};

mod channels;
mod div_apu_step;

const SQUARE_CHANNEL_TICKS: u8 = 2;
const NOISE_CHANNEL_TICKS: u8 = 8;

trait AudioChannel {
    fn run(&mut self, audio_registers: &mut impl AudioRegisters) -> DigitalAudioOutput;
    fn disable(&mut self, audio_registers: &mut impl AudioRegisters);
    fn is_enabled(&self) -> bool;
}

//tick is only used to understand when to call the different channels
//apu is called 2Mhz times but for example noise can be called 262khz and pulse 1Mhz times
#[derive(Debug, Copy, Clone)]
pub struct Apu {
    square_channel1: SquareChannel,
    square_channel2: SquareChannel,
    wave_channel: WaveChannel,
    noise_channel: NoiseChannel,
    ticks: u8,
    previous_samples: PreviousSamples,
    enabled: bool,
    div_apu_step: DivApuStep,
}

#[derive(Debug, Copy, Clone)]
struct PreviousSamples {
    square1: u8,
    square2: u8,
    noise: u8
}

impl PreviousSamples {
    pub fn new() -> Self {
        Self {
            square1: 7,
            square2: 7,
            noise: 7
        }
    }
}

impl Apu {
    pub fn new() -> Self {
        Self {
            square_channel1: SquareChannel::new(SquareChannelNumber::One),
            square_channel2: SquareChannel::new(SquareChannelNumber::Two),
            wave_channel: WaveChannel::new(),
            noise_channel: NoiseChannel::new(),
            ticks: 0,
            previous_samples: PreviousSamples::new(),
            enabled: false,
            div_apu_step: DivApuStep::Zero,
        }
    }

    //as said, we count ticks to understand when to call the various channels. If it's still not the
    //time to enter them, then we use the previously saved sample. Remember that we call the APU
    //at 2Mhz speed, so only the wave channels gets called every time.
    pub fn run(&mut self, audio_registers: &mut impl AudioRegisters) -> StereoAnalogAudioOutput {
        if audio_registers.is_apu_enabled() {

            //if we're re-enabling the APU, then frame sequencer "needs to be set such that next step is Zero"
            //this means...setting it at the last step.
            if !self.enabled {
                self.div_apu_step = DivApuStep::Seven;
            }

            self.enabled = true;
            self.ticks += 1;

            let square_channel1_digital_output = if (self.ticks % SQUARE_CHANNEL_TICKS) == 0 {
                run_channel_if_dac_enabled(&mut self.square_channel1, audio_registers, Channel::Square1)
            } else {
                self.previous_samples.square1
            };

            let square_channel2_digital_output = if (self.ticks % SQUARE_CHANNEL_TICKS) == 0 {
                run_channel_if_dac_enabled(&mut self.square_channel2, audio_registers, Channel::Square2)
            } else {
                self.previous_samples.square2
            };

            let wave_channel_digital_output = run_channel_if_dac_enabled(&mut self.wave_channel, audio_registers, Channel::Wave);

            let noise_channel_digital_output = if (self.ticks % NOISE_CHANNEL_TICKS) == 0 {
                self.ticks = 0;
                run_channel_if_dac_enabled(&mut self.noise_channel, audio_registers, Channel::Noise)
            } else {
                self.previous_samples.noise
            };

            audio_registers.write_pcm12_internal(square_channel1_digital_output, square_channel2_digital_output);
            audio_registers.write_pcm34_internal(wave_channel_digital_output, noise_channel_digital_output);

            self.previous_samples.square1 = square_channel1_digital_output;
            self.previous_samples.square2 = square_channel2_digital_output;
            self.previous_samples.noise = noise_channel_digital_output;

            let sound_panning = audio_registers.read_sound_panning();
            let master_volume = audio_registers.read_master_volume();

            let (left_digital_output, right_digital_output) = mix_panned_audio(square_channel1_digital_output,
                                                                                             square_channel2_digital_output,
                                                                                             wave_channel_digital_output,
                                                                                             noise_channel_digital_output, sound_panning);

            let left_analog_output = apply_master_volume(
                map_digital_to_analog(left_digital_output),
                master_volume.left_output_volume
            );

            let right_analog_output = apply_master_volume(
                map_digital_to_analog(right_digital_output),
                master_volume.right_output_volume
            );

            StereoAnalogAudioOutput {
                left_output: left_analog_output,
                right_output: right_analog_output,
            }

        } else {
            if self.enabled {
                self.enabled = false;
                self.ticks = 0;
                self.previous_samples = PreviousSamples::new();

                self.square_channel1.disable_by_apu_off(audio_registers);
                self.square_channel2.disable_by_apu_off(audio_registers);
                self.wave_channel.disable_by_apu_off(audio_registers);
                self.noise_channel.disable(audio_registers);

                audio_registers.reset_all_audio_registers();
            }
            StereoAnalogAudioOutput::mute()
        }
    }

    pub fn sweep_direction_overridden(&mut self, direction: Direction, audio_registers: &mut impl AudioRegisters) {
        self.square_channel1.sweep_direction_overridden(direction, audio_registers);
    }

    pub fn channel_length_counter_written(&mut self, channel: Channel) {
        match channel {
            Channel::Square1 => self.square_channel1.length_counter_written(),
            Channel::Square2 => self.square_channel2.length_counter_written(),
            Channel::Wave => self.wave_channel.length_counter_written(),
            Channel::Noise => self.noise_channel.length_counter_written(),
        }
    }

    pub fn sync_length(&mut self, length_enabled: bool, audio_registers: &mut impl AudioRegisters, channel: Channel) {
        match channel {
            Channel::Square1 => self.square_channel1.sync_length(length_enabled, audio_registers, self.div_apu_step),
            Channel::Square2 => self.square_channel2.sync_length(length_enabled, audio_registers, self.div_apu_step),
            Channel::Wave => self.wave_channel.sync_length(length_enabled, audio_registers, self.div_apu_step),
            Channel::Noise => self.noise_channel.sync_length(length_enabled, audio_registers, self.div_apu_step),
        }
    }

    //frame sequencer clock works only when APU is enabled. Remember that whenever disabled, it resets to Zero
    pub fn div_apu_tick(&mut self, audio_registers: &mut impl AudioRegisters) {
        if self.enabled {
            self.div_apu_step = self.div_apu_step.next();
            self.square_channel1.div_apu_tick(self.div_apu_step, audio_registers);
            self.square_channel2.div_apu_tick(self.div_apu_step, audio_registers);
            self.wave_channel.div_apu_tick(self.div_apu_step, audio_registers);
            self.noise_channel.div_apu_tick(self.div_apu_step, audio_registers);
        }
    }

    pub fn trigger(&mut self, channel: Channel, audio_registers: &mut impl AudioRegisters) {
        match channel {
            Channel::Square1 => self.square_channel1.trigger(audio_registers, self.div_apu_step),
            Channel::Square2 => self.square_channel2.trigger(audio_registers, self.div_apu_step),
            Channel::Wave => self.wave_channel.trigger(audio_registers, self.div_apu_step),
            Channel::Noise => self.noise_channel.trigger(audio_registers, self.div_apu_step),
        }
    }

}


fn run_channel_if_dac_enabled(audio_channel: &mut impl AudioChannel, audio_registers: &mut impl AudioRegisters, channel: Channel) -> DigitalAudioOutput {
    if audio_registers.is_dac_enabled(channel) {
        audio_channel.run(audio_registers)
    } else {
        //if DAC is disabled, channel is disabled and output fades into digital 7.5 (analog 0)
        //putting 7 because I'm too lazy to change all types to cater for 7.5
        if audio_channel.is_enabled() {
            audio_channel.disable(audio_registers);
        }
        0
    }
}

fn mix_panned_audio(square1: DigitalAudioOutput,
                    square2: DigitalAudioOutput,
                    wave: DigitalAudioOutput,
                    noise: DigitalAudioOutput,
                    sound_panning: SoundPanning) -> (DigitalAudioOutput, DigitalAudioOutput) {

    let left_output = if sound_panning.channel_1_in_left_output { square1 } else { 0 } +
        if sound_panning.channel_2_in_left_output { square2 } else { 0 } +
        if sound_panning.channel_3_in_left_output { wave } else { 0 } +
        if sound_panning.channel_4_in_left_output { noise } else { 0 };

    let right_output = if sound_panning.channel_1_in_right_output { square1 } else { 0 } +
        if sound_panning.channel_2_in_right_output { square2 } else { 0 } +
        if sound_panning.channel_3_in_right_output { wave } else { 0 } +
        if sound_panning.channel_4_in_right_output { noise } else { 0 };

    (left_output, right_output)
}

fn apply_master_volume(analog_audio: AnalogAudioOutput,
                       volume_output: u8) -> AnalogAudioOutput {

    //remember it goes from 0 to 7 but we want it from 1 to 8
    let volume_modifier = volume_output + 1;

    //we need to temporarily extend from i16 to i32 so that we can multiply first and >> later
    //the idea is that the range goes from 12.5% to 100% (so in steps of 1/8), so we can multiply by
    //the value of master_volume and later shift 3 times (divide by 8)

    (((analog_audio as i32) * volume_modifier as i32) >> 3) as i16
}

const MAX_DIGITAL_OUTPUT: DigitalAudioOutput = 0xF * 4;
const ANALOG_RANGE: f64 = AnalogAudioOutput::MAX as f64 - AnalogAudioOutput::MIN as f64;

fn map_digital_to_analog(digital_output: DigitalAudioOutput) -> AnalogAudioOutput {
    //digital_output / max_digital_output gives me where I am in the current range (the one that goes from 0 to F)
    //then I can multiply it in the new range number
    //I'm using f64 because I need floating point precision when dividing and multiplying
    //I'm also negating the result because the conversion maps in opposite (0 -> 1, F -> -1)
    let position_in_digital_range: f64 = digital_output as f64 / MAX_DIGITAL_OUTPUT as f64;

    -((position_in_digital_range * ANALOG_RANGE) - AnalogAudioOutput::MAX as f64) as AnalogAudioOutput
}

