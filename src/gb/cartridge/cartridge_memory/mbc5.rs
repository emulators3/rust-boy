use crate::gb::cartridge::cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::rom::Rom;
use crate::gb::cartridge::cartridge_memory::{Mbc, RamSize, RomSize};
use crate::gb::types::{GbAddr, GbByte};
use crate::utils::bitutils::{join_bytes, lower_half};
use crate::utils::constants::Bit;

#[derive(Debug, Copy, Clone, PartialEq)]
enum Mbc5RamSize {
    KB128, KB32, KB8, Zero
}

impl Mbc5RamSize {
    pub fn from(ram_size: RamSize) -> Self {
        match ram_size {
            RamSize::Zero => Mbc5RamSize::Zero,
            RamSize::KB8 =>  Mbc5RamSize::KB8,
            RamSize::KB32 => Mbc5RamSize::KB32,
            RamSize::KB128 => Mbc5RamSize::KB128,
            n => panic!("MBC5 only supports 8, 32 and 128 RAM banks. Ram size passed: {:?}", n),
        }
    }

    pub fn mask(&self) -> u8 {
        match self {
            Mbc5RamSize::Zero => 0b0,
            Mbc5RamSize::KB8 =>  0b0,
            Mbc5RamSize::KB32 => 0b11,
            Mbc5RamSize::KB128 => 0b1111,
        }
    }
}


#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Mbc5 {
    primary_rom_bank_register: u8,
    secondary_rom_bank_register: Bit,

    rom_bank_index: u16, //9 bits
    ram_bank_index: u8, //2 bits

    ram_size: Mbc5RamSize,
    rom_bank_index_mask: u16,

    ram_enabled: bool,
}

impl Mbc for Mbc5 {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[0][rom_address as usize]
    }

    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[self.rom_bank_index as usize][rom_address as usize]
    }

    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte {
        if self.ram_non_empty_and_enabled() {
            ram[self.ram_bank_index as usize][ram_address as usize]
        } else {
            0xFF
        }
    }

    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte) {
        if self.ram_non_empty_and_enabled() {
            ram[self.ram_bank_index as usize][ram_address as usize] = datum;
        }
    }

    fn execute(&mut self, datum: GbByte, address: GbAddr) {
        match address {
            0x0000..=0x1FFF => {
                self.ram_enabled = should_enable_ram(datum)
            },

            0x2000..=0x2FFF => {
                self.primary_rom_bank_register = datum;
                self.regenerate_rom_bank_index();
            }

            0x3000..=0x3FFF => {
                self.secondary_rom_bank_register = (datum & 0b1) == 1;
                self.regenerate_rom_bank_index()
            }

            0x4000..=0x5FFF => {
                self.ram_bank_index = datum & self.ram_size.mask();
            }

            _ => panic!("Tried to execute a command on MBC5 by writing at {:04x} - not supported!", address)
        }
    }
}


impl Mbc5 {
    pub fn from(rom_size: RomSize, ram_size: RamSize) -> Self {
        Self {
            primary_rom_bank_register: 0,
            secondary_rom_bank_register: false,

            rom_bank_index: 0,
            ram_bank_index: 0,

            ram_size: Mbc5RamSize::from(ram_size),
            rom_bank_index_mask: rom_size.mask(),

            ram_enabled: false,
        }
    }

    fn regenerate_rom_bank_index(&mut self) {
        let higher_byte: u8 = self.secondary_rom_bank_register as u8;
        self.rom_bank_index = join_bytes(higher_byte, self.primary_rom_bank_register) & self.rom_bank_index_mask;
    }

    fn ram_non_empty_and_enabled(&self) -> bool {
        self.ram_size != Mbc5RamSize::Zero && self.ram_enabled
    }

}


fn should_enable_ram(datum: GbByte) -> bool {
    matches!(lower_half(datum), 0xA)
}

#[cfg(test)]
mod mbc5_test;
