use crate::gb::cartridge::cartridge_memory::{Mbc, RamSize};
use crate::gb::cartridge::cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::rom::Rom;
use crate::gb::types::{GbAddr, GbByte};


#[derive(Debug, Copy, Clone, PartialEq)]
enum NoMbcRamSize {
    Zero, KB8
}

impl NoMbcRamSize {
    pub fn from(ram_size: RamSize) -> Self {
        match ram_size {
            RamSize::Zero => NoMbcRamSize::Zero,
            RamSize::KB8 => NoMbcRamSize::KB8,
            n => panic!("NoMBC only supports 8KB RAM banks. Ram size id passed: {:?}", n),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct NoMbc {
    ram_size: NoMbcRamSize
}

impl NoMbc {
    pub fn from(ram_size: RamSize) -> Self {
        Self {
            ram_size: NoMbcRamSize::from(ram_size),
        }
    }
}

impl Mbc for NoMbc {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[0][rom_address as usize]
    }

    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[1][rom_address as usize]
    }

    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte {
        if self.ram_size == NoMbcRamSize::KB8 {
            ram[0][ram_address as usize]
        } else {
            0xFF
        }
    }

    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte) {
        if self.ram_size == NoMbcRamSize::KB8 {
            ram[0][ram_address as usize] = datum
        }
    }

    fn execute(&mut self, _datum: GbByte, _address: GbAddr) {}
}

#[cfg(test)]
mod no_mbc_test;
