use std::ops::Deref;
use crate::gb::cartridge::cartridge_memory::RamSize;
use crate::gb::types::{GbByte, RamBank};
use crate::utils::constants::RAM_BANK_SIZE;

#[derive(Debug, PartialEq)]
pub enum Ram {
    Banks0(Box<[RamBank;  0]>),
    Banks1(Box<[RamBank;  1]>),
    Banks4(Box<[RamBank;  4]>),
    Banks16(Box<[RamBank; 16]>),
    Banks8(Box<[RamBank; 8]>),
}

impl Ram {

    pub fn init(ram_size: RamSize, maybe_save_data: Option<Vec<u8>>) -> Ram {
        match maybe_save_data {
            None => init_without_save(ram_size),
            Some(save_data) => init_with_save(ram_size, save_data),
        }

    }

    pub fn get_all(&self) -> &[RamBank] {
        match self {
            Ram::Banks0(banks) => banks.deref(),
            Ram::Banks1(banks) => banks.deref(),
            Ram::Banks4(banks) => banks.deref(),
            Ram::Banks16(banks) => banks.deref(),
            Ram::Banks8(banks) => banks.deref(),
        }
    }
}

fn init_without_save(ram_size: RamSize) -> Ram {
    match ram_size {
        RamSize::Zero => Ram::Banks0(Box::new([[0; RAM_BANK_SIZE]; 0])),
        RamSize::KB8 => Ram::Banks1(Box::new([[0; RAM_BANK_SIZE]; 1])),
        RamSize::KB32 => Ram::Banks4(Box::new([[0; RAM_BANK_SIZE]; 4])),
        RamSize::KB128 => Ram::Banks16(Box::new([[0; RAM_BANK_SIZE]; 16])),
        RamSize::KB64 => Ram::Banks8(Box::new([[0; RAM_BANK_SIZE]; 8])),
    }
}

fn init_with_save(ram_size: RamSize, save_data: Vec<u8>) -> Ram {
    let ram_in_banks: Vec<Vec<u8>> =
        save_data.chunks(RAM_BANK_SIZE)
            .map(|chunk| chunk.to_vec())
            .collect();

    match ram_size {
        RamSize::Zero => Ram::Banks0(from_data(&ram_in_banks)),
        RamSize::KB8 => Ram::Banks1(from_data(&ram_in_banks)),
        RamSize::KB32 => Ram::Banks4(from_data(&ram_in_banks)),
        RamSize::KB128 => Ram::Banks16(from_data(&ram_in_banks)),
        RamSize::KB64 => Ram::Banks8(from_data(&ram_in_banks)),
    }
}


fn from_data<const N: usize>(data: &[Vec<u8>]) -> Box<[[u8; RAM_BANK_SIZE]; N]> {
    let mut ram = Box::new([[0; RAM_BANK_SIZE]; N]);
    for i in 0..N {
        for j in 0..RAM_BANK_SIZE {
            ram[i][j] = data[i][j];
        }
    }
    ram
}

impl std::ops::Index<usize> for Ram {
    type Output = [GbByte; RAM_BANK_SIZE];
    fn index(&self, bank_index: usize) -> &Self::Output {
        use Ram::*;
        match self {
            Banks0(banks) => &banks[bank_index],
            Banks1(banks) => &banks[bank_index],
            Banks4(banks) => &banks[bank_index],
            Banks16(banks) => &banks[bank_index],
            Banks8(banks) => &banks[bank_index],
        }
    }
}

impl std::ops::IndexMut<usize> for Ram {
    fn index_mut(&mut self, bank_index: usize) -> &mut Self::Output {
        use Ram::*;
        match self {
            Banks0(banks) => &mut banks[bank_index],
            Banks1(banks) => &mut banks[bank_index],
            Banks4(banks) => &mut banks[bank_index],
            Banks16(banks) => &mut banks[bank_index],
            Banks8(banks) => &mut banks[bank_index],
        }
    }
}
