use crate::gb::cartridge::cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::rom::Rom;
use crate::gb::cartridge::{RamSize, RomSize};
use crate::gb::cartridge::cartridge_memory::Mbc;
use crate::gb::types::{GbAddr, GbByte};
use crate::utils::bitutils::{lower_byte, lower_five_bits, lower_two_bits};

#[derive(Debug, Copy, Clone, PartialEq)]
enum BankingMode {
    Simple, Advanced
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum Mbc1RamSize {
    KB32, KB8, Zero
}

impl Mbc1RamSize {
    pub fn from(ram_size: RamSize) -> Self {
        match ram_size {
            RamSize::Zero => Mbc1RamSize::Zero,
            RamSize::KB8 => Mbc1RamSize::KB8,
            RamSize::KB32 => Mbc1RamSize::KB32,
            n => panic!("MBC1 only supports 8 and 32 RAM banks. Ram size id passed: {:?}", n),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Mbc1 {
    primary_bank_register: u8,
    secondary_bank_register: u8,

    zero_rom_bank_index: u8,
    rom_bank_index: u8, //5 bits
    ram_bank_index: u8, //2 bits

    ram_size: Mbc1RamSize,
    rom_bank_index_mask: u8,

    ram_enabled: bool,
    banking_mode: BankingMode
}

impl Mbc for Mbc1 {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[self.zero_rom_bank_index as usize][rom_address as usize]
    }

    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[self.rom_bank_index as usize][rom_address as usize]
    }

    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte {
        if self.ram_non_empty_and_enabled() {
            ram[self.ram_bank_index as usize][ram_address as usize]
        } else {
            0xFF
        }
    }

    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte) {
        if self.ram_non_empty_and_enabled() {
            ram[self.ram_bank_index as usize][ram_address as usize] = datum;
        }
    }

    fn execute(&mut self, datum: GbByte, address: GbAddr) {
        match address {
            0x0000..=0x1FFF => {
                self.ram_enabled = should_enable_ram(datum)
            },

            0x2000..=0x3FFF => {
                self.primary_bank_register = lower_five_bits(datum);
                self.regenerate_rom_index();
            }

            0x4000..=0x5FFF => {
                self.secondary_bank_register = lower_two_bits(datum);
                self.regenerate_indexes();
            }

            0x6000..=0x7FFF => {
                match datum {
                    0 => self.banking_mode = BankingMode::Simple,
                    _ => self.banking_mode = BankingMode::Advanced,
                }
            }
            _ => panic!("Tried to execute a command on MBC1 by writing at {:04x} - not supported!", address)
        }
    }
}

impl Mbc1 {
    pub fn from(rom_size: RomSize, ram_size: RamSize) -> Self {
        Self {
            primary_bank_register: 0,
            secondary_bank_register: 0,

            zero_rom_bank_index: 0,
            rom_bank_index: 1,
            ram_bank_index: 0,

            rom_bank_index_mask: lower_byte(rom_size.mask()),
            ram_size: Mbc1RamSize::from(ram_size),

            ram_enabled: false,
            banking_mode: BankingMode::Simple
        }
    }

    fn regenerate_rom_index(&mut self) {
        //if the 5 bits of the first register == 0, then map to 1
        let index_from_primary_register = match self.primary_bank_register {
            0 => 1,
            n => n
        };

        let rom_bank_index_before_mask = match self.ram_size {
            Mbc1RamSize::Zero | Mbc1RamSize::KB8 => lower_two_bits(self.secondary_bank_register) << 5 | index_from_primary_register,
            Mbc1RamSize::KB32 => index_from_primary_register,
        };

        //remember that the index is masked with the maximum number of bits needed to map all the rom
        self.rom_bank_index = rom_bank_index_before_mask & self.rom_bank_index_mask;
    }

    fn regenerate_ram_index(&mut self) {
        self.ram_bank_index = if self.ram_size == Mbc1RamSize::KB32 && self.banking_mode == BankingMode::Advanced {
            lower_two_bits(self.secondary_bank_register)
        } else {
            0
        };
    }

    fn regenerate_zero_rom_index(&mut self) {
        self.zero_rom_bank_index = if self.banking_mode == BankingMode::Advanced {
            lower_two_bits(self.secondary_bank_register) << 5
        } else {
            0
        };
    }

    fn regenerate_indexes(&mut self) {
        self.regenerate_rom_index();
        self.regenerate_ram_index();
        self.regenerate_zero_rom_index();
    }

    fn ram_non_empty_and_enabled(&self) -> bool {
        self.ram_size != Mbc1RamSize::Zero && self.ram_enabled
    }

}

fn should_enable_ram(datum: GbByte) -> bool {
    matches!(datum, 0x0A)
}

#[cfg(test)]
mod mbc1_test;
