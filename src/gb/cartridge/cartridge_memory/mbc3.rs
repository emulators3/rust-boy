use crate::gb::cartridge::cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::rom::Rom;
use crate::gb::cartridge::cartridge_memory::{Mbc, RamSize, RomSize};
use crate::gb::types::{GbAddr, GbByte};
use crate::utils::bitutils::{lower_byte, lower_seven_bits, test_bit};
use crate::utils::constants::Bit;

const TICKS_PER_SECOND: u8 = 128;

#[derive(Debug, Copy, Clone, PartialEq)]
enum Mbc3RamSize {
    KB32, KB8, Zero
}

impl Mbc3RamSize {
    pub fn from(ram_size: RamSize) -> Self {
        match ram_size {
            RamSize::Zero => Mbc3RamSize::Zero,
            RamSize::KB8 =>  Mbc3RamSize::KB8,
            RamSize::KB32 => Mbc3RamSize::KB32,
            n => panic!("MBC3 only supports 8 and 32 RAM banks. Ram size passed: {:?}", n),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum RamAddressMode {
    Ram, Rtc(RtcRegister)
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum RtcRegister {
    Seconds, Minutes, Hours, LowerDayCounter, UpperDayCounter
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct RtcRegisters {
    seconds: u8,
    minutes: u8,
    hours: u8,
    lower_day_counter: u8,
    upper_day_counter: UpperDayCounter,

    latched_seconds: u8,
    latched_minutes: u8,
    latched_hours: u8,
    latched_lower_day_counter: u8,
    latched_upper_day_counter: LatchedUpperDayCounter,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct UpperDayCounter {
    upper_day_counter_bit: Bit,
    halt: bool,
    day_counter_carry: Bit,
}

impl UpperDayCounter {
    pub fn new() -> Self {
        Self {
            upper_day_counter_bit: false,
            halt: false,
            day_counter_carry: false
        }
    }

    pub fn from(datum: u8) -> Self {
        Self {
            upper_day_counter_bit: test_bit(datum, 0),
            halt: test_bit(datum, 6),
            day_counter_carry: test_bit(datum, 7),
        }
    }

    pub fn increment(&mut self) {
        //if the 9th bit is already set, then we should set the carry flag, no matter what was its value already
        //obviously, given it was increment, we need to reset the 9th bit
        if self.upper_day_counter_bit {
            self.upper_day_counter_bit = false;
            self.day_counter_carry = true;
        } else {
            self.upper_day_counter_bit = true;
        }
    }

}

#[derive(Debug, Copy, Clone, PartialEq)]
struct LatchedUpperDayCounter {
    upper_day_counter_bit: Bit,
    halt: bool,
    day_counter_carry: Bit,
}

impl LatchedUpperDayCounter {
    pub fn new() -> Self {
        Self {
            upper_day_counter_bit: false,
            halt: false,
            day_counter_carry: false
        }
    }

    pub fn from(upper_day_counter: UpperDayCounter) -> Self {
        Self {
            upper_day_counter_bit: upper_day_counter.upper_day_counter_bit,
            halt: upper_day_counter.halt,
            day_counter_carry: upper_day_counter.day_counter_carry,
        }
    }

    pub fn to_byte(self) -> GbByte {
        (self.day_counter_carry as u8) << 1 |
        (self.halt as u8)              << 6 |
        (self.upper_day_counter_bit as u8)
    }
}


impl RtcRegisters {
    pub fn new() -> Self {
        Self {
            seconds: 0,
            minutes: 0,
            hours: 0,
            lower_day_counter: 0,
            upper_day_counter: UpperDayCounter::new(),

            latched_seconds: 0,
            latched_minutes: 0,
            latched_hours: 0,
            latched_lower_day_counter: 0,
            latched_upper_day_counter: LatchedUpperDayCounter::new(),
        }
    }


    pub fn increment_seconds(&mut self) {
        //this method will percolate the increment from seconds to lower/upper day counter.
        //the overflow management is inside the upper_day_counter
        self.seconds +=1;

        if self.seconds > 59 {
            self.seconds = 0;
            self.minutes += 1;
        }

        if self.minutes > 59 {
            self.minutes = 0;
            self.hours +=1;
        }

        if self.hours > 23 {
            self.hours = 0;

            let (result, overflow) = self.lower_day_counter.overflowing_add(1);
            self.lower_day_counter = result;

            if overflow {
                self.upper_day_counter.increment();
            }
        }
    }

    pub fn latch(&mut self) {
        self.latched_seconds = self.seconds;
        self.latched_minutes = self.minutes;
        self.latched_hours = self.hours;
        self.latched_lower_day_counter = self.lower_day_counter;
        self.latched_upper_day_counter = LatchedUpperDayCounter::from(self.upper_day_counter);
    }

}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Rtc {
    previous_latch_command: u8,
    ticks: u8,
    registers: RtcRegisters,
}


impl Rtc {
    pub fn new() -> Self {
        Self {
            previous_latch_command: 0xFF,
            ticks: 0,
            registers: RtcRegisters::new(),
        }
    }

    pub fn latch_command(&mut self, current_latch_command: u8) {
        if self.previous_latch_command == 0x00 && current_latch_command == 0x01 {
            self.registers.latch();
        }
        self.previous_latch_command = current_latch_command;
    }

    pub fn tick(&mut self) {
        if !self.registers.upper_day_counter.halt {
            self.ticks += 1;

            if self.ticks == TICKS_PER_SECOND {
                self.ticks = 0;
                self.registers.increment_seconds();
            }
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Mbc3 {
    rom_bank_index: u8, //7 bits
    ram_bank_index: u8, //2 bits

    ram_size: Mbc3RamSize,
    rom_bank_index_mask: u8,

    ram_enabled: bool,
    ram_address_mode: RamAddressMode,

    with_rtc: bool,
    rtc: Rtc,
}

impl Mbc for Mbc3 {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[0][rom_address as usize]
    }

    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[self.rom_bank_index as usize][rom_address as usize]
    }

    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte {
        match self.ram_address_mode {
            RamAddressMode::Ram => {
                if self.ram_non_empty_and_enabled() {
                    ram[self.ram_bank_index as usize][ram_address as usize]
                } else {
                    0xFF
                }
            },
            RamAddressMode::Rtc(RtcRegister::Seconds) => {
                self.rtc.registers.latched_seconds
            },
            RamAddressMode::Rtc(RtcRegister::Minutes) => {
                self.rtc.registers.latched_minutes
            },
            RamAddressMode::Rtc(RtcRegister::Hours) => {
                self.rtc.registers.latched_hours
            },
            RamAddressMode::Rtc(RtcRegister::LowerDayCounter) => {
                self.rtc.registers.latched_lower_day_counter
            },
            RamAddressMode::Rtc(RtcRegister::UpperDayCounter) => {
                self.rtc.registers.latched_upper_day_counter.to_byte()
            },
        }
    }

    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte) {
        match self.ram_address_mode {
            RamAddressMode::Ram => {
                if self.ram_non_empty_and_enabled() {
                    ram[self.ram_bank_index as usize][ram_address as usize] = datum;
                }
            },
            RamAddressMode::Rtc(RtcRegister::Seconds) => {
                self.rtc.registers.seconds = datum;
            },
            RamAddressMode::Rtc(RtcRegister::Minutes) => {
                self.rtc.registers.minutes = datum;
            },
            RamAddressMode::Rtc(RtcRegister::Hours) => {
                self.rtc.registers.hours = datum;
            },
            RamAddressMode::Rtc(RtcRegister::LowerDayCounter) => {
                self.rtc.registers.lower_day_counter = datum;
            },
            RamAddressMode::Rtc(RtcRegister::UpperDayCounter) => {
                self.rtc.registers.upper_day_counter = UpperDayCounter::from(datum);
            },
        }
    }

    fn execute(&mut self, datum: GbByte, address: GbAddr) {
        match address {
            0x0000..=0x1FFF => {
                self.ram_enabled = should_enable_ram(datum)
            },

            0x2000..=0x3FFF => {
                self.rom_bank_index = match datum {
                    0 => 1,
                    _ => lower_seven_bits(datum) &self.rom_bank_index_mask,
                }
            }

            0x4000..=0x5FFF => {
                if self.with_rtc {
                    self.execute_0x4000_with_rtc(datum);
                } else {
                    self.execute_0x4000_no_rtc(datum);
                }
            }

            0x6000..=0x7FFF => self.rtc.latch_command(datum),

            _ => panic!("Tried to execute a command on MBC3 by writing at {:04x} - not supported!", address)
        }
    }

}


impl Mbc3 {
    pub fn from_with_rtc(rom_size: RomSize, ram_size: RamSize) -> Self {
        Self::from(rom_size, ram_size, true)
    }

    pub fn from_no_rtc(rom_size: RomSize, ram_size: RamSize) -> Self {
        Self::from(rom_size, ram_size, false)
    }

    fn from(rom_size: RomSize, ram_size: RamSize, with_rtc: bool) -> Self {
        Self {
            rom_bank_index: 1,
            ram_bank_index: 0,

            ram_size: Mbc3RamSize::from(ram_size),
            rom_bank_index_mask: lower_byte(rom_size.mask()),

            ram_enabled: false,
            ram_address_mode: RamAddressMode::Ram,

            with_rtc,
            rtc: Rtc::new(),
        }
    }

    pub fn tick(&mut self) {
        if self.with_rtc {
            self.rtc.tick();
        }
    }

    fn execute_0x4000_no_rtc(&mut self, datum: GbByte) {
        match datum {
            //I should set the ram index with the datum passed if and only if there are
            //ram banks to choose, otherwise set 0 or 1 (zero ram or 1 bank - 8KB)
            0x0..=0x3 => {
                self.ram_address_mode = RamAddressMode::Ram;

                if self.ram_size == Mbc3RamSize::KB32 {
                    self.ram_bank_index = datum
                }
            },
            _ => panic!("Trying to pass datum {:04x} to address 0x4000-0x5FFF in MBC3 - unsupported!", datum),
        }
    }

    fn execute_0x4000_with_rtc(&mut self, datum: GbByte) {
        match datum {
            //I should set the ram index with the datum passed if and only if there are
            //ram banks to choose, otherwise set 0 or 1 (zero ram or 1 bank - 8KB)
            0x0..=0x3 => {
                self.ram_address_mode = RamAddressMode::Ram;

                if self.ram_size == Mbc3RamSize::KB32 {
                    self.ram_bank_index = datum
                }
            },

            0x8 => self.ram_address_mode = RamAddressMode::Rtc(RtcRegister::Seconds),
            0x9 => self.ram_address_mode = RamAddressMode::Rtc(RtcRegister::Minutes),
            0xA => self.ram_address_mode = RamAddressMode::Rtc(RtcRegister::Hours),
            0xB => self.ram_address_mode = RamAddressMode::Rtc(RtcRegister::LowerDayCounter),
            0xC => self.ram_address_mode = RamAddressMode::Rtc(RtcRegister::UpperDayCounter),

            _ => panic!("Trying to pass datum {:04x} to address 0x4000-0x5FFF in MBC3 with RTC - unsupported!", datum),
        }
    }

    fn ram_non_empty_and_enabled(&self) -> bool {
        self.ram_size != Mbc3RamSize::Zero && self.ram_enabled
    }
}

fn should_enable_ram(datum: GbByte) -> bool {
    matches!(datum, 0x0A)
}

#[cfg(test)]
mod mbc3_test;
