use crate::gb::cartridge::cartridge_memory::{Mbc, RamSize, RomSize};
use crate::gb::cartridge::cartridge_memory::mbc5::Mbc5;

#[test]
fn address_0x0000_enables_ram_when_0XA() {
    for test_data in [
        RamTestData { datum: 0x0A },
        RamTestData { datum: 0x1A },
        RamTestData { datum: 0x2A },
        RamTestData { datum: 0x3A },
        RamTestData { datum: 0x4A },
        RamTestData { datum: 0x5A },
        RamTestData { datum: 0x6A },
        RamTestData { datum: 0x7A },
        RamTestData { datum: 0x8A },
        RamTestData { datum: 0x9A },
        RamTestData { datum: 0xAA },
        RamTestData { datum: 0xBA },
        RamTestData { datum: 0xCA },
        RamTestData { datum: 0xDA },
        RamTestData { datum: 0xEA },
        RamTestData { datum: 0xFA },
    ] {
        let mut mbc5 = setup(RomSize::KB512, RamSize::KB8);
        mbc5.execute(test_data.datum, 0x0000);

        let mut expected_mbc5 = setup(RomSize::KB512, RamSize::KB8);
        expected_mbc5.ram_enabled = true;

        assert_eq!(mbc5, expected_mbc5);
    }
}

#[test]
fn address_0x0000_disables_ram_when_not_0XA() {
    let mut mbc5 = setup(RomSize::KB512,RamSize::KB8);
    mbc5.ram_enabled = true;
    mbc5.execute(0x0B, 0x0000);

    let mut expected_mbc5 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc5.ram_enabled = false;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x2000_first_8_bits_rom_bank_index() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::KB8);
    mbc5.execute(0b11111100, 0x2000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::KB8);
    expected_mbc5.primary_rom_bank_register = 0b11111100;
    expected_mbc5.rom_bank_index = 0b011111100;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x2000_first_8_bits_rom_bank_index_masked_because_rom_size() {
    let mut mbc5 = setup(RomSize::MB2,RamSize::KB8);
    mbc5.execute(0b11111100, 0x2000);

    let mut expected_mbc5 = setup(RomSize::MB2,RamSize::KB8);
    expected_mbc5.primary_rom_bank_register = 0b11111100;
    expected_mbc5.rom_bank_index = 0b001111100;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x2000_first_8_bits_rom_bank_index_0_for_real() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::KB8);
    mbc5.execute(0b0, 0x2000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::KB8);
    expected_mbc5.primary_rom_bank_register = 0b0;
    expected_mbc5.rom_bank_index = 0b0;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x3000_9th_bit_rom_index() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::KB8);
    mbc5.primary_rom_bank_register = 0b01111111;
    mbc5.execute(0b11110001, 0x3000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::KB8);
    expected_mbc5.primary_rom_bank_register = 0b01111111;
    expected_mbc5.secondary_rom_bank_register = true;
    expected_mbc5.rom_bank_index = 0b101111111;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x3000_9th_bit_rom_index_masked_because_rom_size() {
    let mut mbc5 = setup(RomSize::MB4,RamSize::KB8);
    mbc5.primary_rom_bank_register = 0b01111111;
    mbc5.execute(0b11110001, 0x3000);

    let mut expected_mbc5 = setup(RomSize::MB4,RamSize::KB8);
    expected_mbc5.primary_rom_bank_register = 0b01111111;
    expected_mbc5.secondary_rom_bank_register = true;
    expected_mbc5.rom_bank_index = 0b001111111;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x4000_ram_bank_number() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::KB128);
    mbc5.execute(0b00001101, 0x4000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::KB128);
    expected_mbc5.ram_bank_index = 0b00001101;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x4000_ram_bank_number_masked_because_of_ram_size() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::KB32);
    mbc5.execute(0b00001101, 0x4000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::KB32);
    expected_mbc5.ram_bank_index = 0b00000001;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x4000_ram_bank_number_0_ram_empty() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::Zero);
    mbc5.execute(0b00001111, 0x4000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::Zero);
    expected_mbc5.ram_bank_index = 0b00000000;

    assert_eq!(mbc5, expected_mbc5);
}

#[test]
fn address_0x4000_ram_bank_number_0_when_only_1_bank() {
    let mut mbc5 = setup(RomSize::MB8,RamSize::KB8);
    mbc5.execute(0b00001111, 0x4000);

    let mut expected_mbc5 = setup(RomSize::MB8,RamSize::KB8);
    expected_mbc5.ram_bank_index = 0b00000000;

    assert_eq!(mbc5, expected_mbc5);
}

struct RamTestData {
    datum: u8
}

pub fn setup(rom_size: RomSize, ram_size: RamSize) -> Mbc5 {
    Mbc5::from(rom_size, ram_size)
}