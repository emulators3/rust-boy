use crate::gb::cartridge::cartridge_memory::{Mbc, RamSize, RomSize};
use crate::gb::cartridge::cartridge_memory::mbc3::{LatchedUpperDayCounter, Mbc3, RamAddressMode, RtcRegister, UpperDayCounter};

#[test]
fn address_0x0000_enables_ram_when_0A() {
    let mut mbc3 = setup(RomSize::KB512,RamSize::KB8);
    mbc3.execute(0x0A, 0x0000);

    let mut expected_mbc3 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc3.ram_enabled = true;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x0000_disables_ram_when_not_0A() {
    let mut mbc3 = setup(RomSize::KB512,RamSize::KB8);
    mbc3.ram_enabled = true;
    mbc3.execute(0x2A, 0x0000);

    let mut expected_mbc3 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc3.ram_enabled = false;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x2000_rom_bank_index() {
    let mut mbc3 = setup(RomSize::MB2,RamSize::KB8);
    mbc3.execute(0b10100101, 0x2000);

    let mut expected_mbc3 = setup(RomSize::MB2,RamSize::KB8);
    expected_mbc3.rom_bank_index = 0b00100101;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x2000_rom_bank_index_1_when_datum_0() {
    let mut mbc3 = setup(RomSize::MB2,RamSize::KB8);
    mbc3.execute(0b0, 0x2000);

    let mut expected_mbc3 = setup(RomSize::MB2,RamSize::KB8);
    expected_mbc3.rom_bank_index = 0b00000001;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x2000_rom_bank_index_masked_because_of_rom_size() {
    let mut mbc3 = setup(RomSize::KB512,RamSize::KB8);
    mbc3.execute(0b11110101, 0x2000);

    let mut expected_mbc3 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc3.rom_bank_index = 0b00010101;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x4000_ram_bank_index_if_enough_ram_space_and_correct_datum() {
    let mut mbc3 = setup(RomSize::MB2,RamSize::KB32);
    mbc3.execute(0x2, 0x4000);

    let mut expected_mbc3 = setup(RomSize::MB2,RamSize::KB32);
    expected_mbc3.ram_bank_index = 0x2;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x4000_ram_bank_index_does_not_change_if_no_ram() {
    let mut mbc3 = setup(RomSize::MB2,RamSize::Zero);
    mbc3.ram_address_mode = RamAddressMode::Rtc(RtcRegister::Hours);
    mbc3.execute(0x2, 0x4000);

    let mut expected_mbc3 = setup(RomSize::MB2,RamSize::Zero);
    expected_mbc3.ram_bank_index = 0x0;
    expected_mbc3.ram_address_mode = RamAddressMode::Ram;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x4000_ram_bank_index_does_not_change_if_8KB_ram() {
    let mut mbc3 = setup(RomSize::MB2,RamSize::KB8);
    mbc3.execute(0x2, 0x4000);

    let mut expected_mbc3 = setup(RomSize::MB2,RamSize::KB8);
    expected_mbc3.ram_bank_index = 0x0;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x4000_selects_rtc_registers() {
    for test_data in [
        RtcTestData { datum: 0x08, expected_rtc_register: RtcRegister::Seconds},
        RtcTestData { datum: 0x09, expected_rtc_register: RtcRegister::Minutes},
        RtcTestData { datum: 0x0A, expected_rtc_register: RtcRegister::Hours},
        RtcTestData { datum: 0x0B, expected_rtc_register: RtcRegister::LowerDayCounter},
        RtcTestData { datum: 0x0C, expected_rtc_register: RtcRegister::UpperDayCounter},
    ] {
        let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
        mbc3.execute(test_data.datum, 0x4000);

        let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
        expected_mbc3.ram_address_mode = RamAddressMode::Rtc(test_data.expected_rtc_register);

        assert_eq!(mbc3, expected_mbc3);
    }
}

#[test]
#[should_panic]
fn address_0x4000_does_not_select_rtc_registers_and_panicks_if_rtc_not_present() {
    for test_data in [
        RamOnlyTestData { datum: 0x08 },
        RamOnlyTestData { datum: 0x09 },
        RamOnlyTestData { datum: 0x0A },
        RamOnlyTestData { datum: 0x0B },
        RamOnlyTestData { datum: 0x0C },
    ] {
        let mut mbc3 = setup_no_rtc(RomSize::MB2, RamSize::KB32);
        mbc3.execute(test_data.datum, 0x4000);
    }
}

#[test]
fn address_0x6000_latches_when_0x00_then_0x01() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.registers.seconds = 1;
    mbc3.rtc.registers.minutes = 2;
    mbc3.rtc.registers.hours = 3;
    mbc3.rtc.registers.lower_day_counter = 0b10011001;
    mbc3.rtc.registers.upper_day_counter = UpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };

    mbc3.execute(0x00, 0x6000);
    mbc3.execute(0x01, 0x6000);

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.registers.seconds = 1;
    expected_mbc3.rtc.registers.minutes = 2;
    expected_mbc3.rtc.registers.hours = 3;
    expected_mbc3.rtc.registers.lower_day_counter = 0b10011001;
    expected_mbc3.rtc.registers.upper_day_counter = UpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };

    expected_mbc3.rtc.registers.latched_seconds = 1;
    expected_mbc3.rtc.registers.latched_minutes = 2;
    expected_mbc3.rtc.registers.latched_hours = 3;
    expected_mbc3.rtc.registers.latched_lower_day_counter = 0b10011001;
    expected_mbc3.rtc.registers.latched_upper_day_counter = LatchedUpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };
    expected_mbc3.rtc.previous_latch_command = 0x01;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x6000_latches_when_0x00_then_0x01_even_there_was_something_before() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.registers.seconds = 1;
    mbc3.rtc.registers.minutes = 2;
    mbc3.rtc.registers.hours = 3;
    mbc3.rtc.registers.lower_day_counter = 0b10011001;
    mbc3.rtc.registers.upper_day_counter = UpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };

    mbc3.execute(0x00, 0x6000);
    mbc3.execute(0x00, 0x6000);
    mbc3.execute(0xD0, 0x6000);
    mbc3.execute(0xB0, 0x6000);
    mbc3.execute(0xB1, 0x6000);
    mbc3.execute(0x01, 0x6000);
    mbc3.execute(0x00, 0x6000);
    mbc3.execute(0x01, 0x6000);

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.registers.seconds = 1;
    expected_mbc3.rtc.registers.minutes = 2;
    expected_mbc3.rtc.registers.hours = 3;
    expected_mbc3.rtc.registers.lower_day_counter = 0b10011001;
    expected_mbc3.rtc.registers.upper_day_counter = UpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };

    expected_mbc3.rtc.registers.latched_seconds = 1;
    expected_mbc3.rtc.registers.latched_minutes = 2;
    expected_mbc3.rtc.registers.latched_hours = 3;
    expected_mbc3.rtc.registers.latched_lower_day_counter = 0b10011001;
    expected_mbc3.rtc.registers.latched_upper_day_counter = LatchedUpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };
    expected_mbc3.rtc.previous_latch_command = 0x01;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn address_0x6000_does_not_latch_when_0x00_not_followed_by_0x01() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.registers.seconds = 1;
    mbc3.rtc.registers.minutes = 2;
    mbc3.rtc.registers.hours = 3;
    mbc3.rtc.registers.lower_day_counter = 0b10011001;
    mbc3.rtc.registers.upper_day_counter = UpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };

    mbc3.execute(0x00, 0x6000);
    mbc3.execute(0x00, 0x6000);
    mbc3.execute(0xD0, 0x6000);
    mbc3.execute(0xB0, 0x6000);
    mbc3.execute(0xB1, 0x6000);
    mbc3.execute(0x01, 0x6000);

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.registers.seconds = 1;
    expected_mbc3.rtc.registers.minutes = 2;
    expected_mbc3.rtc.registers.hours = 3;
    expected_mbc3.rtc.registers.lower_day_counter = 0b10011001;
    expected_mbc3.rtc.registers.upper_day_counter = UpperDayCounter {
        upper_day_counter_bit: true,
        halt: true,
        day_counter_carry: true,
    };

    expected_mbc3.rtc.previous_latch_command = 0x01;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_does_not_have_any_effect_if_rtc_not_present() {
    let mut mbc3 = setup_no_rtc(RomSize::MB2, RamSize::KB32);

    mbc3.tick();

    let mut expected_mbc3 = setup_no_rtc(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn only_one_tick_should_not_change_rtc_registers() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);

    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 1;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_does_not_change_even_ticks_if_counter_is_halted() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.registers.upper_day_counter.halt = true;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.registers.upper_day_counter.halt = true;
    expected_mbc3.rtc.ticks = 0;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_change_ticks_when_resumed() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks=120;
    mbc3.rtc.registers.upper_day_counter.halt = true;
    mbc3.tick();
    mbc3.rtc.registers.upper_day_counter.halt = false;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 121;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_should_increment_one_second_after_128_ticks() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 1;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_increments_minutes_when_59_seconds() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.rtc.registers.seconds = 59;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 0;
    expected_mbc3.rtc.registers.minutes = 1;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_increments_hours_when_59_minutes() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.rtc.registers.seconds = 59;
    mbc3.rtc.registers.minutes = 59;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 0;
    expected_mbc3.rtc.registers.minutes = 0;
    expected_mbc3.rtc.registers.hours = 1;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_increments_lower_day_counter_when_23_hours() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.rtc.registers.seconds = 59;
    mbc3.rtc.registers.minutes = 59;
    mbc3.rtc.registers.hours = 23;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 0;
    expected_mbc3.rtc.registers.minutes = 0;
    expected_mbc3.rtc.registers.hours = 0;
    expected_mbc3.rtc.registers.lower_day_counter = 1;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_sets_upper_day_counter_bit_when_overflows() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.rtc.registers.seconds = 59;
    mbc3.rtc.registers.minutes = 59;
    mbc3.rtc.registers.hours = 23;
    mbc3.rtc.registers.lower_day_counter = 0b11111111;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 0;
    expected_mbc3.rtc.registers.minutes = 0;
    expected_mbc3.rtc.registers.hours = 0;
    expected_mbc3.rtc.registers.lower_day_counter = 0;
    expected_mbc3.rtc.registers.upper_day_counter.upper_day_counter_bit = true;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_sets_upper_day_counter_carry_bit_when_upper_bit_overflows() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.rtc.registers.seconds = 59;
    mbc3.rtc.registers.minutes = 59;
    mbc3.rtc.registers.hours = 23;
    mbc3.rtc.registers.lower_day_counter = 0b11111111;
    mbc3.rtc.registers.upper_day_counter.upper_day_counter_bit = true;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 0;
    expected_mbc3.rtc.registers.minutes = 0;
    expected_mbc3.rtc.registers.hours = 0;
    expected_mbc3.rtc.registers.lower_day_counter = 0;
    expected_mbc3.rtc.registers.upper_day_counter.upper_day_counter_bit = false;
    expected_mbc3.rtc.registers.upper_day_counter.day_counter_carry = true;

    assert_eq!(mbc3, expected_mbc3);
}

#[test]
fn tick_sets_upper_day_counter_carry_bit_when_upper_bit_overflows_even_if_carry_already_set() {
    let mut mbc3 = setup(RomSize::MB2, RamSize::KB32);
    mbc3.rtc.ticks = 127;
    mbc3.rtc.registers.seconds = 59;
    mbc3.rtc.registers.minutes = 59;
    mbc3.rtc.registers.hours = 23;
    mbc3.rtc.registers.lower_day_counter = 0b11111111;
    mbc3.rtc.registers.upper_day_counter.upper_day_counter_bit = true;
    mbc3.rtc.registers.upper_day_counter.day_counter_carry = true;
    mbc3.tick();

    let mut expected_mbc3 = setup(RomSize::MB2, RamSize::KB32);
    expected_mbc3.rtc.ticks = 0;
    expected_mbc3.rtc.registers.seconds = 0;
    expected_mbc3.rtc.registers.minutes = 0;
    expected_mbc3.rtc.registers.hours = 0;
    expected_mbc3.rtc.registers.lower_day_counter = 0;
    expected_mbc3.rtc.registers.upper_day_counter.upper_day_counter_bit = false;
    expected_mbc3.rtc.registers.upper_day_counter.day_counter_carry = true;

    assert_eq!(mbc3, expected_mbc3);
}

struct RamOnlyTestData {
    datum: u8
}

struct RtcTestData {
    datum: u8,
    expected_rtc_register: RtcRegister
}

pub fn setup(rom_size: RomSize, ram_size: RamSize) -> Mbc3 {
    Mbc3::from_with_rtc(rom_size, ram_size)
}

pub fn setup_no_rtc(rom_size: RomSize, ram_size: RamSize) -> Mbc3 {
    Mbc3::from_no_rtc(rom_size, ram_size)
}