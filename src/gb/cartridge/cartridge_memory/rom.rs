use crate::gb::cartridge::cartridge_memory::RomSize;
use crate::gb::types::{
    GbByte,
};

pub type RomBank = [GbByte; Rom::BANK_SIZE];

// ROM
#[derive(Debug, PartialEq)]
pub enum Rom {
    Banks2(Box<[RomBank;   2]>),
    Banks4(Box<[RomBank;   4]>),
    Banks8(Box<[RomBank;   8]>),
    Banks16(Box<[RomBank;  16]>),
    Banks32(Box<[RomBank;  32]>),
    Banks64(Box<[RomBank;  64]>),
    Banks128(Box<[RomBank; 128]>),
    Banks256(Box<[RomBank; 256]>),
    Banks512(Box<[RomBank; 512]>),
}

impl Rom {
    pub const BANK_SIZE: usize = 16 * 1024;

    pub fn init(rom_size: RomSize, rom: &[Vec<u8>]) -> Rom {
        match rom_size {
            RomSize::KB32 => Rom::Banks2(from_data(rom)),
            RomSize::KB64 => Rom::Banks4(from_data(rom)),
            RomSize::KB128 => Rom::Banks8(from_data(rom)),
            RomSize::KB256 => Rom::Banks16(from_data(rom)),
            RomSize::KB512 => Rom::Banks32(from_data(rom)),
            RomSize::MB1 => Rom::Banks64(from_data(rom)),
            RomSize::MB2 => Rom::Banks128(from_data(rom)),
            RomSize::MB4 => Rom::Banks256(from_data(rom)),
            RomSize::MB8 => Rom::Banks512(from_data(rom)),
        }
    }

}

fn from_data<const N: usize>(data: &[Vec<u8>]) -> Box<[[u8; Rom::BANK_SIZE]; N]> {
    let mut rom = Box::new([[0; Rom::BANK_SIZE]; N]);
    for i in 0..N {
        for j in 0..Rom::BANK_SIZE {
            rom[i][j] = data[i][j];
        }
    }
    rom
}

impl std::ops::Index<usize> for Rom {
    type Output = [GbByte; Rom::BANK_SIZE];
    fn index(&self, bank_index: usize) -> &Self::Output {
        use Rom::*;
        match self {
            Banks2(banks) => &banks[bank_index],
            Banks4(banks) => &banks[bank_index],
            Banks8(banks) => &banks[bank_index],
            Banks16(banks) => &banks[bank_index],
            Banks32(banks) => &banks[bank_index],
            Banks64(banks) => &banks[bank_index],
            Banks128(banks) => &banks[bank_index],
            Banks256(banks) => &banks[bank_index],
            Banks512(banks) => &banks[bank_index],
        }
    }
}
