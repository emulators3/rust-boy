use crate::gb::cartridge::cartridge_memory::mbc1::{BankingMode, Mbc1};
use crate::gb::cartridge::{RamSize, RomSize};
use crate::gb::cartridge::cartridge_memory::Mbc;

#[test]
fn address_0x0000_enables_ram_when_0A() {
    let mut mbc1 = setup(RomSize::KB512,RamSize::KB8);
    mbc1.execute(0x0A, 0x0000);

    let mut expected_mbc1 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc1.ram_enabled = true;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x0000_disables_ram_when_not_0A() {
    let mut mbc1 = setup(RomSize::KB512,RamSize::KB8);
    mbc1.ram_enabled = true;
    mbc1.execute(0x1A, 0x0000);

    let mut expected_mbc1 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc1.ram_enabled = false;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x2000_primary_rom_bank() {
    let mut mbc1 = setup(RomSize::KB512,RamSize::KB8);
    mbc1.execute(0b00000111, 0x2000);

    let mut expected_mbc1 = setup(RomSize::KB512,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00000111;
    expected_mbc1.rom_bank_index = 0b00000111;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x2000_rom_bank_index_masked_when_over_rom_size() {
    let mut mbc1 = setup(RomSize::KB256,RamSize::KB8);
    mbc1.execute(0b00011111, 0x2000);

    let mut expected_mbc1 = setup(RomSize::KB256,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00011111;
    expected_mbc1.rom_bank_index = 0b00001111;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x2000_rom_bank_index_when_secondary_register_already_present() {
    let mut mbc1 = setup(RomSize::MB2,RamSize::KB8);
    mbc1.secondary_bank_register = 0b00000010;
    mbc1.execute(0b00011111, 0x2000);

    let mut expected_mbc1 = setup(RomSize::MB2,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00011111;
    expected_mbc1.secondary_bank_register = 0b00000010;
    expected_mbc1.rom_bank_index = 0b01011111;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x2000_rom_bank_index_when_secondary_register_already_present_but_ram_32KB() {
    let mut mbc1 = setup(RomSize::MB2,RamSize::KB32);
    mbc1.secondary_bank_register = 0b00000010;
    mbc1.execute(0b00011111, 0x2000);

    let mut expected_mbc1 = setup(RomSize::MB2,RamSize::KB32);
    expected_mbc1.primary_bank_register = 0b00011111;
    expected_mbc1.secondary_bank_register = 0b00000010;
    expected_mbc1.rom_bank_index = 0b00011111;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x2000_rom_bank_index_points_to_zero_if_not_enough_rom_size_and_primary_bank_register_is_10001() {
    let mut mbc1 = setup(RomSize::KB256,RamSize::KB32);
    mbc1.execute(0b00010000, 0x2000);

    let mut expected_mbc1 = setup(RomSize::KB256,RamSize::KB32);
    expected_mbc1.primary_bank_register = 0b00010000;
    expected_mbc1.rom_bank_index = 0b00000000;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x4000_secondary_rom_bank_only_lower_2_bits() {
    let mut mbc1 = setup(RomSize::MB2,RamSize::KB8);
    mbc1.primary_bank_register = 0b00000100;
    mbc1.execute(0b00000111, 0x4000);

    let mut expected_mbc1 = setup(RomSize::MB2,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00000100;
    expected_mbc1.secondary_bank_register = 0b00000011;
    expected_mbc1.rom_bank_index = 0b01100100;

    assert_eq!(mbc1, expected_mbc1);
}


#[test]
fn address_0x4000_secondary_rom_bank_masked_because_rom_not_enough_size() {
    let mut mbc1 = setup(RomSize::MB1,RamSize::KB8);
    mbc1.primary_bank_register = 0b00000100;
    mbc1.execute(0b00000111, 0x4000);

    let mut expected_mbc1 = setup(RomSize::MB1,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00000100;
    expected_mbc1.secondary_bank_register = 0b00000011;
    expected_mbc1.rom_bank_index = 0b00100100;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x4000_changes_ram_bank_and_zero_rom_bank_if_enough_ram_size_and_banking_mode_advanced() {
    let mut mbc1 = setup(RomSize::MB1,RamSize::KB32);
    mbc1.primary_bank_register = 0b00000100;
    mbc1.banking_mode = BankingMode::Advanced;
    mbc1.execute(0b00000111, 0x4000);

    let mut expected_mbc1 = setup(RomSize::MB1,RamSize::KB32);
    expected_mbc1.primary_bank_register = 0b00000100;
    expected_mbc1.secondary_bank_register = 0b00000011;
    expected_mbc1.banking_mode = BankingMode::Advanced;
    expected_mbc1.rom_bank_index = 0b00000100;
    expected_mbc1.ram_bank_index = 0b00000011;
    expected_mbc1.zero_rom_bank_index = 0b1100000;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x4000_changes_only_zero_rom_bank_if_banking_mode_advanced_but_not_enough_ram_size() {
    let mut mbc1 = setup(RomSize::KB256,RamSize::KB8);
    mbc1.primary_bank_register = 0b00000100;
    mbc1.banking_mode = BankingMode::Advanced;
    mbc1.execute(0b00000111, 0x4000);

    let mut expected_mbc1 = setup(RomSize::KB256,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00000100;
    expected_mbc1.secondary_bank_register = 0b00000011;
    expected_mbc1.banking_mode = BankingMode::Advanced;
    expected_mbc1.rom_bank_index = 0b00000100;
    expected_mbc1.ram_bank_index = 0b00000000;
    expected_mbc1.zero_rom_bank_index = 0b1100000;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x4000_rom_will_point_to_0b00100001_if_primary_bank_is_zero() {
    let mut mbc1 = setup(RomSize::MB1,RamSize::KB8);
    mbc1.primary_bank_register = 0b00000000;
    mbc1.execute(0b00000001, 0x4000);

    let mut expected_mbc1 = setup(RomSize::MB1,RamSize::KB8);
    expected_mbc1.primary_bank_register = 0b00000000;
    expected_mbc1.secondary_bank_register = 0b00000001;
    expected_mbc1.rom_bank_index = 0b00100001;
    expected_mbc1.ram_bank_index = 0b00000000;
    expected_mbc1.zero_rom_bank_index = 0b0000000;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x6000_set_banking_mode_simple_when_0() {
    let mut mbc1 = setup(RomSize::MB1,RamSize::KB8);
    mbc1.banking_mode = BankingMode::Advanced;
    mbc1.execute(0b00000000, 0x6000);

    let mut expected_mbc1 = setup(RomSize::MB1,RamSize::KB8);
    expected_mbc1.banking_mode = BankingMode::Simple;

    assert_eq!(mbc1, expected_mbc1);
}

#[test]
fn address_0x6000_set_banking_mode_advanced_when_1() {
    let mut mbc1 = setup(RomSize::MB1,RamSize::KB8);
    mbc1.execute(0b00000001, 0x6000);

    let mut expected_mbc1 = setup(RomSize::MB1,RamSize::KB8);
    expected_mbc1.banking_mode = BankingMode::Advanced;

    assert_eq!(mbc1, expected_mbc1);
}


pub fn setup(rom_size: RomSize, ram_size: RamSize) -> Mbc1 {
    Mbc1::from(rom_size, ram_size)
}