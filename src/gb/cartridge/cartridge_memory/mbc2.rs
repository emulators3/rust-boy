use crate::gb::cartridge::cartridge_memory::{Mbc, RomSize};
use crate::gb::cartridge::cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::rom::Rom;
use crate::gb::types::{GbAddr, GbByte};
use crate::utils::bitutils::{lower_byte, lower_half, test_bit_16};


#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Mbc2 {
    rom_bank_index: u8,
    rom_bank_index_mask: u8,
    ram_enabled: bool,
}

impl Mbc for Mbc2 {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[0][rom_address as usize]
    }

    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        rom[self.rom_bank_index as usize][rom_address as usize]
    }

    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte {
        if self.ram_enabled {
            ram[0][ram_address as usize]
        } else {
            0xFF
        }
    }

    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte) {
        if self.ram_enabled {
            ram[0][ram_address as usize] = datum;
        }
    }

    fn execute(&mut self, datum: GbByte, address: GbAddr) {
        match address {
            0x0000..=0x3FFF => {
                //if bit 8 of address is 1, then the lower 4 bits of the datum are used to set the
                //rom index, with the usual 0=>1 and masking to top rom size
                if test_bit_16(address, 8) {
                    self.rom_bank_index = match lower_half(datum) {
                        0 => 1,
                        n => n & self.rom_bank_index_mask
                    }
                } else {
                    self.ram_enabled = should_enable_ram(datum);
                }

            },

            _ => panic!("Tried to execute a command on MBC2 by writing at {:04x} - not supported!", address)
        }
    }
}

impl Mbc2 {
    pub fn from(rom_size: RomSize) -> Self {
        Self {
            rom_bank_index: 1,
            rom_bank_index_mask: lower_byte(rom_size.mask()),
            ram_enabled: false,
        }
    }
}


fn should_enable_ram(datum: GbByte) -> bool {
    matches!(datum, 0x0A)
}

#[cfg(test)]
mod mbc2_test;
