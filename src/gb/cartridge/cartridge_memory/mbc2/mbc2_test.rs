use crate::gb::cartridge::cartridge_memory::mbc2::Mbc2;
use crate::gb::cartridge::cartridge_memory::{Mbc, RomSize};

#[test]
fn address_0x0000_enables_ram_when_8_bit_is_0_and_datum_0A() {
    let mut mbc2 = setup(RomSize::KB256);
    mbc2.execute(0x0A, 0b0000000000000000);

    let mut expected_mbc2 = setup(RomSize::KB256);
    expected_mbc2.ram_enabled = true;

    assert_eq!(mbc2, expected_mbc2);
}

#[test]
fn address_0x0000_disables_ram_when_8_bit_is_0_and_datum_not_0A() {
    let mut mbc2 = setup(RomSize::KB256);
    mbc2.ram_enabled = true;
    mbc2.execute(0x1A, 0b0000000000000000);

    let mut expected_mbc2 = setup(RomSize::KB256);
    expected_mbc2.ram_enabled = false;

    assert_eq!(mbc2, expected_mbc2);
}

#[test]
fn address_0x0000_changes_rom_when_8_bit_is_1_index_only_lower_half_datum() {
    let mut mbc2 = setup(RomSize::KB256);

    mbc2.execute(0b01011010, 0b0000000100000000);

    let mut expected_mbc2 = setup(RomSize::KB256);
    expected_mbc2.rom_bank_index = 0b00001010;

    assert_eq!(mbc2, expected_mbc2);
}

#[test]
fn address_0x0000_changes_rom_when_8_bit_is_1_index_only_lower_half_datum_and_masked_because_of_rom_size() {
    let mut mbc2 = setup(RomSize::KB128);

    mbc2.execute(0b01011010, 0b0000000100000000);

    let mut expected_mbc2 = setup(RomSize::KB128);
    expected_mbc2.rom_bank_index = 0b00000010;

    assert_eq!(mbc2, expected_mbc2);
}

#[test]
#[should_panic]
fn fail_with_address_over_0x3FFF() {
    let mut mbc2 = setup(RomSize::KB256);

    mbc2.execute(0b01011010, 0x4000);
}

pub fn setup(rom_size: RomSize) -> Mbc2 { Mbc2::from(rom_size) }