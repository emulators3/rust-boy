use crate::gb::cartridge::cartridge_memory::mbc1::Mbc1;
use crate::gb::cartridge::cartridge_memory::mbc2::Mbc2;
use crate::gb::cartridge::cartridge_memory::mbc3::Mbc3;
use crate::gb::cartridge::cartridge_memory::mbc5::Mbc5;
use crate::gb::cartridge::cartridge_memory::no_mbc::NoMbc;
use crate::gb::cartridge::cartridge_memory::ram::Ram;
use crate::gb::cartridge::cartridge_memory::rom::Rom;
use crate::gb::types::{GbAddr, GbByte};

pub mod rom;
pub mod ram;
mod no_mbc;
mod mbc1;
mod mbc2;
mod mbc3;
mod mbc5;


pub trait Mbc {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte;
    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte;
    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte;
    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte);
    fn execute(&mut self, datum: GbByte, address: GbAddr);
}

#[derive(Debug, PartialEq)]
pub enum MemoryBankController {
    None(NoMbc),
    Mbc1(Mbc1),
    Mbc2(Mbc2),
    Mbc3(Mbc3),
    Mbc5(Mbc5),
}

impl MemoryBankController {
    pub fn from(mbc_type: u8, rom_size: RomSize, ram_size: RamSize) -> Self {
        match mbc_type {
            0x0 => MemoryBankController::None(NoMbc::from(ram_size)),
            0x1 | 0x2 | 0x3 | 0x8 | 0x9 | 0xB | 0xC | 0xD => MemoryBankController::Mbc1(Mbc1::from(rom_size, ram_size)),
            0x5 | 0x6 => MemoryBankController::Mbc2(Mbc2::from(rom_size)),
            0xF | 0x10 => MemoryBankController::Mbc3(Mbc3::from_with_rtc(rom_size, ram_size)), //MBC3 with RTC
            0x11..=0x13 => MemoryBankController::Mbc3(Mbc3::from_no_rtc(rom_size, ram_size)), //MBC3 with no RTC?? (even if there is BATT)
            0x19..=0x1E => MemoryBankController::Mbc5(Mbc5::from(rom_size, ram_size)),
            _ => unreachable!(),
        }
    }

    #[allow(clippy::single_match)]
    pub fn tick(&mut self) {
        match self {
            MemoryBankController::Mbc3(mbc3) => mbc3.tick(),
            _ => (),
        }
    }
}

impl Mbc for MemoryBankController {
    fn read_rom_bank_0(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        match self {
            MemoryBankController::None(no_mbc) => no_mbc.read_rom_bank_0(rom, rom_address),
            MemoryBankController::Mbc1(mbc1) => mbc1.read_rom_bank_0(rom, rom_address),
            MemoryBankController::Mbc2(mbc2) => mbc2.read_rom_bank_0(rom, rom_address),
            MemoryBankController::Mbc3(mbc3) => mbc3.read_rom_bank_0(rom, rom_address),
            MemoryBankController::Mbc5(mbc5) => mbc5.read_rom_bank_0(rom, rom_address),
        }
    }

    fn read_rom_bank_n(&self, rom: &Rom, rom_address: GbAddr) -> GbByte {
        match self {
            MemoryBankController::None(no_mbc) => no_mbc.read_rom_bank_n(rom, rom_address),
            MemoryBankController::Mbc1(mbc1) => mbc1.read_rom_bank_n(rom, rom_address),
            MemoryBankController::Mbc2(mbc2) => mbc2.read_rom_bank_n(rom, rom_address),
            MemoryBankController::Mbc3(mbc3) => mbc3.read_rom_bank_n(rom, rom_address),
            MemoryBankController::Mbc5(mbc5) => mbc5.read_rom_bank_n(rom, rom_address),
        }
    }

    fn read_ram(&self, ram: &Ram, ram_address: GbAddr) -> GbByte {
        match self {
            MemoryBankController::None(no_mbc) => no_mbc.read_ram(ram, ram_address),
            MemoryBankController::Mbc1(mbc1) => mbc1.read_ram(ram, ram_address),
            MemoryBankController::Mbc2(mbc2) => mbc2.read_ram(ram, ram_address),
            MemoryBankController::Mbc3(mbc3) => mbc3.read_ram(ram, ram_address),
            MemoryBankController::Mbc5(mbc5) => mbc5.read_ram(ram, ram_address),
        }
    }

    fn write_ram(&mut self, ram: &mut Ram, ram_address: GbAddr, datum: GbByte) {
        match self {
            MemoryBankController::None(no_mbc) => no_mbc.write_ram(ram, ram_address, datum),
            MemoryBankController::Mbc1(mbc1) => mbc1.write_ram(ram, ram_address, datum),
            MemoryBankController::Mbc2(mbc2) => mbc2.write_ram(ram, ram_address, datum),
            MemoryBankController::Mbc3(mbc3) => mbc3.write_ram(ram, ram_address, datum),
            MemoryBankController::Mbc5(mbc5) => mbc5.write_ram(ram, ram_address, datum),
        }
    }

    fn execute(&mut self, datum: GbByte, address: GbAddr) {
        match self {
            MemoryBankController::None(no_mbc) => no_mbc.execute(datum, address),
            MemoryBankController::Mbc1(mbc1) => mbc1.execute(datum, address),
            MemoryBankController::Mbc2(mbc2) => mbc2.execute(datum, address),
            MemoryBankController::Mbc3(mbc3) => mbc3.execute(datum, address),
            MemoryBankController::Mbc5(mbc5) => mbc5.execute(datum, address),
        }
    }
}


#[derive(Debug, PartialEq, Clone, Copy)]
pub enum RomSize {
    KB32, KB64, KB128, KB256, KB512, MB1, MB2, MB4, MB8,
}

impl RomSize {
    pub fn from(datum: u8) -> Self {
        match datum {
            0 => RomSize::KB32,
            1 => RomSize::KB64,
            2 => RomSize::KB128,
            3 => RomSize::KB256,
            4 => RomSize::KB512,
            5 => RomSize::MB1,
            6 => RomSize::MB2,
            7 => RomSize::MB4,
            8 => RomSize::MB8,
            _ => panic!("Unknown rom size for index {}", datum),
        }
    }

    pub fn mask(&self) -> u16 {
        match self {
            RomSize::KB32 => 0b0,
            RomSize::KB64 => 0b11,
            RomSize::KB128 => 0b111,
            RomSize::KB256 => 0b1111,
            RomSize::KB512 => 0b11111,
            RomSize::MB1 => 0b111111,
            RomSize::MB2 => 0b1111111,
            RomSize::MB4 => 0b11111111,
            RomSize::MB8 => 0b111111111,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum RamSize {
    Zero, KB8, KB32, KB128, KB64
}

impl RamSize {
    pub fn from(datum: u8) -> Self {
        match datum {
            0 => RamSize::Zero,
            2 => RamSize::KB8,
            3 => RamSize::KB32,
            4 => RamSize::KB128,
            5 => RamSize::KB64,
            _ => panic!("Unknown ram size for index {}", datum),
        }
    }
}
