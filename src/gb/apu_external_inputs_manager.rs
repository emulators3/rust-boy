use crate::gb::apu::Apu;
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, Sweep};
use crate::gb::memory::io_registers::IoReg;
use crate::utils::bitutils::test_bit;

#[derive(Debug, Copy, Clone)]
struct DivApuManager {
    old_div: u8,
}

impl DivApuManager {
    pub fn new() -> Self {
        Self {
            old_div: 0
        }
    }

    //I will check if the 4th bit of div is falling edge
    pub fn check(&mut self, apu: &mut Apu, audio_registers: &mut impl AudioRegisters) {
        let new_div = audio_registers.read_io_internal(IoReg::DIV);

        if test_bit(self.old_div, 4) && !test_bit(new_div, 4) {
            apu.div_apu_tick(audio_registers);
        }

        self.old_div = new_div;
    }

}

#[derive(Debug, Copy, Clone)]
pub struct ApuExternalInputsManager {
    div_apu_manager: DivApuManager
}

impl ApuExternalInputsManager {

    pub fn new() -> Self {
        Self {
            div_apu_manager: DivApuManager::new()
        }
    }

    pub fn check(&mut self, apu: &mut Apu, regs: &mut impl AudioRegisters) {
        self.div_apu_manager.check(apu, regs);
        sweep_writing_check(apu, regs);
        channels_length_counter_writing_check(apu, regs);
        channels_trigger_check(apu, regs);
    }

}

/*Gameboy reacts immediately to change of mode in Sweep. If at least one sweep iteration has been done
  with negative mode, as soon as Addition mode is written, channel gets disabled.
*/
fn sweep_writing_check(apu: &mut Apu, regs: &mut impl AudioRegisters) {
    if let Some(datum) = regs.get_signal_nr_10() {
        apu.sweep_direction_overridden(Sweep::from(datum).direction, regs);
        regs.reset_signal_nr_10();
    }
}

//this is needed because as soon as the NRX1 is written, we should unlock the lenght timer if it was
//frozen. It may be unlock either by triggering the channel OR writing to the length counter
fn channels_length_counter_writing_check(apu: &mut Apu, regs: &mut impl AudioRegisters) {
    channel_length_counter_writing_check(regs.get_signal_nr_x1(Channel::Square1), apu, Channel::Square1, regs);
    channel_length_counter_writing_check(regs.get_signal_nr_x1(Channel::Square2), apu, Channel::Square2, regs);
    channel_length_counter_writing_check(regs.get_signal_nr_x1(Channel::Wave), apu, Channel::Wave, regs);
    channel_length_counter_writing_check(regs.get_signal_nr_x1(Channel::Noise), apu, Channel::Noise, regs);
}

fn channel_length_counter_writing_check(nrx1_written: bool, apu: &mut Apu, channel: Channel, regs: &mut impl AudioRegisters) {
    if nrx1_written {
        apu.channel_length_counter_written(channel);
        regs.reset_signal_nr_x1(channel);
    }
}



/*
  the idea is that this will understand if a channel NRX4 has been written, and it will
  - sync the length enable bit immediately (so it takes effect and the timer increase glitch may be checked)
  - trigger the channel
*/
fn channels_trigger_check(apu: &mut Apu, regs: &mut impl AudioRegisters) {
    channel_trigger_check(regs.get_signal_nr_x4(Channel::Square1), apu, regs, Channel::Square1);
    channel_trigger_check(regs.get_signal_nr_x4(Channel::Square2), apu, regs, Channel::Square2);
    channel_trigger_check(regs.get_signal_nr_x4(Channel::Wave), apu, regs, Channel::Wave);
    channel_trigger_check(regs.get_signal_nr_x4(Channel::Noise), apu, regs, Channel::Noise);
}

fn channel_trigger_check(maybe_datum: Option<u8>, apu: &mut Apu, audio_registers: &mut impl AudioRegisters, channel: Channel) {
    if let Some(datum) = maybe_datum {
        apu.sync_length(test_bit(datum, 6), audio_registers, channel);
        let trigger = test_bit(datum, 7);

        if trigger {
            apu.trigger(channel, audio_registers);
        }
        audio_registers.reset_signal_nr_x4(channel);
    }
}