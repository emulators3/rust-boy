#[derive(Debug, Copy, Clone)]
pub enum PixelColor {
    Blank, Light, Lightest, Dark, Darkest
}