#[derive(Debug, PartialEq)]
pub struct Signals {
    pub div_reset_requested: bool,

    pub nr_10_written: Option<u8>,

    pub nr_11_written: bool,
    pub nr_21_written: bool,
    pub nr_31_written: bool,
    pub nr_41_written: bool,

    pub nr_14_written: Option<u8>,
    pub nr_24_written: Option<u8>,
    pub nr_34_written: Option<u8>,
    pub nr_44_written: Option<u8>,

    pub wave_sample: Option<WaveSampleSignal>,
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct WaveSampleSignal {
    pub index_currently_read: u8,
    pub sample_currently_read: u8
}

impl Signals {
    pub fn new() -> Self {
        Self {
            div_reset_requested: false,

            nr_10_written: None,

            nr_11_written: false,
            nr_21_written: false,
            nr_31_written: false,
            nr_41_written: false,

            nr_14_written: None,
            nr_24_written: None,
            nr_34_written: None,
            nr_44_written: None,

            wave_sample: None,
        }
    }
}