use crate::gb::interrupt_lines::InterruptLines;
use crate::gb::memory::io_registers::IoRegistersInternal;
use crate::gb::memory::video_memory::lcd_status_flags::StatMode;
use crate::gb::memory::video_memory::VideoMemory;
use crate::gb::timer::Timer;
use crate::gb::types::{GbAddr, GbByte};
use crate::utils::bitutils::test_bit;

// Interrupts
#[repr(u16)]
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Interrupt {
    VBlank,
    LcdStat,
    Timer,
    Serial,
    Joypad,
}

impl Interrupt {
    pub const fn address(&self) -> GbAddr {
        use Interrupt::*;
        match self {
            VBlank  => 0x0040,
            LcdStat => 0x0048,
            Timer   => 0x0050,
            Serial  => 0x0058,
            Joypad  => 0x0060,
        }
    }

    pub const fn index(&self) -> u8 {
        use Interrupt::*;
        match self {
            VBlank  => 0,
            LcdStat => 1,
            Timer   => 2,
            Serial  => 3,
            Joypad  => 4,
        }
    }

    pub const fn mask(&self) -> u8 {
        use Interrupt::*;
        match self {
            VBlank  => 0b00001,
            LcdStat => 0b00010,
            Timer   => 0b00100,
            Serial  => 0b01000,
            Joypad  => 0b10000,
        }
    }
}

pub fn interrupt_main_check(regs: &mut impl VideoMemory, timer: Timer, old_p1: GbByte, new_p1: GbByte, interrupt_lines: &mut InterruptLines) {
    vblank_check(regs, interrupt_lines);
    stat_check(regs, interrupt_lines);
    timer_overflow_check(timer, regs, interrupt_lines);
    joypad_check(old_p1, new_p1, regs,interrupt_lines);
    //TODO complete with the serial interrupt check
}

fn vblank_check(regs: &mut impl VideoMemory, interrupt_lines: &mut InterruptLines) {
    let stat_register = regs.read_stat();
    if stat_register.mode_flag == StatMode::VBlank {
        interrupt_lines.set_high(Interrupt::VBlank, regs)
    } else {
        interrupt_lines.set_low(Interrupt::VBlank)
    }
}

pub fn dump_stat_checks(regs: &mut impl VideoMemory) {
    let stat_register = regs.read_stat();

    print!("\tLYC_E:{} LYC:{} MD2_E:{} MD2:{} MD1_E:{} MD1:{} MD0_E:{} MD0:{}",
           stat_register.ly_coincidence_interrupt, stat_register.coincidence_flag,
           stat_register.mode2_oam_interrupt, stat_register.mode_flag == StatMode::OamSearch,
           stat_register.mode1_vblank_interrupt, stat_register.mode_flag == StatMode::VBlank,
           stat_register.mode0_hblank_interrupt, stat_register.mode_flag == StatMode::HBlank
    );
}

fn stat_check(regs: &mut impl VideoMemory, interrupt_lines: &mut InterruptLines) {
    let stat_register = regs.read_stat();
    let mut should_set_high: bool = false;

    if stat_register.ly_coincidence_interrupt && stat_register.coincidence_flag {
        should_set_high = true;
    }

    if stat_register.mode2_oam_interrupt && stat_register.mode_flag == StatMode::OamSearch {
        should_set_high = true;
    }

    if stat_register.mode1_vblank_interrupt && stat_register.mode_flag == StatMode::VBlank {
        should_set_high = true;
    }

    if stat_register.mode0_hblank_interrupt && stat_register.mode_flag == StatMode::HBlank {
        should_set_high = true;
    }

    if should_set_high {
        interrupt_lines.set_high(Interrupt::LcdStat, regs)
    } else {
        interrupt_lines.set_low(Interrupt::LcdStat)
    }

}

fn timer_overflow_check(timer: Timer, io_regs: &mut impl IoRegistersInternal, interrupt_lines: &mut InterruptLines) {
    if timer.is_timer_interrupt_requested() {
        interrupt_lines.set_high(Interrupt::Timer, io_regs)
    } else {
        interrupt_lines.set_low(Interrupt::Timer)
    }
}

fn joypad_check(old_p1: GbByte, new_p1: GbByte, io_regs: &mut impl IoRegistersInternal, interrupt_lines: &mut InterruptLines) {
    if (test_bit(old_p1, 0) && !test_bit(new_p1, 0)) ||
        (test_bit(old_p1, 1) && !test_bit(new_p1, 1)) ||
        (test_bit(old_p1, 2) && !test_bit(new_p1, 2)) ||
        (test_bit(old_p1, 3) && !test_bit(new_p1, 3)) {
        interrupt_lines.set_high(Interrupt::Joypad, io_regs);
    } else {
        interrupt_lines.set_low(Interrupt::Joypad)
    }
}