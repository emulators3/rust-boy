use crate::gb::apu::div_apu_step::DivApuStep::*;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum DivApuStep {
    Zero, One, Two, Three, Four, Five, Six, Seven
}

impl DivApuStep {
    pub fn next(&self) -> DivApuStep {
        match self {
            Zero => One,
            One => Two,
            Two => Three,
            Three => Four,
            Four => Five,
            Five => Six,
            Six => Seven,
            Seven => Zero,
        }
    }

    pub fn is_length_step(&self) -> bool {
        matches!(self, Zero | Two | Four | Six)
    }

    pub fn is_envelope_step(&self) -> bool {
        matches!(self, Seven)
    }

    pub fn is_sweep_step(&self) -> bool {
        matches!(self, Two | Six)
    }
}