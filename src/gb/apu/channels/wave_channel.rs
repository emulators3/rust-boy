use crate::gb::apu::AudioChannel;
use crate::gb::apu::channels::LengthFunction;
use crate::gb::apu::div_apu_step::DivApuStep;
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, WaveChannelVolume};
use crate::utils::bitutils::{higher_half, lower_half};
use crate::utils::constants::DigitalAudioOutput;

#[derive(Debug, Copy, Clone)]
struct WavePattern {
    ticks: u16,
    index: u8,
    current_sample: u8,
    high_nibble: bool,
    wavelength_counter: u16,
}

impl WavePattern {

    pub fn new(sample: u8, wavelength: u16) -> Self {
        Self {
            ticks: 0,
            index: 0,
            current_sample: sample,
            high_nibble: true,
            wavelength_counter: Self::calculate_wave_ticks_needed(wavelength) + 3 //I don't know why, but when triggering 3 needs to be added. I've seen it also in other emulators.
        }
    }

    pub fn reset(&mut self) {
        self.index=0;
        self.ticks=0;
        self.high_nibble= true;
    }

    pub fn tick(&mut self, audio_registers: &mut impl AudioRegisters) {
        let channel = Channel::Wave;
        self.ticks +=1;

        //when advancing in the wave pattern, we need to load the next sample AND re-read wavelength,
        //as documentation says that wavelength changes are taken in consideration only when reading
        //next sample

        if self.ticks == self.wavelength_counter {
            self.ticks = 0;
            self.index = (self.index + 1) % 32;

            self.wavelength_counter = Self::calculate_wave_ticks_needed(audio_registers.read_frequency_timer(channel));

            self.current_sample = audio_registers.read_wave_pattern_sample(self.index);

            //even is left nibble, odd is right nibble
            self.high_nibble = (self.index % 2) == 0;

            audio_registers.write_signal_wave_sample_currently_read(self.index, self.current_sample);
        }
    }

    pub fn get_sample(&self) -> u8 {
        if self.high_nibble {
            higher_half(self.current_sample)
        } else {
            lower_half(self.current_sample)
        }
    }

    //wave RAM corruption happening when wave channel is triggered 1 cycle before an active read from wave RAM
    //cycle here is not t-cycle, but a "wave" cycle, so when the wave_pattern cycle ends (in the end it's 2 t-cycles)
    //because wave, at least in this EMU implementation, ticks once every 2 t-cycles
    pub fn wave_ram_glitch_corruption_check(&self, audio_registers: &mut impl AudioRegisters) {
        if (self.ticks + 1) == self.wavelength_counter {
            Self::wave_ram_glitch_corruption((self.index + 1) % 32, audio_registers);
        }
    }

    fn wave_ram_glitch_corruption(index: u8, audio_registers: &mut impl AudioRegisters) {
        //if we will read from byte 0 to byte 3, then only the first byte is rewritten
        if index < 8 {
            audio_registers.write_wave_pattern_sample(0, audio_registers.read_wave_pattern_sample(index));
        } else {
            //otherwise we will rewrite the first 4 bytes of wave ram with the 4 bytes in the
            //sequence 4-7 / 8-11 / 12-15 depending on where the offset is now
            //I could have implemented it in some smarter way but I prefer readability over smartiness OMG OPTIMIZED I'm so smart bro xdddd
            //remember that I'm storing the index as half nibbles, so first byte is index 0-1, second is 2-3 and so on...
            let data: [u8;4] = match index {
                8..=15 => [audio_registers.read_wave_pattern_sample(8), audio_registers.read_wave_pattern_sample(10), audio_registers.read_wave_pattern_sample(12), audio_registers.read_wave_pattern_sample(14)],
                16..=23 => [audio_registers.read_wave_pattern_sample(16), audio_registers.read_wave_pattern_sample(18), audio_registers.read_wave_pattern_sample(20), audio_registers.read_wave_pattern_sample(22)],
                _ => [audio_registers.read_wave_pattern_sample(24), audio_registers.read_wave_pattern_sample(26), audio_registers.read_wave_pattern_sample(28), audio_registers.read_wave_pattern_sample(30)],
            };

            audio_registers.write_wave_pattern_sample(0, data[0]);
            audio_registers.write_wave_pattern_sample(1, data[1]);
            audio_registers.write_wave_pattern_sample(2, data[2]);
            audio_registers.write_wave_pattern_sample(3, data[3]);
        }
    }

    fn calculate_wave_ticks_needed(wavelength: u16) -> u16 {
        2048 - wavelength
    }
}

#[derive(Debug, Copy, Clone)]
pub struct WaveChannel {
    enabled: bool,
    wave_pattern: WavePattern,
    length: LengthFunction,
}

impl AudioChannel for WaveChannel {
    fn run(&mut self, audio_registers: &mut impl AudioRegisters) -> DigitalAudioOutput {
        if self.enabled {
            self.wave_pattern.tick(audio_registers);

            let volume = audio_registers.read_wave_channel_volume();
            self.generate_output(volume)
        } else {
            0
        }
    }

    fn disable(&mut self, audio_registers: &mut impl AudioRegisters) {
        self.enabled = false;
        self.wave_pattern.reset();
        audio_registers.write_channel_status_in_master_register_internal(Channel::Wave, false);
    }

    fn is_enabled(&self) -> bool {
        self.enabled
    }
}

impl WaveChannel {

    pub fn new() -> Self {
        Self {
            enabled: false,
            wave_pattern: WavePattern {
                ticks: 0,
                index: 0,
                high_nibble: true,
                current_sample: 0xFF,
                wavelength_counter: 0,
            },
            length: LengthFunction::new(0x100),
        }
    }

    pub fn trigger(&mut self, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep) {
        let channel = Channel::Wave;

        self.enabled = true;
        audio_registers.write_channel_status_in_master_register_internal(channel, true);

        self.wave_pattern.wave_ram_glitch_corruption_check(audio_registers);

        self.wave_pattern = WavePattern::new(
            self.wave_pattern.current_sample,
            audio_registers.read_frequency_timer(channel)
        );

        self.length.unlock_by_channel_trigger(audio_registers, div_apu_step, channel);
    }

    pub fn length_counter_written(&mut self) {
        self.length.unlock();
    }

    pub fn sync_length(&mut self, length_enabled: bool, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep) {
        self.length.sync_length_enabled(length_enabled, audio_registers, div_apu_step, Channel::Wave);
        if self.length.is_overflow() {
            self.length.reset_overflow();
            self.disable(audio_registers);
        }
    }

    pub fn div_apu_tick(&mut self, div_apu_step: DivApuStep, audio_registers: &mut impl AudioRegisters) {
        let channel = Channel::Wave;

        if div_apu_step.is_length_step() {
            self.length.increment_timer(audio_registers, channel);

            if self.length.is_overflow() {
                self.length.reset_overflow();
                self.disable(audio_registers);
            }
        }
    }

    fn generate_output(&self, volume: WaveChannelVolume) -> DigitalAudioOutput {
        match volume {
            WaveChannelVolume::Mute => 0,
            WaveChannelVolume::Max => self.wave_pattern.get_sample(),
            WaveChannelVolume::Half => self.wave_pattern.get_sample() >> 1,
            WaveChannelVolume::Quarter => self.wave_pattern.get_sample() >> 2,
        }
    }

    //current sample is reset when APU is turned on. We may reset it when it's turned off anwyway
    pub fn disable_by_apu_off(&mut self, audio_registers: &mut impl AudioRegisters) {
        self.disable(audio_registers);
        self.wave_pattern.current_sample = 0x0;
    }

}