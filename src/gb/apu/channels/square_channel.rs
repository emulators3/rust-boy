use crate::gb::apu::AudioChannel;
use crate::gb::apu::channels::{EnvelopeFunction, LengthFunction, SweepFunction};
use crate::gb::apu::div_apu_step::DivApuStep;
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, Direction, SquareChannelNumber, WaveForm};
use crate::utils::constants::DigitalAudioOutput;

#[derive(Debug, Copy, Clone)]
struct Wave {
    form: WaveForm,
    current_step: u8,
    ticks: u16,
    frequency_ticks_needed: u16
}

impl Wave {

    pub fn new(wave_form: WaveForm, step: u8, frequency_timer: u16) -> Self {
        Self {
            form: wave_form,
            current_step: step,
            ticks: 0,
            frequency_ticks_needed: Self::calculate_frequency_ticks_needed(frequency_timer)
        }
    }

    pub fn tick(&mut self, channel: Channel, audio_registers: &mut impl AudioRegisters) {
        self.ticks +=1;

        if self.ticks == self.frequency_ticks_needed {
            self.ticks = 0;
            self.current_step = (self.current_step + 1) % 8;

            let frequency_timer = audio_registers.read_frequency_timer(channel);
            self.frequency_ticks_needed = Self::calculate_frequency_ticks_needed(frequency_timer);
        }
    }

    pub fn get_current_wave_output(&self) -> u8 {
        self.form.wave_duty()[self.current_step as usize]
    }

    fn calculate_frequency_ticks_needed(frequency_timer: u16) -> u16 {
        2048 - frequency_timer
    }
}

#[derive(Debug, Copy, Clone)]
pub struct SquareChannel {
    enabled: bool,
    channel_number: SquareChannelNumber,
    sweep: SweepFunction,
    wave: Wave,
    length: LengthFunction,
    envelope: EnvelopeFunction,
}

impl AudioChannel for SquareChannel {
    fn run(&mut self, audio_registers: &mut impl AudioRegisters) -> DigitalAudioOutput {
        if self.enabled {
            let channel = Channel::from(self.channel_number);

            self.wave.tick(channel, audio_registers);
            self.generate_output()
        } else {
            0
        }
    }

    fn disable(&mut self, audio_registers: &mut impl AudioRegisters) {
        self.enabled = false;
        audio_registers.write_channel_status_in_master_register_internal(Channel::from(self.channel_number), false);
    }

    fn is_enabled(&self) -> bool {
        self.enabled
    }
}

impl SquareChannel {

    pub fn new(channel_number: SquareChannelNumber) -> Self {
        Self {
            channel_number,
            enabled: false,
            sweep: SweepFunction::new(),
            wave: Wave {
                form: WaveForm::Zero,
                current_step: 0,
                ticks: 0,
                frequency_ticks_needed: 0,
            },
            length: LengthFunction::new(0x40),
            envelope: EnvelopeFunction::new(),
        }
    }

    pub fn trigger(&mut self, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep) {
        let channel = Channel::from(self.channel_number);

        self.enabled = true;
        audio_registers.write_channel_status_in_master_register_internal(channel, true);

        let read_waveform = audio_registers.read_square_channel_waveform(self.channel_number);
        let wave_previous_step = self.wave.current_step;
        let frequency_timer = audio_registers.read_frequency_timer(channel);

        if self.channel_number == SquareChannelNumber::One {
            self.sweep.reload(audio_registers);
        }

        self.wave = Wave::new(
            read_waveform,
            wave_previous_step,
            frequency_timer
        );

        let read_envelope = audio_registers.read_channel_envelope(channel);
        self.envelope = EnvelopeFunction {
            volume: read_envelope.volume,
            direction: read_envelope.direction,
            pace: read_envelope.pace,
            ticks: 0,
        };

        self.sweep.run_for_trigger(audio_registers);
        if self.sweep.is_overflow() {
            self.sweep.reset_overflow();
            self.disable(audio_registers);
        }

        self.length.unlock_by_channel_trigger(audio_registers, div_apu_step, channel);
    }

    pub fn sweep_direction_overridden(&mut self, direction: Direction, audio_registers: &mut impl AudioRegisters) {
        if direction == Direction::Increase && self.sweep.has_already_calculated_with_negative() {
            self.disable(audio_registers);
        }
    }

    pub fn length_counter_written(&mut self) {
        self.length.unlock();
    }

    pub fn sync_length(&mut self, length_enabled: bool, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep) {
        self.length.sync_length_enabled(length_enabled, audio_registers, div_apu_step, Channel::from(self.channel_number));
        if self.length.is_overflow() {
            self.length.reset_overflow();
            self.disable(audio_registers);
        }
    }

    pub fn div_apu_tick(&mut self, div_apu_step: DivApuStep, audio_registers: &mut impl AudioRegisters) {
        let channel = Channel::from(self.channel_number);

        if div_apu_step.is_length_step() {
            self.length.increment_timer(audio_registers, channel);

            if self.length.is_overflow() {
                self.length.reset_overflow();
                self.disable(audio_registers);
            }
        }

        if div_apu_step.is_sweep_step() && self.channel_number == SquareChannelNumber::One {
            self.sweep.tick(audio_registers);

            if self.sweep.is_overflow() {
                self.sweep.reset_overflow();
                self.disable(audio_registers);
            }
        }

        if div_apu_step.is_envelope_step() {
            self.envelope.tick();
        }
    }

    fn generate_output(&self) -> DigitalAudioOutput {
        let wave_signal = self.wave.get_current_wave_output();
        let volume: u8 = self.envelope.volume;

        wave_signal * volume
    }

    //duty step counter and timer are reset when APU is turned off
    pub fn disable_by_apu_off(&mut self, audio_registers: &mut impl AudioRegisters) {
        self.disable(audio_registers);
        self.wave.current_step = 0;
        self.wave.ticks = 0;
    }

}