use crate::gb::apu::AudioChannel;
use crate::gb::apu::channels::{EnvelopeFunction, LengthFunction};
use crate::gb::apu::div_apu_step::DivApuStep;
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, LFSRWidth, NoiseChannelRandomness};
use crate::utils::bitutils::{get_bit_16, test_bit_16, write_bit_16};
use crate::utils::constants::DigitalAudioOutput;

const CLOCK_DIVIDER_VALUES: [u32;8] = [1,2,4,6,8,10,12,14];

#[derive(Debug, Copy, Clone)]
struct Lfsr {
    register: u16,
    width: LFSRWidth,
    randomness_counter: u16,
    last_shifted_bit: u8,
    ticks: u16,
}

impl Lfsr {
    pub fn new() -> Self {
        Self {
            register: 0,
            width: LFSRWidth::bits_15,
            randomness_counter: 0,
            last_shifted_bit: 0,
            ticks: 0,
        }
    }

    pub fn from(randomness: NoiseChannelRandomness) -> Self {
        Self {
            register: 0,
            width: randomness.width,
            last_shifted_bit: 0,
            ticks: 0,
            randomness_counter: Self::calculate_ticks_needed(randomness.clock_shift, randomness.clock_divider)
        }
    }

    pub fn run(&mut self, audio_registers: & impl AudioRegisters) -> u8 {
        self.ticks +=1;

        if self.ticks == self.randomness_counter {
            self.ticks = 0;

            //I just want to know if bit 0 and 1 are equal. If they are, write 1, else 0
            let result = test_bit_16(self.register, 0) == test_bit_16(self.register, 1);
            let result_as_bit = u8::from(result);

            self.register = write_bit_16(self.register, 15, result_as_bit);

            //write also to 7 when width is 7
            if self.width == LFSRWidth::bits_7 {
                self.register = write_bit_16(self.register, 7, result_as_bit);
            }

            self.last_shifted_bit = get_bit_16(self.register, 0) as u8;
            self.register >>= 1;

            //reload randomness and counter at every iteration
            let randomness = audio_registers.read_noise_channel_randomness();
            self.width = randomness.width;
            self.randomness_counter = Self::calculate_ticks_needed(randomness.clock_shift, randomness.clock_divider);
        }

        self.last_shifted_bit
    }

    fn calculate_ticks_needed(clock_shift: u8, clock_divider: u8) -> u16 {
        //I will calculate just the divider as I need to know how many ticks I need to reach
        //the 262144hz speed. Given that the look up table for clock divider is already multiplied by 2
        //then we need to divide by 2 the result (shift 1 bit)

        // 1<<0 = 1 --- 1<<1 = 2 ---- 1<<2 = 4
        //this is exactly what 2^s means
        let clock = 1 << clock_shift;

        ((CLOCK_DIVIDER_VALUES[clock_divider as usize] * clock) as u16 ) >> 1
    }
}

#[derive(Debug, Copy, Clone)]
pub struct NoiseChannel {
    enabled: bool,
    length: LengthFunction,
    envelope: EnvelopeFunction,
    lfsr: Lfsr,
}

impl AudioChannel for NoiseChannel {
    fn run(&mut self, audio_registers: &mut impl AudioRegisters) -> DigitalAudioOutput {
        if self.enabled {
            //return output of lsfr * volume
            self.lfsr.run(audio_registers) * self.envelope.volume
        } else {
            0
        }
    }

    fn disable(&mut self, audio_registers: &mut impl AudioRegisters) {
        self.enabled = false;
        audio_registers.write_channel_status_in_master_register_internal(Channel::Noise, false);
    }

    fn is_enabled(&self) -> bool {
        self.enabled
    }
}

impl NoiseChannel {

    pub fn new() -> Self {
        Self {
            enabled: false,
            length: LengthFunction::new(0x40),
            envelope: EnvelopeFunction::new(),
            lfsr: Lfsr::new(),
        }
    }

    pub fn trigger(&mut self, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep) {
        let channel = Channel::Noise;
        let noise_channel_randomness = audio_registers.read_noise_channel_randomness();

        self.enabled = true;
        audio_registers.write_channel_status_in_master_register_internal(channel, true);

        self.lfsr = Lfsr::from(noise_channel_randomness);

        let read_envelope = audio_registers.read_channel_envelope(channel);
        self.envelope = EnvelopeFunction {
            volume: read_envelope.volume,
            direction: read_envelope.direction,
            pace: read_envelope.pace,
            ticks: 0,
        };

        self.length.unlock_by_channel_trigger(audio_registers, div_apu_step, channel);
    }

    pub fn length_counter_written(&mut self) {
        self.length.unlock();
    }

    pub fn sync_length(&mut self, length_enabled: bool, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep) {
        self.length.sync_length_enabled(length_enabled, audio_registers, div_apu_step, Channel::Noise);
        if self.length.is_overflow() {
            self.length.reset_overflow();
            self.disable(audio_registers);
        }
    }

    pub fn div_apu_tick(&mut self, div_apu_step: DivApuStep, audio_registers: &mut impl AudioRegisters) {
        let channel = Channel::Noise;

        if div_apu_step.is_length_step() {
            self.length.increment_timer(audio_registers, channel);

            if self.length.is_overflow() {
                self.length.reset_overflow();
                self.disable(audio_registers);
            }
        }

        if div_apu_step.is_envelope_step() {
            self.envelope.tick();
        }
    }
}