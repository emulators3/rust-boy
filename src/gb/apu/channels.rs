use crate::gb::apu::div_apu_step::DivApuStep;
use crate::gb::memory::audio_registers::{AudioRegisters, Channel, Direction, Sweep};

pub mod square_channel;
pub mod wave_channel;
pub mod noise_channel;

#[derive(Debug, Copy, Clone)]
struct SweepFunction {
    enabled: bool,
    pace: u8,
    frequency_timer_shadow: u16,
    is_overflown: bool,
    negative_sweep_calculated: bool
}

impl SweepFunction {
    pub fn new() -> Self {
        Self {
            enabled: false,
            pace: 0,
            frequency_timer_shadow: 0,
            is_overflown: false,
            negative_sweep_calculated: false
        }
    }

    pub fn reload(&mut self, audio_registers: &mut impl AudioRegisters) {
        let read_sweep = audio_registers.read_square_channel_sweep();
        let frequency_timer = audio_registers.read_frequency_timer(Channel::Square1);

      /*  Sweep is enabled only on channel trigger
          The internal enabled flag is set if either the sweep period or shift are non-zero, cleared otherwise.  */
        self.enabled = read_sweep.pace > 0 || read_sweep.slope > 0;

        //timer 0 is treated as timer 8
        self.pace = if read_sweep.pace > 0 { read_sweep.pace } else { 8 };
        self.frequency_timer_shadow = frequency_timer;
        self.negative_sweep_calculated = false;
    }

    pub fn tick(&mut self, audio_registers: &mut impl AudioRegisters) {
        //decrement and if after decrement is 0, then this is the cycle in which sweep should act
        if self.pace > 0 {
            self.pace -= 1;

            /*DISCLAIMER: this is something very badly explained everywhere. Generally speaking you can find
              that "timer 0 is treated as 8". In reality what that means is that whenever the timer reaches 0,
              THEN it checks the data in NR10 again. If the data in NR10 is 0, then it sets timer to 8, otherwise
              it executes the sweep.
              It's super cumbersome, but at least here it's explained properly.
            */
            if self.pace == 0 {

                let new_pace = audio_registers.read_square_channel_sweep().pace;

                if self.enabled && (new_pace > 0) {
                    self.apply_sweep(audio_registers);
                    self.pace = new_pace;
                } else {
                    self.pace = 8;
                }
            }
        }
    }

    //when channel is triggered and sweep shift is non-zero, frequency calculation and overflow check are performed immediately
    pub fn run_for_trigger(&mut self, audio_registers: &mut impl AudioRegisters) {
        if self.enabled && (audio_registers.read_square_channel_sweep().slope > 0) {
            self.apply_sweep_for_trigger(audio_registers);
        }
    }

    pub fn is_overflow(&self) -> bool {
        self.is_overflown
    }

    pub fn reset_overflow(&mut self) {
        self.is_overflown = false;
    }

    pub fn has_already_calculated_with_negative(&self) -> bool {
        self.negative_sweep_calculated
    }

    fn apply_sweep(&mut self, audio_registers: &mut impl AudioRegisters) {
        let sweep = audio_registers.read_square_channel_sweep();

        /*if at least one sweep has been already executed in negative mode from the last channel trigger
          then if the mode has been changed into Addition in NR10, the channel auto-disables

          I'm just re-using the overflow flag here...
        */
        if self.negative_sweep_calculated && (sweep.direction == Direction::Increase) {
            self.is_overflown = true;
        } else {
            let new_frequency_timer = self.calculate_new_frequency_timer(&sweep);

            //if overflows, then set the overflow flag, otherwise...
            if Self::check_overflow(new_frequency_timer) {
                self.is_overflown = true;
            } else {
                //if slope/shift is > 0, then we can save the new frequency timer inside NR13/14.
                if sweep.slope > 0 {
                    self.frequency_timer_shadow = new_frequency_timer;
                    audio_registers.write_frequency_timer(Channel::Square1, new_frequency_timer);

                    //For some reason the sweep unit checks again the overflow on a newly calculated value and in case set the overflow flag.
                    self.is_overflown = Self::check_overflow(self.calculate_new_frequency_timer(&sweep));
                }
            }
        }
    }

    //The trigger sweep implementation differs from the div-apu sweep
    //because it does not save the newly calculated frequency in NR13/14 or shadow frequency
    //and it also does not try the second calculation/check
    fn apply_sweep_for_trigger(&mut self, audio_registers: &mut impl AudioRegisters) {
        let sweep = audio_registers.read_square_channel_sweep();
        let new_frequency_timer = self.calculate_new_frequency_timer(&sweep);

        //if overflows, then set the overflow flag, otherwise...
        if Self::check_overflow(new_frequency_timer) {
            self.is_overflown = true;
        }
    }

    fn calculate_new_frequency_timer(&mut self, sweep: &Sweep) -> u16 {
        let slopped_frequency_timer = self.frequency_timer_shadow >> sweep.slope;

        match sweep.direction {
            Direction::Increase => self.frequency_timer_shadow + slopped_frequency_timer,
            Direction::Decrease => {
                self.negative_sweep_calculated = true;
                self.frequency_timer_shadow - slopped_frequency_timer
            },
        }
    }

    fn check_overflow(frequency_timer: u16) -> bool {
        frequency_timer > 0x7FF
    }
}

#[derive(Debug, Copy, Clone)]
struct EnvelopeFunction {
    volume: u8,
    direction: Direction,
    pace: u8,
    ticks: u8,
}

impl EnvelopeFunction {

    pub fn new() -> Self {
        Self {
            volume: 0,
            direction: Direction::Decrease,
            pace: 0,
            ticks: 0,
        }
    }

    pub fn tick(&mut self) {
        self.ticks = self.ticks.overflowing_add(1).0;

        //pace == 0 means envelope is disabled
        if (self.pace > 0) && (self.ticks == self.pace) {
            self.ticks = 0;

            match self.direction {
                Direction::Increase => {
                    //max volume is F
                    if self.volume < 0xF {
                        self.volume += 1;
                    }
                },
                Direction::Decrease => {
                    //min volume is 0
                    if self.volume > 0x0 {
                        self.volume -= 1;
                    }
                },
            }
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct LengthFunction {
    enabled: bool,
    threshold: u16,
    overflown: bool,
    zero_lock: bool
}

impl LengthFunction {
    pub fn new(channel_threshold: u16) -> Self {
        Self {
            enabled: false,
            threshold: channel_threshold,
            overflown: false,
            zero_lock: false
        }
    }

    pub fn sync_length_enabled(&mut self, length_enabled: bool, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep, channel: Channel) {
        let enable_glitch_happened = self.check_length_enable_glitch(length_enabled, div_apu_step);

        //unlock length clock if it's re-activated
        self.enabled = length_enabled;

        if enable_glitch_happened {
            self.increment_timer(audio_registers, channel);
        }
    }

    pub fn increment_timer(&mut self, audio_registers: &mut impl AudioRegisters, channel: Channel) {
        if self.enabled {
            let current_timer = audio_registers.read_length_timer(channel) as u16;

            if !(self.zero_lock && (current_timer == 0)) {
                //increase then check if the increased timer would overflow (I'm modeling the threshold such that reaching it is already overflowing)
                let timer = current_timer + 1;

                audio_registers.write_length_timer(timer as u8, channel);

                if timer >= self.threshold {
                    self.overflown = true;
                    self.zero_lock = true;
                }
            }
        }
    }

    pub fn is_overflow(&self) -> bool {
        self.overflown
    }

    pub fn reset_overflow(&mut self) {
        self.overflown = false;
    }

    /*
        If a channel is triggered when the frame sequencer's next step is one that doesn't clock the length counter
        and the length counter is now enabled and length is being set to 64 (256 for wave channel) because it was previously zero,
        it is set to 63 instead (255 for wave channel).
        https://gbdev.gg8.se/wiki/articles/Gameboy_sound_hardware#Obscure_behaviour
    */
    pub fn unlock_by_channel_trigger(&mut self, audio_registers: &mut impl AudioRegisters, div_apu_step: DivApuStep, channel: Channel) {
        let current_timer = audio_registers.read_length_timer(channel);
        let trigger_glitch_happened = self.check_channel_trigger_glitch(current_timer, div_apu_step);

        self.unlock();

        if trigger_glitch_happened {
            self.increment_timer(audio_registers, channel);
        }
    }

    pub fn unlock(&mut self) {
        self.zero_lock = false;
    }

    fn check_length_enable_glitch(&mut self, current_enabled: bool, div_apu_step: DivApuStep) -> bool {
        !(div_apu_step.next().is_length_step())
            && !self.enabled
            && current_enabled
    }

    fn check_channel_trigger_glitch(&mut self, current_timer: u8, div_apu_step: DivApuStep) -> bool {
        !(div_apu_step.next().is_length_step())
            && (current_timer == 0)
            && self.zero_lock
            && self.enabled
    }
}
