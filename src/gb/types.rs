use crate::utils::constants::RAM_BANK_SIZE;

// technically these are a word and dword (double word) respectively, but
pub type GbByte = u8;
pub type GbWord = u16;
pub type GbAddr = u16;

pub type GbFlag = bool;
pub type RamBank = [GbByte; RAM_BANK_SIZE];