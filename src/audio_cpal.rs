use std::sync::Arc;
use std::thread;
use std::thread::JoinHandle;
use cpal::{BufferSize, SampleRate, StreamConfig};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use crossbeam::atomic::AtomicCell;
use crate::gb::stereo_analog_audio_output::StereoAnalogAudioOutput;
use kanal::Receiver;

#[derive(Debug)]
pub struct AudioCpal {
}

const MAX_AUDIO_QUEUE_RATE: f64 = 2.0;

impl AudioCpal {

    pub fn new() -> Self {
        Self{
        }
    }

    pub fn run(self,
               audio_rx: Receiver<StereoAnalogAudioOutput>,
               keep_running: Arc<AtomicCell<bool>>) -> JoinHandle<()> {

        let mut old_audio = StereoAnalogAudioOutput::mute();
        let mut is_right = false;
        let thread = thread::Builder::new().name(String::from("audio"));

        thread.spawn(move || {
            println!("audio component running");

            let host = cpal::default_host();
            let device = host.default_output_device().expect("no output device available");

            let stream_config  = StreamConfig {
                channels: 2,
                sample_rate: SampleRate(262144),
                buffer_size: BufferSize::Default,
            };

            let stream = device.build_output_stream(
                &stream_config,
                move |data: &mut [i16], _: &cpal::OutputCallbackInfo| {

                    //Emergency check to avoid having audio lagging behind
                    //I remove exceeding data by picking it out at intervals so that difference
                    //is not so noticeable
                    let maybe_removal_interval = calculate_removal_interval(data.len() as u64, audio_rx.len() as u64);
                    let mut removal_counter: u64 = 0;

                    for sample in data {

                        if let Some(interval) = maybe_removal_interval {
                            if removal_counter == interval {
                                removal_counter = 0;
                                audio_rx.try_recv().unwrap();
                            } else {
                                removal_counter +=1;
                            }
                        }

                        if is_right {
                            is_right = false;
                            *sample = old_audio.right_output;
                        } else {
                            is_right = true;
                            *sample = match audio_rx.try_recv() {
                                Ok(Some(audio)) => {
                                    old_audio = audio;
                                    audio.left_output
                                },
                                Ok(None) => {
                                    old_audio.left_output
                                }
                                Err(err) => {
                                    println!("{:}", err);
                                    panic!();
                                },
                            };
                        }

                    }

                },
                move |_err| {
                    // react to errors here.
                },
                None // None=blocking, Some(Duration)=timeout
            ).unwrap();


            stream.play().unwrap();

            loop {
                if !keep_running.load() {
                    break;
                }
            }

        }).unwrap()
    }

}


fn calculate_removal_interval(cpal_queue_length: u64, gameboy_audio_queue_length: u64) -> Option<u64> {
    let max_gameboy_audio_queue_length = ((cpal_queue_length as f64) * MAX_AUDIO_QUEUE_RATE) as u64;

    if gameboy_audio_queue_length > max_gameboy_audio_queue_length {
        let difference = gameboy_audio_queue_length - cpal_queue_length;
        Some(max_gameboy_audio_queue_length/difference)
    } else {
        None
    }
}