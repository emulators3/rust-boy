// use std::sync::Arc;
// use std::thread;
// use std::thread::JoinHandle;
// use crossbeam::atomic::AtomicCell;
// use sdl2::audio::{AudioQueue, AudioSpecDesired};
// use crate::Sdl2SendWrapper;
// use crate::gb::stereo_analog_audio_output::StereoAnalogAudioOutput;
// use kanal::Receiver;
//
// #[derive(Debug)]
// pub struct Audio {
// }
//
// impl Audio {
//
//     pub fn new() -> Self {
//         Self{
//         }
//     }
//
//     pub fn run(self,
//                sdl: Arc<Sdl2SendWrapper>,
//                audio_rx: Receiver<StereoAnalogAudioOutput>,
//                keep_running: Arc<AtomicCell<bool>>) -> JoinHandle<()> {
//
//         let mut old_audio = StereoAnalogAudioOutput::mute();
//         let thread = thread::Builder::new().name(String::from("audio"));
//
//         thread.spawn(move || {
//             println!("audio component running");
//
//             let desired_spec = AudioSpecDesired {
//                 freq: Some(32768),
//                 channels: Some(2),  // stereo
//                 samples: None       // default sample size
//             };
//
//             let audio_queue: AudioQueue<i16> = sdl.0.audio().unwrap().open_queue(None, &desired_spec).unwrap();
//             audio_queue.resume();
//
//             loop {
//                 match audio_rx.try_recv() {
//                     Ok(Some(audio)) => {
//                         old_audio = audio;
//                         audio_queue.queue_audio([audio.left_output, audio.right_output].as_slice()).unwrap();
//                     },
//                     Ok(None) => {
//                         //empirical way to avoid hearing disturbing pops when engine goes too slow and does not produce enough audio
//                         if audio_queue.size() <= 4196 {
//                             for _n in 0..((4196-audio_queue.size()) >> 4) {
//                                 audio_queue.queue_audio([old_audio.left_output, old_audio.right_output].as_slice()).unwrap();
//                             }
//                         }
//                     }
//                     Err(err) => {
//                         println!("{:}", err);
//                         panic!();
//                     },
//                 };
//
//                 if !keep_running.load() {
//                     break;
//                 }
//             }
//         }).unwrap()
//     }
// }