mod gb;
mod gui_fltk;
mod utils;
mod audio_cpal;
mod gameboy_daemon;

use std::env;
use std::process::exit;
use sdl2::Sdl;
use crate::gui_fltk::Gui;


pub struct Sdl2SendWrapper(Sdl);

unsafe impl Sync for Sdl2SendWrapper {}
unsafe impl Send for Sdl2SendWrapper {}

//TODO actually it would be better to invert dependency and let audio and video retrieve data from GB
//at specific hertz. Using an atomic cell to read the data would mean that if the gb lags, then audio/vide
//will not wait but just retrieve older data. No stutter but needs to be perfectly synchronized

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() > 1 {
        manage_arguments();
    } else {
        Gui::new().run_sync();
        println!("Bye");
    }
}

fn manage_arguments() {
    println!("Usage: rust-boy");
    exit(1);
}