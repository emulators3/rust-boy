// use std::{thread};
// use std::thread::JoinHandle;
// use sdl2::pixels::{Color, PixelFormatEnum};
// use crate::utils::constants::{WINDOW_LINE_SIZE, WINDOW_LINES, WindowScreen};
// use std::sync::Arc;
// use crossbeam::atomic::AtomicCell;
// use kanal::Receiver;
// use sdl2::event::Event;
// use sdl2::keyboard::Keycode;
// use crate::gb::joypad_state::{JoypadKey, JoypadState};
// use crate::gb::pixel_color::PixelColor;
// use crate::Sdl2SendWrapper;
//
//
// #[derive(Debug)]
// pub struct Gui {}
//
// impl Gui {
//
//     pub fn new() -> Self {
//         Self{}
//     }
//
//     pub fn run(self,
//                sdl: Arc<Sdl2SendWrapper>,
//                video_rx: Receiver<Box<WindowScreen>>,
//                joypad_tx: Arc<AtomicCell<JoypadState>>,
//                emulation_state: Arc<AtomicCell<bool>>) -> JoinHandle<()> {
//
//         let thread = thread::Builder::new().name(String::from("gui"));
//
//         thread.spawn(move || {
//             println!("gui component running");
//
//             let mut joypad_state = JoypadState::new();
//
//             let window = sdl.0.video().unwrap().window("rust-boy demo", 640, 576)
//                 .position_centered()
//                 .build()
//                 .unwrap();
//
//             let mut canvas = window.into_canvas()
//                 .build()
//                 .unwrap();
//
//             let texture_creator = canvas.texture_creator();
//
//             let mut texture = texture_creator
//                 .create_texture_streaming(PixelFormatEnum::RGB24, 160, 144)
//                 .unwrap();
//
//             canvas.set_draw_color(Color::RGB(255, 255, 255));
//             canvas.clear();
//             canvas.present();
//
//             /*TODO this needs to be moved to a different thread so that telling LYS works*/
//             let mut event_pump = sdl.0.event_pump().unwrap();
//
//             loop {
//
//                 for event in event_pump.poll_iter() {
//                     match event {
//                         Event::KeyDown { keycode: Some(Keycode::Up), .. } => joypad_state.key_down(JoypadKey::Up),
//                         Event::KeyDown { keycode: Some(Keycode::Down), .. } => joypad_state.key_down(JoypadKey::Down),
//                         Event::KeyDown { keycode: Some(Keycode::Left), .. } => joypad_state.key_down(JoypadKey::Left),
//                         Event::KeyDown { keycode: Some(Keycode::Right), .. } => joypad_state.key_down(JoypadKey::Right),
//                         Event::KeyDown { keycode: Some(Keycode::A), .. } => joypad_state.key_down(JoypadKey::A),
//                         Event::KeyDown { keycode: Some(Keycode::S), .. } => joypad_state.key_down(JoypadKey::B),
//                         Event::KeyDown { keycode: Some(Keycode::Z), .. } => joypad_state.key_down(JoypadKey::Select),
//                         Event::KeyDown { keycode: Some(Keycode::X), .. } => joypad_state.key_down(JoypadKey::Start),
//
//                         Event::KeyUp { keycode: Some(Keycode::Up), .. } => joypad_state.key_up(JoypadKey::Up),
//                         Event::KeyUp { keycode: Some(Keycode::Down), .. } => joypad_state.key_up(JoypadKey::Down),
//                         Event::KeyUp { keycode: Some(Keycode::Left), .. } => joypad_state.key_up(JoypadKey::Left),
//                         Event::KeyUp { keycode: Some(Keycode::Right), .. } => joypad_state.key_up(JoypadKey::Right),
//                         Event::KeyUp { keycode: Some(Keycode::A), .. } => joypad_state.key_up(JoypadKey::A),
//                         Event::KeyUp { keycode: Some(Keycode::S), .. } => joypad_state.key_up(JoypadKey::B),
//                         Event::KeyUp { keycode: Some(Keycode::Z), .. } => joypad_state.key_up(JoypadKey::Select),
//                         Event::KeyUp { keycode: Some(Keycode::X), .. } => joypad_state.key_up(JoypadKey::Start),
//
//                         Event::Quit { .. } | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => emulation_state.store(false),
//                         _ => {}
//                     }
//                 }
//
//                 joypad_tx.store(joypad_state);
//
//                 match video_rx.recv() {
//                     Ok(screen_data) => {
//                         texture.with_lock(None, |buffer: &mut [u8], pitch: usize| {
//                             for i in 0..WINDOW_LINES {
//                                 for j in 0..WINDOW_LINE_SIZE {
//                                     let color = convert_to_color(screen_data[i][j]);
//
//                                     let offset = (i * pitch) + (j * 3);
//
//                                     buffer[offset] = color[0];
//                                     buffer[offset + 1] = color[1];
//                                     buffer[offset + 2] = color[2];
//                                 }
//                             }
//                         }).unwrap();
//
//                         canvas.copy(&texture, None, None).unwrap();
//                         canvas.present();
//                     },
//                     Err(err) => {
//                         println!("{:}", err);
//                         panic!();
//                     },
//                 };
//
//                 if !emulation_state.load() {
//                     break;
//                 }
//             }
//         }).unwrap()
//     }
// }
//
// fn convert_to_color(pixel_color: PixelColor) -> [u8;3] {
//     match pixel_color {
//         PixelColor::Blank => [255,255,255],
//         PixelColor::Lightest => [0xFE,0xFE,0xFE],
//         PixelColor::Light => [0xAA,0xAA,0xAA],
//         PixelColor::Dark => [0x55,0x55,0x55],
//         PixelColor::Darkest => [0x00,0x00,0x00],
//     }
// }