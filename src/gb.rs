pub mod types;
mod cpu;
mod mmu;
mod memory;
mod cartridge;
mod interrupt_lines;
mod interrupt;
mod ppu;
pub mod pixel_color;
mod timer;
pub mod joypad_state;
mod input_manager;
mod apu;
mod apu_external_inputs_manager;
pub mod stereo_analog_audio_output;

use std::{thread};
use std::sync::Arc;
use std::thread::JoinHandle;
use std::time::Duration;
use crossbeam::atomic::AtomicCell;
use kanal::Sender;
use cpu::Cpu;
use mmu::Mmu;
use cartridge::Cartridge;
use crate::gb::apu::Apu;
use crate::gb::apu_external_inputs_manager::ApuExternalInputsManager;
use crate::gb::input_manager::manage_input;
use crate::gb::interrupt_lines::InterruptLines;
use crate::gb::interrupt::{dump_stat_checks, interrupt_main_check};
use crate::gb::joypad_state::JoypadState;
use crate::gb::memory::audio_registers::AudioRegisters;
use crate::utils::constants::WindowScreen;
use crate::gb::ppu::Ppu;
use crate::gb::stereo_analog_audio_output::StereoAnalogAudioOutput;
use crate::gb::timer::Timer;
use crate::gb::types::RamBank;

#[derive(Debug, Copy, Clone)]
pub enum EmulationState {
    Running, Pause, Close
}

#[derive(Debug)]
pub struct GameBoy {
    cpu: Cpu,
    mmu: Mmu,
    ppu: Ppu,
    apu: Apu,
    timer: Timer,
    apu_external_inputs_manager: ApuExternalInputsManager,
    interrupt_lines: InterruptLines,
}

impl GameBoy {

    pub fn from_binary_rom(binary_rom: Vec<u8>, maybe_save_data: Option<Vec<u8>>) -> Self {
        let cartridge = Cartridge::from_binary_rom(binary_rom, maybe_save_data);
        Self {
            cpu: Cpu::new(&cartridge),
            mmu: Mmu::new(cartridge),
            ppu: Ppu::new(),
            apu: Apu::new(),
            timer: Timer::new(),
            apu_external_inputs_manager: ApuExternalInputsManager::new(),
            interrupt_lines: InterruptLines::new(),
        }
    }

    /*
      The main idea is that this should emulate how the GB works internally.
      - For each dot-cycle we want to check if some interrupt condition is verified, and
      activate the bit in the IF (for each dot-cycle mimics that it's continuously checked)

      - For each dot-cycle the LCD updates the relevant registers with its status
        (for each dot-cycle mimics that it's continuously checked)

      - Every 4 dot-cycles, a cpu operation can be executed

      - Every 70224 dot-cycles we know that an entire screen frame has completed, so we can send
        the whole screen to the GUI
    */

    //TODO implement bootrom: the bootrom is mapped on the first 256 bytes of the address space (0000-00ff), then the bootrom unmaps itself by writing to ff50 (iirc?), which is a one time switch 
    pub fn run(mut self,
               audio_tx: Sender<StereoAnalogAudioOutput>,
               video_tx: Sender<Box<WindowScreen>>,
               joypad_rx: Arc<AtomicCell<JoypadState>>,
               gb_emulation_state: Arc<AtomicCell<EmulationState>>) -> JoinHandle<GameBoy>  {
        let thread = thread::Builder::new().name(String::from("gb"));

        thread.spawn (move || {
            println!("GB component running");
            let mut loop_helper = spin_sleep_util::interval(Duration::from_micros(16743)); // 4194304 (dots per second) / 70224 (video frame) ≈0.01674 seconds

            let log: bool = false;
            // let mut stack: u16 = 0;
            let total_dots: u128 = 0;

            let mut lcd_wait_cycles: u32 = 0;
            let mut cpu_wait_cycles: u32 = 0;
            let mut audio_wait_cycles: u32 = 0;
            let mut cartridge_ticks: u8 = 127;
            let mut send_video_to_gui_wait_cycles: u32 = 70223;
            let mut audio_ticks: usize = 0;

            //TODO move this inside APU in some way - run should return an option, and when populated
            //then we should send it to audio
            let mut left_buffer: i32 = 0;
            let mut right_buffer: i32 = 0;

            loop {

                match gb_emulation_state.load() {
                    EmulationState::Pause => {}
                    EmulationState::Close => {
                        break;
                    }
                    EmulationState::Running => {
                        for n in 0..70224 {

                            // if self.cpu.program_counter == 0x0F95 {
                            //     log = true;
                            //     stack = self.cpu.stack_pointer + 2;
                            // }
                            //
                            // if self.cpu.stack_pointer == stack {
                            //     log = false;
                            // }

                            if log {
                                print!("BEGIN");
                                self.cpu.dump();
                                self.timer.dump(&self.mmu);
                                self.ppu.dump(&mut self.mmu, n);
                                print!("\tDOTCLOCKS:{}", total_dots);
                                dump_stat_checks(&mut self.mmu);
                                self.interrupt_lines.dump();
                            }

                            // print!("t-cycles:{}", total_dots);
                            // self.cpu.dump();

                            self.timer.tick(&mut self.mmu);

                            let (old_p1, new_p1) = manage_input(joypad_rx.load(), &mut self.mmu);


                            //TODO it's definitely better to split the ticking from the outputting
                            if audio_wait_cycles == 0 {
                                audio_wait_cycles = 1;
                                let audio = self.apu.run(&mut self.mmu);
                                left_buffer += audio.left_output as i32;
                                right_buffer += audio.right_output as i32;

                                //TODO understand if the whole audio resampling should be moved into audio.rs
                                if audio_ticks == 7 {
                                    audio_ticks = 0;
                                    let audio_output = StereoAnalogAudioOutput {
                                        left_output: (left_buffer >> 3) as i16,
                                        right_output: (right_buffer >> 3) as i16
                                    };

                                    left_buffer = 0;
                                    right_buffer = 0;

                                    match audio_tx.try_send(audio_output) {
                                        Ok(_) => (),
                                        Err(err) => {
                                            println!("{:}", err);
                                            panic!();
                                        },
                                    };
                                } else {
                                    audio_ticks += 1;
                                }
                            } else {
                                audio_wait_cycles -= 1;
                            }

                            if lcd_wait_cycles == 0 {
                                lcd_wait_cycles = self.ppu.run(&mut self.mmu, n);
                                self.ppu.check_ly_lyc(&mut self.mmu);
                            } else {
                                lcd_wait_cycles -= 1;
                            }

                            if cpu_wait_cycles == 0 {
                                cpu_wait_cycles = self.cpu.step(&mut self.mmu);
                                self.timer.manage_div_reset(&mut self.mmu);
                                self.timer.manage_tac_change(&mut self.mmu);
                                self.ppu.check_ly_lyc(&mut self.mmu);
                            } else {
                                cpu_wait_cycles -= 1;
                            }

                            self.apu_external_inputs_manager.check(&mut self.apu, &mut self.mmu);

                            //just needed for MBC3 RTC
                            if cartridge_ticks == 0 {
                                self.mmu.tick();
                                cartridge_ticks = 127;
                            } else {
                                cartridge_ticks -= 1;
                            }

                            //idea is that the sample currently read is available only for 1 t-cycle, so it will be reset at the end
                            //remember that this is read during apu running
                            self.mmu.reset_signal_wave_sample_currently_read();

                            self.timer.manage_timer_overflow(&mut self.mmu);
                            interrupt_main_check(&mut self.mmu, self.timer, old_p1, new_p1, &mut self.interrupt_lines);

                            //we have finished vblank, let's try to send the video
                            if send_video_to_gui_wait_cycles == 0 {
                                let window = Box::new(self.ppu.get_window());
                                match video_tx.try_send(window) {
                                    Ok(_) => (),
                                    Err(err) => {
                                        println!("{:}", err);
                                        panic!();
                                    },
                                };
                                send_video_to_gui_wait_cycles = 70223; // wait till next vblank
                            } else {
                                send_video_to_gui_wait_cycles -= 1;
                            }

                            // total_dots +=1;
                            // println!();
                            if log {
                                println!();
                                print!("END\t");
                                self.cpu.dump();
                                self.timer.dump(&self.mmu);
                                self.ppu.dump(&mut self.mmu, n);
                                print!("\tDOTCLOCKS:{}", total_dots);
                                dump_stat_checks(&mut self.mmu);
                                self.interrupt_lines.dump();
                                println!();
                            }

                        }
                    }
                }

                loop_helper.tick();
            }
            self
        }).unwrap()
    }

    pub fn get_cartridge_ram(&self) -> &[RamBank] {
        self.mmu.get_cartridge_ram()
    }

}